﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using System.Data;
using System.Data.SqlClient;

namespace CONTROLADOR
{
    public class CUsuarioNegociaciones
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdLinea { get; set; }


        public string Insertar(CUsuarioNegociaciones UsuarioLineas, ref SqlConnection SqlCon, ref SqlTransaction SqlTra)
        {
            string rpta = "";
            try
            {

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_InsertarUsuarioNegociaciones";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParIddetalle_producto = new SqlParameter();
                ParIddetalle_producto.ParameterName = "@id";
                ParIddetalle_producto.SqlDbType = SqlDbType.Int;
                ParIddetalle_producto.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParIddetalle_producto);

                SqlParameter ParIdproducto = new SqlParameter();
                ParIdproducto.ParameterName = "@idusuario";
                ParIdproducto.SqlDbType = SqlDbType.Int;
                ParIdproducto.Value = UsuarioLineas.IdUsuario;
                SqlCmd.Parameters.Add(ParIdproducto);

                SqlParameter ParIdproveedor = new SqlParameter();
                ParIdproveedor.ParameterName = "@idlinea";
                ParIdproveedor.SqlDbType = SqlDbType.Int;
                ParIdproveedor.Value = UsuarioLineas.IdLinea;
                SqlCmd.Parameters.Add(ParIdproveedor);

                //Ejecutamos nuestro comando
                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }

            return rpta;

        }
    }
}
