﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ENTIDAD;

namespace CONTROLADOR
{
    public class CUsuario
    {
        public string Insertar(EUsuario Usuario, List<CUsuarioNegociaciones> UsuarioNegociaciones, List<CUsuarioOportunidades> UsuarioOportunidades)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //Establecer la trasacción
                SqlTransaction SqlTra = SqlCon.BeginTransaction();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_MantUsuario";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                
                SqlParameter ParIdproveedor = new SqlParameter();
                ParIdproveedor.ParameterName = "@id";
                ParIdproveedor.SqlDbType = SqlDbType.Int;
                ParIdproveedor.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParIdproveedor);
                
                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 100;
                ParNombre.Value = Usuario.Nombre;
                SqlCmd.Parameters.Add(ParNombre);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParApePaterno = new SqlParameter();
                ParApePaterno.ParameterName = "@ape_paterno";
                ParApePaterno.SqlDbType = SqlDbType.VarChar;
                ParApePaterno.Size = 80;
                ParApePaterno.Value = Usuario.Ape_Paterno;
                SqlCmd.Parameters.Add(ParApePaterno);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParApeMaterno = new SqlParameter();
                ParApeMaterno.ParameterName = "@ape_materno";
                ParApeMaterno.SqlDbType = SqlDbType.VarChar;
                ParApeMaterno.Size = 80;
                ParApeMaterno.Value = Usuario.Ape_Materno;
                SqlCmd.Parameters.Add(ParApeMaterno);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@correo del Procedimiento Almacenado
                SqlParameter ParSexo = new SqlParameter();
                ParSexo.ParameterName = "@sexo";
                ParSexo.SqlDbType = SqlDbType.Char;
                ParSexo.Size = 1;
                ParSexo.Value = Usuario.Sexo;
                SqlCmd.Parameters.Add(ParSexo);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@telefono del Procedimiento Almacenado
                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Usuario.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@celular1 del Procedimiento Almacenado
                SqlParameter ParUsuario = new SqlParameter();
                ParUsuario.ParameterName = "@usuario";
                ParUsuario.SqlDbType = SqlDbType.VarChar;
                ParUsuario.Size = 80;
                ParUsuario.Value = Usuario.Usuario;
                SqlCmd.Parameters.Add(ParUsuario);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParPassword = new SqlParameter();
                ParPassword.ParameterName = "@password";
                ParPassword.SqlDbType = SqlDbType.VarChar;
                ParPassword.Size = 80;
                ParPassword.Value = Usuario.Password;
                SqlCmd.Parameters.Add(ParPassword);
                //Agregamos el parámetro al comando

                SqlParameter ParOporMax = new SqlParameter();
                ParOporMax.ParameterName = "@oportunidades_max";
                ParOporMax.SqlDbType = SqlDbType.Int;
                ParOporMax.Value = Usuario.Oport_Max;
                SqlCmd.Parameters.Add(ParOporMax);


                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Usuario.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                SqlParameter ParSuperLinea = new SqlParameter();
                ParSuperLinea.ParameterName = "@superlinea";
                ParSuperLinea.SqlDbType = SqlDbType.Int;
                ParSuperLinea.Value = Usuario.SuperLinea;
                SqlCmd.Parameters.Add(ParSuperLinea);

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se ingreso el registro de forma correcta";

                /////////////  NEGOCIACIONES  ////////////

                if (Usuario.Opcion < 2)
                {
                    if (Rpta.Equals("OK"))
                    {
                        //Obtener el código del ingreso generado
                        Usuario.Id = Convert.ToInt32(SqlCmd.Parameters["@id"].Value);
                        foreach (CUsuarioNegociaciones det in UsuarioNegociaciones)
                        {
                            det.IdUsuario = Usuario.Id;
                            //Llamar al método insertar de la clase DDetalle_Ingreso
                            Rpta = det.Insertar(det, ref SqlCon, ref SqlTra);
                            if (!Rpta.Equals("OK"))
                            {
                                break;
                            }
                        }

                    }
                }

                ///////////////////////// Oportunidades  ///////////////////////////////////////
                if (Usuario.Opcion < 2)
                {
                    if (Rpta.Equals("OK"))
                    {
                        //Obtener el código del ingreso generado
                        Usuario.Id = Convert.ToInt32(SqlCmd.Parameters["@id"].Value);
                        foreach (CUsuarioOportunidades det in UsuarioOportunidades)
                        {
                            det.IdUsuario = Usuario.Id;
                            //Llamar al método insertar de la clase DDetalle_Ingreso
                            Rpta = det.Insertar(det, ref SqlCon, ref SqlTra);
                            if (!Rpta.Equals("OK"))
                            {
                                break;
                            }
                        }

                    }
                    
                }

                if (Rpta.Equals("OK"))
                {
                    SqlTra.Commit();
                }
                else
                {
                    SqlTra.Rollback();
                }

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public string Editar(EUsuario Usuario)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantUsuario";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //4. Agregar los parámetros al comando
                //Establecemos los valores para el parámetro
                //@idproveedor del Procedimiento Almacenado
                SqlParameter ParId = new SqlParameter();
                ParId.ParameterName = "@id";
                ParId.SqlDbType = SqlDbType.Int;
                //Le declaramos que el parámetro es de salida,
                //porque obtendremos el código generado por la base de datos
                ParId.Value = Usuario.Id;
                SqlCmd.Parameters.Add(ParId);
                //Agregamos el parámetro al comando


                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.VarChar;
                ParNombre.Size = 100;
                ParNombre.Value = Usuario.Nombre;
                SqlCmd.Parameters.Add(ParNombre);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParApePaterno = new SqlParameter();
                ParApePaterno.ParameterName = "@ape_paterno";
                ParApePaterno.SqlDbType = SqlDbType.VarChar;
                ParApePaterno.Size = 80;
                ParApePaterno.Value = Usuario.Ape_Paterno;
                SqlCmd.Parameters.Add(ParApePaterno);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParApeMaterno = new SqlParameter();
                ParApeMaterno.ParameterName = "@ape_materno";
                ParApeMaterno.SqlDbType = SqlDbType.VarChar;
                ParApeMaterno.Size = 80;
                ParApeMaterno.Value = Usuario.Ape_Materno;
                SqlCmd.Parameters.Add(ParApeMaterno);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@correo del Procedimiento Almacenado
                SqlParameter ParSexo = new SqlParameter();
                ParSexo.ParameterName = "@sexo";
                ParSexo.SqlDbType = SqlDbType.Char;
                ParSexo.Size = 1;
                ParSexo.Value = Usuario.Sexo;
                SqlCmd.Parameters.Add(ParSexo);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@telefono del Procedimiento Almacenado
                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Usuario.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@celular1 del Procedimiento Almacenado
                SqlParameter ParUsuario = new SqlParameter();
                ParUsuario.ParameterName = "@usuario";
                ParUsuario.SqlDbType = SqlDbType.VarChar;
                ParUsuario.Size = 80;
                ParUsuario.Value = Usuario.Usuario;
                SqlCmd.Parameters.Add(ParUsuario);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParPassword = new SqlParameter();
                ParPassword.ParameterName = "@password";
                ParPassword.SqlDbType = SqlDbType.VarChar;
                ParPassword.Size = 80;
                ParPassword.Value = Usuario.Password;
                SqlCmd.Parameters.Add(ParPassword);
                //Agregamos el parámetro al comando

                SqlParameter ParOporMax = new SqlParameter();
                ParOporMax.ParameterName = "@oportunidades_max";
                ParOporMax.SqlDbType = SqlDbType.Int;
                ParOporMax.Value = Usuario.Oport_Max;
                SqlCmd.Parameters.Add(ParOporMax);


                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Usuario.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                SqlParameter ParSuperLinea = new SqlParameter();
                ParSuperLinea.ParameterName = "@superlinea";
                ParSuperLinea.SqlDbType = SqlDbType.Int;
                ParSuperLinea.Value = Usuario.SuperLinea;
                SqlCmd.Parameters.Add(ParSuperLinea);

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se actualizo el registro de forma correcta";

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexión con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public DataTable Mostrar(EUsuario Usuario)
        {
            DataTable DtResultado = new DataTable("Usuario");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarUsuarios";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Usuario.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string Eliminar(EUsuario Usuario)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_EliminarUsuarios";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //4. Agregar los parámetros al comando
                //Establecemos los valores para el parámetro
                //@idproveedor del Procedimiento Almacenado
                SqlParameter ParIdproveedor = new SqlParameter();
                ParIdproveedor.ParameterName = "@id";
                ParIdproveedor.SqlDbType = SqlDbType.Int;
                //Le declaramos que el parámetro es de salida,
                //porque obtendremos el código generado por la base de datos
                ParIdproveedor.Value = Usuario.Id;
                SqlCmd.Parameters.Add(ParIdproveedor);
                //Agregamos el parámetro al comando

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se pudo eliminar el registro";

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;
            }
            finally
            {
                //6. Cerramos la conexión con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public DataTable MostrarNegociaciones(string Texto)
        {
            DataTable DtResultado = new DataTable("Usuario_Negociaciones");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_UsuarioNegociaciones";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@texto";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 50;
                ParTextoBuscar.Value = Texto;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Char;
                ParOpcion.Size = 1;
                ParOpcion.Value = 1;
                SqlCmd.Parameters.Add(ParOpcion);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable MostrarOportunidades(string Texto)
        {
            DataTable DtResultado = new DataTable("Usuario_Oportunidades");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_UsuarioOportunidades";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@texto";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 50;
                ParTextoBuscar.Value = Texto;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Char;
                ParOpcion.Size = 1;
                ParOpcion.Value = 1;
                SqlCmd.Parameters.Add(ParOpcion);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string EliminarNegociaciones(EUsuario Usuario, List<CUsuarioNegociaciones> UsuarioNegociaciones)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //Establecer la trasacción
                SqlTransaction SqlTra = SqlCon.BeginTransaction();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_UsuarioNegociaciones";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@texto";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 50;
                ParTextoBuscar.Value = Usuario.Id;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = 2;
                SqlCmd.Parameters.Add(ParOpcion);

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se pudo eliminar el registro";


                /////////////  NEGOCIACIONES  ////////////

                if (Usuario.Opcion < 2)
                {
                    //if (Rpta.Equals("OK") )
                    //{
                        //Obtener el código del ingreso generado
                        //Usuario.Id = Convert.ToInt32(SqlCmd.Parameters["@id"].Value);
                        foreach (CUsuarioNegociaciones det in UsuarioNegociaciones)
                        {
                            det.IdUsuario = Usuario.Id;
                            //Llamar al método insertar de la clase DDetalle_Ingreso
                            Rpta = det.Insertar(det, ref SqlCon, ref SqlTra);
                            if (!Rpta.Equals("OK"))
                            {
                                break;
                            }
                        }

                    //}

                    if (Rpta.Equals("OK"))
                    {
                        SqlTra.Commit();
                    }
                    else
                    {
                        SqlTra.Rollback();
                    }
                }
                

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;
            }
            finally
            {
                //6. Cerramos la conexión con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public string EliminarOportunidades(EUsuario Usuario, List<CUsuarioOportunidades> UsuarioOportunidades)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //Establecer la trasacción
                SqlTransaction SqlTra = SqlCon.BeginTransaction();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_UsuarioOportunidades";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@texto";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 50;
                ParTextoBuscar.Value = Usuario.Id;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = 2;
                SqlCmd.Parameters.Add(ParOpcion);

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se pudo eliminar el registro";

                ///////////////////////// Oportunidades  ///////////////////////////////////////
                if (Usuario.Opcion < 2)
                {
                    //if (Rpta.Equals("OK"))
                    //{
                        foreach (CUsuarioOportunidades det in UsuarioOportunidades)
                        {
                            det.IdUsuario = Usuario.Id;
                            //Llamar al método insertar de la clase DDetalle_Ingreso
                            Rpta = det.Insertar(det, ref SqlCon, ref SqlTra);
                            if (!Rpta.Equals("OK"))
                            {
                                break;
                            }
                        }

                    //}

                }

                if (Rpta.Equals("OK"))
                {
                    SqlTra.Commit();
                }
                else
                {
                    SqlTra.Rollback();
                }


            }
            catch (Exception ex)
            {
                Rpta = ex.Message;
            }
            finally
            {
                //6. Cerramos la conexión con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public DataTable Login(EUsuario Usuario)
        {
            DataTable DtResultado = new DataTable("Usuario");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_Login";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParUsuario = new SqlParameter();
                ParUsuario.ParameterName = "@usuario";
                ParUsuario.SqlDbType = SqlDbType.VarChar;
                ParUsuario.Size = 80;
                ParUsuario.Value = Usuario.Usuario;
                SqlCmd.Parameters.Add(ParUsuario);

                SqlParameter ParPassword = new SqlParameter();
                ParPassword.ParameterName = "@password";
                ParPassword.SqlDbType = SqlDbType.VarChar;
                ParPassword.Size = 150;
                ParPassword.Value = Usuario.Password;
                SqlCmd.Parameters.Add(ParPassword);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Usuario.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }
    }
}
