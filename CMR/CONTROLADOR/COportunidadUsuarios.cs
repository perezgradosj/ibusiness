﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CONTROLADOR
{
    public class COportunidadUsuarios
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdOportunidad { get; set; }

        public string Insertar(COportunidadUsuarios OportunidadesUsuarios, ref SqlConnection SqlCon, ref SqlTransaction SqlTra)
        {
            string rpta = "";
            try
            {

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_InsertarOportunidadUsuario";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParId = new SqlParameter();
                ParId.ParameterName = "@id";
                ParId.SqlDbType = SqlDbType.Int;
                ParId.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParId);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = OportunidadesUsuarios.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@idoportunidad";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = OportunidadesUsuarios.IdOportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad);

                //Ejecutamos nuestro comando
                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }

            return rpta;

        }
    }
}
