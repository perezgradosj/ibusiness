﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ENTIDAD;

namespace CONTROLADOR
{
    public class CContactos
    {
        public DataTable Mostrar(EContactos Contactos)
        {
            DataTable DtResultado = new DataTable("Contactos");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarContactos";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTextoOpcion = new SqlParameter();
                ParTextoOpcion.ParameterName = "@textobuscar";
                ParTextoOpcion.SqlDbType = SqlDbType.VarChar;
                ParTextoOpcion.Size = 50;
                ParTextoOpcion.Value = Contactos.TextoBuscar;
                SqlCmd.Parameters.Add(ParTextoOpcion);

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@idnegociacion";
                ParIdNegociacion.SqlDbType = SqlDbType.Int;
                ParIdNegociacion.Value = Contactos.IdNegociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                SqlParameter ParOportunidad = new SqlParameter();
                ParOportunidad.ParameterName = "@idoportunidad";
                ParOportunidad.SqlDbType = SqlDbType.Int;
                ParOportunidad.Value = Contactos.IdOportunidad;
                SqlCmd.Parameters.Add(ParOportunidad);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Contactos.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable BuscarNombre(EContactos Contactos)
        {
            DataTable DtResultado = new DataTable("Contactos");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarContactos";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                //3.Enviamos wl parámetro de Búsqueda
                SqlParameter ParTextoBuscar = new SqlParameter();
                ParTextoBuscar.ParameterName = "@textobuscar";
                ParTextoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTextoBuscar.Size = 50;
                ParTextoBuscar.Value = Contactos.TextoBuscar;
                SqlCmd.Parameters.Add(ParTextoBuscar);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Contactos.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public void deleteEmpresaContacto(int result)
        {
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.cn;
                SqlCon.Open();
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_EmpresaContacto_Delete";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@id", result);

                int response = SqlCmd.ExecuteNonQuery();
                SqlCon.Close();
            }
            catch (Exception ex) { }
        }

        public int verifyContactoOportunidad(int idcontacto, int idOportunidad)
        {
            int result = 0;
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.cn;
                SqlCon.Open();
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_EmpresaContacto_FindByIdContactoAndIdOportunidad";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@idcontacto", idcontacto);
                SqlCmd.Parameters.AddWithValue("@idoportunidad", idOportunidad);

                SqlDataReader reader = SqlCmd.ExecuteReader();
                if (reader.HasRows) {
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"]);
                    }
                }
                SqlCon.Close();
            }
            catch (Exception ex) { }
            return result;
        }

        public string MantContacto(EContactos Contacto)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.cn;
                SqlCon.Open();
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantContacto";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Contacto.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Contacto.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Value = Contacto.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParNombres = new SqlParameter();
                ParNombres.ParameterName = "@nombres";
                ParNombres.SqlDbType = SqlDbType.VarChar;
                ParNombres.Size = 150;
                ParNombres.Value = Contacto.Nombres;
                SqlCmd.Parameters.Add(ParNombres);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParCorreo = new SqlParameter();
                ParCorreo.ParameterName = "@correo";
                ParCorreo.SqlDbType = SqlDbType.NVarChar;
                ParCorreo.Size = 100;
                ParCorreo.Value = Contacto.Correo;
                SqlCmd.Parameters.Add(ParCorreo);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParMovil = new SqlParameter();
                ParMovil.ParameterName = "@movil";
                ParMovil.SqlDbType = SqlDbType.VarChar;
                ParMovil.Size = 250;
                ParMovil.Value = Contacto.Movil;
                SqlCmd.Parameters.Add(ParMovil);
                //Agregamos el parámetro al comando

                //Establecemos los valores para el parámetro
                //@correo del Procedimiento Almacenado
                SqlParameter ParFijo = new SqlParameter();
                ParFijo.ParameterName = "@fijo";
                ParFijo.SqlDbType = SqlDbType.VarChar;
                ParFijo.Size = 80;
                ParFijo.Value = Contacto.Fijo;
                SqlCmd.Parameters.Add(ParFijo);
                //Agregamos el parámetro al comando

                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Contacto.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";

                if (Contacto.Opcion == 1)
                {
                    if (Rpta == "OK")
                    {
                        Rpta = Convert.ToString(SqlCmd.Parameters["@id"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                //Rpta = ex.Message;
                Rpta = "Error";
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
            return Rpta;
        }

        public string MantenimientoEmpresaContacto(int id, int idempresa, int idnegociacion,
            int idoportunidad, int idcontacto, int opcion)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MANT_EMPRESA_CONTACTO";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (opcion <= 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Value = id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParIdContacto = new SqlParameter();
                ParIdContacto.ParameterName = "@idcontacto";
                ParIdContacto.SqlDbType = SqlDbType.Int;
                ParIdContacto.Value = idcontacto;
                SqlCmd.Parameters.Add(ParIdContacto);

                SqlParameter PasIdEmpresa = new SqlParameter();
                PasIdEmpresa.ParameterName = "@idempresa";
                PasIdEmpresa.SqlDbType = SqlDbType.Int;
                PasIdEmpresa.Value = idempresa;
                SqlCmd.Parameters.Add(PasIdEmpresa);

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@IDNEGOCIACION";
                ParIdNegociacion.SqlDbType = SqlDbType.Int;
                ParIdNegociacion.Value = idnegociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@IDOPORTUNIDAD";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = idoportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";

            }
            catch (Exception ex)
            {
                
            }
            return Rpta;
        }
    }
}
