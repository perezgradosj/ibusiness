﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using System.Data;
using System.Data.SqlClient;

namespace CONTROLADOR
{
    public class CEmpresa
    {
        public DataTable Mostrar(EEmpresa Empresa)
        {
            DataTable DtResultado = new DataTable("Empresa");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarEmpresa";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTextoOpcion = new SqlParameter();
                ParTextoOpcion.ParameterName = "@textobuscar";
                ParTextoOpcion.SqlDbType = SqlDbType.VarChar;
                ParTextoOpcion.Size = 50;
                ParTextoOpcion.Value = Empresa.TextoBuscar;
                SqlCmd.Parameters.Add(ParTextoOpcion);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Empresa.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string MantEmpresa(EEmpresa Empresa)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //SqlTransaction SqlTra = SqlCon.BeginTransaction();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                //SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_MantEmpresa";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Empresa.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Empresa.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    //Le declaramos que el parámetro es de salida,
                    //porque obtendremos el código generado por la base de datos
                    ParId.Value = Empresa.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParRazonSocial = new SqlParameter();
                ParRazonSocial.ParameterName = "@razon_social";
                ParRazonSocial.SqlDbType = SqlDbType.VarChar;
                ParRazonSocial.Size = 250;
                ParRazonSocial.Value = Empresa.Razon_Social;
                SqlCmd.Parameters.Add(ParRazonSocial);

                //SqlCmd.Parameters.Add("@razon_social", SqlDbType.VarChar, 250).Value = Empresa.Razon_Social;


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParRuc = new SqlParameter();
                ParRuc.ParameterName = "@ruc";
                ParRuc.SqlDbType = SqlDbType.Char;
                ParRuc.Size = 11;
                ParRuc.Value = Empresa.Ruc;
                SqlCmd.Parameters.Add(ParRuc);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParRubro = new SqlParameter();
                ParRubro.ParameterName = "@rubro";
                ParRubro.SqlDbType = SqlDbType.NVarChar;
                ParRubro.Size = 350;
                ParRubro.Value = Empresa.Rubro;
                SqlCmd.Parameters.Add(ParRubro);
                //Agregamos el parámetro al comando

                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Empresa.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se ingreso el registro de forma correcta";

                if (Empresa.Opcion == 1)
                {
                    if (Rpta == "OK")
                    {
                        ///var id = Convert.ToInt32(SqlCmd.Parameters["@id"].Value);
                        Rpta = Convert.ToString(SqlCmd.Parameters["@id"].Value);

                        //foreach (EContactos econtacto in contactos)
                        //{
                        //    CContactos cContactos = new CContactos();
                        //    econtacto.IdEmpresa = Convert.ToInt32(id);
                        //    econtacto.Opcion = 1;
                        //    Rpta = cContactos.MantContacto(econtacto, ref SqlCon, ref SqlTra);

                        //    if (!Rpta.Equals("OK"))
                        //    {
                        //        break;
                        //    }
                        //}
                    }
                }

                //if (Rpta.Equals("OK"))
                //{
                //    SqlTra.Commit();
                //    Rpta = Convert.ToString(SqlCmd.Parameters["@id"].Value);
                //}
                //else
                //{
                //    SqlTra.Rollback();
                //}
            }
            catch (Exception ex)
            {
                //Rpta = ex.Message;
                Rpta = "Error";
            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }
    }
}
