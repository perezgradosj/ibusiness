﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CONTROLADOR
{
    public class COportunidadLineas
    {
        public int Id { get; set; }
        public int IdLinea { get; set; }
        public int IdOportunidad { get; set; }

        public string Insertar(COportunidadLineas OportunidadesLineas, ref SqlConnection SqlCon, ref SqlTransaction SqlTra)
        {
            string rpta = "";
            try
            {

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.Transaction = SqlTra;
                SqlCmd.CommandText = "SP_InsertarOportunidadLineas";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParId = new SqlParameter();
                ParId.ParameterName = "@id";
                ParId.SqlDbType = SqlDbType.Int;
                ParId.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParId);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = OportunidadesLineas.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@idoportunidad";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = OportunidadesLineas.IdOportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad);

                //Ejecutamos nuestro comando
                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";

            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }

            return rpta;

        }
    }
}
