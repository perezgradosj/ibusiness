﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ENTIDAD;

namespace CONTROLADOR
{
    public class CNegociaciones
    {
        public DataTable MostrarLineas()
        {
            DataTable DtResultado = new DataTable("Linea");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_MostrarLineas";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                //3. No hay parámetros

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable Mostrar(ENegociacion Negociacion)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarNegociaciones";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTexoBuscar = new SqlParameter();
                ParTexoBuscar.ParameterName = "@textobuscar";
                ParTexoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTexoBuscar.Size = 50;
                ParTexoBuscar.Value = Negociacion.TextoBuscar;
                SqlCmd.Parameters.Add(ParTexoBuscar);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Negociacion.Estado;
                SqlCmd.Parameters.Add(ParEstado);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Negociacion.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Negociacion.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParTamanoPagina = new SqlParameter();
                ParTamanoPagina.ParameterName = "@tamano_pagina";
                ParTamanoPagina.SqlDbType = SqlDbType.Int;
                ParTamanoPagina.Value = Negociacion.TamanoPagina;
                SqlCmd.Parameters.Add(ParTamanoPagina);

                SqlParameter ParPaginaActual = new SqlParameter();
                ParPaginaActual.ParameterName = "@pagina_actual";
                ParPaginaActual.SqlDbType = SqlDbType.Int;
                ParPaginaActual.Value = Negociacion.PaginaActual;
                SqlCmd.Parameters.Add(ParPaginaActual);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Negociacion.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable MostrarDatosDePaginacion(ENegociacion Negociacion)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_DATOS_PAGINACION_NEGOCIACION";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTexoBuscar = new SqlParameter();
                ParTexoBuscar.ParameterName = "@textobuscar";
                ParTexoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTexoBuscar.Size = 50;
                ParTexoBuscar.Value = Negociacion.TextoBuscar;
                SqlCmd.Parameters.Add(ParTexoBuscar);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Negociacion.Estado;
                SqlCmd.Parameters.Add(ParEstado);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Negociacion.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Negociacion.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParTamanoPagina = new SqlParameter();
                ParTamanoPagina.ParameterName = "@tamano_pagina";
                ParTamanoPagina.SqlDbType = SqlDbType.Int;
                ParTamanoPagina.Value = Negociacion.TamanoPagina;
                SqlCmd.Parameters.Add(ParTamanoPagina);

                SqlParameter ParPaginaActual = new SqlParameter();
                ParPaginaActual.ParameterName = "@pagina_actual";
                ParPaginaActual.SqlDbType = SqlDbType.Int;
                ParPaginaActual.Value = Negociacion.PaginaActual;
                SqlCmd.Parameters.Add(ParPaginaActual);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Negociacion.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string MantNegociacion(ENegociacion Negociacion)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantNegociacion";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Negociacion.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Negociacion.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    //Le declaramos que el parámetro es de salida,
                    //porque obtendremos el código generado por la base de datos
                    ParId.Value = Negociacion.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParFechaRegistro = new SqlParameter();
                ParFechaRegistro.ParameterName = "@fecha_registro";
                ParFechaRegistro.SqlDbType = SqlDbType.DateTime;
                ParFechaRegistro.Value = Negociacion.Fecha_Registro;
                SqlCmd.Parameters.Add(ParFechaRegistro);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParFechaConversion = new SqlParameter();
                ParFechaConversion.ParameterName = "@fecha_conversion";
                ParFechaConversion.SqlDbType = SqlDbType.DateTime;
                ParFechaConversion.Value = Negociacion.Fecha_Conversion;
                SqlCmd.Parameters.Add(ParFechaConversion);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.NVarChar;
                ParNombre.Size = 350;
                ParNombre.Value = Negociacion.Nombre;
                SqlCmd.Parameters.Add(ParNombre);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@correo del Procedimiento Almacenado
                SqlParameter ParMonto = new SqlParameter();
                ParMonto.ParameterName = "@monto";
                ParMonto.SqlDbType = SqlDbType.Money;
                ParMonto.Value = Negociacion.Monto;
                SqlCmd.Parameters.Add(ParMonto);
                //Agregamos el parámetro al comando

                SqlParameter ParTipoMoneda = new SqlParameter();
                ParTipoMoneda.ParameterName = "@tipo_moneda";
                ParTipoMoneda.SqlDbType = SqlDbType.Int;
                ParTipoMoneda.Value = Negociacion.Tipo_Moneda;
                SqlCmd.Parameters.Add(ParTipoMoneda);

                //Establecemos los valores para el parámetro
                //@telefono del Procedimiento Almacenado
                SqlParameter ParEtapa = new SqlParameter();
                ParEtapa.ParameterName = "@etapa";
                ParEtapa.SqlDbType = SqlDbType.Int;
                ParEtapa.Value = Negociacion.Etapa;
                SqlCmd.Parameters.Add(ParEtapa);

                SqlParameter PasEstado = new SqlParameter();
                PasEstado.ParameterName = "@estado";
                PasEstado.SqlDbType = SqlDbType.Int;
                PasEstado.Value = Negociacion.Estado;
                SqlCmd.Parameters.Add(PasEstado);

                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Negociacion.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                SqlParameter ParIdusuario = new SqlParameter();
                ParIdusuario.ParameterName = "@idusuario";
                ParIdusuario.SqlDbType = SqlDbType.Int;
                ParIdusuario.Value = Negociacion.IdUsuario;
                SqlCmd.Parameters.Add(ParIdusuario);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Negociacion.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);
                //Agregamos el parámetro al comando
                
                SqlParameter ParMonto_Facturado = new SqlParameter();
                ParMonto_Facturado.ParameterName = "@monto_facturado";
                ParMonto_Facturado.SqlDbType = SqlDbType.Money;
                ParMonto_Facturado.Value = Negociacion.Monto_Facturado;
                SqlCmd.Parameters.Add(ParMonto_Facturado);

                SqlParameter ParIdEmpresa = new SqlParameter();
                ParIdEmpresa.ParameterName = "@idempresa";
                ParIdEmpresa.SqlDbType = SqlDbType.Int;
                ParIdEmpresa.Value = Negociacion.IdEmpresa;
                SqlCmd.Parameters.Add(ParIdEmpresa);

                SqlParameter ParRequerimiento = new SqlParameter();
                ParRequerimiento.ParameterName = "@requerimiento";
                ParRequerimiento.SqlDbType = SqlDbType.NVarChar;
                ParRequerimiento.Value = Negociacion.Requerimiento;
                SqlCmd.Parameters.Add(ParRequerimiento); 

                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@idoportunidad";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = Negociacion.IdOportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad);

                //Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se ingreso el registro de forma correcta";

                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";


                //if (Oportunidad.Opcion == 1)
                //{
                if (Rpta == "OK")
                {
                    Rpta = SqlCmd.Parameters["@id"].Value.ToString();
                }
                else
                {
                    Rpta = "0";
                }
                //}

                //5. Ejecutamos el commando

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }
    }
}
