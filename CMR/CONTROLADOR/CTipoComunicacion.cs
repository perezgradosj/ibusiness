﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ENTIDAD;

namespace CONTROLADOR
{
    public class CTipoComunicacion
    {
        public DataTable Mostrar()
        {
            DataTable DtResultado = new DataTable("Tipo_Comunicaciones");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_Mostrar_TipoComunicacion";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                //3. No hay parámetros

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string Mantenimiento(ETipoComunicacion tipoComunicacion)
        {
            string rpta = "";
            SqlConnection connection = new SqlConnection();

            try
            {
                connection.ConnectionString = Conexion.cn;
                connection.Open();
                SqlCommand command = new SqlCommand("SP_MANT_TIPOCOMUNICACIONES", connection);
                command.CommandType = CommandType.StoredProcedure;

                //Parametros
                if (tipoComunicacion.Opcion == 1)
                {
                    command.Parameters.Add("@ID", SqlDbType.Int).Direction = ParameterDirection.Output;   
                }
                else if (tipoComunicacion.Opcion > 1)
                {
                    command.Parameters.Add("@ID", SqlDbType.Int).Value = tipoComunicacion.Id;
                }
                
                command.Parameters.Add("@NOMBRE", SqlDbType.VarChar, 150).Value = tipoComunicacion.Nombre;
                command.Parameters.Add("@OPCION", SqlDbType.Int).Value = tipoComunicacion.Opcion;

                rpta = command.ExecuteNonQuery() == 1 ? "OK" : "0";

                if (rpta == "OK")
                {
                    rpta = command.Parameters["@ID"].Value.ToString();
                }
                else
                {
                    rpta = "0";
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return rpta;
        }
    }
}
