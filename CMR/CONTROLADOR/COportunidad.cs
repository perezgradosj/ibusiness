﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ENTIDAD;

namespace CONTROLADOR
{
    public class COportunidad
    {
        public DataTable MostrarLineas()
        {
            DataTable DtResultado = new DataTable("Linea");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_MostrarLineas";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                //3. No hay parámetros

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable Mostrar(EOportunidad Oportunidad)
        {
            DataTable DtResultado = new DataTable("Oportunidad");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarOportunidades";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTexoBuscar = new SqlParameter();
                ParTexoBuscar.ParameterName = "@textobuscar";
                ParTexoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTexoBuscar.Size = 50;
                ParTexoBuscar.Value = Oportunidad.TexoBuscar;
                SqlCmd.Parameters.Add(ParTexoBuscar);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Oportunidad.Estado;
                SqlCmd.Parameters.Add(ParEstado);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Oportunidad.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Oportunidad.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParTamanoPagina = new SqlParameter();
                ParTamanoPagina.ParameterName = "@tamano_pagina";
                ParTamanoPagina.SqlDbType = SqlDbType.Int;
                ParTamanoPagina.Value = Oportunidad.TamanoPagina;
                SqlCmd.Parameters.Add(ParTamanoPagina);

                SqlParameter ParPaginaActual = new SqlParameter();
                ParPaginaActual.ParameterName = "@pagina_actual";
                ParPaginaActual.SqlDbType = SqlDbType.Int;
                ParPaginaActual.Value = Oportunidad.PaginaActual;
                SqlCmd.Parameters.Add(ParPaginaActual);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Oportunidad.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public DataTable MostrarDatosDePaginacion(EOportunidad Oportunidad)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_DATOS_PAGINACION_OPORTUNIDAD";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTexoBuscar = new SqlParameter();
                ParTexoBuscar.ParameterName = "@textobuscar";
                ParTexoBuscar.SqlDbType = SqlDbType.VarChar;
                ParTexoBuscar.Size = 50;
                ParTexoBuscar.Value = Oportunidad.TexoBuscar;
                SqlCmd.Parameters.Add(ParTexoBuscar);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Oportunidad.Estado;
                SqlCmd.Parameters.Add(ParEstado);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Oportunidad.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Oportunidad.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParTamanoPagina = new SqlParameter();
                ParTamanoPagina.ParameterName = "@tamano_pagina";
                ParTamanoPagina.SqlDbType = SqlDbType.Int;
                ParTamanoPagina.Value = Oportunidad.TamanoPagina;
                SqlCmd.Parameters.Add(ParTamanoPagina);

                SqlParameter ParPaginaActual = new SqlParameter();
                ParPaginaActual.ParameterName = "@pagina_actual";
                ParPaginaActual.SqlDbType = SqlDbType.Int;
                ParPaginaActual.Value = Oportunidad.PaginaActual;
                SqlCmd.Parameters.Add(ParPaginaActual);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@Opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Oportunidad.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string MantOportunidad(EOportunidad Oportunidad)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //Establecer la trasacción
                
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantOportunidad";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Oportunidad.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Oportunidad.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    //Le declaramos que el parámetro es de salida,
                    //porque obtendremos el código generado por la base de datos
                    ParId.Value = Oportunidad.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParIdTipoComunicacion = new SqlParameter();
                ParIdTipoComunicacion.ParameterName = "@idtipo_comunicacion";
                ParIdTipoComunicacion.SqlDbType = SqlDbType.Int;
                ParIdTipoComunicacion.Value = Oportunidad.IdTipoComunicacion;
                SqlCmd.Parameters.Add(ParIdTipoComunicacion);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParNombre = new SqlParameter();
                ParNombre.ParameterName = "@nombre";
                ParNombre.SqlDbType = SqlDbType.NVarChar;
                ParNombre.Size = 100;
                ParNombre.Value = Oportunidad.Nombre;
                SqlCmd.Parameters.Add(ParNombre);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParRequerimiento = new SqlParameter();
                ParRequerimiento.ParameterName = "@requerimiento";
                ParRequerimiento.SqlDbType = SqlDbType.NVarChar;
                //ParRequerimiento.Size = MAX;
                ParRequerimiento.Value = Oportunidad.Requerimiento;
                SqlCmd.Parameters.Add(ParRequerimiento);
                //Agregamos el parámetro al comando

                //Establecemos los valores para el parámetro
                //@telefono del Procedimiento Almacenado
                SqlParameter ParIdEmpresa = new SqlParameter();
                ParIdEmpresa.ParameterName = "@idempresa";
                ParIdEmpresa.SqlDbType = SqlDbType.Int;
                ParIdEmpresa.Value = Oportunidad.IdEmpresa;
                SqlCmd.Parameters.Add(ParIdEmpresa);

                SqlParameter ParFecha = new SqlParameter();
                ParFecha.ParameterName = "@fecha";
                ParFecha.SqlDbType = SqlDbType.DateTime;
                ParFecha.Value = Oportunidad.Fecha;
                SqlCmd.Parameters.Add(ParFecha);

                SqlParameter ParIdLinea = new SqlParameter();
                ParIdLinea.ParameterName = "@idlinea";
                ParIdLinea.SqlDbType = SqlDbType.Int;
                ParIdLinea.Value = Oportunidad.IdLinea;
                SqlCmd.Parameters.Add(ParIdLinea);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Oportunidad.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Oportunidad.Estado;
                SqlCmd.Parameters.Add(ParEstado);


                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Oportunidad.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                //Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se ingreso el registro de forma correcta";

                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";


                //if (Oportunidad.Opcion == 1)
                //{
                    if (Rpta == "OK")
                    {
                        Rpta = SqlCmd.Parameters["@id"].Value.ToString();

                    
                    }
                    else
                    {
                        Rpta = "0";
                    }

                
                //}

                //5. Ejecutamos el commando

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        public string RegistrarSolicitudOportunidad(int Oportunidad, int Usuario, int Opcion)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantOportunidades_Solicitados";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParId = new SqlParameter();
                ParId.ParameterName = "@id";
                ParId.SqlDbType = SqlDbType.Int;
                ParId.Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add(ParId);

                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@idoportunidad";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = Oportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = Usuario;
                SqlCmd.Parameters.Add(ParIdUsuario);
                //Agregamos el parámetro al comando

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se pudo completar la solicitud";
            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }

        //////////////////////////// SOLICITUDES
        public DataTable MostrarSolicitudes(int IdOportunidad, int IdUsuario)
        {
            DataTable DtResultado = new DataTable("Oportunidades_Solicitados");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarSolicitudes_Oportunidad";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL
                
                SqlParameter ParIdOportunidad = new SqlParameter();
                ParIdOportunidad.ParameterName = "@idoportunidad";
                ParIdOportunidad.SqlDbType = SqlDbType.Int;
                ParIdOportunidad.Value = IdOportunidad;
                SqlCmd.Parameters.Add(ParIdOportunidad); 

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int;
                ParIdUsuario.Value = IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }
    }
}
