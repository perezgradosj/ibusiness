﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using System.Data;
using System.Data.SqlClient;

namespace CONTROLADOR
{
    public class CActividad
    {
        public DataTable Mostrar(EActividad Actividad)
        {
            DataTable DtResultado = new DataTable();    
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_ListarActividades";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParTextoOpcion = new SqlParameter();
                ParTextoOpcion.ParameterName = "@textobuscar";
                ParTextoOpcion.SqlDbType = SqlDbType.NVarChar;
                ParTextoOpcion.Size = 50;
                ParTextoOpcion.Value = Actividad.TextoBuscar;
                SqlCmd.Parameters.Add(ParTextoOpcion);

                SqlParameter ParOpcion = new SqlParameter(); 
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Actividad.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                SqlParameter ParIdUsuario = new SqlParameter();
                ParIdUsuario.ParameterName = "@idusuario";
                ParIdUsuario.SqlDbType = SqlDbType.Int; 
                ParIdUsuario.Value = Actividad.IdUsuario;
                SqlCmd.Parameters.Add(ParIdUsuario);

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@idnegociacion";
                ParIdNegociacion.SqlDbType = SqlDbType.Int;
                ParIdNegociacion.Value = Actividad.IdNegociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string MantActividad(EActividad Actividad)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MantActividad";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Actividad.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Actividad.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@id";
                    ParId.SqlDbType = SqlDbType.Int;
                    //Le declaramos que el parámetro es de salida,
                    //porque obtendremos el código generado por la base de datos
                    ParId.Value = Actividad.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParIdTipoActividad = new SqlParameter();
                ParIdTipoActividad.ParameterName = "@idtipo_actividad";
                ParIdTipoActividad.SqlDbType = SqlDbType.Int;
                ParIdTipoActividad.Value = Actividad.IdTipoActividad;
                SqlCmd.Parameters.Add(ParIdTipoActividad);


                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParAsunto = new SqlParameter();
                ParAsunto.ParameterName = "@asunto";
                ParAsunto.SqlDbType = SqlDbType.NVarChar;
                //ParAsunto.Size = max;
                ParAsunto.Value = Actividad.Asunto;
                SqlCmd.Parameters.Add(ParAsunto);
                //Agregamos el parámetro al comando


                //Establecemos los valores para el parámetro
                //@direccion del Procedimiento Almacenado
                SqlParameter ParDescripcion = new SqlParameter();
                ParDescripcion.ParameterName = "@descripcion";
                ParDescripcion.SqlDbType = SqlDbType.NVarChar;
                //ParDescripcion.Size = max;
                ParDescripcion.Value = Actividad.Descripcion;
                SqlCmd.Parameters.Add(ParDescripcion);
                //Agregamos el parámetro al comando

                SqlParameter ParFecha = new SqlParameter();
                ParFecha.ParameterName = "@fecha";
                ParFecha.SqlDbType = SqlDbType.DateTime;
                ParFecha.Value = Actividad.Fecha;
                SqlCmd.Parameters.Add(ParFecha);

                SqlParameter ParIdContacto = new SqlParameter();
                ParIdContacto.ParameterName = "@idcontacto";
                ParIdContacto.SqlDbType = SqlDbType.Int;
                ParIdContacto.Value = Actividad.IdContacto;
                SqlCmd.Parameters.Add(ParIdContacto);

                SqlParameter ParHora = new SqlParameter();
                ParHora.ParameterName = "@hora";
                ParHora.SqlDbType = SqlDbType.DateTime;
                ParHora.Value = Actividad.Hora;
                SqlCmd.Parameters.Add(ParHora);

                SqlParameter ParIdusuario = new SqlParameter();
                ParIdusuario.ParameterName = "@idusuario";
                ParIdusuario.SqlDbType = SqlDbType.Int;
                ParIdusuario.Value = Actividad.IdUsuario;
                SqlCmd.Parameters.Add(ParIdusuario);

                SqlParameter ParEstado = new SqlParameter();
                ParEstado.ParameterName = "@estado";
                ParEstado.SqlDbType = SqlDbType.Int;
                ParEstado.Value = Actividad.Estado;
                SqlCmd.Parameters.Add(ParEstado); 

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@idnegociacion";
                ParIdNegociacion.SqlDbType = SqlDbType.Int;
                ParIdNegociacion.Value = Actividad.IdNegociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                //Establecemos los valores para el parámetro
                //@celular2 del Procedimiento Almacenado
                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Actividad.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);
                //Agregamos el parámetro al comando

                //5. Ejecutamos el commando
                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";
                
                    if (Rpta == "OK")
                    {
                        Rpta = Convert.ToString(SqlCmd.Parameters["@id"].Value);
                    }
                
            }
            catch (Exception ex)
            {
                //Rpta = ex.Message;
                Rpta = "Error";
            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }
    }
}
