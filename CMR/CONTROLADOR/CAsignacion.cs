﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using ENTIDAD;

namespace CONTROLADOR
{
    public class CAsignacion
    {
        public DataTable Mostrar(string usuario, string negociacion, int opcion)
        {
            DataTable DtResultado = new DataTable();
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexion
                SqlCon.ConnectionString = Conexion.cn;

                //2. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;//La conexión que va a usar el comando
                SqlCmd.CommandText = "SP_LISTAR_ASIGNACION_HISTORIAL";//El comando a ejecutar
                SqlCmd.CommandType = CommandType.StoredProcedure;
                //Decirle al comando que va a ejecutar una sentencia SQL

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@negociacion";
                ParIdNegociacion.SqlDbType = SqlDbType.NVarChar;
                ParIdNegociacion.Size = 50;
                ParIdNegociacion.Value = negociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                SqlParameter IdUsuario = new SqlParameter();
                IdUsuario.ParameterName = "@usuario";
                IdUsuario.SqlDbType = SqlDbType.NVarChar;
                IdUsuario.Size = 50;
                IdUsuario.Value = usuario;
                SqlCmd.Parameters.Add(IdUsuario);

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@opcion";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //4. El DataAdapter que va a ejecutar el comando y
                //es el encargado de llena el DataTable
                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);//Llenamos el DataTable
            }
            catch (Exception ex)
            {
                DtResultado = null;

            }
            return DtResultado;
        }

        public string MantAsignacionesHistorial(EAsignaciones Asignaciones)
        {
            string Rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //1. Establecer la cadena de conexión
                SqlCon.ConnectionString = Conexion.cn;
                //2. Abrir la conexión de la BD
                SqlCon.Open();
                //3. Establecer el comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "SP_MANT_ASIG_HISTORIAL";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                if (Asignaciones.Opcion == 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@ID";
                    ParId.SqlDbType = SqlDbType.Int;
                    ParId.Direction = ParameterDirection.Output;
                    SqlCmd.Parameters.Add(ParId);
                }
                else if (Asignaciones.Opcion > 1)
                {
                    SqlParameter ParId = new SqlParameter();
                    ParId.ParameterName = "@ID";
                    ParId.SqlDbType = SqlDbType.Int;
                    //Le declaramos que el parámetro es de salida,
                    //porque obtendremos el código generado por la base de datos
                    ParId.Value = Asignaciones.Id;
                    SqlCmd.Parameters.Add(ParId);
                }

                SqlParameter ParIdNegociacion = new SqlParameter();
                ParIdNegociacion.ParameterName = "@IDNEGOCIACION";
                ParIdNegociacion.SqlDbType = SqlDbType.Int;
                ParIdNegociacion.Value = Asignaciones.IdNegociacion;
                SqlCmd.Parameters.Add(ParIdNegociacion);

                SqlParameter IdUsuario = new SqlParameter();
                IdUsuario.ParameterName = "@IDUSUARIO";
                IdUsuario.SqlDbType = SqlDbType.Int;
                IdUsuario.Value = Asignaciones.IdUsuario;
                SqlCmd.Parameters.Add(IdUsuario);

                SqlParameter ParFechaInicio = new SqlParameter();
                ParFechaInicio.ParameterName = "@FECHA_INICIO";
                ParFechaInicio.SqlDbType = SqlDbType.DateTime;
                ParFechaInicio.Value = Asignaciones.Fecha_Inicio;
                SqlCmd.Parameters.Add(ParFechaInicio);
                
                //Establecemos los valores para el parámetro
                //@ruc del Procedimiento Almacenado
                SqlParameter ParFechaFin = new SqlParameter();
                ParFechaFin.ParameterName = "@FECHA_FIN";
                ParFechaFin.SqlDbType = SqlDbType.DateTime;
                ParFechaFin.Value = Asignaciones.Fecha_Fin;
                SqlCmd.Parameters.Add(ParFechaFin);
                //Agregamos el parámetro al comando

                SqlParameter ParOpcion = new SqlParameter();
                ParOpcion.ParameterName = "@OPCION";
                ParOpcion.SqlDbType = SqlDbType.Int;
                ParOpcion.Value = Asignaciones.Opcion;
                SqlCmd.Parameters.Add(ParOpcion);

                //Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "No se ingreso el registro de forma correcta";

                Rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "0";


                //if (Oportunidad.Opcion == 1)
                //{
                if (Rpta == "OK")
                {
                    Rpta = SqlCmd.Parameters["@ID"].Value.ToString();
                }
                else
                {
                    Rpta = "0";
                }
                //}

                //5. Ejecutamos el commando

            }
            catch (Exception ex)
            {
                Rpta = ex.Message;

            }
            finally
            {
                //6. Cerramos la conexion con la BD
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return Rpta;
        }
    }
}
