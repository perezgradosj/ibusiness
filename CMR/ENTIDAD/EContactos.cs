﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EContactos
    {
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Correo { get; set; }
        public string Movil { get; set; }
        public string Fijo { get; set; }
        public int IdEmpresa { get; set; }

        public int IdNegociacion { get; set; }
        public int IdOportunidad { get; set; }

        public string TextoBuscar { get; set; }
        public int Opcion { get; set; }
    }
}
