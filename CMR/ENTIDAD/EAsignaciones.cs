﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EAsignaciones
    {
        public int Id { get; set; }
        public int IdNegociacion { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha_Inicio { get; set; }
        public DateTime Fecha_Fin { get; set; }
        public int Opcion { get; set; }
    }
}
