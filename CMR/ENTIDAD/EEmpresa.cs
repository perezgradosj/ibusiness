﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EEmpresa
    {
        public int Id { get; set; }
        public string Ruc { get; set; }
        public string Razon_Social { get; set; }
        public string Rubro { get; set; }

        public int Opcion { get; set; }
        public string TextoBuscar { get; set; }
    }
}
