﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EActividad
    {
        public int Id { get; set; }
        public int IdTipoActividad { get; set; }
        public string Asunto { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int IdContacto { get; set; }
        public DateTime Hora { get; set; }
        public int IdUsuario { get; set; }
        public int Estado { get; set; }
        public int IdNegociacion { get; set; }
        public string TextoBuscar { get; set; }

        public int Opcion { get; set; }
    }
}
