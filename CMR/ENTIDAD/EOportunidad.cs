﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EOportunidad
    {
        public int Id { get; set; }
        public int IdTipoComunicacion { get; set; }
        public string Nombre { get; set; }
        public string Requerimiento { get; set; }
        public int IdContacto { get; set; }
        public int IdEmpresa { get; set; }
        public DateTime Fecha { get; set; } = DateTime.Today;
        public int IdLinea { get; set; }
        public int IdUsuario { get; set; }
        public int Estado { get; set; }
        public int TamanoPagina { get; set; }
        public int PaginaActual { get; set; }

        public string TexoBuscar { get; set; }
        public int Opcion { get; set; }
    }
}
