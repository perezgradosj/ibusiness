﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class EUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Ape_Paterno { get; set; }
        public string Ape_Materno { get; set; }
        public char Sexo { get; set; }
        public int IdLinea { get; set; }
        public string Linea { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public int Oport_Max { get; set; }
        public int Opcion { get; set; }
        public int SuperLinea { get; set; }

        public int LineaAdicional { get; set; }

        public EUsuario(int id, string nombre, string ape_paterno, string ape_materno, char sexo, int idlinea,
            string linea, string usuario, string password)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Ape_Paterno = ape_paterno;
            this.Ape_Materno = ape_materno;
            this.Sexo = sexo;
            this.IdLinea = idlinea;
            this.Linea = linea;
            this.Usuario = usuario;
            this.Password = password;
        }

        public EUsuario() { }
        
    }
}
