﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD
{
    public class ENegociacion
    {
        public int Id { get; set; } = 0;
        public DateTime Fecha_Registro { get; set; } = DateTime.Today;
        public DateTime Fecha_Conversion { get; set; } = DateTime.Today;
        public string Nombre { get; set; } = "";
        public double Monto { get; set; } = 0;
        public int Etapa { get; set; } = 0;
        public int Tipo_Moneda { get; set; } = 0;
        public int Estado { get; set; } = 0;
        public int IdUsuario { get; set; } = 0;
        public int IdLinea { get; set; } = 0;
        public string TextoBuscar { get; set; } = "";
        public int IdContacto { get; set; } = 0;
        public double Monto_Facturado { get; set; } = 0;
        public int IdEmpresa { get; set; } = 0;
        public string Requerimiento { get; set; } = "";
        public int IdOportunidad { get; set; }
        public int TamanoPagina { get; set; }
        public int PaginaActual { get; set; }
        public int Opcion { get; set; } = 0;
    }
}
