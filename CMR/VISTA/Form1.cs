﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MODELO;

namespace VISTA
{
    public partial class Form1 : Form
    {
        int id = 0;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource = MContactos.Mostrar(textEdit1.Text, 2);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (gvdata.RowCount == 0)
            {
                this.id = 0;
            }

            MessageBox.Show(gvdata.GetFocusedRowCellValue("id").ToString());

            //string d =  MContactos.MantContacto(0, "DEMOS", "demo@dcgte.com", 999777888, "8896756", 1);
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (textEdit1.Text != "")
            {
                gridControl1.DataSource = MContactos.Mostrar(textEdit1.Text, 2);
                gridControl1.Visible = true;
                gvdata.Columns[0].Visible = false;
                gvdata.Columns[2].Visible = false;
                gvdata.Columns[3].Visible = false;
                gvdata.Columns[4].Visible = false;

                if (gvdata.RowCount < 1)
                {
                    gridControl1.Visible = false;
                }
            }
            else
            {
                gridControl1.Visible = false;
            }
        }

        private void textEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down: gridControl1.Focus();
                    break;
            }
        }

        private void gridControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            textEdit1.Focus();
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                textEdit1.Text = gvdata.GetFocusedRowCellValue("nombres").ToString();
                this.id = Convert.ToInt32(gvdata.GetFocusedRowCellValue("id").ToString());
                gridControl1.Visible = false;
            }
        }

        private void gridControl1_DoubleClick(object sender, EventArgs e)
        {
            textEdit1.Text = gvdata.GetFocusedRowCellValue("id").ToString();
            this.id = Convert.ToInt32(gvdata.GetFocusedRowCellValue("id").ToString());
        }
    }
}
