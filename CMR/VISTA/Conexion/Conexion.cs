﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VISTA.Conexion
{
    public partial class Conexion : Form
    {
        ConexionService cnxSrv;

        public Conexion()
        {
            InitializeComponent();
            cnxSrv = new ConexionService();
        }

        private void rdBtnConUser_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rdBtnConUser.IsChecked == true)
            {
                txtNomUsu.Enabled = true;
                txtPass.Enabled = true;
            }
        }

        private void chkGuardar_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            btnAceptar.Enabled = false;
        }

        private void Conexion_Load(object sender, EventArgs e)
        {
            txtNomUsu.Enabled = false;
            txtPass.Enabled = false;
            cargaPropiedades();
        }

        private void cargaPropiedades()
        {
            if (Properties.Settings.Default.cadenaConexion != "")
            {
                cmbServidor.Text = Properties.Settings.Default.nomServidor;
                cmbBaseDatos.Text = Properties.Settings.Default.nombreBD;
                rdBtnConUser.IsChecked = Properties.Settings.Default.checkUsuario;
                if (rdBtnConUser.IsChecked == true)
                {
                    chkGuardar.Checked = Properties.Settings.Default.checkGuardar;
                    if (chkGuardar.Checked == true)
                        txtPass.Text = Properties.Settings.Default.passSrv;
                    txtNomUsu.Text = Properties.Settings.Default.usuSrv;
                }
            }
        }

        private void btnProbarConex_Click(object sender, EventArgs e)
        {
            pruebaConexion();
        }

        private void pruebaConexion()
        {
            try
            {
                cnxSrv.pruebaConex(cmbBaseDatos.Text, cmbServidor.Text, txtNomUsu.Text, txtPass.Text, rdBtnConUser.IsChecked);
                XtraMessageBox.Show("Conexion realizada Correctamente", "Mensaje", MessageBoxButtons.OK,
                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                btnAceptar.Enabled = true;
            }
            catch (System.Data.SqlClient.SqlException exp)
            {
                XtraMessageBox.Show("Error en la Conexion :" + exp.Message);
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cnxSrv.cnxOk == true)
                {
                    Properties.Settings.Default.cadenaConexion = cnxSrv.cadenaConexion;
                    Properties.Settings.Default.nomServidor = cmbServidor.Text;
                    Properties.Settings.Default.nombreBD = cmbBaseDatos.Text;
                    Properties.Settings.Default.usuSrv = txtNomUsu.Text;
                    Properties.Settings.Default.passSrv = txtPass.Text;
                    Properties.Settings.Default.checkContra = chkContra.Checked;
                    Properties.Settings.Default.checkUsuario = rdBtnConUser.IsChecked;
                    Properties.Settings.Default.checkGuardar = chkGuardar.Checked;
                    Properties.Settings.Default.Save();
                    this.Close();
                    this.Dispose();
                }
            }
            catch (Exception exp)
            {
                XtraMessageBox.Show(exp.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void despliegaBD()
        {
            try
            {
                cnxSrv.llenaBaseDatos(cmbBaseDatos, cmbServidor.Text, txtNomUsu.Text,
                    txtPass.Text, rdBtnConUser.IsChecked);
                btnAceptar.Enabled = false;
            }
            catch (System.Data.SqlClient.SqlException exp)
            {
                XtraMessageBox.Show(exp.Message);
            }
        }

        private void cmbBaseDatos_PopupOpening(object sender, CancelEventArgs e)
        {
            despliegaBD();
        }

        private void cmbServidor_PopupOpening(object sender, CancelEventArgs e)
        {
            cmbServidor = cnxSrv.llenaServidor(cmbServidor);
            btnAceptar.Enabled = false;
        }

        private void rdBtnSinUser_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rdbSinUser.IsChecked == true)
            {
                txtNomUsu.Enabled = false;
                txtPass.Enabled = false;
                txtNomUsu.Text = "";
                txtPass.Text = "";
            }
        }
    }
}
