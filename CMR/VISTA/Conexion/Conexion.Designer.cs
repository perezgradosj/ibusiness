﻿namespace VISTA.Conexion
{
    partial class Conexion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProbarConex = new DevExpress.XtraEditors.SimpleButton();
            this.cmbBaseDatos = new Telerik.WinControls.UI.RadDropDownList();
            this.txtPass = new DevExpress.XtraEditors.TextEdit();
            this.txtNomUsu = new DevExpress.XtraEditors.TextEdit();
            this.rdBtnConUser = new Telerik.WinControls.UI.RadRadioButton();
            this.rdbSinUser = new Telerik.WinControls.UI.RadRadioButton();
            this.chkGuardar = new Telerik.WinControls.UI.RadCheckBox();
            this.chkContra = new Telerik.WinControls.UI.RadCheckBox();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.cmbServidor = new Telerik.WinControls.UI.RadDropDownList();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaseDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomUsu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdBtnConUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbSinUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGuardar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServidor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnProbarConex
            // 
            this.btnProbarConex.Location = new System.Drawing.Point(228, 302);
            this.btnProbarConex.Name = "btnProbarConex";
            this.btnProbarConex.Size = new System.Drawing.Size(90, 23);
            this.btnProbarConex.TabIndex = 35;
            this.btnProbarConex.Text = "Probar Conexion";
            this.btnProbarConex.Click += new System.EventHandler(this.btnProbarConex_Click);
            // 
            // cmbBaseDatos
            // 
            this.cmbBaseDatos.Location = new System.Drawing.Point(36, 276);
            this.cmbBaseDatos.Name = "cmbBaseDatos";
            this.cmbBaseDatos.Size = new System.Drawing.Size(282, 20);
            this.cmbBaseDatos.TabIndex = 34;
            this.cmbBaseDatos.PopupOpening += new System.ComponentModel.CancelEventHandler(this.cmbBaseDatos_PopupOpening);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(163, 196);
            this.txtPass.Name = "txtPass";
            this.txtPass.Properties.UseSystemPasswordChar = true;
            this.txtPass.Size = new System.Drawing.Size(139, 20);
            this.txtPass.TabIndex = 33;
            // 
            // txtNomUsu
            // 
            this.txtNomUsu.Location = new System.Drawing.Point(163, 171);
            this.txtNomUsu.Name = "txtNomUsu";
            this.txtNomUsu.Size = new System.Drawing.Size(139, 20);
            this.txtNomUsu.TabIndex = 32;
            // 
            // rdBtnConUser
            // 
            this.rdBtnConUser.Location = new System.Drawing.Point(36, 150);
            this.rdBtnConUser.Name = "rdBtnConUser";
            this.rdBtnConUser.Size = new System.Drawing.Size(280, 18);
            this.rdBtnConUser.TabIndex = 31;
            this.rdBtnConUser.TabStop = false;
            this.rdBtnConUser.Text = "Usar un nombre de usuario y contraseña especificos";
            this.rdBtnConUser.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rdBtnConUser_ToggleStateChanged);
            // 
            // rdbSinUser
            // 
            this.rdbSinUser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rdbSinUser.Location = new System.Drawing.Point(36, 127);
            this.rdbSinUser.Name = "rdbSinUser";
            this.rdbSinUser.Size = new System.Drawing.Size(238, 18);
            this.rdbSinUser.TabIndex = 30;
            this.rdbSinUser.Text = "Usar la seguridad integrada de windows NT";
            this.rdbSinUser.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.rdbSinUser.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.rdBtnSinUser_ToggleStateChanged);
            // 
            // chkGuardar
            // 
            this.chkGuardar.Location = new System.Drawing.Point(163, 222);
            this.chkGuardar.Name = "chkGuardar";
            this.chkGuardar.Size = new System.Drawing.Size(120, 18);
            this.chkGuardar.TabIndex = 29;
            this.chkGuardar.Text = "Guardar Contraseña";
            this.chkGuardar.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chkGuardar_ToggleStateChanged);
            // 
            // chkContra
            // 
            this.chkContra.Location = new System.Drawing.Point(27, 222);
            this.chkContra.Name = "chkContra";
            this.chkContra.Size = new System.Drawing.Size(129, 18);
            this.chkContra.TabIndex = 28;
            this.chkContra.Text = "Contraseña en Blanco";
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(263, 73);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(70, 20);
            this.btnActualizar.TabIndex = 27;
            this.btnActualizar.Text = "Actualizar";
            // 
            // cmbServidor
            // 
            this.cmbServidor.Location = new System.Drawing.Point(36, 73);
            this.cmbServidor.Name = "cmbServidor";
            this.cmbServidor.Size = new System.Drawing.Size(221, 20);
            this.cmbServidor.TabIndex = 26;
            this.cmbServidor.PopupOpening += new System.ComponentModel.CancelEventHandler(this.cmbServidor_PopupOpening);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(27, 199);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(63, 13);
            this.labelControl7.TabIndex = 25;
            this.labelControl7.Text = "Contraseña :";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(27, 174);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(94, 13);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Nombre de usuario:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(5, 108);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(275, 13);
            this.labelControl6.TabIndex = 23;
            this.labelControl6.Text = "2. Escriba la informacion para iniciar sesion en el servidor ";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(5, 257);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(204, 13);
            this.labelControl3.TabIndex = 22;
            this.labelControl3.Text = "3. Seleccione la base de datos del servidor";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(5, 54);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(222, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "1. Seleccione o Escriba el nombre del servidor:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(5, 35);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(306, 13);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "Especifique lo siguiente para conectarse a datos de SQL Server ";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(284, 367);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 20);
            this.btnCancelar.TabIndex = 10;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(203, 367);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 20);
            this.btnAceptar.TabIndex = 9;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnProbarConex);
            this.groupControl1.Controls.Add(this.cmbBaseDatos);
            this.groupControl1.Controls.Add(this.txtPass);
            this.groupControl1.Controls.Add(this.txtNomUsu);
            this.groupControl1.Controls.Add(this.rdBtnConUser);
            this.groupControl1.Controls.Add(this.rdbSinUser);
            this.groupControl1.Controls.Add(this.chkGuardar);
            this.groupControl1.Controls.Add(this.chkContra);
            this.groupControl1.Controls.Add(this.btnActualizar);
            this.groupControl1.Controls.Add(this.cmbServidor);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(347, 349);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "DATOS DE CONEXION";
            // 
            // Conexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 400);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.groupControl1);
            this.Name = "Conexion";
            this.Text = "Conexion";
            this.Load += new System.EventHandler(this.Conexion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaseDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomUsu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdBtnConUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdbSinUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkGuardar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkContra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbServidor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnProbarConex;
        private Telerik.WinControls.UI.RadDropDownList cmbBaseDatos;
        private DevExpress.XtraEditors.TextEdit txtPass;
        private DevExpress.XtraEditors.TextEdit txtNomUsu;
        private Telerik.WinControls.UI.RadRadioButton rdBtnConUser;
        private Telerik.WinControls.UI.RadRadioButton rdbSinUser;
        private Telerik.WinControls.UI.RadCheckBox chkGuardar;
        private Telerik.WinControls.UI.RadCheckBox chkContra;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private Telerik.WinControls.UI.RadDropDownList cmbServidor;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.GroupControl groupControl1;
    }
}