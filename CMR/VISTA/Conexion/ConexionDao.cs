﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace VISTA.Conexion
{
    class ConexionDao
    {

        internal DataTable traeBD(string cadenaConexion)
        {
            DataTable aux;
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                using (SqlDataAdapter adap = new SqlDataAdapter("select name from sys.databases", conn))
                {
                    aux = new DataTable();
                    adap.Fill(aux);

                    adap.Dispose();
                    conn.Close();
                }
            }
            return aux;
        }

        internal void pruebaConexion(string cadenaConexion)
        {
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                conn.Open();
            }
        }



        internal void creaTabla(string query, string cadenaConexion)
        {
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    conn.Dispose();
                }

            }
        }

        internal bool ExistTabla(string query, string cadenaConexion)
        {
            using (SqlConnection conn = new SqlConnection(cadenaConexion))
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    string i = (string)cmd.ExecuteScalar();
                    conn.Close();
                    conn.Dispose();
                    if (i != "" && i != null)
                    {
                        return true;
                    }
                }

            }
            return false;
        }
    }
}
