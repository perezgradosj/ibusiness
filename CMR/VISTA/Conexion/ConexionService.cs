﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace VISTA.Conexion
{
    class ConexionService
    {

        SqlDataSourceEnumerator servers;
        DataTable tablaserver;
        public string cadenaConexion { set; get; }
        public bool cnxOk { get; set; }
        ConexionDao cnxDao;

        public ConexionService()
        {
            cnxDao = new ConexionDao();
            servers = SqlDataSourceEnumerator.Instance;
            tablaserver = new DataTable();
            cnxOk = false;
            cadenaConexion = Properties.Settings.Default.cadenaConexion;
        }


        internal RadDropDownList llenaServidor(RadDropDownList cmbServidor)
        {
            if (tablaserver.Rows.Count == 0)
            {
                tablaserver = servers.GetDataSources();
                List<string> listaServidores = new List<string>();
                foreach (DataRow rowServidor in tablaserver.Rows)
                {
                    if (String.IsNullOrEmpty(rowServidor["InstanceName"].ToString()))
                        listaServidores.Add(rowServidor["ServerName"].ToString());
                    else
                        listaServidores.Add(rowServidor["ServerName"] + "\\" + rowServidor["InstanceName"]);
                }
                cmbServidor.DataSource = listaServidores;
            }

            return cmbServidor;
        }


        internal RadDropDownList llenaBaseDatos(RadDropDownList cmbBaseDatos, string cmbServidor,
            string txtNomUsu, string txtPass, bool checkUser)
        {
            if (checkUser)
            {
                cadenaConexion = "Password=" + txtPass + ";User ID=" + txtNomUsu + ";Initial " +
                                  "Catalog=master ;Data Source=" + cmbServidor;
            }
            else
            { cadenaConexion = "Integrated Security=True;Initial Catalog=master ;Data Source=" + cmbServidor; }

            try
            {
                cmbBaseDatos.DataSource = cnxDao.traeBD(cadenaConexion);
                cmbBaseDatos.DisplayMember = "name";
            }
            catch (Exception)
            {
                throw;
            }

            return cmbBaseDatos;
        }

        internal void pruebaConex(string cmbBaseDatos, string cmbServidor,
            string txtNomUsu, string txtPass, bool chkUser)
        {
            creaConexion(cmbBaseDatos, cmbServidor, txtNomUsu, txtPass, chkUser);
            try
            {
                cnxDao.pruebaConexion(cadenaConexion);
                cnxOk = true;
            }
            catch (Exception)
            {
                cnxOk = false;
                throw;
            }

        }

        private void creaConexion(string cmbBaseDatos, string cmbServidor,
            string txtNomUsu, string txtPass, bool chkUser)
        {
            if (chkUser)
            {
                cadenaConexion = "Password=" + txtPass + ";User ID=" + txtNomUsu + ";Initial " +
                                  "Catalog=" + cmbBaseDatos + ";Data Source=" + cmbServidor;
            }
            else
            { cadenaConexion = "Integrated Security=True;Initial Catalog= " + cmbBaseDatos + " ;Data Source=" + cmbServidor; }

        }

        internal void CreaTablasUsuario()
        {
            string query2 = "SELECT NAME FROM sysobjects  WHERE XTYPE = 'U' " +
                            " and NAME = 'empresas'";
            if (cnxDao.ExistTabla(query2, cadenaConexion))
            {
                string query1 = " SELECT NAME " +
                      "  FROM sysobjects  " +
                      "  WHERE XTYPE = 'U' " +
                      " and NAME = 'usuarioZK' ";
                if (!cnxDao.ExistTabla(query1, cadenaConexion))
                {
                    string query = " create table usuarioZK( " +
                        " id_usuario int identity(1,1) not null, " +
                        " usu_codigo varchar(10) unique not null," +
                        " usu_nombre varchar(50) not null,  " +
                        " usu_apellidos varchar(200) not null, " +
                        " usu_dni varchar(15) not null , " +
                        " usu_Usuario varchar(100) not null, " +
                        " usu_pass varchar(100) not null, " +
                        " usu_fecha Datetime default GetDate(), " +
                        "Constraint PK_USUARIOZK primary key(id_usuario) )";
                    cnxDao.creaTabla(query, cadenaConexion);
                }
            }
            else
            {

                throw new System.ArgumentException("Base de Datos Incorrecta");
            }
        }

        internal bool ExistTabla()
        {
            bool val = false;
            try
            {
                string query1 = "SELECT NAME FROM sysobjects  " +
                        " WHERE XTYPE = 'U' and NAME = 'empresas' ";
                if (cnxDao.ExistTabla(query1, cadenaConexion))
                {
                    val = true;
                }
            }
            catch (Exception)
            {
                val = false;
                throw;
            }

            return val;
        }

    }
}
