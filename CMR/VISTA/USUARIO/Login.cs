﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;
using MODELO;
using ENTIDAD;

namespace VISTA.USUARIO
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public Login()
        {
            InitializeComponent();
        }

        private void ValidarDatos(string Usuario, string Password)
        {
            if (Password == "Ingrese Contraseña")
            {
                Password = string.Empty; 
            }
            else if (Password != "")
            {
                Password =  MSeguridad.Encriptar(Password);
            }

            DataTable Datos = MUsuario.Login(Usuario, Password, 1);

            if (Datos.Rows.Count == 0)
            {
                MessageBox.Show("Usuario o Contraseña incorrecto", "Sistema DCGTEC");
            }
            else
            {
                DataRow row = Datos.Rows[0];
                MessageBox.Show("Bienvenido " + row["nombre"].ToString() + " " + row["ape_paterno"].ToString(), "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Principal principal = new Principal();

                principal.id = Convert.ToInt32(row["id"].ToString());
                principal.nombre = row["nombre"].ToString();
                principal.ape_paterno = row["ape_paterno"].ToString();
                principal.ape_materno = row["ape_materno"].ToString();
                principal.sexo = Convert.ToChar(row["sexo"].ToString());
                principal.idlinea = Convert.ToInt32(row["idlinea"].ToString());
                principal.linea = row["linea"].ToString();
                principal.usuario = row["usuario"].ToString();
                principal.SuperLinea = Convert.ToInt32(row["super_linea"].ToString());
                principal.MaxAsignaciones = Convert.ToInt32(row["MaxAsignaciones"].ToString());
                principal.Show();
                this.Hide();

            }
        }

        private void txtusr_Enter(object sender, EventArgs e)
        {
            if (txtusr.Text == "Ingrese Usuario")
            {
                txtusr.Text = "";
                txtusr.ForeColor = Color.DimGray;
            }
        }

        private void txtusr_Leave(object sender, EventArgs e)
        {
            if (txtusr.Text == "")
            {
                txtusr.Text = "Ingrese Usuario";
                txtusr.ForeColor = Color.DarkGray;
            }
        }

        private void txtpwr_Enter(object sender, EventArgs e)
        {
            if (txtpsw.Text == "Ingrese Contraseña")
            {
                txtpsw.Text = "";
                txtpsw.ForeColor = Color.DimGray;
                txtpsw.Properties.PasswordChar = '*';
            }
        }

        private void txtpwr_Leave(object sender, EventArgs e)
        {
            if (txtpsw.Text == "")
            {
                txtpsw.Text = "Ingrese Contraseña";
                txtpsw.ForeColor = Color.DarkGray;
                txtpsw.Properties.PasswordChar = '\0';
            }
        }

        private void btningresar_Click(object sender, EventArgs e)
        {
            this.ValidarDatos(txtusr.Text, txtpsw.Text);
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pbxCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbxMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtpsw_KeyPress(object sender, KeyPressEventArgs e)
        {
            Control.IsKeyLocked(Keys.CapsLock);

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.ValidarDatos(txtusr.Text, txtpsw.Text);
            }
        }
    }
}