﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace VISTA.USUARIO
{
    public partial class RegUsuario : DevExpress.XtraEditors.XtraForm
    {
        //Variable que nos indica si vamos a insertar un nuevo producto
        private bool VarNuevo = false;
        //Variable que nos indica si vamos a modificar un producto
        private bool VarEditar = false;

        //Variable para identificar la funcion ocultar visualizacion de contraseña
        private bool MostrarContraseña = false;


        /// Valiables Locales
        private int id = 0;
        /// /////////////////
        string pass = "";
        /// <summary>
        /// ///////////
        /// </summary>
        private DataTable dtNegociaciones;
        private DataTable dtOportunidades;
        /// ////////////////////////
        /// 
        private static RegUsuario _instancia;

        //Creamos una instancia para poder utilizar los
        //Objetos del formulario
        public static RegUsuario GetInstancia()
        {
            if (_instancia == null)
            {
                _instancia = new RegUsuario();
            }
            return _instancia;
        }

        public void setOportunidades(string _Id, string _Nombre)
        {
            bool registrar = true;
            if (luelinea.Text == _Nombre)
            {
                this.MensajeError("Linea Seleccionada como principal");
            }
            else
            {
                foreach (DataRow row in dtOportunidades.Rows)
                {
                    if (Convert.ToString(row["id"]) == _Id)
                    {
                        registrar = false;
                        this.MensajeError("Linea ya Registrada");
                    }
                }
                if (registrar)
                {
                    //Agregar ese detalle al datalistadoDetalle
                    DataRow row = this.dtOportunidades.NewRow();
                    row["id"] = _Id;
                    row["nombre"] = _Nombre;
                    this.dtOportunidades.Rows.Add(row);
                }
            }
        }

        public void setNegociaciones(string _Id, string _Nombre)
        {
            bool registrar = true;
            if (luelinea.Text == _Nombre)
            {
                this.MensajeError("Linea Seleccionada como principal");
            }
            else
            {
                foreach (DataRow row in dtNegociaciones.Rows)
                {
                    if (Convert.ToString(row["id"]) == _Id)
                    {
                        registrar = false;
                        this.MensajeError("Linea ya Registrada");
                    }
                }
                if (registrar)
                {
                    //Agregar ese detalle al datalistadoDetalle
                    DataRow row = this.dtNegociaciones.NewRow();
                    row["id"] = _Id;
                    row["nombre"] = _Nombre;
                    this.dtNegociaciones.Rows.Add(row);
                }
            }
        }

        public RegUsuario()
        {
            InitializeComponent();
        }

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CargarNodesNegociaciones()
        {
            //MNegociaciones mnegociaciones = new MNegociaciones();
            //DataTable dt = (DataTable)mnegociaciones.MostrarNodes();

            //tlnegociaciones.BeginUnboundLoad();
            //TreeListNode parentForRootNodes = null;
            //tlnegociaciones.Nodes.Clear();
            //if (dt != null)
            //{
            //    foreach (DataRow dr in dt.Rows)//se leen las filas del Datatable asignado
            //    {
            //        TreeListNode rootNode = tlnegociaciones.AppendNode(
            //            new object[] { dr.ItemArray[1].ToString() }, parentForRootNodes, CheckState.Unchecked,
            //            Convert.ToInt32(dr.ItemArray[0].ToString()));
            //    }
            //    tlnegociaciones.EndUnboundLoad();
            //}

        }

        private void CargarNodesOportunidades()
        {
            //MOportunidades moportunidades = new MOportunidades();
            //DataTable dt = (DataTable)moportunidades.MostrarNodes();

            //tlnegociaciones.BeginUnboundLoad();
            //TreeListNode parentForRootNodes = null;
            //tloportunidades.Nodes.Clear();
            //if (dt != null)
            //{
            //    foreach (DataRow dr in dt.Rows)//se leen las filas del Datatable asignado
            //    {
            //        TreeListNode rootNode = tloportunidades.AppendNode(
            //            new object[] { dr.ItemArray[1].ToString() }, parentForRootNodes, CheckState.Unchecked,
            //            Convert.ToInt32(dr.ItemArray[0].ToString()));
            //    }
            //    tlnegociaciones.EndUnboundLoad();
            //}
        }

        private void LlenarComboLineas(LookUpEdit lookUpEdit)
        {
            MLineas mLineas = new MLineas();
            DataTable dt = mLineas.Mostrar();

            lookUpEdit.Properties.ValueMember = "id";
            lookUpEdit.Properties.DisplayMember = "nombre";

            lookUpEdit.Properties.DataSource = dt;
        }

        //private void CargarNodesXUsuario(int id)
        //{
        //    MUsuario mUsuario = new MUsuario();
        //    DataTable dt = (DataTable)mUsuario.MostrarNegociaciones();

        //    TreeListNode parentForRootNodes = null;
        //    tloportunidades.UncheckAll();
        //    if (dt != null)
        //    {
        //        foreach (DataRow dr in dt.Rows)//se leen las filas del Datatable asignado
        //        {
        //            TreeListNode rootNode = tloportunidades.AppendNode(
        //        new object[] { dr.ItemArray[1].ToString() },
        //        parentForRootNodes);
        //        }
        //    }

        //}

        private void Guardar()
        {
            
            try
            {
                string rpta = "";
                if (this.txtNombres.Text == string.Empty || 
                    this.txtApe_Paterno.Text == string.Empty )
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(txtNombres, "Ingrese un Valor");
                    errorIcono.SetError(txtApe_Paterno, "Ingrese un Valor");
                }
                else
                {
                    if (this.VarNuevo)
                    {
                        char sexo = Convert.ToChar(cboSexo.Text.Substring(0,1));
                        int idlinea = Convert.ToInt32(luelinea.GetColumnValue("id"));
                        //List<TreeListNode> nodesNegociaciones = tlnegociaciones.GetAllCheckedNodes();
                        //List<TreeListNode> nodesOportunidades = tloportunidades.GetAllCheckedNodes();
                        string Contra = MSeguridad.Encriptar(this.txtpassword.Text);
                        int SuperLinea = 0;

                        if (chkAdmin.CheckState == CheckState.Checked)
                        {
                            SuperLinea = 1;
                        }

                        rpta = MUsuario.Insertar(this.txtNombres.Text.Trim().ToUpper(), 
                                                this.txtApe_Paterno.Text,
                                                this.txtApe_Materno.Text, 
                                                sexo, 
                                                idlinea, 
                                                this.txtUsuario.Text,
                                                Contra,
                                                1, 
                                                dtNegociaciones,
                                                dtOportunidades,
                                                Convert.ToInt32(txtOport_Max.Text),
                                                SuperLinea);
                    }
                    else
                    {
                        char sexo = Convert.ToChar(cboSexo.Text.Substring(0, 1));
                        int idlinea = Convert.ToInt32(luelinea.GetColumnValue("id"));

                        var de = MSeguridad.Encriptar(this.txtpassword.Text);

                        int SuperLinea = 0;

                        if (chkAdmin.CheckState == CheckState.Checked)
                        {
                            SuperLinea = 1;
                        }
                        rpta = MUsuario.Editar(this.id,
                                                this.txtNombres.Text.Trim().ToUpper(),
                                                this.txtApe_Paterno.Text,
                                                this.txtApe_Materno.Text,
                                                sexo,
                                                idlinea,
                                                this.txtUsuario.Text,
                                                MSeguridad.Encriptar(this.txtpassword.Text),
                                                Convert.ToInt32(txtOport_Max.Text),
                                                2,
                                                SuperLinea);
                        if (rpta == "OK")
                        {
                            MUsuario.EliminarNegociaciones(this.id, dtNegociaciones);
                            MUsuario.EliminarOportunidades(this.id, dtOportunidades);
                        }
                    }

                    if (rpta.Equals("OK"))
                    {
                        if (this.VarNuevo)
                        {
                            this.MensajeOk("Se Insertó de forma correcta el registro");
                        }
                        else
                        {
                            this.MensajeOk("Se Actualizó de forma correcta el registro");
                        }
                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                    this.VarNuevo = false;
                    this.VarEditar = false;
                    this.Botones();
                    this.Limpiar();
                    this.MostrarUsuarios();


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void OcultarColumnas()
        {
            this.dgvUsuarios.Columns[0].Visible = false;
            this.dgvUsuarios.Columns[4].Visible = false;
            this.dgvUsuarios.Columns[5].Visible = false;
            this.dgvUsuarios.Columns[6].Visible = false;
            this.dgvUsuarios.Columns[7].Visible = false;
            this.dgvUsuarios.Columns[8].Visible = false;
            this.dgvUsuarios.Columns[9].Visible = false;
        }

        private void MostrarUsuarios()
        {
            this.dgcUsuarios.DataSource = MUsuario.Mostrar();
            this.OcultarColumnas();
        }

        private void Habilitar(bool Valor)
        {
            this.txtNombres.Enabled = Valor;
            this.txtApe_Paterno.Enabled = Valor;
            this.txtApe_Materno.Enabled = Valor;
            this.cboSexo.Enabled = Valor;
            this.luelinea.Enabled = Valor;
            this.txtpassword.Enabled = Valor;
            this.txtUsuario.Enabled = Valor;
            this.btnAgregarNego.Enabled = Valor;
            this.btnEliminarNego.Enabled = Valor;
            this.btnAgregarOpor.Enabled = Valor;
            this.btnEliminarOpor.Enabled = Valor;
            this.txtOport_Max.Enabled = Valor;
            this.chkAdmin.Enabled = Valor;
        }

        private void Botones()
        {
            if (this.VarNuevo || this.VarEditar)
            {
                this.Habilitar(true);
                this.btnnuevo.Enabled = false;
                this.btnguardar.Enabled = true;
                this.btneditar.Visible = false;
                this.btncancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.btnnuevo.Enabled = true;
                this.btnguardar.Enabled = false;
                this.btneditar.Visible = true;
                this.btncancelar.Enabled = false;
            }
        }

        private void Limpiar()
        {
            this.id = 0;
            this.txtNombres.Text = string.Empty;
            this.txtApe_Paterno.Text = string.Empty;
            this.txtApe_Materno.Text = string.Empty;
            this.cboSexo.Text = string.Empty;
            this.luelinea.Text = string.Empty;
            this.txtpassword.Text = string.Empty;
            this.txtUsuario.Text = string.Empty;
            this.tlnegociaciones.UncheckAll();
            this.tloportunidades.UncheckAll();
            this.crearTablaNegociaciones();
            this.crearTablaOportunidades();
        }

        private void Eliminar()
        {
            try
            {
                if (id == 0)
                {
                    MessageBox.Show("Debe Seleccionar un elemento a eliminar", "Sistema SPE");
                }
                else
                {
                    DialogResult Opcion;
                    Opcion = MessageBox.Show("¿Realmente desea eliminar los registros?", "Sistema SPE", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (Opcion == DialogResult.OK)
                    {
                        string Rpta = "";

                        Rpta = MUsuario.Eliminar(id);

                        if (Rpta.Equals("OK"))
                        {
                            this.MensajeOk("Se eliminó de forma correcta el registro");
                        }
                        else
                        {
                            //Mostramos el mensaje de error
                            this.MensajeError(Rpta);
                        }

                        this.MostrarUsuarios();
                        this.id = 0;
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex);
            }
        }

        private void crearTablaNegociaciones()
        {
            this.dtNegociaciones = new DataTable("DtNegociaciones");
            this.dtNegociaciones.Columns.Add("id", System.Type.GetType("System.Int32"));
            this.dtNegociaciones.Columns.Add("nombre", System.Type.GetType("System.String"));
            //Relacionar nuestro DataGRidView con nuestro DataTable
            this.gcNegociaciones.DataSource = this.dtNegociaciones;

            gvNegociaciones.Columns[0].Visible = false;
        }

        private void crearTablaOportunidades()
        {
            this.dtOportunidades = new DataTable("DtOportunidades");
            this.dtOportunidades.Columns.Add("id", System.Type.GetType("System.Int32"));
            this.dtOportunidades.Columns.Add("nombre", System.Type.GetType("System.String"));
            //Relacionar nuestro DataGRidView con nuestro DataTable
            this.gcOportunidades.DataSource = this.dtOportunidades;
            gvOportunidades.Columns[0].Visible = false;
        }

        private void MostrarNegociacionesxUsuario()
        {

            dtNegociaciones.Clear();

            DataTable table = MUsuario.MostrarNegociaciones(Convert.ToString(this.id));

            foreach (DataRow dtRow in table.Rows)
            {
                //Nuevo_Usuario nu = new Nuevo_Usuario();
                //AddOwnedForm(nu);

                //Agregar ese detalle al datalistadoDetalle
                DataRow row = this.dtOportunidades.NewRow();
                string _Id = dtRow[0].ToString();
                string _Nombre = dtRow[1].ToString();
                setNegociaciones(_Id,_Nombre);
            }
            //this.gcNegociaciones.DataSource = MUsuario.MostrarNegociaciones(Convert.ToString(this.id));

        }

        private void MostrarOportunidadesxUsuario()
        {
            dtOportunidades.Clear();

            DataTable table = MUsuario.MostrarOportunidades(Convert.ToString(this.id));

            foreach (DataRow dtRow in table.Rows)
            {
                //Agregar ese detalle al datalistadoDetalle
                DataRow row = this.dtOportunidades.NewRow();
                string _Id = dtRow[0].ToString();
                string _Nombre = dtRow[1].ToString();
                setOportunidades(_Id, _Nombre);
            }

            //this.gcOportunidades.DataSource = MUsuario.MostrarOportunidades(Convert.ToString(this.id));

        }

        private void Nuevo_Usuario_Load(object sender, EventArgs e)
        {
            //Deshabilita los controles
            this.Habilitar(false);
            //Establece los botones
            this.Botones();
            CargarNodesNegociaciones();
            CargarNodesOportunidades();
            LlenarComboLineas(luelinea);
            MostrarUsuarios();
            crearTablaNegociaciones();
            crearTablaOportunidades();
        }

        private void btn_verpassword_Click(object sender, EventArgs e)
        {
            if (this.MostrarContraseña == false)
            {
                txtpassword.Properties.PasswordChar = '\0';
                MostrarContraseña = true;
            }

            else if (this.MostrarContraseña == true)
            {
                txtpassword.Properties.PasswordChar = '*';
                MostrarContraseña = false;
            }

        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            this.VarNuevo = true;
            this.VarEditar = false;
            this.Botones();
            this.Limpiar();
            this.txtNombres.Focus();
            this.txtOport_Max.Text = "4";
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void btneditar_Click(object sender, EventArgs e)
        {
            //Si no ha seleccionado un producto no puede modificar
            if (this.id > 0)
            {
                this.VarEditar = true;
                this.Botones();
                this.btneditar.Visible = false;
            }
            else
            {
                this.MensajeError("Debe de buscar un registro para Modificar");
            }
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
            this.Limpiar();
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.VarNuevo = false;
            this.VarEditar = false;
            this.Botones();
            this.Limpiar();
            this.id = 0;
            this.btneditar.Visible = true;
        }

        private void dgcUsuarios_Click(object sender, EventArgs e)
        {
            try
            {

                id = Convert.ToInt32(dgvUsuarios.GetFocusedRowCellValue("id").ToString());
                txtNombres.Text = dgvUsuarios.GetFocusedRowCellValue("nombre").ToString();
                txtApe_Paterno.Text = dgvUsuarios.GetFocusedRowCellValue("ape_paterno").ToString();
                txtApe_Materno.Text = dgvUsuarios.GetFocusedRowCellValue("ape_materno").ToString();
                string sexo = dgvUsuarios.GetFocusedRowCellValue("sexo").ToString();
                string linea = dgvUsuarios.GetFocusedRowCellValue("linea").ToString();
                luelinea.EditValue = luelinea.Properties.GetKeyValueByDisplayText(dgvUsuarios.GetFocusedRowCellValue("linea").ToString());
                //lookUpEdit1.EditValue = lookUpEdit1.Properties.GetKeyValueByDisplayText("USD");
                txtUsuario.Text = dgvUsuarios.GetFocusedRowCellValue("usuario").ToString();
                txtpassword.Text = MSeguridad.DesEncriptar(dgvUsuarios.GetFocusedRowCellValue("password").ToString());
                txtOport_Max.Text = dgvUsuarios.GetFocusedRowCellValue("Oportunidades_Max").ToString();
                int superlinea =Convert.ToInt32(dgvUsuarios.GetFocusedRowCellValue("super_linea").ToString());

                if (sexo == "M")
                {
                    cboSexo.SelectedIndex = 1;
                }

                if (sexo == "F")
                {
                    cboSexo.SelectedIndex = 2;
                }

                if (superlinea == 1)
                {
                    chkAdmin.CheckState = CheckState.Checked;
                }
                else
                {
                    chkAdmin.CheckState = CheckState.Unchecked;
                }

                MostrarNegociacionesxUsuario();
                MostrarOportunidadesxUsuario();

            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        private void btnAgregarNego_Click(object sender, EventArgs e)
        {
            try
            {
                Vistas.VistaLineas VistaLineas = new Vistas.VistaLineas();
                VistaLineas.data = "Negociaciones";
                AddOwnedForm(VistaLineas);
                VistaLineas.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void btnAgregarOpor_Click(object sender, EventArgs e)
        {
            try
            {
                Vistas.VistaLineas VistaLineas = new Vistas.VistaLineas();
                VistaLineas.data = "Oportunidades";
                AddOwnedForm(VistaLineas);
                VistaLineas.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void btnEliminarNego_Click(object sender, EventArgs e)
        {
            gvNegociaciones.DeleteSelectedRows();
        }

        private void btnEliminarOpor_Click(object sender, EventArgs e)
        {
            gvOportunidades.DeleteSelectedRows();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            MostrarUsuarios();
        }

        private void txtOport_Max_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }
    }
}