﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;
using MODELO;

namespace VISTA.USUARIO
{
    public partial class Usuario : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        private int IdUsuario = 0;
        private string Nombres = "";
        private string Ape_Paterno = "";
        private string Ape_Materno = "";
        private string Linea = "";
        private int Idlinea = 0;
        private char Sexo = '0';
        private string User = "";

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void SetDatos(int idusuario, string nombres, string ape_paterno, string ape_materno, string linea, 
                             char sexo, string user)
        {
            this.IdUsuario = idusuario;
            this.Nombres = nombres;
            this.Ape_Paterno = ape_paterno;
            this.Ape_Materno = ape_materno;
            //this.Idlinea = idlinea;
            this.Linea = linea;
            this.Sexo = sexo;
            this.User = user;
        }
        public Usuario()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";
                char sexo = Convert.ToChar(cboSexo.Text.Substring(0, 1));
                rpta = MUsuario.Editar(this.IdUsuario, txtNombres.Text, txtApe_Paterno.Text, txtApe_Materno.Text,
                                sexo, Idlinea, lblUsuario.Text, "", 0, 4, 0);

                if (rpta == "OK")
                {
                    MensajeOk("Datos actualizados correctamente");
                }
                else
                {
                    MensajeOk("Los datos no se actualizaron");
                }
            }
            catch (Exception ex)
            {

            }
            
        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            this.txtNombres.Text = this.Nombres;
            this.txtApe_Paterno.Text = this.Ape_Paterno;
            this.txtApe_Materno.Text = this.Ape_Materno;

            if (Sexo == 'M')
            {
                cboSexo.SelectedIndex = 1;
            }
            if (Sexo == 'F')
            {
                cboSexo.SelectedIndex = 2;
            }
            this.lblLinea.Text = this.Linea;
            this.lblUsuario.Text = this.User;
        }

        private void btnRestablecerContraseña_Click(object sender, EventArgs e)
        {
            RestablecerContra restablecer = new RestablecerContra();
            restablecer.IdUsuario = this.IdUsuario;
            restablecer.ShowDialog();
        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}