﻿namespace VISTA.USUARIO
{
    partial class RestablecerContra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnRestabecerContraseña = new DevExpress.XtraEditors.SimpleButton();
            this.txtContraNueva2 = new DevExpress.XtraEditors.TextEdit();
            this.txtContra_Nueva = new DevExpress.XtraEditors.TextEdit();
            this.txtContraActual = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContraNueva2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContra_Nueva.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContraActual.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.btnRestabecerContraseña);
            this.groupControl1.Controls.Add(this.txtContraNueva2);
            this.groupControl1.Controls.Add(this.txtContra_Nueva);
            this.groupControl1.Controls.Add(this.txtContraActual);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(584, 211);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Restablecer Contraseña";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = global::VISTA.Properties.Resources.close_16x16;
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(566, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(16, 16);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnRestabecerContraseña
            // 
            this.btnRestabecerContraseña.Location = new System.Drawing.Point(234, 149);
            this.btnRestabecerContraseña.Name = "btnRestabecerContraseña";
            this.btnRestabecerContraseña.Size = new System.Drawing.Size(150, 40);
            this.btnRestabecerContraseña.TabIndex = 6;
            this.btnRestabecerContraseña.Text = "Restablecer Contraseña";
            this.btnRestabecerContraseña.Click += new System.EventHandler(this.btnRestabecerContraseña_Click);
            // 
            // txtContraNueva2
            // 
            this.txtContraNueva2.Location = new System.Drawing.Point(180, 109);
            this.txtContraNueva2.Name = "txtContraNueva2";
            this.txtContraNueva2.Properties.PasswordChar = '*';
            this.txtContraNueva2.Size = new System.Drawing.Size(300, 20);
            this.txtContraNueva2.TabIndex = 5;
            // 
            // txtContra_Nueva
            // 
            this.txtContra_Nueva.Location = new System.Drawing.Point(180, 83);
            this.txtContra_Nueva.Name = "txtContra_Nueva";
            this.txtContra_Nueva.Properties.PasswordChar = '*';
            this.txtContra_Nueva.Size = new System.Drawing.Size(300, 20);
            this.txtContra_Nueva.TabIndex = 4;
            // 
            // txtContraActual
            // 
            this.txtContraActual.Location = new System.Drawing.Point(180, 45);
            this.txtContraActual.Name = "txtContraActual";
            this.txtContraActual.Properties.PasswordChar = '*';
            this.txtContraActual.Size = new System.Drawing.Size(300, 20);
            this.txtContraActual.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(84, 112);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(90, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Repita Contraseña";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(84, 86);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(90, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Nueva Contraseña";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(84, 48);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Contraseña Actual";
            // 
            // RestablecerContra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 211);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RestablecerContra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RestablecerContra";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContraNueva2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContra_Nueva.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContraActual.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnRestabecerContraseña;
        private DevExpress.XtraEditors.TextEdit txtContraNueva2;
        private DevExpress.XtraEditors.TextEdit txtContra_Nueva;
        private DevExpress.XtraEditors.TextEdit txtContraActual;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}