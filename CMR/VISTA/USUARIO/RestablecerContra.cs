﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;

namespace VISTA.USUARIO
{
    public partial class RestablecerContra : DevExpress.XtraEditors.XtraForm
    {
        public int IdUsuario = 0;

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public RestablecerContra()
        {
            InitializeComponent();
        }

        private void btnRestabecerContraseña_Click(object sender, EventArgs e)
        {
            try
            {
                string rpta = "";

                if (txtContra_Nueva.Text == txtContraNueva2.Text)
                {
                    DialogResult dialog = MessageBox.Show("¿Deseas cambiar tu contraseña?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialog == DialogResult.Yes)
                    {
                        DataTable Datos = MUsuario.Login("", MSeguridad.Encriptar(txtContraActual.Text), 2);

                        if (Datos.Rows.Count == 0)
                        {
                            MensajeError("Contraseña actual incorrecta");
                        }
                        else
                        {
                            rpta = MUsuario.Editar(this.IdUsuario,
                                                        "",
                                                        "",
                                                        "",
                                                        '0',
                                                        0,
                                                        "",
                                                        MSeguridad.Encriptar(this.txtContra_Nueva.Text),
                                                        0,
                                                        3,
                                                        0);

                            if (rpta == "OK")
                            {
                                MensajeOk("Contraseña actualizada correctamente");
                            }
                            else
                            {
                                MensajeError("Error al actualizar la contraseña");
                            }
                        }
                    }
                }
                else
                {
                    MensajeError("Contraseñas no coinciden");
                }
            }
            catch (Exception ex)
            {

            }
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}