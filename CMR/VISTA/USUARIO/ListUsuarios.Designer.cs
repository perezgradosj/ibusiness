﻿namespace VISTA.USUARIO
{
    partial class ListUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListUsuarios));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.dgcUsuarios = new DevExpress.XtraGrid.GridControl();
            this.dgvUsuarios = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.txtTexto = new DevExpress.XtraEditors.TextEdit();
            this.cboTFiltro = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgcUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTexto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTFiltro.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(922, 471);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "USUARIO";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dgcUsuarios);
            this.panelControl2.Controls.Add(this.btnActualizar);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 116);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(918, 353);
            this.panelControl2.TabIndex = 19;
            // 
            // btnActualizar
            // 
            this.btnActualizar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizar.ImageOptions.Image")));
            this.btnActualizar.Location = new System.Drawing.Point(952, 6);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(23, 23);
            this.btnActualizar.TabIndex = 17;
            // 
            // dgcUsuarios
            // 
            this.dgcUsuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgcUsuarios.Location = new System.Drawing.Point(2, 2);
            this.dgcUsuarios.MainView = this.dgvUsuarios;
            this.dgcUsuarios.Name = "dgcUsuarios";
            this.dgcUsuarios.Size = new System.Drawing.Size(914, 349);
            this.dgcUsuarios.TabIndex = 3;
            this.dgcUsuarios.TabStop = false;
            this.dgcUsuarios.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvUsuarios});
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.GridControl = this.dgcUsuarios;
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.OptionsBehavior.Editable = false;
            this.dgvUsuarios.OptionsView.ShowAutoFilterRow = true;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.txtTexto);
            this.panelControl1.Controls.Add(this.cboTFiltro);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 20);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(918, 96);
            this.panelControl1.TabIndex = 18;
            // 
            // txtTexto
            // 
            this.txtTexto.Location = new System.Drawing.Point(184, 53);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.Size = new System.Drawing.Size(524, 20);
            this.txtTexto.TabIndex = 3;
            // 
            // cboTFiltro
            // 
            this.cboTFiltro.EditValue = "Nombre";
            this.cboTFiltro.Location = new System.Drawing.Point(78, 53);
            this.cboTFiltro.Name = "cboTFiltro";
            this.cboTFiltro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTFiltro.Properties.Items.AddRange(new object[] {
            "Nombre",
            "Apellido Paterno",
            "Apellido Materno",
            "Linea"});
            this.cboTFiltro.Size = new System.Drawing.Size(100, 20);
            this.cboTFiltro.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(10, 56);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(62, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tipo de Filtro";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(188, 31);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Lista de Usuarios";
            // 
            // ListUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 471);
            this.Controls.Add(this.groupControl1);
            this.Name = "ListUsuarios";
            this.Text = "ListUsuarios";
            this.Load += new System.EventHandler(this.ListUsuarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgcUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTexto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTFiltro.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl dgcUsuarios;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvUsuarios;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.TextEdit txtTexto;
        private DevExpress.XtraEditors.ComboBoxEdit cboTFiltro;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}