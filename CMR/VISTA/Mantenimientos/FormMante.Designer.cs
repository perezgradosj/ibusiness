﻿namespace VISTA.Mantenimientos
{
    partial class FormMante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnTipoComunicacion = new DevExpress.XtraEditors.SimpleButton();
            this.PanelContenedor = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelContenedor)).BeginInit();
            this.PanelContenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelControl1.Appearance.Options.UseFont = true;
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.btnTipoComunicacion);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelControl1.Location = new System.Drawing.Point(2, 2);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(151, 486);
            this.panelControl1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.simpleButton1.ImageOptions.Image = global::VISTA.Properties.Resources.cards_32x32;
            this.simpleButton1.Location = new System.Drawing.Point(2, 52);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(147, 50);
            this.simpleButton1.TabIndex = 3;
            this.simpleButton1.Text = "Lineas";
            // 
            // btnTipoComunicacion
            // 
            this.btnTipoComunicacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTipoComunicacion.ImageOptions.Image = global::VISTA.Properties.Resources.chartsshowlegend_32x32;
            this.btnTipoComunicacion.Location = new System.Drawing.Point(2, 2);
            this.btnTipoComunicacion.Name = "btnTipoComunicacion";
            this.btnTipoComunicacion.Size = new System.Drawing.Size(147, 50);
            this.btnTipoComunicacion.TabIndex = 2;
            this.btnTipoComunicacion.Text = "Tipo de Comunicacion";
            this.btnTipoComunicacion.Click += new System.EventHandler(this.btnTipoComunicacion_Click);
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.Controls.Add(this.panelControl1);
            this.PanelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenedor.Location = new System.Drawing.Point(0, 0);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(856, 490);
            this.PanelContenedor.TabIndex = 2;
            // 
            // FormMante
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 490);
            this.Controls.Add(this.PanelContenedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMante";
            this.Text = "FormMante";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormMante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelContenedor)).EndInit();
            this.PanelContenedor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnTipoComunicacion;
        private DevExpress.XtraEditors.PanelControl PanelContenedor;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}