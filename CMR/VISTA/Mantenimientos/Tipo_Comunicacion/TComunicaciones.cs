﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;

namespace VISTA.Mantenimientos.Tipo_Comunicacion
{
    public partial class TComunicaciones : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public TComunicaciones()
        {
            InitializeComponent();
        }

        public void Mostrar()
        {
            MTipoComunicacion tipoComunicacion = new MTipoComunicacion();
            gcTComunicaciones.DataSource =  tipoComunicacion.Mostrar();

            gvTComunicaciones.Columns[0].Visible = false;
        }

        private void TComunicaciones_Load(object sender, EventArgs e)
        {
            Mostrar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarTComunicacion tComunicacion = new AgregarTComunicacion();
            AddOwnedForm(tComunicacion);
            tComunicacion.ShowDialog();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(gvTComunicaciones.GetFocusedRowCellValue("id").ToString());
            MTipoComunicacion.Mantenimiento(Id, "", 3);

            Mostrar();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}