﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;

namespace VISTA.Mantenimientos.Tipo_Comunicacion
{
    public partial class AgregarTComunicacion : DevExpress.XtraEditors.XtraForm
    {
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public AgregarTComunicacion()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string rpta = "";
            try
            {
                rpta = MTipoComunicacion.Mantenimiento(0, txtdescripcion.Text, 1);

                if (Convert.ToInt32(rpta) > 0)
                {
                    MensajeOk("Datos guardados correctamente");
                }
                else
                {
                    MensajeError("Error al guardar los datos");
                }

                TComunicaciones comunicaciones = Owner as TComunicaciones;
                comunicaciones.Mostrar();
                this.Close();
            }
            catch (Exception ex )
            {

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        

        private void AgregarTComunicacion_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}