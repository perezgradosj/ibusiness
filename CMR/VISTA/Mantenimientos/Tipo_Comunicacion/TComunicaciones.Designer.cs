﻿namespace VISTA.Mantenimientos.Tipo_Comunicacion
{
    partial class TComunicaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnBorrar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAgregar = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.txtdescripcion = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcTComunicaciones = new DevExpress.XtraGrid.GridControl();
            this.gvTComunicaciones = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTComunicaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTComunicaciones)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnBorrar);
            this.groupControl1.Controls.Add(this.btnAgregar);
            this.groupControl1.Controls.Add(this.simpleButton1);
            this.groupControl1.Controls.Add(this.txtdescripcion);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(384, 93);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Tipo de Comunicaciones";
            this.groupControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.groupControl1_MouseDown);
            // 
            // btnBorrar
            // 
            this.btnBorrar.ImageOptions.Image = global::VISTA.Properties.Resources.clearall_16x16;
            this.btnBorrar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBorrar.Location = new System.Drawing.Point(320, 65);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(23, 23);
            this.btnBorrar.TabIndex = 10;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.ImageOptions.Image = global::VISTA.Properties.Resources.add_16x16;
            this.btnAgregar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAgregar.Location = new System.Drawing.Point(349, 64);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(23, 23);
            this.btnAgregar.TabIndex = 9;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = global::VISTA.Properties.Resources.close_16x16;
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(362, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(16, 16);
            this.simpleButton1.TabIndex = 8;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.Location = new System.Drawing.Point(79, 38);
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Size = new System.Drawing.Size(293, 20);
            this.txtdescripcion.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descripcion";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gcTComunicaciones);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 93);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(384, 304);
            this.groupControl2.TabIndex = 1;
            // 
            // gcTComunicaciones
            // 
            this.gcTComunicaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTComunicaciones.Location = new System.Drawing.Point(2, 20);
            this.gcTComunicaciones.MainView = this.gvTComunicaciones;
            this.gcTComunicaciones.Name = "gcTComunicaciones";
            this.gcTComunicaciones.Size = new System.Drawing.Size(380, 282);
            this.gcTComunicaciones.TabIndex = 0;
            this.gcTComunicaciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTComunicaciones});
            // 
            // gvTComunicaciones
            // 
            this.gvTComunicaciones.GridControl = this.gcTComunicaciones;
            this.gvTComunicaciones.Name = "gvTComunicaciones";
            this.gvTComunicaciones.OptionsBehavior.Editable = false;
            this.gvTComunicaciones.OptionsView.ShowGroupPanel = false;
            // 
            // TComunicaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 397);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TComunicaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TComunicaciones";
            this.Load += new System.EventHandler(this.TComunicaciones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTComunicaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTComunicaciones)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtdescripcion;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gcTComunicaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTComunicaciones;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnAgregar;
        private DevExpress.XtraEditors.SimpleButton btnBorrar;
    }
}