﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VISTA.Mantenimientos
{
    public partial class FormMante : DevExpress.XtraEditors.XtraForm
    {
        public void AbrirFormEnPanel<MiForm>() where MiForm : Form, new()
        {
            Form Formulario;
            Formulario = PanelContenedor.Controls.OfType<MiForm>().FirstOrDefault();//Busca en la Conecion el Form

            if (Formulario == null)
            {
                Formulario = new MiForm();
                Formulario.TopLevel = false;
                PanelContenedor.Controls.Add(Formulario);
                PanelContenedor.Tag = Formulario;
                //Formulario.MdiParent = this;
                Formulario.Show();
                //Formulario.BringToFront();
            }
            else
            {
                Formulario.BringToFront();
            }
        }

        public FormMante()
        {
            InitializeComponent();
        }

        private void FormMante_Load(object sender, EventArgs e)
        {
            MdiClient ctlMDI;
            // Loop through all of the form's controls looking
            // for the control of type MdiClient.
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    // Attempt to cast the control to type MdiClient.
                    ctlMDI = (MdiClient)ctl;

                    // Set the BackColor of the MdiClient control.
                    ctlMDI.BackColor = this.BackColor;
                }
                catch (InvalidCastException exc)
                {
                    // Catch and ignore the error if casting failed.
                }
            }
        }

        private void btnTipoComunicacion_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel<Tipo_Comunicacion.TComunicaciones>();
        }
    }
}