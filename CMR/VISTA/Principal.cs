﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using ENTIDAD;

namespace VISTA
{
    public partial class Principal : DevExpress.XtraEditors.XtraForm
    {
        public int id = 0;
        public string nombre = "";
        public string ape_paterno = "";
        public string ape_materno = "";
        public char sexo = ' ';
        public int idlinea = 0;
        public string linea = "";
        public string usuario = "";
        public int SuperLinea = 0;
        public int MaxAsignaciones = 0;

        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public Principal()
        {
            InitializeComponent();
        }

        int LX, LY;


        //METODO PARA REDIMENCIONAR/CAMBIAR TAMAÑO A FORMULARIO  TIEMPO DE EJECUCION ----------------------------------------------------------
        private int tolerance = 15;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //----------------DIBUJAR RECTANGULO / EXCLUIR ESQUINA PANEL 
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));

            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);

            region.Exclude(sizeGripRectangle);
            this.panelPrincipal.Region = region;
            this.Invalidate();
        }
        //----------------COLOR Y GRIP DE RECTANGULO INFERIOR
        protected override void OnPaint(PaintEventArgs e)
        {

            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(55, 61, 69));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);

            base.OnPaint(e);
            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

        public void AbrirFormEnPanel<MiForm>() where MiForm : Form, new()
        {
            Form Formulario;
            Formulario = PanelContenedor.Controls.OfType<MiForm>().FirstOrDefault();//Busca en la Conecion el Form

            if (Formulario == null)
            {
                Formulario = new MiForm();
                Formulario.TopLevel = false;
                PanelContenedor.Controls.Add(Formulario);
                PanelContenedor.Tag = Formulario;
                //Formulario.MdiParent = this;
                Formulario.Show();
                //Formulario.BringToFront();
            }
            else
            {
                Formulario.BringToFront();
            }
        }

        private void pxslide_Click(object sender, EventArgs e)
        {
            //if (MenuVertical.Width == 250)
            //{
            //    MenuVertical.Width = 50;
            //    panelUsuario.Visible = false;
            //}
            //else
            //{
            //    MenuVertical.Width = 250;
            //    panelUsuario.Visible = true;
            //}
        }

        private void pbxcerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿ Seguro que desea salir de  sistema ?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) ==DialogResult.Yes)
            {
                Application.Exit();
            }
            
        }

        private void pbxmaximizar_Click(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;
            LX = this.Location.X;
            LY = this.Location.Y;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            pbxrestaurar.Visible = true;
            pbxmaximizar.Visible = false;
        }

        private void pbxrestaurar_Click(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Normal;
            this.Size = new Size(1300,650);
            this.Location = new Point(LX,LY);
            pbxrestaurar.Visible = false;
            pbxmaximizar.Visible = true;
        }

        private void pbxminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            this.timer.Enabled = true;
            this.lblApellidos.Text = ape_paterno + " " + ape_materno;
            this.lblNombres.Text = nombre;
            this.lblLinea.Text = linea;
            if (this.SuperLinea == 1)
            {
                this.lblLinea.Text = "SUPERVISOR " + linea;
                
            }
            else if (this.SuperLinea == 0)
            {
                this.lblLinea.Text = linea;
            }

            if (this.SuperLinea == 3)
            {
                this.btnConfiguracion.Visible = true;
            }
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            pbxrestaurar.Visible = true;
            pbxmaximizar.Visible = false;

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            this.lblHora.Text = DateTime.Now.ToLongTimeString();
            this.lblfecha.Text = DateTime.Now.ToLongDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.SuperLinea == 3)
            {
                AbrirFormEnPanel<USUARIO.RegUsuario>();
            }
            else
            {
                USUARIO.Usuario Usuario;
                Usuario = PanelContenedor.Controls.OfType<USUARIO.Usuario>().FirstOrDefault();//Busca en la Conecion el Form

                if (Usuario == null)
                {
                    Usuario = new USUARIO.Usuario();
                    Usuario.TopLevel = false;
                    PanelContenedor.Controls.Add(Usuario);
                    PanelContenedor.Tag = Usuario;
                    Usuario.SetDatos(this.id, this.nombre, this.ape_paterno, this.ape_materno, 
                                    this.linea, this.sexo, this.usuario);
                    //Formulario.MdiParent = this;
                    Usuario.StartPosition = FormStartPosition.CenterScreen;
                    Usuario.Show();
                    Usuario.BringToFront();
                }
                else
                {
                    Usuario.BringToFront();
                }
            }
            
        }

        private void btnMaximizarCompleto_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            DialogResult resul = MessageBox.Show("¿ Seguro que desea Cerar sesion ?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (resul == DialogResult.Yes)
            {
                this.Close();
                USUARIO.Login login = new USUARIO.Login();
                login.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Negociacion.LisNegociaciones Formulario;
            Formulario = PanelContenedor.Controls.OfType<Negociacion.LisNegociaciones>().FirstOrDefault();//Busca en la Conecion el Form

            if (Formulario == null)
            {
                Formulario = new Negociacion.LisNegociaciones();
                Formulario.TopLevel = false;
                PanelContenedor.Controls.Add(Formulario);
                PanelContenedor.Tag = Formulario;
                Formulario.SetDatosUsuario(this.id, this.SuperLinea, this.idlinea);
                //Formulario.MdiParent = this;
                Formulario.Show();
                Formulario.BringToFront();
            }
            else
            {
                Formulario.BringToFront();
                Formulario.setActualizar();
            }
        }

        private void btmConfiguracion_Click(object sender, EventArgs e)
        {
            AbrirFormEnPanel<Mantenimientos.FormMante>();
        }

        public void AbrirOportunidades()
        {
            Procesos.ListOportunidades Formulario;
            Formulario = PanelContenedor.Controls.OfType<Procesos.ListOportunidades>().FirstOrDefault();//Busca en la Conecion el Form

            if (Formulario == null)
            {
                Formulario = new Procesos.ListOportunidades();
                Formulario.TopLevel = false;
                PanelContenedor.Controls.Add(Formulario);
                PanelContenedor.Tag = Formulario;

                Formulario.SetDatosUsuario(this.id, this.SuperLinea, this.idlinea, this.MaxAsignaciones);
                //Formulario.MdiParent = this;
                Formulario.Show();
                Formulario.BringToFront();
            }
            else
            {
                Formulario.BringToFront();
                //Formulario.setActualizar();
            }
        }

        private void btnOportunidades_Click(object sender, EventArgs e)
        {
                Procesos.ListOportunidades Formulario;
                Formulario = PanelContenedor.Controls.OfType<Procesos.ListOportunidades>().FirstOrDefault();//Busca en la Conecion el Form

                if (Formulario == null)
                {
                    Formulario = new Procesos.ListOportunidades();
                    Formulario.TopLevel = false;
                    PanelContenedor.Controls.Add(Formulario);
                    PanelContenedor.Tag = Formulario;
                    
                    Formulario.SetDatosUsuario(this.id, this.SuperLinea, this.idlinea, this.MaxAsignaciones);
                    //Formulario.MdiParent = this;
                    Formulario.Show();
                    Formulario.BringToFront();
                }
                else
                {
                    Formulario.BringToFront();
                    //Formulario.setActualizar();
                }
        }
    }
}