﻿namespace VISTA
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.lblfecha = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblHora = new DevExpress.XtraEditors.LabelControl();
            this.btnMaximizarCompleto = new System.Windows.Forms.PictureBox();
            this.pbxminimizar = new System.Windows.Forms.PictureBox();
            this.pbxmaximizar = new System.Windows.Forms.PictureBox();
            this.pbxcerrar = new System.Windows.Forms.PictureBox();
            this.pxslide = new System.Windows.Forms.PictureBox();
            this.pbxrestaurar = new System.Windows.Forms.PictureBox();
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnConfiguracion = new System.Windows.Forms.Button();
            this.panelUsuario = new System.Windows.Forms.Panel();
            this.lblLinea = new DevExpress.XtraEditors.LabelControl();
            this.lblNombres = new DevExpress.XtraEditors.LabelControl();
            this.lblApellidos = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOportunidades = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.panelPrincipal.SuspendLayout();
            this.PanelContenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.BarraTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximizarCompleto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxminimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxmaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxcerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxslide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxrestaurar)).BeginInit();
            this.MenuVertical.SuspendLayout();
            this.panelUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.Controls.Add(this.PanelContenedor);
            this.panelPrincipal.Controls.Add(this.BarraTitulo);
            this.panelPrincipal.Controls.Add(this.MenuVertical);
            this.panelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(1300, 650);
            this.panelPrincipal.TabIndex = 0;
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.Controls.Add(this.pictureBox1);
            this.PanelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenedor.Location = new System.Drawing.Point(250, 50);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(1050, 600);
            this.PanelContenedor.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::VISTA.Properties.Resources.LogoDCGtec;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1050, 600);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.White;
            this.BarraTitulo.Controls.Add(this.lblfecha);
            this.BarraTitulo.Controls.Add(this.pictureBox4);
            this.BarraTitulo.Controls.Add(this.lblHora);
            this.BarraTitulo.Controls.Add(this.btnMaximizarCompleto);
            this.BarraTitulo.Controls.Add(this.pbxminimizar);
            this.BarraTitulo.Controls.Add(this.pbxmaximizar);
            this.BarraTitulo.Controls.Add(this.pbxcerrar);
            this.BarraTitulo.Controls.Add(this.pxslide);
            this.BarraTitulo.Controls.Add(this.pbxrestaurar);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(250, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1050, 50);
            this.BarraTitulo.TabIndex = 4;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            // 
            // lblfecha
            // 
            this.lblfecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblfecha.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfecha.Appearance.Options.UseFont = true;
            this.lblfecha.Location = new System.Drawing.Point(373, 28);
            this.lblfecha.Name = "lblfecha";
            this.lblfecha.Size = new System.Drawing.Size(211, 19);
            this.lblfecha.TabIndex = 1;
            this.lblfecha.Text = "Miercoles, 21 de Febrero de 2017";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = global::VISTA.Properties.Resources.power_black;
            this.pictureBox4.Location = new System.Drawing.Point(903, 7);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(25, 25);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // lblHora
            // 
            this.lblHora.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora.Appearance.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.Appearance.Options.UseFont = true;
            this.lblHora.Location = new System.Drawing.Point(465, 3);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(84, 27);
            this.lblHora.TabIndex = 0;
            this.lblHora.Text = "00:00:00";
            // 
            // btnMaximizarCompleto
            // 
            this.btnMaximizarCompleto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximizarCompleto.Image = global::VISTA.Properties.Resources.maximizar;
            this.btnMaximizarCompleto.Location = new System.Drawing.Point(57, 7);
            this.btnMaximizarCompleto.Name = "btnMaximizarCompleto";
            this.btnMaximizarCompleto.Size = new System.Drawing.Size(25, 25);
            this.btnMaximizarCompleto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnMaximizarCompleto.TabIndex = 5;
            this.btnMaximizarCompleto.TabStop = false;
            this.btnMaximizarCompleto.Visible = false;
            this.btnMaximizarCompleto.Click += new System.EventHandler(this.btnMaximizarCompleto_Click);
            // 
            // pbxminimizar
            // 
            this.pbxminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxminimizar.Image = global::VISTA.Properties.Resources.restar;
            this.pbxminimizar.Location = new System.Drawing.Point(956, 10);
            this.pbxminimizar.Name = "pbxminimizar";
            this.pbxminimizar.Size = new System.Drawing.Size(20, 20);
            this.pbxminimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxminimizar.TabIndex = 3;
            this.pbxminimizar.TabStop = false;
            this.pbxminimizar.Click += new System.EventHandler(this.pbxminimizar_Click);
            // 
            // pbxmaximizar
            // 
            this.pbxmaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxmaximizar.Image = global::VISTA.Properties.Resources.maximizarr;
            this.pbxmaximizar.Location = new System.Drawing.Point(982, 10);
            this.pbxmaximizar.Name = "pbxmaximizar";
            this.pbxmaximizar.Size = new System.Drawing.Size(20, 20);
            this.pbxmaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxmaximizar.TabIndex = 2;
            this.pbxmaximizar.TabStop = false;
            this.pbxmaximizar.Click += new System.EventHandler(this.pbxmaximizar_Click);
            // 
            // pbxcerrar
            // 
            this.pbxcerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxcerrar.Image = global::VISTA.Properties.Resources.cerrar;
            this.pbxcerrar.Location = new System.Drawing.Point(1008, 10);
            this.pbxcerrar.Name = "pbxcerrar";
            this.pbxcerrar.Size = new System.Drawing.Size(20, 20);
            this.pbxcerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxcerrar.TabIndex = 1;
            this.pbxcerrar.TabStop = false;
            this.pbxcerrar.Click += new System.EventHandler(this.pbxcerrar_Click);
            // 
            // pxslide
            // 
            this.pxslide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pxslide.Image = global::VISTA.Properties.Resources.menu_lienal;
            this.pxslide.Location = new System.Drawing.Point(16, 7);
            this.pxslide.Name = "pxslide";
            this.pxslide.Size = new System.Drawing.Size(35, 35);
            this.pxslide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pxslide.TabIndex = 0;
            this.pxslide.TabStop = false;
            this.pxslide.Click += new System.EventHandler(this.pxslide_Click);
            // 
            // pbxrestaurar
            // 
            this.pbxrestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxrestaurar.Image = global::VISTA.Properties.Resources.restaurar;
            this.pbxrestaurar.Location = new System.Drawing.Point(982, 10);
            this.pbxrestaurar.Name = "pbxrestaurar";
            this.pbxrestaurar.Size = new System.Drawing.Size(20, 20);
            this.pbxrestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxrestaurar.TabIndex = 4;
            this.pbxrestaurar.TabStop = false;
            this.pbxrestaurar.Click += new System.EventHandler(this.pbxrestaurar_Click);
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(162)))), ((int)(((byte)(35)))));
            this.MenuVertical.Controls.Add(this.button3);
            this.MenuVertical.Controls.Add(this.btnConfiguracion);
            this.MenuVertical.Controls.Add(this.panelUsuario);
            this.MenuVertical.Controls.Add(this.button1);
            this.MenuVertical.Controls.Add(this.btnOportunidades);
            this.MenuVertical.Controls.Add(this.pictureBox2);
            this.MenuVertical.Controls.Add(this.button2);
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 0);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(250, 650);
            this.MenuVertical.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(162)))), ((int)(((byte)(35)))));
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Image = global::VISTA.Properties.Resources.analitica;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(0, 251);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(250, 40);
            this.button3.TabIndex = 6;
            this.button3.Text = "Reporte";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // btnConfiguracion
            // 
            this.btnConfiguracion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(162)))), ((int)(((byte)(35)))));
            this.btnConfiguracion.FlatAppearance.BorderSize = 0;
            this.btnConfiguracion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnConfiguracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguracion.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfiguracion.ForeColor = System.Drawing.Color.White;
            this.btnConfiguracion.Image = global::VISTA.Properties.Resources.ajustes_con_engranajes;
            this.btnConfiguracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguracion.Location = new System.Drawing.Point(0, 292);
            this.btnConfiguracion.Name = "btnConfiguracion";
            this.btnConfiguracion.Size = new System.Drawing.Size(250, 40);
            this.btnConfiguracion.TabIndex = 5;
            this.btnConfiguracion.Text = "Configuracion";
            this.btnConfiguracion.UseVisualStyleBackColor = false;
            this.btnConfiguracion.Visible = false;
            this.btnConfiguracion.Click += new System.EventHandler(this.btmConfiguracion_Click);
            // 
            // panelUsuario
            // 
            this.panelUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelUsuario.Controls.Add(this.lblLinea);
            this.panelUsuario.Controls.Add(this.lblNombres);
            this.panelUsuario.Controls.Add(this.lblApellidos);
            this.panelUsuario.Controls.Add(this.pictureBox3);
            this.panelUsuario.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUsuario.Location = new System.Drawing.Point(0, 576);
            this.panelUsuario.Name = "panelUsuario";
            this.panelUsuario.Size = new System.Drawing.Size(250, 74);
            this.panelUsuario.TabIndex = 1;
            // 
            // lblLinea
            // 
            this.lblLinea.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinea.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblLinea.Appearance.Options.UseFont = true;
            this.lblLinea.Appearance.Options.UseForeColor = true;
            this.lblLinea.Location = new System.Drawing.Point(58, 49);
            this.lblLinea.Name = "lblLinea";
            this.lblLinea.Size = new System.Drawing.Size(177, 17);
            this.lblLinea.TabIndex = 3;
            this.lblLinea.Text = "SUPERVISOR TRAXPARK";
            // 
            // lblNombres
            // 
            this.lblNombres.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombres.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblNombres.Appearance.Options.UseFont = true;
            this.lblNombres.Appearance.Options.UseForeColor = true;
            this.lblNombres.Location = new System.Drawing.Point(58, 24);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Size = new System.Drawing.Size(98, 19);
            this.lblNombres.TabIndex = 2;
            this.lblNombres.Text = "Nombres Name";
            // 
            // lblApellidos
            // 
            this.lblApellidos.Appearance.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApellidos.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblApellidos.Appearance.Options.UseFont = true;
            this.lblApellidos.Appearance.Options.UseForeColor = true;
            this.lblApellidos.Location = new System.Drawing.Point(58, 3);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(132, 19);
            this.lblApellidos.TabIndex = 1;
            this.lblApellidos.Text = "Apellidos Lastname";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::VISTA.Properties.Resources.silueta_de_usuario;
            this.pictureBox3.Location = new System.Drawing.Point(3, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(49, 66);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 1;
            this.pictureBox3.TabStop = false;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::VISTA.Properties.Resources.user;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 124);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(250, 40);
            this.button1.TabIndex = 1;
            this.button1.Text = "USUARIOS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOportunidades
            // 
            this.btnOportunidades.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(162)))), ((int)(((byte)(35)))));
            this.btnOportunidades.FlatAppearance.BorderSize = 0;
            this.btnOportunidades.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btnOportunidades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOportunidades.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOportunidades.ForeColor = System.Drawing.Color.White;
            this.btnOportunidades.Image = global::VISTA.Properties.Resources.Oportunidad2;
            this.btnOportunidades.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOportunidades.Location = new System.Drawing.Point(0, 170);
            this.btnOportunidades.Name = "btnOportunidades";
            this.btnOportunidades.Size = new System.Drawing.Size(250, 40);
            this.btnOportunidades.TabIndex = 0;
            this.btnOportunidades.Text = "OPORTUNIDADES";
            this.btnOportunidades.UseVisualStyleBackColor = false;
            this.btnOportunidades.Click += new System.EventHandler(this.btnOportunidades_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(250, 106);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(162)))), ((int)(((byte)(35)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::VISTA.Properties.Resources.Oportunidad1;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(0, 210);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(250, 40);
            this.button2.TabIndex = 4;
            this.button2.Text = "NEGOCIACIONES";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.panelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(700, 450);
            this.Name = "Principal";
            this.ShowMdiChildCaptionInParentTitle = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.panelPrincipal.ResumeLayout(false);
            this.PanelContenedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.BarraTitulo.ResumeLayout(false);
            this.BarraTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximizarCompleto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxminimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxmaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxcerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxslide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxrestaurar)).EndInit();
            this.MenuVertical.ResumeLayout(false);
            this.panelUsuario.ResumeLayout(false);
            this.panelUsuario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.PictureBox pbxminimizar;
        private System.Windows.Forms.PictureBox pbxmaximizar;
        private System.Windows.Forms.PictureBox pbxcerrar;
        private System.Windows.Forms.PictureBox pxslide;
        private System.Windows.Forms.PictureBox pbxrestaurar;
        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnOportunidades;
        private DevExpress.XtraEditors.LabelControl lblfecha;
        private DevExpress.XtraEditors.LabelControl lblHora;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox btnMaximizarCompleto;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panelUsuario;
        private DevExpress.XtraEditors.LabelControl lblApellidos;
        private System.Windows.Forms.PictureBox pictureBox3;
        private DevExpress.XtraEditors.LabelControl lblLinea;
        private DevExpress.XtraEditors.LabelControl lblNombres;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private System.Windows.Forms.Button btnConfiguracion;
        public System.Windows.Forms.Panel PanelContenedor;
        private System.Windows.Forms.Button button3;
    }
}