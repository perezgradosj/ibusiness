﻿namespace VISTA.Reportes
{
    partial class rptNegociaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblfalta = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblcuota_mensual = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_pedidas_soles = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_perdidas_dolares = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_total_perdidas = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_ganadas_soles = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_ganadas_dolares = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_total_ganadas = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_pendientes_soles = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_pendientes_dolares = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_total_pendientes = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_total_cotizaciones = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_cotizaciones_dolares = new DevExpress.XtraReports.UI.XRLabel();
            this.lblvalor_cotizaciones_soles = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblFechaHasta = new DevExpress.XtraReports.UI.XRLabel();
            this.lblFechaDesde = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTitulo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblmonto_facturado_soles = new DevExpress.XtraReports.UI.XRLabel();
            this.lblmonto_facturado_dolares = new DevExpress.XtraReports.UI.XRLabel();
            this.lblmonto_total_facturado = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 312.5F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.HeightF = 18.75F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine3,
            this.xrLabel32,
            this.lblmonto_facturado_soles,
            this.lblmonto_facturado_dolares,
            this.lblmonto_total_facturado,
            this.xrLabel28,
            this.lblfalta,
            this.xrLine2,
            this.lblcuota_mensual,
            this.xrLabel27,
            this.xrLabel20,
            this.lblvalor_pedidas_soles,
            this.lblvalor_perdidas_dolares,
            this.lblvalor_total_perdidas,
            this.xrLabel16,
            this.lblvalor_ganadas_soles,
            this.lblvalor_ganadas_dolares,
            this.lblvalor_total_ganadas,
            this.xrLabel12,
            this.lblvalor_pendientes_soles,
            this.lblvalor_pendientes_dolares,
            this.lblvalor_total_pendientes,
            this.xrLine1,
            this.xrLabel10,
            this.lblvalor_total_cotizaciones,
            this.lblvalor_cotizaciones_dolares,
            this.lblvalor_cotizaciones_soles,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel3,
            this.lblFechaHasta,
            this.lblFechaDesde,
            this.xrLabel2,
            this.xrLabel1,
            this.lblTitulo});
            this.PageHeader.HeightF = 440.7852F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel28
            // 
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(0F, 319.8781F);
            this.xrLabel28.Multiline = true;
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Falta";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblfalta
            // 
            this.lblfalta.LocationFloat = new DevExpress.Utils.PointFloat(550.8013F, 319.8781F);
            this.lblfalta.Multiline = true;
            this.lblfalta.Name = "lblfalta";
            this.lblfalta.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblfalta.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblfalta.StylePriority.UseTextAlignment = false;
            this.lblfalta.Text = "S/ 1000\r\n. 1000";
            this.lblfalta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 296.8782F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(700.8015F, 23F);
            // 
            // lblcuota_mensual
            // 
            this.lblcuota_mensual.LocationFloat = new DevExpress.Utils.PointFloat(550.8014F, 273.8782F);
            this.lblcuota_mensual.Multiline = true;
            this.lblcuota_mensual.Name = "lblcuota_mensual";
            this.lblcuota_mensual.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblcuota_mensual.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblcuota_mensual.StylePriority.UseTextAlignment = false;
            this.lblcuota_mensual.Text = "S/ 1000\r\n. 1000";
            this.lblcuota_mensual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 273.8782F);
            this.xrLabel27.Multiline = true;
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Cuota Mensual";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 203.4616F);
            this.xrLabel20.Multiline = true;
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Negociaciones Perdidas";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblvalor_pedidas_soles
            // 
            this.lblvalor_pedidas_soles.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 203.4616F);
            this.lblvalor_pedidas_soles.Multiline = true;
            this.lblvalor_pedidas_soles.Name = "lblvalor_pedidas_soles";
            this.lblvalor_pedidas_soles.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_pedidas_soles.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_pedidas_soles.StylePriority.UseTextAlignment = false;
            this.lblvalor_pedidas_soles.Text = "S/. 1000";
            this.lblvalor_pedidas_soles.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_perdidas_dolares
            // 
            this.lblvalor_perdidas_dolares.LocationFloat = new DevExpress.Utils.PointFloat(375.1765F, 203.4616F);
            this.lblvalor_perdidas_dolares.Multiline = true;
            this.lblvalor_perdidas_dolares.Name = "lblvalor_perdidas_dolares";
            this.lblvalor_perdidas_dolares.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_perdidas_dolares.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_perdidas_dolares.StylePriority.UseTextAlignment = false;
            this.lblvalor_perdidas_dolares.Text = "$ 1000\r\n. 1000";
            this.lblvalor_perdidas_dolares.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_total_perdidas
            // 
            this.lblvalor_total_perdidas.LocationFloat = new DevExpress.Utils.PointFloat(550.8015F, 203.4616F);
            this.lblvalor_total_perdidas.Multiline = true;
            this.lblvalor_total_perdidas.Name = "lblvalor_total_perdidas";
            this.lblvalor_total_perdidas.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_total_perdidas.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_total_perdidas.StylePriority.UseTextAlignment = false;
            this.lblvalor_total_perdidas.Text = "S/ 1000\r\n. 1000";
            this.lblvalor_total_perdidas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 180.4616F);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Negociaciones Ganadas";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblvalor_ganadas_soles
            // 
            this.lblvalor_ganadas_soles.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 180.4616F);
            this.lblvalor_ganadas_soles.Multiline = true;
            this.lblvalor_ganadas_soles.Name = "lblvalor_ganadas_soles";
            this.lblvalor_ganadas_soles.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_ganadas_soles.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_ganadas_soles.StylePriority.UseTextAlignment = false;
            this.lblvalor_ganadas_soles.Text = "S/. 1000";
            this.lblvalor_ganadas_soles.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_ganadas_dolares
            // 
            this.lblvalor_ganadas_dolares.LocationFloat = new DevExpress.Utils.PointFloat(375.1765F, 180.4616F);
            this.lblvalor_ganadas_dolares.Multiline = true;
            this.lblvalor_ganadas_dolares.Name = "lblvalor_ganadas_dolares";
            this.lblvalor_ganadas_dolares.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_ganadas_dolares.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_ganadas_dolares.StylePriority.UseTextAlignment = false;
            this.lblvalor_ganadas_dolares.Text = "$ 1000\r\n. 1000";
            this.lblvalor_ganadas_dolares.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_total_ganadas
            // 
            this.lblvalor_total_ganadas.LocationFloat = new DevExpress.Utils.PointFloat(550.8015F, 180.4616F);
            this.lblvalor_total_ganadas.Multiline = true;
            this.lblvalor_total_ganadas.Name = "lblvalor_total_ganadas";
            this.lblvalor_total_ganadas.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_total_ganadas.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_total_ganadas.StylePriority.UseTextAlignment = false;
            this.lblvalor_total_ganadas.Text = "S/ 1000\r\n. 1000";
            this.lblvalor_total_ganadas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 157.4615F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Negociaciones Pendientes";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblvalor_pendientes_soles
            // 
            this.lblvalor_pendientes_soles.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 157.4615F);
            this.lblvalor_pendientes_soles.Multiline = true;
            this.lblvalor_pendientes_soles.Name = "lblvalor_pendientes_soles";
            this.lblvalor_pendientes_soles.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_pendientes_soles.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_pendientes_soles.StylePriority.UseTextAlignment = false;
            this.lblvalor_pendientes_soles.Text = "S/. 1000";
            this.lblvalor_pendientes_soles.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_pendientes_dolares
            // 
            this.lblvalor_pendientes_dolares.LocationFloat = new DevExpress.Utils.PointFloat(375.1765F, 157.4615F);
            this.lblvalor_pendientes_dolares.Multiline = true;
            this.lblvalor_pendientes_dolares.Name = "lblvalor_pendientes_dolares";
            this.lblvalor_pendientes_dolares.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_pendientes_dolares.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_pendientes_dolares.StylePriority.UseTextAlignment = false;
            this.lblvalor_pendientes_dolares.Text = "$ 1000\r\n. 1000";
            this.lblvalor_pendientes_dolares.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_total_pendientes
            // 
            this.lblvalor_total_pendientes.LocationFloat = new DevExpress.Utils.PointFloat(550.8015F, 157.4615F);
            this.lblvalor_total_pendientes.Multiline = true;
            this.lblvalor_total_pendientes.Name = "lblvalor_total_pendientes";
            this.lblvalor_total_pendientes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_total_pendientes.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_total_pendientes.StylePriority.UseTextAlignment = false;
            this.lblvalor_total_pendientes.Text = "S/ 1000\r\n. 1000";
            this.lblvalor_total_pendientes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 134.4615F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(700.8015F, 23F);
            // 
            // xrLabel10
            // 
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(550.8015F, 88.46152F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "S/ Total";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_total_cotizaciones
            // 
            this.lblvalor_total_cotizaciones.LocationFloat = new DevExpress.Utils.PointFloat(550.8015F, 111.4615F);
            this.lblvalor_total_cotizaciones.Multiline = true;
            this.lblvalor_total_cotizaciones.Name = "lblvalor_total_cotizaciones";
            this.lblvalor_total_cotizaciones.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_total_cotizaciones.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_total_cotizaciones.StylePriority.UseTextAlignment = false;
            this.lblvalor_total_cotizaciones.Text = "S/ 1000\r\n. 1000";
            this.lblvalor_total_cotizaciones.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_cotizaciones_dolares
            // 
            this.lblvalor_cotizaciones_dolares.LocationFloat = new DevExpress.Utils.PointFloat(375.1765F, 111.4615F);
            this.lblvalor_cotizaciones_dolares.Multiline = true;
            this.lblvalor_cotizaciones_dolares.Name = "lblvalor_cotizaciones_dolares";
            this.lblvalor_cotizaciones_dolares.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_cotizaciones_dolares.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_cotizaciones_dolares.StylePriority.UseTextAlignment = false;
            this.lblvalor_cotizaciones_dolares.Text = "$ 1000\r\n. 1000";
            this.lblvalor_cotizaciones_dolares.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblvalor_cotizaciones_soles
            // 
            this.lblvalor_cotizaciones_soles.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 111.4615F);
            this.lblvalor_cotizaciones_soles.Multiline = true;
            this.lblvalor_cotizaciones_soles.Name = "lblvalor_cotizaciones_soles";
            this.lblvalor_cotizaciones_soles.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblvalor_cotizaciones_soles.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblvalor_cotizaciones_soles.StylePriority.UseTextAlignment = false;
            this.lblvalor_cotizaciones_soles.Text = "S/. 1000";
            this.lblvalor_cotizaciones_soles.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(129.1667F, 23F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Fechas:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(375.1765F, 88.46152F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "$";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 88.46152F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "S/.";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(318.7181F, 50F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(36.69879F, 23F);
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Hasta";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblFechaHasta
            // 
            this.lblFechaHasta.LocationFloat = new DevExpress.Utils.PointFloat(355.417F, 50F);
            this.lblFechaHasta.Multiline = true;
            this.lblFechaHasta.Name = "lblFechaHasta";
            this.lblFechaHasta.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblFechaHasta.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.lblFechaHasta.StylePriority.UseTextAlignment = false;
            this.lblFechaHasta.Text = "18/07/97";
            this.lblFechaHasta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblFechaDesde
            // 
            this.lblFechaDesde.LocationFloat = new DevExpress.Utils.PointFloat(226.186F, 50F);
            this.lblFechaDesde.Multiline = true;
            this.lblFechaDesde.Name = "lblFechaDesde";
            this.lblFechaDesde.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblFechaDesde.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.lblFechaDesde.StylePriority.UseTextAlignment = false;
            this.lblFechaDesde.Text = "18/07/20019";
            this.lblFechaDesde.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(199.904F, 50F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(26.28204F, 23F);
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "De:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 111.4615F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(129.1667F, 23F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Valor de Cotizaciones";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTitulo
            // 
            this.lblTitulo.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.lblTitulo.Multiline = true;
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTitulo.SizeF = new System.Drawing.SizeF(969F, 23F);
            this.lblTitulo.StylePriority.UseFont = false;
            this.lblTitulo.StylePriority.UseTextAlignment = false;
            this.lblTitulo.Text = "NEGOCIACIONES";
            this.lblTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(0F, 250.8782F);
            this.xrLabel32.Multiline = true;
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "Monto Facturado";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblmonto_facturado_soles
            // 
            this.lblmonto_facturado_soles.LocationFloat = new DevExpress.Utils.PointFloat(199.9039F, 250.8782F);
            this.lblmonto_facturado_soles.Multiline = true;
            this.lblmonto_facturado_soles.Name = "lblmonto_facturado_soles";
            this.lblmonto_facturado_soles.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblmonto_facturado_soles.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblmonto_facturado_soles.StylePriority.UseTextAlignment = false;
            this.lblmonto_facturado_soles.Text = "S/. 1000";
            this.lblmonto_facturado_soles.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblmonto_facturado_dolares
            // 
            this.lblmonto_facturado_dolares.LocationFloat = new DevExpress.Utils.PointFloat(375.1764F, 250.8782F);
            this.lblmonto_facturado_dolares.Multiline = true;
            this.lblmonto_facturado_dolares.Name = "lblmonto_facturado_dolares";
            this.lblmonto_facturado_dolares.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblmonto_facturado_dolares.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblmonto_facturado_dolares.StylePriority.UseTextAlignment = false;
            this.lblmonto_facturado_dolares.Text = "$ 1000\r\n. 1000";
            this.lblmonto_facturado_dolares.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblmonto_total_facturado
            // 
            this.lblmonto_total_facturado.LocationFloat = new DevExpress.Utils.PointFloat(550.8013F, 250.8782F);
            this.lblmonto_total_facturado.Multiline = true;
            this.lblmonto_total_facturado.Name = "lblmonto_total_facturado";
            this.lblmonto_total_facturado.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblmonto_total_facturado.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblmonto_total_facturado.StylePriority.UseTextAlignment = false;
            this.lblmonto_total_facturado.Text = "S/ 1000\r\n. 1000";
            this.lblmonto_total_facturado.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 226.4616F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(700.8015F, 23F);
            // 
            // rptNegociaciones
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.PageHeader});
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4Rotated;
            this.Version = "18.1";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblFechaHasta;
        private DevExpress.XtraReports.UI.XRLabel lblFechaDesde;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblTitulo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_pedidas_soles;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_perdidas_dolares;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_total_perdidas;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_ganadas_soles;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_ganadas_dolares;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_total_ganadas;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_pendientes_soles;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_pendientes_dolares;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_total_pendientes;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_total_cotizaciones;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_cotizaciones_dolares;
        private DevExpress.XtraReports.UI.XRLabel lblvalor_cotizaciones_soles;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblfalta;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblcuota_mensual;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel lblmonto_facturado_soles;
        private DevExpress.XtraReports.UI.XRLabel lblmonto_facturado_dolares;
        private DevExpress.XtraReports.UI.XRLabel lblmonto_total_facturado;
    }
}
