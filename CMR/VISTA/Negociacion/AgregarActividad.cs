﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;
using MODELO;

namespace VISTA.Negociacion
{
    public partial class AgregarActividad : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public int IdUsuario = 0;
        public int IdNegociacion = 0;
        public int IdActividad = 0;
        public int Estado = 0;
        public int IdActividadAnterior = 0;
        public int EstadoNegociacion = 0;

        //Variable que nos indica si vamos a insertar un nuevo producto
        public bool VarNuevo = true;
        //Variable que nos indica si vamos a modificar un producto
        public bool VarEditar = false;

        string TActividad;
        public int Entrada = 0;

        public AgregarActividad()
        {
            InitializeComponent();
        }

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        
        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void CargarTipoComunicaciones()
        {
            DataTable dt = MActividad.Mostrar("", 0, 0, 2);
            
            lueTActividad.Properties.ValueMember = "id";
            lueTActividad.Properties.DisplayMember = "nombre";

            lueTActividad.Properties.DataSource = dt;

            
        }

        public void setLookUpEdit(string tactividad)
        {
            this.TActividad = tactividad;
            //Botones();
        }

        private void AgregarActividad_Load(object sender, EventArgs e)
        {
            CargarTipoComunicaciones();

            if (this.VarEditar)
            {
                chkActCompletada.Visible = true;
                if (Estado == 1)
                {
                    chkActCompletada.CheckState = CheckState.Checked;
                }

                lueTActividad.EditValue = lueTActividad.Properties.GetKeyValueByDisplayText(TActividad);
            }
            if (Estado == 1)
            {
                btnRegistrar.Enabled = false;
            }

            if (this.EstadoNegociacion == 0)
            {
                btnRegistrar.Enabled = true;
            }

            deFecha.Text = DateTime.Today.ToLongDateString().ToString();
            teHora.Time = Convert.ToDateTime(DateTime.Now.ToLongTimeString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var d = Convert.ToDateTime(deFecha.Text);
        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            string Rpta = "";
            try
            {
                if (lueTActividad.Text == "[Vacío]" | deFecha.Text == "" | txtAsunto.Text == "" )
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(lueTActividad, "Ingrese un Valor");
                    errorIcono.SetError(deFecha, "Ingrese un Valor");
                    errorIcono.SetError(txtAsunto, "Ingrese un Valor");
                }
                else
                {
                    if (this.VarNuevo)
                    {
                        Rpta = MActividad.MantActividad(0,
                                                 Convert.ToInt32(lueTActividad.GetColumnValue("id")),
                                                 txtAsunto.Text,
                                                 txtDescripcion.Text,
                                                 Convert.ToDateTime(deFecha.Text),
                                                 0,
                                                 Convert.ToDateTime(teHora.Text),
                                                 IdUsuario,
                                                 0,
                                                 IdNegociacion,
                                                 1);

                        //if (IdActividadAnterior != 0)
                        //{
                        //    Rpta = MActividad.MantActividad(IdActividadAnterior,
                        //                         Convert.ToInt32(lueTActividad.GetColumnValue("id")),
                        //                         txtAsunto.Text,
                        //                         txtDescripcion.Text,
                        //                         Convert.ToDateTime(deFecha.Text),
                        //                         0,
                        //                         Convert.ToDateTime(teHora.Text),
                        //                         IdUsuario,
                        //                         1,
                        //                         IdNegociacion,
                        //                         5);
                        //}
                        

                    }
                    else
                    {
                        if (chkActCompletada.CheckState == CheckState.Checked)
                        {
                            Estado = 1;
                        }
                        else
                        {
                            Estado = 0;
                        }

                        Rpta = MActividad.MantActividad(IdActividad,
                                                 Convert.ToInt32(lueTActividad.GetColumnValue("id")),
                                                 txtAsunto.Text,
                                                 txtDescripcion.Text,
                                                 Convert.ToDateTime(deFecha.Text),
                                                 0,
                                                 Convert.ToDateTime(teHora.Text),
                                                 IdUsuario,
                                                 Estado,
                                                 IdNegociacion,
                                                 2);
                    }

                    //if (Convert.ToInt32(Rpta) > 0)
                    //{
                        if (this.VarNuevo)
                        {
                            this.MensajeOk("Se Insertó de forma correcta el registro");
                            this.VarNuevo = false;
                            this.VarEditar = true;
                            this.errorIcono.ClearErrors();
                        }
                        else
                        {
                            this.MensajeOk("Se Actualizó de forma correcta el registro");
                            this.errorIcono.ClearErrors();
                        }
                    //}
                    //else
                    //{
                    //    this.MensajeError("Error al registrar los datos");
                    //}
                    if (Entrada == 0)
                    {
                        ListActividades list = Owner as ListActividades;
                        //listOportunidades.MostrarOportunidades("", IdUsuario, 1, 0, IdLinea);
                        list.setActualizar();
                    }
                    

                    if (Entrada == 1)
                    {
                        LisNegociaciones lisNegociaciones = Owner as LisNegociaciones;
                        lisNegociaciones.setActualizar();
                    }

                    this.Close();

                }
            }
            catch (Exception ex)
            {

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}