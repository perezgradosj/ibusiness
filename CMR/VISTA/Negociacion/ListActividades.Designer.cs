﻿namespace VISTA.Negociacion
{
    partial class ListActividades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gcActividades = new DevExpress.XtraGrid.GridControl();
            this.gvActividades = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdTipoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAsunto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDescripcion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFecha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdContacto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcHora = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdNegociacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAgregar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCerrar = new DevExpress.XtraEditors.SimpleButton();
            this.gcEst = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcActividades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActividades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.panelControl2);
            this.groupControl1.Controls.Add(this.panelControl1);
            this.groupControl1.Controls.Add(this.btnCerrar);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(476, 441);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Actividades";
            this.groupControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.groupControl1_MouseDown);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gcActividades);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(2, 69);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(472, 370);
            this.panelControl2.TabIndex = 13;
            // 
            // gcActividades
            // 
            this.gcActividades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcActividades.Location = new System.Drawing.Point(2, 2);
            this.gcActividades.MainView = this.gvActividades;
            this.gcActividades.Name = "gcActividades";
            this.gcActividades.Size = new System.Drawing.Size(468, 366);
            this.gcActividades.TabIndex = 11;
            this.gcActividades.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvActividades});
            this.gcActividades.DoubleClick += new System.EventHandler(this.gcActividades_DoubleClick);
            // 
            // gvActividades
            // 
            this.gvActividades.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcId,
            this.gcIdTipoActividad,
            this.gcTActividad,
            this.gcAsunto,
            this.gcDescripcion,
            this.gcFecha,
            this.gcIdContacto,
            this.gcHora,
            this.gcIdUsuario,
            this.gcEstado,
            this.gcIdNegociacion,
            this.gcUsuario,
            this.gcEst});
            this.gvActividades.GridControl = this.gcActividades;
            this.gvActividades.Name = "gvActividades";
            this.gvActividades.OptionsBehavior.Editable = false;
            this.gvActividades.OptionsView.ShowGroupPanel = false;
            this.gvActividades.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvActividades_RowCellStyle);
            // 
            // gcId
            // 
            this.gcId.Caption = "Id";
            this.gcId.FieldName = "id";
            this.gcId.Name = "gcId";
            this.gcId.Visible = true;
            this.gcId.VisibleIndex = 0;
            // 
            // gcIdTipoActividad
            // 
            this.gcIdTipoActividad.Caption = "Id Tipo Actividad";
            this.gcIdTipoActividad.FieldName = "idtipo_actividad";
            this.gcIdTipoActividad.Name = "gcIdTipoActividad";
            this.gcIdTipoActividad.Visible = true;
            this.gcIdTipoActividad.VisibleIndex = 1;
            // 
            // gcTActividad
            // 
            this.gcTActividad.Caption = "Tipo Actividad";
            this.gcTActividad.FieldName = "TActividad";
            this.gcTActividad.Name = "gcTActividad";
            this.gcTActividad.Visible = true;
            this.gcTActividad.VisibleIndex = 2;
            // 
            // gcAsunto
            // 
            this.gcAsunto.Caption = "Asunto";
            this.gcAsunto.FieldName = "asunto";
            this.gcAsunto.Name = "gcAsunto";
            this.gcAsunto.Visible = true;
            this.gcAsunto.VisibleIndex = 3;
            // 
            // gcDescripcion
            // 
            this.gcDescripcion.Caption = "Descripcion";
            this.gcDescripcion.FieldName = "descripcion";
            this.gcDescripcion.Name = "gcDescripcion";
            this.gcDescripcion.Visible = true;
            this.gcDescripcion.VisibleIndex = 4;
            // 
            // gcFecha
            // 
            this.gcFecha.Caption = "Fecha";
            this.gcFecha.FieldName = "fecha";
            this.gcFecha.Name = "gcFecha";
            this.gcFecha.Visible = true;
            this.gcFecha.VisibleIndex = 5;
            // 
            // gcIdContacto
            // 
            this.gcIdContacto.Caption = "Id Contacto";
            this.gcIdContacto.FieldName = "idcontacto";
            this.gcIdContacto.Name = "gcIdContacto";
            this.gcIdContacto.Visible = true;
            this.gcIdContacto.VisibleIndex = 6;
            // 
            // gcHora
            // 
            this.gcHora.Caption = "Hora";
            this.gcHora.FieldName = "hora";
            this.gcHora.Name = "gcHora";
            this.gcHora.Visible = true;
            this.gcHora.VisibleIndex = 7;
            // 
            // gcIdUsuario
            // 
            this.gcIdUsuario.Caption = "Id Usuario";
            this.gcIdUsuario.FieldName = "idusuario";
            this.gcIdUsuario.Name = "gcIdUsuario";
            this.gcIdUsuario.Visible = true;
            this.gcIdUsuario.VisibleIndex = 8;
            // 
            // gcEstado
            // 
            this.gcEstado.Caption = "Estado";
            this.gcEstado.FieldName = "estado";
            this.gcEstado.Name = "gcEstado";
            this.gcEstado.Visible = true;
            this.gcEstado.VisibleIndex = 9;
            // 
            // gcIdNegociacion
            // 
            this.gcIdNegociacion.Caption = "Id Negociacion";
            this.gcIdNegociacion.FieldName = "idnegociacion";
            this.gcIdNegociacion.Name = "gcIdNegociacion";
            this.gcIdNegociacion.Visible = true;
            this.gcIdNegociacion.VisibleIndex = 10;
            // 
            // gcUsuario
            // 
            this.gcUsuario.Caption = "Usuario";
            this.gcUsuario.FieldName = "usuario";
            this.gcUsuario.Name = "gcUsuario";
            this.gcUsuario.Visible = true;
            this.gcUsuario.VisibleIndex = 11;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnActualizar);
            this.panelControl1.Controls.Add(this.btnAgregar);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(2, 23);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(472, 46);
            this.panelControl1.TabIndex = 12;
            // 
            // btnActualizar
            // 
            this.btnActualizar.Enabled = false;
            this.btnActualizar.ImageOptions.Image = global::VISTA.Properties.Resources.refresh_32x32;
            this.btnActualizar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnActualizar.Location = new System.Drawing.Point(48, 5);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(32, 32);
            this.btnActualizar.TabIndex = 12;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Enabled = false;
            this.btnAgregar.ImageOptions.Image = global::VISTA.Properties.Resources.add_32x32;
            this.btnAgregar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAgregar.Location = new System.Drawing.Point(10, 5);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(32, 32);
            this.btnAgregar.TabIndex = 11;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.ImageOptions.Image = global::VISTA.Properties.Resources.close_16x16;
            this.btnCerrar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnCerrar.Location = new System.Drawing.Point(455, 3);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(16, 16);
            this.btnCerrar.TabIndex = 10;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // gcEst
            // 
            this.gcEst.Caption = "Estado";
            this.gcEst.FieldName = "est";
            this.gcEst.Name = "gcEst";
            this.gcEst.Visible = true;
            this.gcEst.VisibleIndex = 12;
            // 
            // ListActividades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 441);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ListActividades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListActividades";
            this.Load += new System.EventHandler(this.ListActividades_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcActividades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvActividades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnCerrar;
        private DevExpress.XtraGrid.GridControl gcActividades;
        private DevExpress.XtraGrid.Views.Grid.GridView gvActividades;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnAgregar;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraGrid.Columns.GridColumn gcId;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdTipoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcTActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcAsunto;
        private DevExpress.XtraGrid.Columns.GridColumn gcDescripcion;
        private DevExpress.XtraGrid.Columns.GridColumn gcFecha;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdContacto;
        private DevExpress.XtraGrid.Columns.GridColumn gcHora;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn gcEstado;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdNegociacion;
        private DevExpress.XtraGrid.Columns.GridColumn gcUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn gcEst;
    }
}