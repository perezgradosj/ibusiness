﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors.Repository;

namespace VISTA.Negociacion
{
    public partial class LisNegociaciones : DevExpress.XtraEditors.XtraForm
    {
        int etapa = 1;

        DataTable dtPendientes;
        DataTable dtGanadas;
        DataTable dtPerdidas;

        DataTable dtFiltroPendientes;
        DataTable dtFiltroCerrados;
        DataTable dtFiltroNoUtil;

        public int IdUsuario = 0;
        public int SuperLinea = 0;
        public int IdLinea = 0;

        /////////////////
        private int PaginaMaximaPendientes = 1;
        private int PaginaMaximaConvertidos = 1;
        private int PaginaMaximaNoUtil = 1;
        bool Pendiente = false;
        bool Convertidos = false;
        bool NoUtil = false;

        public LisNegociaciones()
        {
            InitializeComponent();
        }

        public void SetDatosUsuario(int id, int superlinea, int idlinea)
        {
            this.IdUsuario = id;
            this.SuperLinea = superlinea;
            this.IdLinea = idlinea;
        }

        private void OcultarColumnas()
        {
            //gvNegociaciones.Columns[0].Visible = false;
            ////gvNegociaciones.Columns[5].Visible = false;
            //gvNegociaciones.Columns[6].Visible = false;
            //gvNegociaciones.Columns[7].Visible = false;
            //gvNegociaciones.Columns[9].Visible = false;
            //gvNegociaciones.Columns[11].Visible = false;
            //gvNegociaciones.Columns[13].Visible = false;
            //gvNegociaciones.Columns[14].Visible = false;
            //gvNegociaciones.Columns[15].Visible = false;
            //gvNegociaciones.Columns[16].Visible = false;
            //gvNegociaciones.Columns[17].Visible = false;
            //gvNegociaciones.Columns[18].Visible = false;
            //gvNegociaciones.Columns[19].Visible = false;
            //gvNegociaciones.Columns[20].Visible = false;
            //gvNegociaciones.Columns[21].Visible = false;

            //gvNegociaciones.Columns[1].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[2].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[3].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[4].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[8].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[10].OptionsColumn.AllowEdit = false;
            //gvNegociaciones.Columns[12].OptionsColumn.AllowEdit = false;
        }

        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnConvertir_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Caption == "1")
            {
                MessageBox.Show("1");
            }
            if (e.Button.Caption == "2")
            {
                MessageBox.Show("2");
            }
            
        }
        public void setActualizar()
        {
            if (this.SuperLinea == 0)
            {
                if (this.Pendiente)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                    //////////MARCAS//
                    marcaPendiente.Visible = true;
                    marcaGanadas.Visible = false;
                    MarcaPerdidas.Visible = false;
                    //////////
                }

                if (this.Convertidos)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaGanadas.Visible = true;
                    MarcaPerdidas.Visible = false;
                    //////////
                }

                if (this.NoUtil)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaGanadas.Visible = false;
                    MarcaPerdidas.Visible = true;
                    //////////
                }

                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
            }
            else if (this.SuperLinea == 1 || SuperLinea == 3)
            {
                if (this.Pendiente)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                    //////////MARCAS//
                    marcaPendiente.Visible = true;
                    marcaGanadas.Visible = false;
                    MarcaPerdidas.Visible = false;
                    //////////
                }

                if (this.Convertidos)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaGanadas.Visible = true;
                    MarcaPerdidas.Visible = false;
                    //////////
                }

                if (this.NoUtil)
                {
                    gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaGanadas.Visible = false;
                    MarcaPerdidas.Visible = true;
                    //////////
                }

                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "GANADAS " + RConvertidos["GANADAS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASGANADAS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "PERDIDAS " + RNoUtil["PERDIDAS"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASPERDIDAS"].ToString());
        }

        int ValidarDecimal(string dato)
        {
            decimal d = Convert.ToDecimal(dato);
            int entero = Convert.ToInt32(d);

            if (d > entero)
            {
                entero = entero + 1;
            }

            return entero;
        }

        private void LisNegociaciones_Load(object sender, EventArgs e)
        {
            this.Pendiente = true;
            this.Convertidos = false;
            this.NoUtil = false;

            if (this.SuperLinea == 0)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
            }
            else if (this.SuperLinea == 1 || this.SuperLinea == 3)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }

            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "GANADAS " + RConvertidos["GANADAS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASGANADAS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "PERDIDAS " + RNoUtil["PERDIDAS"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASPERDIDAS"].ToString());

            lblFiltro.Text = "(PENDIENTES)";
        }

        private void gvNegociaciones_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            try
            {
                if (gvNegociaciones.GetFocusedRowCellValue("estado").ToString() == "2")
                {
                    GridView view = (GridView)sender;
                    GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                    if (hitInfo.InRow)
                    {
                        view.FocusedRowHandle = hitInfo.RowHandle;
                        cmsNegociaciones.Show(view.GridControl, e.Point);
                    }
                }
            }
            catch (Exception )
            {

            }
            
            
        }

        private void tsIAgregarActividad_Click(object sender, EventArgs e)
        {
            AgregarActividad actividad = new AgregarActividad();
            actividad.IdNegociacion = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString());
            actividad.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
            if (gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString() != "")
            {
                actividad.IdActividadAnterior = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString());
            }
            actividad.Entrada = 1;
            AddOwnedForm(actividad);
            actividad.Show();

        }

        private void gcNegociaciones_DoubleClick(object sender, EventArgs e)
        {
            try
            {   
                if (this.IdUsuario == Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString()))
                {
                    Negociacion negociacion = new Negociacion();
                    negociacion.Id = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString());
                    negociacion.semonto.Value = Convert.ToDecimal(gvNegociaciones.GetFocusedRowCellValue("monto").ToString());
                    negociacion.deFechaRegistro.Text = Convert.ToDateTime(gvNegociaciones.GetFocusedRowCellValue("fecha_registro").ToString()).ToLongDateString();
                    negociacion.deFechaConversion.Text = Convert.ToDateTime(gvNegociaciones.GetFocusedRowCellValue("fecha_conversion").ToString()).ToLongDateString();
                    negociacion.txtNombre.Text = gvNegociaciones.GetFocusedRowCellValue("nombre").ToString();
                    negociacion.txtRequerimiento.Text = gvNegociaciones.GetFocusedRowCellValue("requerimiento").ToString();
                    //negociacion.txtContacto.Text = gvNegociaciones.GetFocusedRowCellValue("contacto").ToString();
                    //negociacion.txtCorreo.Text = gvNegociaciones.GetFocusedRowCellValue("correo").ToString();
                    //negociacion.txtMovil.Text = gvNegociaciones.GetFocusedRowCellValue("movil").ToString();
                    //negociacion.txtFijo.Text = gvNegociaciones.GetFocusedRowCellValue("fijo").ToString();
                    negociacion.txtRuc.Text = gvNegociaciones.GetFocusedRowCellValue("ruc").ToString();
                    negociacion.txtRazonSocial.Text = gvNegociaciones.GetFocusedRowCellValue("razon_social").ToString();
                    negociacion.txtrubro.Text = gvNegociaciones.GetFocusedRowCellValue("rubro").ToString();
                    negociacion.seMontoFacturado.Value = Convert.ToDecimal(gvNegociaciones.GetFocusedRowCellValue("monto_facturado").ToString());
                    negociacion.rcEtapas.Rating = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("etapa").ToString());
                    negociacion.TipoMoneda = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("tipo_moneda").ToString());
                    negociacion.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
                    negociacion.setLookUpEdit(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("etapa").ToString()),
                                                 gvNegociaciones.GetFocusedRowCellValue("linea").ToString(),
                                                 gvNegociaciones.GetFocusedRowCellValue("usuario").ToString());
                    negociacion.Estado = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("estado").ToString());

                    negociacion.OpcionListContactos = 1;
                    negociacion.IdEmpresaContacto = gvNegociaciones.GetFocusedRowCellValue("idempresa").ToString();

                    //if (gvNegociaciones.GetFocusedRowCellValue("idcontacto").ToString() != "")
                    //{
                    //    negociacion.IdContacto = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idcontacto").ToString());
                    //}
                    if (gvNegociaciones.GetFocusedRowCellValue("idempresa").ToString() != "")
                    {
                        negociacion.IdEmpresa = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idempresa").ToString());
                    }

                    negociacion.VarEditar = true;
                    negociacion.VarNuevo = false;
                    negociacion.gcAutocompletarContacto.Visible = false;
                    negociacion.gcAutocompletarEmpresa.Visible = false;
                    
                    AddOwnedForm(negociacion);
                    negociacion.ShowDialog();
                }
                else if (this.SuperLinea == 1)
                {
                    Negociacion negociacion = new Negociacion();
                    negociacion.semonto.Value = Convert.ToDecimal(gvNegociaciones.GetFocusedRowCellValue("monto").ToString());
                    //regOportunidad.lueTipoComunicacion.EditValue = regOportunidad.lueTipoComunicacion.Properties.GetKeyValueByDisplayText(gvOportunidades.GetFocusedRowCellValue("tcomunicacion").ToString());
                    negociacion.deFechaRegistro.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_registro").ToString();
                    negociacion.deFechaConversion.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_conversion").ToString();
                    //regOportunidad.lueLinea.EditValue = regOportunidad.lueLinea.Properties.GetKeyValueByDisplayText(gvOportunidades.GetFocusedRowCellValue("linea").ToString());
                    //regOportunidad.lueUsuario.EditValue = regOportunidad.lueUsuario.Properties.GetKeyValueByDisplayText(gvOportunidades.GetFocusedRowCellValue("usuario").ToString());
                    negociacion.txtNombre.Text = gvNegociaciones.GetFocusedRowCellValue("nombre").ToString();
                    negociacion.txtRequerimiento.Text = gvNegociaciones.GetFocusedRowCellValue("requerimiento").ToString();
                    negociacion.txtContacto.Text = gvNegociaciones.GetFocusedRowCellValue("contacto").ToString();
                    negociacion.txtCorreo.Text = gvNegociaciones.GetFocusedRowCellValue("correo").ToString();
                    negociacion.txtMovil.Text = gvNegociaciones.GetFocusedRowCellValue("movil").ToString();
                    negociacion.txtFijo.Text = gvNegociaciones.GetFocusedRowCellValue("fijo").ToString();
                    negociacion.txtRuc.Text = gvNegociaciones.GetFocusedRowCellValue("ruc").ToString();
                    negociacion.txtRazonSocial.Text = gvNegociaciones.GetFocusedRowCellValue("razon_social").ToString();
                    negociacion.txtrubro.Text = gvNegociaciones.GetFocusedRowCellValue("rubro").ToString();
                    negociacion.setLookUpEdit(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("etapa").ToString()),
                                                 gvNegociaciones.GetFocusedRowCellValue("linea").ToString(),
                                                 gvNegociaciones.GetFocusedRowCellValue("usuario").ToString());

                    if (gvNegociaciones.GetFocusedRowCellValue("idcontacto").ToString() != "")
                    {
                        negociacion.IdContacto = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idcontacto").ToString());
                    }
                    if (gvNegociaciones.GetFocusedRowCellValue("idempresa").ToString() != "")
                    {
                        negociacion.IdEmpresa = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idempresa").ToString());

                    }
                    negociacion.VarEditar = true;
                    negociacion.VarNuevo = false;
                    negociacion.gcAutocompletarContacto.Visible = false;
                    negociacion.gcAutocompletarEmpresa.Visible = false;
                    AddOwnedForm(negociacion);
                    negociacion.ShowDialog();
                }
            }
            catch (Exception )
            {

            }
        }

        private void tsIEditarActividad_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString() == "")
                {
                    MessageBox.Show("No hay actividad por editar", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (this.IdUsuario == Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString()))
                    {
                        AgregarActividad agregarActividad = new AgregarActividad();
                        agregarActividad.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
                        agregarActividad.deFecha.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_actividad").ToString().Substring(0, 10);
                        agregarActividad.teHora.Time = Convert.ToDateTime(gvNegociaciones.GetFocusedRowCellValue("hora_actividad").ToString());//.Substring(11, 8)
                        agregarActividad.txtAsunto.Text = gvNegociaciones.GetFocusedRowCellValue("actividad").ToString();
                        agregarActividad.txtDescripcion.Text = gvNegociaciones.GetFocusedRowCellValue("descripcion_actividad").ToString();
                        agregarActividad.IdNegociacion = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idnegociacion_actividad").ToString());
                        agregarActividad.IdActividad = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString());
                        agregarActividad.setLookUpEdit(gvNegociaciones.GetFocusedRowCellValue("tipo_actividad").ToString());
                        agregarActividad.Estado = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("estado_actividad").ToString());
                        agregarActividad.VarEditar = true;
                        agregarActividad.VarNuevo = false;

                        AddOwnedForm(agregarActividad);
                        agregarActividad.ShowDialog();
                    }
                    else if (this.SuperLinea == 1)
                    {
                        AgregarActividad agregarActividad = new AgregarActividad();
                        agregarActividad.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
                        agregarActividad.deFecha.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_actividad").ToString().Substring(0,8);
                        agregarActividad.teHora.Text = gvNegociaciones.GetFocusedRowCellValue("hora_actividad").ToString();
                        agregarActividad.txtAsunto.Text = gvNegociaciones.GetFocusedRowCellValue("actividad").ToString();
                        agregarActividad.txtDescripcion.Text = gvNegociaciones.GetFocusedRowCellValue("descripcion_actividad").ToString();
                        agregarActividad.IdNegociacion = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idnegociacion_actividad").ToString());
                        agregarActividad.IdActividad = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString());
                        agregarActividad.setLookUpEdit(gvNegociaciones.GetFocusedRowCellValue("tipo_actividad").ToString());
                        agregarActividad.Estado = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("estado_actividad").ToString());
                        agregarActividad.VarEditar = true;
                        agregarActividad.VarNuevo = false;

                        AddOwnedForm(agregarActividad);
                        agregarActividad.ShowDialog();
                    }
                }
                
            }
            catch (Exception EX)
            {

                MessageBox.Show(EX.ToString());
            }
        }

        private void gvNegociaciones_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            //if (e.Column.Caption == "Actividad")
            //{
            //    e.
            //}

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            setActualizar();
        }

        private void gcNegociaciones_Click(object sender, EventArgs e)
        {

        }

        private void itemActividad_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            Point Point = Cursor.Position;

            //GridView view = (GridView)sender;
            //GridHitInfo hitInfo = view.CalcHitInfo(Point);
            //if (hitInfo.InRow)
            //{
            //    view.FocusedRowHandle = hitInfo.RowHandle;
                contextMenuStrip1.Show(Point);
            //}
        }

        private void gcNegociaciones_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void item_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            e.Button.Appearance.BackColor = Color.Blue;
        }

        private void Etapas_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (Convert.ToString(e.Button.Tag) == "1")
            {
                etapa = 1;
            }

            if (Convert.ToString(e.Button.Tag) == "2")
            {
                etapa = 2;
            }

            if (Convert.ToString(e.Button.Tag) == "3")
            {
                etapa = 3;
            }

            if (Convert.ToString(e.Button.Tag) == "4")
            {
                etapa = 4;
            }

            if (Convert.ToString(e.Button.Tag) == "5")
            {
                etapa = 5;
            }

            if (etapa == 1)
            {
                ItemEtap.Buttons[0].Appearance.BackColor = Color.Green;
            }

            if (etapa == 2)
            {
                ItemEtap.Buttons[0].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[1].Appearance.BackColor = Color.Green;
            }

            if (etapa == 3)
            {
                ItemEtap.Buttons[0].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[1].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[2].Appearance.BackColor = Color.Green;
            }

            if (etapa == 4)
            {
                ItemEtap.Buttons[0].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[1].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[2].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[3].Appearance.BackColor = Color.Green;
            }

            if (etapa == 5)
            {
                ItemEtap.Buttons[0].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[1].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[2].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[3].Appearance.BackColor = Color.Green;
                ItemEtap.Buttons[4].Appearance.BackColor = Color.Green;
            }
        }

        private void gvNegociaciones_ShowingEditor(object sender, CancelEventArgs e)
        {

        }

        private void ItemEtapas_BeforeShowToolTip(object sender, DevExpress.XtraEditors.Repository.RatingToolTipEventArgs e)
        {
            switch (Decimal.ToInt32(e.Value))
            {
                case 1:
                    e.Text = "Cotizacion";
                    break;
                case 2:
                    e.Text = "Negociacion";
                    break;
                case 3:
                    e.Text = "Pre Confirmacion";
                    break;
                case 4:
                    e.Text = "OC";
                    break;
                case 5:
                    e.Text = "Cierre";
                    break;
            }
        }

        private void ItemEtapas_ItemClick(object sender, DevExpress.XtraEditors.Repository.ItemEventArgs e)
        {
            try
            {
                string r = "";
                r = MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                               DateTime.Today,
                                               DateTime.Today,
                                               "",
                                               0,
                                               e.Index + 1,
                                               Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("tipo_moneda").ToString()),
                                               0, 0, 0, 0, 
                                               0,"", 0, 4);

                if (e.Index == 4)
                {
                    Class.MsgBoxUtil msgBoxUtil = new Class.MsgBoxUtil();
                    msgBoxUtil.HackMessageBox("GANADA", "PERDIDA", "CANCELAR");

                    DialogResult resul = MessageBox.Show("¿ Negociacion Ganada o Perdida ?", "Sistema DCGTEC", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                    if (resul == DialogResult.Cancel)
                    {
                        MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                               DateTime.Today,
                                               DateTime.Today,
                                               "",
                                               0,
                                               4,
                                               0,
                                               0, 0, 0, 0,
                                               0, "", 0, 4);
                        this.setActualizar();
                    }

                    if (resul == DialogResult.No)
                    {
                        MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                               2,
                                               5,
                                               5);
                        this.setActualizar();
                    }

                    if (resul == DialogResult.Yes)
                    {
                        if (gvNegociaciones.GetFocusedRowCellValue("monto").ToString() == "" ||
                        gvNegociaciones.GetFocusedRowCellValue("monto").ToString() == "0.0000" ||
                        gvNegociaciones.GetFocusedRowCellValue("monto_facturado").ToString() == "" ||
                        gvNegociaciones.GetFocusedRowCellValue("monto_facturado").ToString() == "0.0000")
                        {
                            MensajeError("No puedes convertir la negociacion con monto y/o monto facturado en 0");
                            MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                                       DateTime.Today,
                                                       DateTime.Today,
                                                       "",
                                                       0,
                                                       4,
                                                       0,
                                                       0, 0, 0, 0,
                                                       0, "", 0, 4);
                            this.setActualizar();
                        }
                        else
                        {
                            MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                               1,
                                               5,
                                               5);
                            this.setActualizar();
                        }
                        
                    }
                    
                    
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCerrado_Click(object sender, EventArgs e)
        {
            this.Pendiente = false;
            this.Convertidos = true;
            this.NoUtil = false;

            //////////MARCAS//
            marcaPendiente.Visible = false;
            marcaGanadas.Visible = true;
            MarcaPerdidas.Visible = false;
            //////////

            if (this.SuperLinea == 0)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
            }
            else if (this.SuperLinea == 1 || SuperLinea == 3)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "GANADAS " + RConvertidos["GANADAS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASGANADAS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "PERDIDAS " + RNoUtil["PERDIDAS"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASPERDIDAS"].ToString());

            gvNegociaciones.OptionsBehavior.Editable = false;

            lblFiltro.Text = "(GANADAS)";

            txtpaginacion.Text = "1";
        }

        private void gvNegociaciones_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView currentView = sender as GridView;
            if (e.Column.Caption == "Actividad")
            {
                e.Appearance.ForeColor = Color.Blue;
                int estadoAct = Convert.ToInt32(currentView.GetRowCellValue(e.RowHandle, "estado_actividad"));
                DateTime date = Convert.ToDateTime(currentView.GetRowCellValue(e.RowHandle, "fecha_actividad"));
                TimeSpan dias = date.Subtract(DateTime.Today);
                if (estadoAct == 0)
                {
                    if (dias.Days <= 2)
                    {
                        e.Appearance.ForeColor = Color.FromArgb(225, 228, 31);
                    }

                    if (0 >= dias.Days)
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }

                    if (dias.Days > 2)
                    {
                        e.Appearance.ForeColor = Color.Blue;
                    }
                }
                if (estadoAct == 1)
                {
                    e.Appearance.ForeColor = Color.Gray;
                }
                
            }
        }

        private void itemActividad_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString() == "")
                {
                    MessageBox.Show("No hay actividad por editar", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (this.IdUsuario == Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString()))
                    {
                        AgregarActividad agregarActividad = new AgregarActividad();
                        agregarActividad.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
                        agregarActividad.deFecha.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_actividad").ToString().Substring(0, 10);
                        agregarActividad.teHora.Time = Convert.ToDateTime(gvNegociaciones.GetFocusedRowCellValue("hora_actividad").ToString());//.Substring(11, 8)
                        agregarActividad.txtAsunto.Text = gvNegociaciones.GetFocusedRowCellValue("actividad").ToString();
                        agregarActividad.txtDescripcion.Text = gvNegociaciones.GetFocusedRowCellValue("descripcion_actividad").ToString();
                        agregarActividad.IdNegociacion = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idnegociacion_actividad").ToString());
                        agregarActividad.IdActividad = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString());
                        agregarActividad.setLookUpEdit(gvNegociaciones.GetFocusedRowCellValue("tipo_actividad").ToString());
                        agregarActividad.Estado = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("estado_actividad").ToString());
                        agregarActividad.VarEditar = true;
                        agregarActividad.VarNuevo = false;
                        agregarActividad.Entrada = 1;
                        AddOwnedForm(agregarActividad);
                        agregarActividad.ShowDialog();
                    }
                    else if (this.SuperLinea == 1)
                    {
                        AgregarActividad agregarActividad = new AgregarActividad();
                        agregarActividad.IdUsuario = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idusuario").ToString());
                        agregarActividad.deFecha.Text = gvNegociaciones.GetFocusedRowCellValue("fecha_actividad").ToString().Substring(0, 8);
                        agregarActividad.teHora.Text = gvNegociaciones.GetFocusedRowCellValue("hora_actividad").ToString();
                        agregarActividad.txtAsunto.Text = gvNegociaciones.GetFocusedRowCellValue("actividad").ToString();
                        agregarActividad.txtDescripcion.Text = gvNegociaciones.GetFocusedRowCellValue("descripcion_actividad").ToString();
                        agregarActividad.IdNegociacion = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("idnegociacion_actividad").ToString());
                        agregarActividad.IdActividad = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id_actividad").ToString());
                        agregarActividad.setLookUpEdit(gvNegociaciones.GetFocusedRowCellValue("tipo_actividad").ToString());
                        agregarActividad.Estado = Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("estado_actividad").ToString());
                        agregarActividad.VarEditar = true;
                        agregarActividad.VarNuevo = false;
                        agregarActividad.Entrada = 1;
                        AddOwnedForm(agregarActividad);
                        agregarActividad.ShowDialog();
                    }
                }

            }
            catch (Exception EX)
            {

                MessageBox.Show(EX.ToString());
            }
        }

        private void btnPendiente_Click(object sender, EventArgs e)
        {
            this.Pendiente = true;
            this.Convertidos = false;
            this.NoUtil = false;

            //////////MARCAS//
            marcaPendiente.Visible = true;
            marcaGanadas.Visible = false;
            MarcaPerdidas.Visible = false;
            //////////

            if (this.SuperLinea == 0)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
            }
            else if (this.SuperLinea == 1 || SuperLinea == 3)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "GANADAS " + RConvertidos["GANADAS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASGANADAS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "PERDIDAS " + RNoUtil["PERDIDAS"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASPERDIDAS"].ToString());

            gvNegociaciones.OptionsBehavior.Editable = true;

            lblFiltro.Text = "(PENDIENTES)";

            txtpaginacion.Text = "1";

        }

        private void btnNoUtil_Click(object sender, EventArgs e)
        {
            this.Pendiente = false;
            this.Convertidos = false;
            this.NoUtil = true;

            //////////MARCAS//
            marcaPendiente.Visible = false;
            marcaGanadas.Visible = false;
            MarcaPerdidas.Visible = true;
            //////////

            if (this.SuperLinea == 0)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
            }
            else if (this.SuperLinea == 1 || SuperLinea == 3)
            {
                gcNegociaciones.DataSource = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                OcultarColumnas();
                /////////////// Boton Pendiente ////////////
                dtPendientes = MNegociaciones.Mostrar("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton Cerrados ////////////
                dtGanadas = MNegociaciones.Mostrar("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                /////////////// Boton No Util ////////////
                dtPerdidas = MNegociaciones.Mostrar("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroPendientes = MNegociaciones.MostrarDatosDePaginacion("", 0, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroCerrados = MNegociaciones.MostrarDatosDePaginacion("", 1, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);

                dtFiltroNoUtil = MNegociaciones.MostrarDatosDePaginacion("", 2, IdUsuario, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "GANADAS " + RConvertidos["GANADAS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASGANADAS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "PERDIDAS " + RNoUtil["PERDIDAS"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASPERDIDAS"].ToString());

            //gvNegociaciones.Columns[5].OptionsColumn.AllowEdit = false;
            gvNegociaciones.OptionsBehavior.Editable = false;

            lblFiltro.Text = "(PERDIDAS)";

            txtpaginacion.Text = "1";
        }

        private void rEACTIVARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = new DialogResult();

                result = MessageBox.Show("¿Deseas reactivar la negociacion?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    MNegociaciones.MantNegociacion(Convert.ToInt32(gvNegociaciones.GetFocusedRowCellValue("id").ToString()),
                                               0,
                                               4,
                                               5);
                    this.setActualizar();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnPrimeraPagina_Click(object sender, EventArgs e)
        {
            txtpaginacion.Text = "1";
            setActualizar();
        }

        private void btnPaginaAnterior_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtpaginacion.Text) > 1)
            {
                txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) - 1);
            }
            setActualizar();
        }

        private void btnPaginaSiguiente_Click(object sender, EventArgs e)
        {
            if (this.Pendiente)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) < PaginaMaximaPendientes)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }

            if (this.Convertidos)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) <= PaginaMaximaConvertidos)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }

            if (this.NoUtil)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) <= PaginaMaximaNoUtil)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }


            setActualizar();
        }

        private void btnUltimaPagina_Click(object sender, EventArgs e)
        {
            if (this.Pendiente)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaPendientes);
            }

            if (this.Convertidos)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaConvertidos);
            }

            if (this.NoUtil)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaNoUtil);
            }
        }

        private void txtpaginacion_EditValueChanged(object sender, EventArgs e)
        {
            setActualizar();
        }

        private void txtpaginacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.setActualizar();
            }
        }

        private void cboCantRegistros_SelectedIndexChanged(object sender, EventArgs e)
        {
            setActualizar();
        }
        
    }
}