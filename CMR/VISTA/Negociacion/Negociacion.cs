﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;

namespace VISTA.Negociacion
{
    public partial class Negociacion : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        //Variable que nos indica si vamos a insertar un nuevo producto
        public bool VarNuevo = true;
        //Variable que nos indica si vamos a modificar un producto
        public bool VarEditar = false;

        ////////////////////////// llenar Look Up Edit
        int Etapa;
        string Linea, Usuario;
        //////////////////////////
        public int Id = 0;
        public int IdContacto = 0;
        public int IdEmpresa = 0;
        public int TipoMoneda = 0;
        public int IdUsuario = 0;
        public int Estado = 0;
        public int IdOportunidad = 0;

        public DataTable dtContactos;
        public int OpcionListContactos = 0;
        public string IdEmpresaContacto = "";
        private int EmpresaNueva = 0;

        public int MensajeEtapa = 1;

        public void setLookUpEdit(int etapa, string linea, string usuario)
        {
            this.Etapa = etapa;
            this.Linea = linea;
            this.Usuario = usuario;
            //Botones();
        }

        public void CargarLineas()
        {
            MLineas mLineas = new MLineas();
            DataTable dt = mLineas.Mostrar();

            lueLinea.Properties.ValueMember = "id";
            lueLinea.Properties.DisplayMember = "nombre";

            lueLinea.Properties.DataSource = dt;

        }

        private void CargarEtapaNegociacion()
        {
            List<string> etapas = new List<string>();
            etapas.Add("Cotizacion");
            etapas.Add("Negociacion");
            etapas.Add("Pre Confirmacion");
            etapas.Add("OC");
            etapas.Add("Cierre");

            lueetapa.Properties.DataSource = etapas; ;
        }

        private void CargarUsuarios()
        {
            int linea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
            MUsuario mUsuario = new MUsuario();
            DataTable dt = mUsuario.Listar(linea);

            lueUsuario.Properties.ValueMember = "id";
            lueUsuario.Properties.DisplayMember = "nombre";

            lueUsuario.Properties.DataSource = dt;

        }

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void CrearTablaContactos()
        {
            this.dtContactos = new DataTable("Contactos");
            this.dtContactos.Columns.Add("id", System.Type.GetType("System.Int32"));
            this.dtContactos.Columns.Add("nombres", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("correo", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("movil", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("fijo", System.Type.GetType("System.String"));
            //Relacionar nuestro DataGRidView con nuestro DataTable
            this.gcContactos.DataSource = this.dtContactos;

            if (OpcionListContactos == 1)
            {
                //this.gcContactos.DataSource = MContactos.Mostrar(IdEmpresaContacto,
                //                                            3);

                this.dtContactos = MContactos.Mostrar(IdEmpresaContacto,
                                                      Id,0,
                                                            4);

                this.gcContactos.DataSource = this.dtContactos;
            }

            this.gvContactos.Columns[0].Visible = false;
        }

        private void LimpiarDatosContacto()
        {
            txtContacto.Text = string.Empty;
            txtCorreo.Text = string.Empty;
            txtMovil.Text = string.Empty;
            txtFijo.Text = string.Empty;
        }

        public Negociacion()
        {
            InitializeComponent();
        }

        private void groupControl5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
            {
            try
            {
                string Rpta = "";
                
                if (txtNombre.Text == "" || txtRequerimiento.Text == "" || lueLinea.Text == "[Vacio]" ||
                    (this.txtRuc.Text == string.Empty & this.dtContactos.Rows.Count > 0))
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(txtNombre, "Ingrese un Valor");
                    errorIcono.SetError(txtRequerimiento, "Ingrese un Valor");
                    errorIcono.SetError(lueLinea, "Ingrese un Valor");
                }
                else
                {
                    if (this.VarNuevo)
                    {
                        int idLinea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
                        int idUsuario = 0;
                        if (lueUsuario.Text != "")
                        {
                            idUsuario = Convert.ToInt32(lueUsuario.GetColumnValue("id"));
                        }

                        if (gvAutocompletarContacto.RowCount == 0)
                        {
                            string valorid = "0";
                            if (txtContacto.Text != "")
                            {
                                //this.IdContacto = 0;
                                //valorid = MContactos.MantContacto(0,
                                //                 txtContacto.Text,
                                //                 txtCorreo.Text,
                                //                 txtMovil.Text,
                                //                 txtFijo.Text,
                                //                 1);
                            }

                            if (valorid == "Error")
                            {
                                this.IdContacto = 0;
                            }
                            else
                            {
                                this.IdContacto = Convert.ToInt32(valorid);
                            }

                        }
                        if (gvAutocompletarEmpresa.RowCount == 0)
                        {
                            string valorid = "0";
                            if (txtRuc.Text != "")
                            {
                                //valorid = MEmpresa.MantEmpresa(0,
                                //             txtRuc.Text,
                                //             txtRazonSocial.Text,
                                //             txtrubro.Text,
                                //             1);
                            }

                            if (valorid == "Error")
                            {
                                this.IdEmpresa = 0;
                            }
                            else
                            {
                                this.IdEmpresa = Convert.ToInt32(valorid);
                            }
                        }
                        //// GUARDAR CONTACTOS /////////////////////

                        MContactos.MantContacto(dtContactos, this.IdEmpresa,0,0,1);

                        if (cboTMoneda.SelectedIndex == 0)
                        {
                            TipoMoneda = 0;
                        }
                        else
                        {
                            TipoMoneda = 1;
                        }
                        
                        Rpta = MNegociaciones.MantNegociacion(0,
                                                       Convert.ToDateTime(deFechaRegistro.Text),
                                                       Convert.ToDateTime(deFechaConversion.Text),
                                                       txtNombre.Text,
                                                       Convert.ToDouble(semonto.Value),
                                                       Convert.ToInt32(rcEtapas.Rating),
                                                       TipoMoneda,
                                                       0,
                                                       idUsuario,
                                                       idLinea,
                                                       Convert.ToDouble(seMontoFacturado.Value),
                                                       IdEmpresa,
                                                       txtRequerimiento.Text,
                                                       0,
                                                       1);
                    }
                    else
                    {
                        int idLinea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
                        int idUsuario = 0;
                        if (lueUsuario.Text != "")
                        {
                            idUsuario = Convert.ToInt32(lueUsuario.GetColumnValue("id"));
                        }

                        if (txtRuc.Text == "")
                        {
                            this.IdEmpresa = 0;
                        }

                        if (gvAutocompletarEmpresa.RowCount == 0)
                        {
                            if (this.IdEmpresa != this.EmpresaNueva)
                            {
                                string valorid = "0";
                                if (txtRuc.Text != "")
                                {
                                    valorid = MEmpresa.MantEmpresa(0,
                                                 txtRuc.Text,
                                                 txtRazonSocial.Text,
                                                 txtrubro.Text,
                                                 1);
                                }

                                if (valorid == "Error")
                                {
                                    this.IdEmpresa = 0;
                                }
                                else
                                {
                                    this.IdEmpresa = Convert.ToInt32(valorid);
                                }
                            }

                            
                        }

                        //// GUARDAR CONTACTOS /////////////////////
                        //MContactos.MantEmpresaContacto(this.IdEmpresa, 0 , Id, 3);
                        MContactos.MantContacto(dtContactos, this.IdEmpresa, Id, 0, 0);

                        if (cboTMoneda.SelectedIndex == 0)
                        {
                            TipoMoneda = 0;
                        }
                        else
                        {
                            TipoMoneda = 1;
                        }

                        Rpta = MNegociaciones.MantNegociacion(Id,
                                                       Convert.ToDateTime(deFechaRegistro.Text),
                                                       Convert.ToDateTime(deFechaConversion.Text),
                                                       txtNombre.Text,
                                                       Convert.ToDouble(semonto.Value),
                                                       Convert.ToInt32(rcEtapas.Rating),
                                                       TipoMoneda,
                                                       0,
                                                       idUsuario,
                                                       idLinea,
                                                       Convert.ToDouble(seMontoFacturado.Value),
                                                       IdEmpresa,
                                                       txtRequerimiento.Text,
                                                       IdOportunidad,
                                                       2);


                    }

                    if (Rpta.Equals("OK"))
                    {
                        if (this.VarNuevo)
                        {
                            this.MensajeOk("Se Insertó de forma correcta el registro");
                            this.errorIcono.ClearErrors();
                        }
                        else
                        {
                            this.MensajeOk("Se Actualizó de forma correcta el registro");
                            this.errorIcono.Dispose();
                        }
                    }

                    if (Convert.ToInt32(Rpta) > 0)
                    {
                        if (this.VarNuevo)
                        {
                            this.MensajeOk("Se Insertó de forma correcta el registro");
                            this.VarNuevo = false;
                            this.VarEditar = true;
                            this.errorIcono.ClearErrors();
                        }
                        else
                        {
                            this.MensajeOk("Se Actualizó de forma correcta el registro");
                            this.errorIcono.ClearErrors();
                        }
                    }

                    else
                    {
                        this.MensajeError(Rpta);
                    }

                    //this.VarNuevo = false;
                    //this.VarEditar = false;
                    //this.Botones();
                    //this.Limpiar();
                    //this.MostrarUsuarios();

                    LisNegociaciones listNegociacion = Owner as LisNegociaciones;
                    //listOportunidades.MostrarOportunidades("", IdUsuario, 1, 0, IdLinea);
                    listNegociacion.setActualizar();
                }
                if (txtRuc.Text != "")
                {
                    if (txtRazonSocial.Text == "")
                    {
                        MensajeError("Ingrese Razon Social de la Empresa");
                    }
                }
                else if (txtRazonSocial.Text != "")
                {
                    if (txtRuc.Text == "")
                    {
                        MensajeError("Ingrese Ruc de la Empresa");
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void lueLinea_EditValueChanged(object sender, EventArgs e)
        {
            CargarUsuarios();
        }

        private void rcEtapas_EditValueChanged(object sender, EventArgs e)
        {
            switch (rcEtapas.Rating)
            {
                case 1:
                    Etapa = 1;
                    break;
                case 2:
                    Etapa = 2;
                    break;
                case 3:
                    Etapa = 3;
                    break;
                case 4:
                    Etapa = 4;
                    break;
                case 5:
                    Etapa = 5;
                    break;
            }
            string texto_etapa = "";
            if (Etapa == 1)
            {
                texto_etapa = "Cotizacion";
            }
            if (Etapa == 2)
            {
                texto_etapa = "Negociacion";
            }
            if (Etapa == 3)
            {
                texto_etapa = "Pre Confirmacion";
            }
            if (Etapa == 4)
            {
                texto_etapa = "OC";
            }
            if (Etapa == 5)
            {
                texto_etapa = "Cierre";

                if (MensajeEtapa == 0)
                {
                    Class.MsgBoxUtil msgBoxUtil = new Class.MsgBoxUtil();
                    msgBoxUtil.HackMessageBox("GANADA", "PERDIDA", "CANCELAR");

                    DialogResult resul = MessageBox.Show("¿ Negociacion Ganada o Perdida ?", "Sistema DCGTEC", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    this.MensajeEtapa = 1;

                    if (resul == DialogResult.Cancel)
                    {
                        //MensajeEtapa = 1;
                        MNegociaciones.MantNegociacion(Id,
                                               DateTime.Today,
                                               DateTime.Today,
                                               "",
                                               0,
                                               4,
                                               0,
                                               0, 0, 0, 0,
                                               0, "", 0, 4);
                        //rcEtapas.Rating = 4;
                        //lueetapa.EditValue = lueetapa.Properties.GetKeyValueByDisplayText("OC");
                    }

                    if (resul == DialogResult.No)
                    {
                        MNegociaciones.MantNegociacion(Id,
                                               2,
                                               5,
                                               5);
                        this.Close();
                    }

                    if (resul == DialogResult.Yes)
                    {
                        if (semonto.Value == 0 ||
                            seMontoFacturado.Value == 0)
                        {
                            MensajeError("No puedes convertir la negociacion con monto y/o monto facturado en 0");
                            MNegociaciones.MantNegociacion(Id,
                                                       DateTime.Today,
                                                       DateTime.Today,
                                                       "",
                                                       0,
                                                       4,
                                                       0,
                                                       0, 0, 0, 0,
                                                       0, "", 0, 4);
                            
                        }
                        else
                        {
                            MNegociaciones.MantNegociacion(Id,
                                               1,
                                               5,
                                               5);
                            this.Close();
                        }

                    }

                    LisNegociaciones ListNegociaciones = Owner as LisNegociaciones;
                    ListNegociaciones.setActualizar();
                    
                }

                
            }
           
            lueetapa.EditValue = lueetapa.Properties.GetKeyValueByDisplayText(texto_etapa);

            MensajeEtapa = 0;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            LisNegociaciones lisNegociaciones = Owner as LisNegociaciones;
            lisNegociaciones.setActualizar();
            this.Close();

            VarNuevo = true;
            VarEditar = false;
        }

        private void txtRuc_EditValueChanged(object sender, EventArgs e)
        {
            if (txtRuc.Text != "")
            {
                gcAutocompletarEmpresa.DataSource = MEmpresa.Mostrar(txtRuc.Text, 3);
                gcAutocompletarEmpresa.Visible = true;
                gvAutocompletarEmpresa.Columns[0].Visible = false;
                gvAutocompletarEmpresa.Columns[2].Visible = false;
                gvAutocompletarEmpresa.Columns[3].Visible = false;

                if (gvAutocompletarEmpresa.RowCount < 1)
                {
                    gcAutocompletarEmpresa.Visible = false;
                }
            }
            else
            {
                gcAutocompletarEmpresa.Visible = false;
                this.IdEmpresa = 0;
            }

            if (txtRuc.Text.Length == 0)
            {
                txtrubro.Text = string.Empty;
                txtRazonSocial.Text = string.Empty;
            }
            this.EmpresaNueva = 0;
        }

        private void txtRuc_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    gcAutocompletarEmpresa.Focus();
                    break;
            }
        }

        private void txtRuc_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            Formulario.AsignacionHistorial historial = new Formulario.AsignacionHistorial();
            historial.Negociacion = txtNombre.Text;
            AddOwnedForm(historial);
            historial.Show();
        }

        private void btnActividades_Click(object sender, EventArgs e)
        {
            ListActividades listActividades = new ListActividades();
            listActividades.IdNegociacion = this.Id;
            listActividades.IdUsuario = this.IdUsuario;
            listActividades.EstadoNegociacion = Estado;
            AddOwnedForm(listActividades);
            listActividades.ShowDialog();
        }

        private void cboTMoneda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboTMoneda.Text == "SOLES")
            {
                semonto.Properties.DisplayFormat.FormatString = "S/ #.##";
                seMontoFacturado.Properties.DisplayFormat.FormatString = "S/ #.##";
            }
            else
            {
                semonto.Properties.DisplayFormat.FormatString = "$ #.##";
                seMontoFacturado.Properties.DisplayFormat.FormatString = "$ #.##";
            }
        }

        private void lueetapa_EditValueChanged(object sender, EventArgs e)
        {
            switch (lueetapa.ItemIndex)
            {
                case 0:
                    rcEtapas.Rating = 1;
                    break;

                case 1:
                    rcEtapas.Rating = 2;
                    break;

                case 2:
                    rcEtapas.Rating = 3;
                    break;

                case 3:
                    rcEtapas.Rating = 4;
                    break;

                case 4:
                    MensajeEtapa = 1;
                    rcEtapas.Rating = 5;

                    if (MensajeEtapa == 0)
                    {
                        Class.MsgBoxUtil msgBoxUtil = new Class.MsgBoxUtil();
                        msgBoxUtil.HackMessageBox("GANADA", "PERDIDA", "CANCELAR");

                        DialogResult resul = MessageBox.Show("¿ Negociacion Ganada o Perdida ?", "Sistema DCGTEC", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                        

                        if (resul == DialogResult.Cancel)
                        {
                            MNegociaciones.MantNegociacion(Id,
                                                   DateTime.Today,
                                                   DateTime.Today,
                                                   "",
                                                   0,
                                                   4,
                                                   0,
                                                   0, 0, 0, 0,
                                                   0, "", 0, 4);
                            //this.setActualizar();
                        }

                        if (resul == DialogResult.No)
                        {
                            MNegociaciones.MantNegociacion(Id,
                                                   2,
                                                   5,
                                                   5);
                            //this.setActualizar();
                        }

                        if (resul == DialogResult.Yes)
                        {
                            if (semonto.Value == 0 ||
                                seMontoFacturado.Value == 0)
                            {
                                MensajeError("No puedes convertir la negociacion con monto y/o monto facturado en 0");
                                MNegociaciones.MantNegociacion(Id,
                                                           DateTime.Today,
                                                           DateTime.Today,
                                                           "",
                                                           0,
                                                           4,
                                                           0,
                                                           0, 0, 0, 0,
                                                           0, "", 0, 4);
                                //this.setActualizar();
                            }
                            else
                            {
                                MNegociaciones.MantNegociacion(Id,
                                                   1,
                                                   5,
                                                   5);
                                //this.setActualizar();
                            }

                        }
                        MensajeEtapa = 1;
                    }
                    LisNegociaciones ListNegociaciones = Owner as LisNegociaciones;
                    ListNegociaciones.setActualizar();

                    break;
            }
        }

        private void btnAgregarContactos_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtContacto.Text == string.Empty || (this.txtCorreo.Text == string.Empty
                    || this.txtFijo.Text == string.Empty || this.txtMovil.Text == string.Empty))
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(txtContacto, "Ingrese un Valor");
                    errorIcono.SetError(txtCorreo, "Ingrese un Valor");
                    errorIcono.SetError(txtMovil, "Ingrese un Valor");
                    errorIcono.SetError(txtFijo, "Ingrese un Valor");
                }
                else
                {
                    bool registrar = true;
                    foreach (DataRow row in dtContactos.Rows)
                    {
                        if (Convert.ToString(row["Nombres"]) == this.txtContacto.Text)
                        {
                            registrar = false;
                            this.MensajeError("Cliente ya ingresado");
                        }
                    }
                    if (registrar)
                    {
                        //Agregar ese detalle al datalistadoDetalle
                        DataRow row = this.dtContactos.NewRow();
                        row["Id"] = this.IdContacto;
                        row["Nombres"] = this.txtContacto.Text;
                        row["Correo"] = this.txtCorreo.Text;
                        row["Movil"] = this.txtMovil.Text;
                        row["Fijo"] = this.txtFijo.Text;
                        this.dtContactos.Rows.Add(row);
                        this.LimpiarDatosContacto();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void btnBorrarContactos_Click(object sender, EventArgs e)
        {
            try
            {
                int indicefila = this.gvContactos.GetFocusedDataSourceRowIndex();
                DataRow row = this.dtContactos.Rows[indicefila];
                //Removemos la fila
                this.dtContactos.Rows.Remove(row);

                //gvContactos.DeleteSelectedRows();
            }
            catch (Exception ex)
            {

            }
        }

        private void txtContacto_EditValueChanged(object sender, EventArgs e)
        {
            if (txtContacto.Text != "")
            {
                gcAutocompletarContacto.DataSource = MContactos.Mostrar(txtContacto.Text, Id,0, 2);
                gcAutocompletarContacto.Visible = true;
                gvAutocompletarContacto.Columns[0].Visible = false;
                gvAutocompletarContacto.Columns[2].Visible = false;
                gvAutocompletarContacto.Columns[3].Visible = false;
                gvAutocompletarContacto.Columns[4].Visible = false;

                if (gvAutocompletarContacto.RowCount < 1)
                {
                    gcAutocompletarContacto.Visible = false;
                }
            }
            else
            {
                gcAutocompletarContacto.Visible = false;
                this.IdContacto = 0;
            }

            if (txtContacto.Text.Length == 0)
            {
                txtCorreo.Text = "";
                txtMovil.Text = "";
                txtFijo.Text = "";
            }
        }

        private void txtContacto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    gvAutocompletarContacto.Focus();
                    break;
            }
        }

        private void gcAutocompletar_DoubleClick(object sender, EventArgs e)
        {
            this.IdContacto = Convert.ToInt32(gvAutocompletarContacto.GetFocusedRowCellValue("id").ToString());
            this.txtContacto.Text = gvAutocompletarContacto.GetFocusedRowCellValue("nombres").ToString();
            this.txtCorreo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("correo").ToString();
            this.txtFijo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("fijo").ToString();
            this.txtMovil.Text = gvAutocompletarContacto.GetFocusedRowCellValue("movil").ToString();
            gcAutocompletarContacto.Visible = false;
        }

        private void gcAutocompletarContacto_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtContacto.Focus();
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.IdContacto = Convert.ToInt32(gvAutocompletarContacto.GetFocusedRowCellValue("id").ToString());
                this.txtContacto.Text = gvAutocompletarContacto.GetFocusedRowCellValue("nombres").ToString();
                this.txtCorreo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("correo").ToString();
                this.txtFijo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("fijo").ToString();
                this.txtMovil.Text = gvAutocompletarContacto.GetFocusedRowCellValue("movil").ToString();
                gcAutocompletarContacto.Visible = false;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void gcAutocompletarEmpresa_DoubleClick(object sender, EventArgs e)
        {
            txtRuc.Focus();
            this.IdEmpresa = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            this.txtRuc.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("ruc").ToString();
            this.txtRazonSocial.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("razon_social").ToString();
            this.txtrubro.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("rubro").ToString();
            this.EmpresaNueva = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            gcAutocompletarEmpresa.Visible = false;
        }

        private void gcAutocompletarEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtRuc.Focus();
            this.IdEmpresa = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            this.txtRuc.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("ruc").ToString();
            this.txtRazonSocial.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("razon_social").ToString();
            this.txtrubro.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("rubro").ToString();
            this.EmpresaNueva = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            gcAutocompletarEmpresa.Visible = false;
        }

        private void Negociacion_Load(object sender, EventArgs e)
        {
            CargarLineas();
            CargarEtapaNegociacion();
            /////////////Carga Editar ////////
            string texto_etapa = "";
            if (Etapa == 1)
            {
                texto_etapa = "Cotizacion";
            }
            if (Etapa == 2)
            {
                texto_etapa = "Negociacion";
            }
            if (Etapa == 3)
            {
                texto_etapa = "Pre Confirmacion";
            }
            if (Etapa == 4)
            {
                texto_etapa = "OC";
            }
            if (Etapa == 5)
            {
                texto_etapa = "Cierre";
            }

            if (this.VarEditar)
            {
                lueetapa.EditValue = lueetapa.Properties.GetKeyValueByDisplayText(texto_etapa);
                lueLinea.EditValue = lueLinea.Properties.GetKeyValueByDisplayText(Linea);
                lueUsuario.EditValue = lueUsuario.Properties.GetKeyValueByDisplayText(Usuario);
            }

            if (this.TipoMoneda == 0)
            {
                cboTMoneda.SelectedIndex = 0;
                semonto.Properties.DisplayFormat.FormatString = "S/ #.##";
                seMontoFacturado.Properties.DisplayFormat.FormatString = "S/ #.##";
            }
            else
            {
                cboTMoneda.SelectedIndex = 1;
                semonto.Properties.DisplayFormat.FormatString = "$ #.##";
                seMontoFacturado.Properties.DisplayFormat.FormatString = "$ #.##";
            }

            if (this.Estado == 0)
            {
                btnNuevo.Enabled = true;
                btnGuardar.Enabled = true;
                semonto.Enabled = true;
                seMontoFacturado.Enabled = true;
                cboTMoneda.Enabled = true;
                lueetapa.Enabled = true;
                deFechaRegistro.Enabled = true;
                deFechaConversion.Enabled = true;
                txtNombre.Enabled = true;
                txtRequerimiento.Enabled = true;
                lueLinea.Enabled = true;
                gcContactos.Enabled = true;
                lueUsuario.Enabled = true;
                txtContacto.Enabled = true;
                btnBorrarContactos.Enabled = true;
                btnAgregarContactos.Enabled = true;
                txtCorreo.Enabled = true;
                txtMovil.Enabled = true;
                txtFijo.Enabled = true;
                txtRazonSocial.Enabled = true;
                txtRuc.Enabled = true;
            }

            CrearTablaContactos();
            MensajeEtapa = 0;
        }
    }
}