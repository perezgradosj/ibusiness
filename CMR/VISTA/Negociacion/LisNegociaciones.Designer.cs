﻿namespace VISTA.Negociacion
{
    partial class LisNegociaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LisNegociaciones));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MarcaPerdidas = new System.Windows.Forms.Panel();
            this.marcaGanadas = new System.Windows.Forms.Panel();
            this.marcaPendiente = new System.Windows.Forms.Panel();
            this.lblFiltro = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcSolicitudes = new DevExpress.XtraGrid.GridControl();
            this.gvSolicitudes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.btnNoUtil = new DevExpress.XtraEditors.SimpleButton();
            this.btnCerrado = new DevExpress.XtraEditors.SimpleButton();
            this.btnPendiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboCantRegistros = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtpaginacion = new DevExpress.XtraEditors.TextEdit();
            this.btnUltimaPagina = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaginaSiguiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrimeraPagina = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaginaAnterior = new DevExpress.XtraEditors.SimpleButton();
            this.gcNegociaciones = new DevExpress.XtraGrid.GridControl();
            this.gvNegociaciones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fecha_registro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFechaConversion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcNombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMonto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMontoFacturado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTipo_Moneda = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRequerimiento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEtapa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemEtapas = new DevExpress.XtraEditors.Repository.RepositoryItemRatingControl();
            this.gcEstado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdLinea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcLinea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdEmpresa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRazonSocial = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcRubro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDescripcionActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcFechaActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcHoraActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdContactoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdTipoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTipoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdUsuarioActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdEstadoActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcIdNegociacionActividad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcAct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.itemActividad = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.ItemEtap = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.itemMemo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsIAgregarActividad = new System.Windows.Forms.ToolStripMenuItem();
            this.tsIEditarActividad = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsNegociaciones = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rEACTIVARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCantRegistros.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNegociaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNegociaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemEtapas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemActividad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemEtap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemMemo)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.cmsNegociaciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MarcaPerdidas);
            this.panel1.Controls.Add(this.marcaGanadas);
            this.panel1.Controls.Add(this.marcaPendiente);
            this.panel1.Controls.Add(this.lblFiltro);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.gcSolicitudes);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.btnActualizar);
            this.panel1.Controls.Add(this.btnNoUtil);
            this.panel1.Controls.Add(this.btnCerrado);
            this.panel1.Controls.Add(this.btnPendiente);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(968, 82);
            this.panel1.TabIndex = 2;
            // 
            // MarcaPerdidas
            // 
            this.MarcaPerdidas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MarcaPerdidas.BackColor = System.Drawing.Color.Green;
            this.MarcaPerdidas.Location = new System.Drawing.Point(806, 12);
            this.MarcaPerdidas.Name = "MarcaPerdidas";
            this.MarcaPerdidas.Size = new System.Drawing.Size(150, 10);
            this.MarcaPerdidas.TabIndex = 19;
            this.MarcaPerdidas.Visible = false;
            // 
            // marcaGanadas
            // 
            this.marcaGanadas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.marcaGanadas.BackColor = System.Drawing.Color.Green;
            this.marcaGanadas.Location = new System.Drawing.Point(650, 12);
            this.marcaGanadas.Name = "marcaGanadas";
            this.marcaGanadas.Size = new System.Drawing.Size(150, 10);
            this.marcaGanadas.TabIndex = 18;
            this.marcaGanadas.Visible = false;
            // 
            // marcaPendiente
            // 
            this.marcaPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.marcaPendiente.BackColor = System.Drawing.Color.Green;
            this.marcaPendiente.Location = new System.Drawing.Point(494, 12);
            this.marcaPendiente.Name = "marcaPendiente";
            this.marcaPendiente.Size = new System.Drawing.Size(150, 10);
            this.marcaPendiente.TabIndex = 17;
            // 
            // lblFiltro
            // 
            this.lblFiltro.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFiltro.Appearance.Options.UseFont = true;
            this.lblFiltro.Location = new System.Drawing.Point(420, 28);
            this.lblFiltro.Name = "lblFiltro";
            this.lblFiltro.Size = new System.Drawing.Size(106, 19);
            this.lblFiltro.TabIndex = 16;
            this.lblFiltro.Text = "(PENDIENTES)";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(199, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(215, 31);
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = "NEGOCIACIONES";
            // 
            // gcSolicitudes
            // 
            this.gcSolicitudes.Location = new System.Drawing.Point(585, 73);
            this.gcSolicitudes.MainView = this.gvSolicitudes;
            this.gcSolicitudes.Name = "gcSolicitudes";
            this.gcSolicitudes.Size = new System.Drawing.Size(289, 55);
            this.gcSolicitudes.TabIndex = 12;
            this.gcSolicitudes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSolicitudes,
            this.gridView2});
            this.gcSolicitudes.Visible = false;
            // 
            // gvSolicitudes
            // 
            this.gvSolicitudes.GridControl = this.gcSolicitudes;
            this.gvSolicitudes.Name = "gvSolicitudes";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcSolicitudes;
            this.gridView2.Name = "gridView2";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.AutoSize = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(12, 20);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(40, 38);
            this.simpleButton2.TabIndex = 11;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.Appearance.Options.UseFont = true;
            this.btnActualizar.AutoSize = true;
            this.btnActualizar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizar.ImageOptions.Image")));
            this.btnActualizar.Location = new System.Drawing.Point(58, 20);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(40, 38);
            this.btnActualizar.TabIndex = 10;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnNoUtil
            // 
            this.btnNoUtil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNoUtil.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoUtil.Appearance.Options.UseFont = true;
            this.btnNoUtil.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNoUtil.ImageOptions.Image")));
            this.btnNoUtil.Location = new System.Drawing.Point(806, 12);
            this.btnNoUtil.Name = "btnNoUtil";
            this.btnNoUtil.Size = new System.Drawing.Size(150, 55);
            this.btnNoUtil.TabIndex = 9;
            this.btnNoUtil.Text = "PERDIDAS   8";
            this.btnNoUtil.Click += new System.EventHandler(this.btnNoUtil_Click);
            // 
            // btnCerrado
            // 
            this.btnCerrado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrado.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrado.Appearance.Options.UseFont = true;
            this.btnCerrado.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrado.ImageOptions.Image")));
            this.btnCerrado.Location = new System.Drawing.Point(650, 12);
            this.btnCerrado.Name = "btnCerrado";
            this.btnCerrado.Size = new System.Drawing.Size(150, 55);
            this.btnCerrado.TabIndex = 8;
            this.btnCerrado.Text = "GANADAS   15";
            this.btnCerrado.Click += new System.EventHandler(this.btnCerrado_Click);
            // 
            // btnPendiente
            // 
            this.btnPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPendiente.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPendiente.Appearance.Options.UseBackColor = true;
            this.btnPendiente.Appearance.Options.UseFont = true;
            this.btnPendiente.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPendiente.ImageOptions.Image")));
            this.btnPendiente.Location = new System.Drawing.Point(494, 12);
            this.btnPendiente.Name = "btnPendiente";
            this.btnPendiente.Size = new System.Drawing.Size(150, 55);
            this.btnPendiente.TabIndex = 7;
            this.btnPendiente.Text = "PENDIENTE   10";
            this.btnPendiente.Click += new System.EventHandler(this.btnPendiente_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Appearance.Options.UseFont = true;
            this.btnBuscar.AutoSize = true;
            this.btnBuscar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.ImageOptions.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(104, 20);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(40, 38);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cboCantRegistros);
            this.panel2.Controls.Add(this.simpleButton1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtpaginacion);
            this.panel2.Controls.Add(this.btnUltimaPagina);
            this.panel2.Controls.Add(this.btnPaginaSiguiente);
            this.panel2.Controls.Add(this.btnPrimeraPagina);
            this.panel2.Controls.Add(this.btnPaginaAnterior);
            this.panel2.Controls.Add(this.gcNegociaciones);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(968, 340);
            this.panel2.TabIndex = 3;
            // 
            // cboCantRegistros
            // 
            this.cboCantRegistros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboCantRegistros.EditValue = "20";
            this.cboCantRegistros.Location = new System.Drawing.Point(12, 315);
            this.cboCantRegistros.Name = "cboCantRegistros";
            this.cboCantRegistros.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCantRegistros.Properties.Items.AddRange(new object[] {
            "20",
            "50",
            "100"});
            this.cboCantRegistros.Size = new System.Drawing.Size(50, 20);
            this.cboCantRegistros.TabIndex = 15;
            this.cboCantRegistros.SelectedIndexChanged += new System.EventHandler(this.cboCantRegistros_SelectedIndexChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ImageOptions.Image = global::VISTA.Properties.Resources.doublelast_16x16;
            this.simpleButton1.Location = new System.Drawing.Point(940, 312);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(25, 25);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(619, 318);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "150 Registros || 20 Paginas";
            // 
            // txtpaginacion
            // 
            this.txtpaginacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpaginacion.EditValue = "1";
            this.txtpaginacion.Location = new System.Drawing.Point(847, 315);
            this.txtpaginacion.Name = "txtpaginacion";
            this.txtpaginacion.Properties.Appearance.Options.UseTextOptions = true;
            this.txtpaginacion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtpaginacion.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtpaginacion.Size = new System.Drawing.Size(25, 20);
            this.txtpaginacion.TabIndex = 12;
            this.txtpaginacion.EditValueChanged += new System.EventHandler(this.txtpaginacion_EditValueChanged);
            this.txtpaginacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpaginacion_KeyPress);
            // 
            // btnUltimaPagina
            // 
            this.btnUltimaPagina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUltimaPagina.ImageOptions.Image = global::VISTA.Properties.Resources.doublelast_16x16;
            this.btnUltimaPagina.Location = new System.Drawing.Point(909, 312);
            this.btnUltimaPagina.Name = "btnUltimaPagina";
            this.btnUltimaPagina.Size = new System.Drawing.Size(25, 25);
            this.btnUltimaPagina.TabIndex = 11;
            this.btnUltimaPagina.Click += new System.EventHandler(this.btnUltimaPagina_Click);
            // 
            // btnPaginaSiguiente
            // 
            this.btnPaginaSiguiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaginaSiguiente.ImageOptions.Image = global::VISTA.Properties.Resources.next_16x16;
            this.btnPaginaSiguiente.Location = new System.Drawing.Point(878, 312);
            this.btnPaginaSiguiente.Name = "btnPaginaSiguiente";
            this.btnPaginaSiguiente.Size = new System.Drawing.Size(25, 25);
            this.btnPaginaSiguiente.TabIndex = 10;
            this.btnPaginaSiguiente.Click += new System.EventHandler(this.btnPaginaSiguiente_Click);
            // 
            // btnPrimeraPagina
            // 
            this.btnPrimeraPagina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrimeraPagina.ImageOptions.Image = global::VISTA.Properties.Resources.doublefirst_16x16;
            this.btnPrimeraPagina.Location = new System.Drawing.Point(785, 312);
            this.btnPrimeraPagina.Name = "btnPrimeraPagina";
            this.btnPrimeraPagina.Size = new System.Drawing.Size(25, 25);
            this.btnPrimeraPagina.TabIndex = 8;
            this.btnPrimeraPagina.Click += new System.EventHandler(this.btnPrimeraPagina_Click);
            // 
            // btnPaginaAnterior
            // 
            this.btnPaginaAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaginaAnterior.ImageOptions.Image = global::VISTA.Properties.Resources.prev_16x16;
            this.btnPaginaAnterior.Location = new System.Drawing.Point(816, 312);
            this.btnPaginaAnterior.Name = "btnPaginaAnterior";
            this.btnPaginaAnterior.Size = new System.Drawing.Size(25, 25);
            this.btnPaginaAnterior.TabIndex = 9;
            this.btnPaginaAnterior.Click += new System.EventHandler(this.btnPaginaAnterior_Click);
            // 
            // gcNegociaciones
            // 
            this.gcNegociaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcNegociaciones.Location = new System.Drawing.Point(0, 0);
            this.gcNegociaciones.MainView = this.gvNegociaciones;
            this.gcNegociaciones.Name = "gcNegociaciones";
            this.gcNegociaciones.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.itemActividad,
            this.ItemEtap,
            this.ItemEtapas,
            this.itemMemo});
            this.gcNegociaciones.Size = new System.Drawing.Size(968, 340);
            this.gcNegociaciones.TabIndex = 0;
            this.gcNegociaciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNegociaciones});
            this.gcNegociaciones.Click += new System.EventHandler(this.gcNegociaciones_Click);
            this.gcNegociaciones.DoubleClick += new System.EventHandler(this.gcNegociaciones_DoubleClick);
            this.gcNegociaciones.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gcNegociaciones_MouseClick);
            // 
            // gvNegociaciones
            // 
            this.gvNegociaciones.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcId,
            this.fecha_registro,
            this.gcFechaConversion,
            this.gcNombre,
            this.gcMonto,
            this.gcMontoFacturado,
            this.gcTipo_Moneda,
            this.gcRequerimiento,
            this.gcEtapa,
            this.gcEstado,
            this.gcIdUsuario,
            this.gcUsuario,
            this.gcIdLinea,
            this.gcLinea,
            this.gcIdEmpresa,
            this.gcRazonSocial,
            this.gcRuc,
            this.gcRubro,
            this.gcIdActividad,
            this.gcActividad,
            this.gcDescripcionActividad,
            this.gcFechaActividad,
            this.gcHoraActividad,
            this.gcIdContactoActividad,
            this.gcIdTipoActividad,
            this.gcTipoActividad,
            this.gcIdUsuarioActividad,
            this.gcIdEstadoActividad,
            this.gcIdNegociacionActividad,
            this.gcAct});
            this.gvNegociaciones.GridControl = this.gcNegociaciones;
            this.gvNegociaciones.Name = "gvNegociaciones";
            this.gvNegociaciones.OptionsView.RowAutoHeight = true;
            this.gvNegociaciones.OptionsView.ShowFooter = true;
            this.gvNegociaciones.OptionsView.ShowGroupPanel = false;
            this.gvNegociaciones.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvNegociaciones_RowCellStyle);
            this.gvNegociaciones.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvNegociaciones_CustomRowCellEdit);
            this.gvNegociaciones.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvNegociaciones_PopupMenuShowing);
            this.gvNegociaciones.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gvNegociaciones_ShowingEditor);
            // 
            // gcId
            // 
            this.gcId.Caption = "id";
            this.gcId.FieldName = "id";
            this.gcId.Name = "gcId";
            this.gcId.Width = 43;
            // 
            // fecha_registro
            // 
            this.fecha_registro.Caption = "Fecha Registro";
            this.fecha_registro.FieldName = "fecha_registro";
            this.fecha_registro.Name = "fecha_registro";
            this.fecha_registro.OptionsColumn.AllowEdit = false;
            this.fecha_registro.Visible = true;
            this.fecha_registro.VisibleIndex = 0;
            this.fecha_registro.Width = 77;
            // 
            // gcFechaConversion
            // 
            this.gcFechaConversion.Caption = "Fecha Conversion";
            this.gcFechaConversion.FieldName = "fecha_conversion";
            this.gcFechaConversion.Name = "gcFechaConversion";
            this.gcFechaConversion.OptionsColumn.AllowEdit = false;
            this.gcFechaConversion.Visible = true;
            this.gcFechaConversion.VisibleIndex = 1;
            this.gcFechaConversion.Width = 77;
            // 
            // gcNombre
            // 
            this.gcNombre.Caption = "Nombre";
            this.gcNombre.FieldName = "nombre";
            this.gcNombre.Name = "gcNombre";
            this.gcNombre.OptionsColumn.AllowEdit = false;
            this.gcNombre.Visible = true;
            this.gcNombre.VisibleIndex = 2;
            this.gcNombre.Width = 77;
            // 
            // gcMonto
            // 
            this.gcMonto.Caption = "Monto";
            this.gcMonto.DisplayFormat.FormatString = "#.##";
            this.gcMonto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gcMonto.FieldName = "monto";
            this.gcMonto.Name = "gcMonto";
            this.gcMonto.OptionsColumn.AllowEdit = false;
            this.gcMonto.Visible = true;
            this.gcMonto.VisibleIndex = 3;
            // 
            // gcMontoFacturado
            // 
            this.gcMontoFacturado.Caption = "Monto Facturado";
            this.gcMontoFacturado.DisplayFormat.FormatString = "#.##";
            this.gcMontoFacturado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gcMontoFacturado.FieldName = "monto_facturado";
            this.gcMontoFacturado.Name = "gcMontoFacturado";
            this.gcMontoFacturado.OptionsColumn.AllowEdit = false;
            this.gcMontoFacturado.Visible = true;
            this.gcMontoFacturado.VisibleIndex = 4;
            // 
            // gcTipo_Moneda
            // 
            this.gcTipo_Moneda.Caption = "Tipo Moneda";
            this.gcTipo_Moneda.FieldName = "tipo_moneda";
            this.gcTipo_Moneda.Name = "gcTipo_Moneda";
            // 
            // gcRequerimiento
            // 
            this.gcRequerimiento.Caption = "Requerimiento";
            this.gcRequerimiento.FieldName = "requerimiento";
            this.gcRequerimiento.Name = "gcRequerimiento";
            // 
            // gcEtapa
            // 
            this.gcEtapa.Caption = "Etapa";
            this.gcEtapa.ColumnEdit = this.ItemEtapas;
            this.gcEtapa.FieldName = "etapa";
            this.gcEtapa.Name = "gcEtapa";
            this.gcEtapa.Visible = true;
            this.gcEtapa.VisibleIndex = 5;
            this.gcEtapa.Width = 89;
            // 
            // ItemEtapas
            // 
            this.ItemEtapas.AutoHeight = false;
            this.ItemEtapas.CheckedGlyph = global::VISTA.Properties.Resources.marca_de_verificacion_verde;
            this.ItemEtapas.Glyph = global::VISTA.Properties.Resources.cerrar1;
            this.ItemEtapas.HoverGlyph = global::VISTA.Properties.Resources.marca_de_verificacion_hober;
            this.ItemEtapas.Name = "ItemEtapas";
            this.ItemEtapas.ItemClick += new DevExpress.XtraEditors.Repository.ItemEventHandler(this.ItemEtapas_ItemClick);
            this.ItemEtapas.BeforeShowToolTip += new DevExpress.XtraEditors.Repository.RatingToolTipEventHandler(this.ItemEtapas_BeforeShowToolTip);
            // 
            // gcEstado
            // 
            this.gcEstado.Caption = "Estado";
            this.gcEstado.FieldName = "estado";
            this.gcEstado.Name = "gcEstado";
            this.gcEstado.Width = 43;
            // 
            // gcIdUsuario
            // 
            this.gcIdUsuario.Caption = "Id Usuario";
            this.gcIdUsuario.FieldName = "idusuario";
            this.gcIdUsuario.Name = "gcIdUsuario";
            this.gcIdUsuario.Width = 43;
            // 
            // gcUsuario
            // 
            this.gcUsuario.Caption = "Usuario";
            this.gcUsuario.FieldName = "usuario";
            this.gcUsuario.Name = "gcUsuario";
            this.gcUsuario.OptionsColumn.AllowEdit = false;
            this.gcUsuario.Visible = true;
            this.gcUsuario.VisibleIndex = 6;
            this.gcUsuario.Width = 73;
            // 
            // gcIdLinea
            // 
            this.gcIdLinea.Caption = "Id Linea";
            this.gcIdLinea.FieldName = "idlinea";
            this.gcIdLinea.Name = "gcIdLinea";
            this.gcIdLinea.Width = 43;
            // 
            // gcLinea
            // 
            this.gcLinea.Caption = "Linea";
            this.gcLinea.FieldName = "linea";
            this.gcLinea.Name = "gcLinea";
            this.gcLinea.OptionsColumn.AllowEdit = false;
            this.gcLinea.Visible = true;
            this.gcLinea.VisibleIndex = 7;
            this.gcLinea.Width = 73;
            // 
            // gcIdEmpresa
            // 
            this.gcIdEmpresa.Caption = "Id Empresa";
            this.gcIdEmpresa.FieldName = "idempresa";
            this.gcIdEmpresa.Name = "gcIdEmpresa";
            // 
            // gcRazonSocial
            // 
            this.gcRazonSocial.Caption = "Razon Social";
            this.gcRazonSocial.FieldName = "razon_social";
            this.gcRazonSocial.Name = "gcRazonSocial";
            this.gcRazonSocial.OptionsColumn.AllowEdit = false;
            this.gcRazonSocial.Visible = true;
            this.gcRazonSocial.VisibleIndex = 8;
            this.gcRazonSocial.Width = 142;
            // 
            // gcRuc
            // 
            this.gcRuc.Caption = "Ruc";
            this.gcRuc.FieldName = "ruc";
            this.gcRuc.Name = "gcRuc";
            // 
            // gcRubro
            // 
            this.gcRubro.Caption = "Rubro";
            this.gcRubro.FieldName = "rubro";
            this.gcRubro.Name = "gcRubro";
            // 
            // gcIdActividad
            // 
            this.gcIdActividad.Caption = "Id Actividad";
            this.gcIdActividad.FieldName = "id_actividad";
            this.gcIdActividad.Name = "gcIdActividad";
            this.gcIdActividad.Width = 43;
            // 
            // gcActividad
            // 
            this.gcActividad.Caption = "Actividad";
            this.gcActividad.FieldName = "actividad";
            this.gcActividad.Name = "gcActividad";
            // 
            // gcDescripcionActividad
            // 
            this.gcDescripcionActividad.Caption = "Descripcion Actividad";
            this.gcDescripcionActividad.FieldName = "descripcion_actividad";
            this.gcDescripcionActividad.Name = "gcDescripcionActividad";
            this.gcDescripcionActividad.Width = 43;
            // 
            // gcFechaActividad
            // 
            this.gcFechaActividad.Caption = "Fecha Actividad";
            this.gcFechaActividad.FieldName = "fecha_actividad";
            this.gcFechaActividad.Name = "gcFechaActividad";
            this.gcFechaActividad.Width = 43;
            // 
            // gcHoraActividad
            // 
            this.gcHoraActividad.Caption = "Hora Actividad";
            this.gcHoraActividad.DisplayFormat.FormatString = "d";
            this.gcHoraActividad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gcHoraActividad.FieldName = "hora_actividad";
            this.gcHoraActividad.Name = "gcHoraActividad";
            this.gcHoraActividad.Width = 43;
            // 
            // gcIdContactoActividad
            // 
            this.gcIdContactoActividad.Caption = "Id Contacto Actividad";
            this.gcIdContactoActividad.FieldName = "idcontacto_actividad";
            this.gcIdContactoActividad.Name = "gcIdContactoActividad";
            this.gcIdContactoActividad.Width = 43;
            // 
            // gcIdTipoActividad
            // 
            this.gcIdTipoActividad.Caption = "Id Tipo Actividad";
            this.gcIdTipoActividad.FieldName = "idtipo_actividad";
            this.gcIdTipoActividad.Name = "gcIdTipoActividad";
            this.gcIdTipoActividad.Width = 43;
            // 
            // gcTipoActividad
            // 
            this.gcTipoActividad.Caption = "Tipo Actividad";
            this.gcTipoActividad.FieldName = "tipo_actividad";
            this.gcTipoActividad.Name = "gcTipoActividad";
            this.gcTipoActividad.Width = 43;
            // 
            // gcIdUsuarioActividad
            // 
            this.gcIdUsuarioActividad.Caption = "Id Usuario Actividad";
            this.gcIdUsuarioActividad.FieldName = "idusuario_actividad";
            this.gcIdUsuarioActividad.Name = "gcIdUsuarioActividad";
            this.gcIdUsuarioActividad.Width = 43;
            // 
            // gcIdEstadoActividad
            // 
            this.gcIdEstadoActividad.Caption = "Id Estado Actividad";
            this.gcIdEstadoActividad.FieldName = "idestado_actividad";
            this.gcIdEstadoActividad.Name = "gcIdEstadoActividad";
            this.gcIdEstadoActividad.Width = 43;
            // 
            // gcIdNegociacionActividad
            // 
            this.gcIdNegociacionActividad.Caption = "Id Negociacion Actividad";
            this.gcIdNegociacionActividad.FieldName = "idnegociacion_actividad";
            this.gcIdNegociacionActividad.Name = "gcIdNegociacionActividad";
            this.gcIdNegociacionActividad.Width = 43;
            // 
            // gcAct
            // 
            this.gcAct.Caption = "Actividad";
            this.gcAct.ColumnEdit = this.itemActividad;
            this.gcAct.FieldName = "act";
            this.gcAct.Name = "gcAct";
            this.gcAct.Visible = true;
            this.gcAct.VisibleIndex = 9;
            this.gcAct.Width = 190;
            // 
            // itemActividad
            // 
            this.itemActividad.AccessibleDescription = "";
            this.itemActividad.AccessibleName = "";
            editorButtonImageOptions1.Image = global::VISTA.Properties.Resources.add_16x16;
            this.itemActividad.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.itemActividad.Name = "itemActividad";
            this.itemActividad.Tag = "";
            this.itemActividad.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.itemActividad.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.itemActividad_ButtonClick);
            this.itemActividad.DoubleClick += new System.EventHandler(this.itemActividad_DoubleClick);
            // 
            // ItemEtap
            // 
            serializableAppearanceObject6.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject6.BackColor2 = System.Drawing.Color.Green;
            serializableAppearanceObject6.BorderColor = System.Drawing.Color.Green;
            serializableAppearanceObject6.Options.UseBackColor = true;
            serializableAppearanceObject6.Options.UseBorderColor = true;
            serializableAppearanceObject10.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject10.BackColor2 = System.Drawing.Color.Green;
            serializableAppearanceObject10.BorderColor = System.Drawing.Color.Green;
            serializableAppearanceObject10.Options.UseBackColor = true;
            serializableAppearanceObject10.Options.UseBorderColor = true;
            serializableAppearanceObject14.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject14.BackColor2 = System.Drawing.Color.Green;
            serializableAppearanceObject14.BorderColor = System.Drawing.Color.Green;
            serializableAppearanceObject14.Options.UseBackColor = true;
            serializableAppearanceObject14.Options.UseBorderColor = true;
            serializableAppearanceObject18.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject18.BackColor2 = System.Drawing.Color.Green;
            serializableAppearanceObject18.BorderColor = System.Drawing.Color.Green;
            serializableAppearanceObject18.Options.UseBackColor = true;
            serializableAppearanceObject18.Options.UseBorderColor = true;
            serializableAppearanceObject22.BackColor = System.Drawing.Color.Green;
            serializableAppearanceObject22.BackColor2 = System.Drawing.Color.Green;
            serializableAppearanceObject22.BorderColor = System.Drawing.Color.Green;
            serializableAppearanceObject22.Options.UseBackColor = true;
            serializableAppearanceObject22.Options.UseBorderColor = true;
            this.ItemEtap.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "1", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "2", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "3", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "4", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "5", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ItemEtap.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.ItemEtap.Name = "ItemEtap";
            this.ItemEtap.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.ItemEtap.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Etapas_ButtonClick);
            // 
            // itemMemo
            // 
            this.itemMemo.Name = "itemMemo";
            this.itemMemo.NullText = "dddd";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsIAgregarActividad,
            this.tsIEditarActividad});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(170, 48);
            // 
            // tsIAgregarActividad
            // 
            this.tsIAgregarActividad.Name = "tsIAgregarActividad";
            this.tsIAgregarActividad.Size = new System.Drawing.Size(169, 22);
            this.tsIAgregarActividad.Text = "Agregar Actividad";
            this.tsIAgregarActividad.Click += new System.EventHandler(this.tsIAgregarActividad_Click);
            // 
            // tsIEditarActividad
            // 
            this.tsIEditarActividad.Name = "tsIEditarActividad";
            this.tsIEditarActividad.Size = new System.Drawing.Size(169, 22);
            this.tsIEditarActividad.Text = "Editar Actividad";
            this.tsIEditarActividad.Click += new System.EventHandler(this.tsIEditarActividad_Click);
            // 
            // cmsNegociaciones
            // 
            this.cmsNegociaciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rEACTIVARToolStripMenuItem});
            this.cmsNegociaciones.Name = "cmsNegociaciones";
            this.cmsNegociaciones.Size = new System.Drawing.Size(135, 26);
            // 
            // rEACTIVARToolStripMenuItem
            // 
            this.rEACTIVARToolStripMenuItem.Name = "rEACTIVARToolStripMenuItem";
            this.rEACTIVARToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.rEACTIVARToolStripMenuItem.Text = "REACTIVAR";
            this.rEACTIVARToolStripMenuItem.Click += new System.EventHandler(this.rEACTIVARToolStripMenuItem_Click);
            // 
            // LisNegociaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 422);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LisNegociaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LisNegociaciones";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LisNegociaciones_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboCantRegistros.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNegociaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNegociaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemEtapas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemActividad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemEtap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemMemo)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.cmsNegociaciones.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl gcSolicitudes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSolicitudes;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraEditors.SimpleButton btnNoUtil;
        private DevExpress.XtraEditors.SimpleButton btnCerrado;
        private DevExpress.XtraEditors.SimpleButton btnPendiente;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraGrid.GridControl gcNegociaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNegociaciones;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsIAgregarActividad;
        private System.Windows.Forms.ToolStripMenuItem tsIEditarActividad;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit itemActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcId;
        private DevExpress.XtraGrid.Columns.GridColumn fecha_registro;
        private DevExpress.XtraGrid.Columns.GridColumn gcFechaConversion;
        private DevExpress.XtraGrid.Columns.GridColumn gcNombre;
        private DevExpress.XtraGrid.Columns.GridColumn gcEtapa;
        private DevExpress.XtraGrid.Columns.GridColumn gcEstado;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn gcUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdLinea;
        private DevExpress.XtraGrid.Columns.GridColumn gcLinea;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcDescripcionActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcFechaActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcHoraActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdContactoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdTipoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcTipoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdUsuarioActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdEstadoActividad;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdNegociacionActividad;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ItemEtap;
        private DevExpress.XtraEditors.Repository.RepositoryItemRatingControl ItemEtapas;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit itemMemo;
        private DevExpress.XtraGrid.Columns.GridColumn gcRequerimiento;
        private DevExpress.XtraGrid.Columns.GridColumn gcMontoFacturado;
        private DevExpress.XtraGrid.Columns.GridColumn gcIdEmpresa;
        private DevExpress.XtraGrid.Columns.GridColumn gcRazonSocial;
        private DevExpress.XtraGrid.Columns.GridColumn gcRuc;
        private DevExpress.XtraGrid.Columns.GridColumn gcRubro;
        private DevExpress.XtraGrid.Columns.GridColumn gcAct;
        private DevExpress.XtraGrid.Columns.GridColumn gcMonto;
        private DevExpress.XtraGrid.Columns.GridColumn gcTipo_Moneda;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ContextMenuStrip cmsNegociaciones;
        private System.Windows.Forms.ToolStripMenuItem rEACTIVARToolStripMenuItem;
        private DevExpress.XtraEditors.LabelControl lblFiltro;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtpaginacion;
        private DevExpress.XtraEditors.SimpleButton btnUltimaPagina;
        private DevExpress.XtraEditors.SimpleButton btnPaginaSiguiente;
        private DevExpress.XtraEditors.SimpleButton btnPrimeraPagina;
        private DevExpress.XtraEditors.SimpleButton btnPaginaAnterior;
        private System.Windows.Forms.Panel MarcaPerdidas;
        private System.Windows.Forms.Panel marcaGanadas;
        private System.Windows.Forms.Panel marcaPendiente;
        private DevExpress.XtraEditors.ComboBoxEdit cboCantRegistros;
    }
}