﻿namespace VISTA.Negociacion
{
    partial class Negociacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Negociacion));
            this.txtRequerimiento = new DevExpress.XtraEditors.MemoEdit();
            this.txtrubro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtRazonSocial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtRuc = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.cboTMoneda = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.seMontoFacturado = new DevExpress.XtraEditors.SpinEdit();
            this.deFechaConversion = new DevExpress.XtraEditors.DateEdit();
            this.deFechaRegistro = new DevExpress.XtraEditors.DateEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lueetapa = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.semonto = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNombre = new DevExpress.XtraEditors.TextEdit();
            this.lueUsuario = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lueLinea = new DevExpress.XtraEditors.LookUpEdit();
            this.gcAutocompletarEmpresa = new DevExpress.XtraGrid.GridControl();
            this.gvAutocompletarEmpresa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.errorIcono = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcAutocompletarContacto = new DevExpress.XtraGrid.GridControl();
            this.gvAutocompletarContacto = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnBorrarContactos = new DevExpress.XtraEditors.SimpleButton();
            this.gcContactos = new DevExpress.XtraGrid.GridControl();
            this.gvContactos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtFijo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtMovil = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtCorreo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtContacto = new DevExpress.XtraEditors.TextEdit();
            this.btnAgregarContactos = new DevExpress.XtraEditors.SimpleButton();
            this.btnActividades = new DevExpress.XtraEditors.SimpleButton();
            this.btnHistorial = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.rcEtapas = new DevExpress.XtraEditors.RatingControl();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTMoneda.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seMontoFacturado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaConversion.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaConversion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaRegistro.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaRegistro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueetapa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.semonto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIcono)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcEtapas.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRequerimiento
            // 
            this.txtRequerimiento.Enabled = false;
            this.txtRequerimiento.Location = new System.Drawing.Point(154, 238);
            this.txtRequerimiento.Name = "txtRequerimiento";
            this.txtRequerimiento.Size = new System.Drawing.Size(300, 96);
            this.txtRequerimiento.TabIndex = 7;
            // 
            // txtrubro
            // 
            this.txtrubro.Enabled = false;
            this.txtrubro.Location = new System.Drawing.Point(132, 103);
            this.txtrubro.Name = "txtrubro";
            this.txtrubro.Size = new System.Drawing.Size(318, 20);
            this.txtrubro.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(11, 61);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 13);
            this.labelControl9.TabIndex = 25;
            this.labelControl9.Text = "Razon Social";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Enabled = false;
            this.txtRazonSocial.Location = new System.Drawing.Point(132, 58);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(318, 20);
            this.txtRazonSocial.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(11, 35);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(33, 13);
            this.labelControl10.TabIndex = 23;
            this.labelControl10.Text = "R.U.C.";
            // 
            // txtRuc
            // 
            this.txtRuc.Enabled = false;
            this.txtRuc.Location = new System.Drawing.Point(132, 32);
            this.txtRuc.Name = "txtRuc";
            this.txtRuc.Properties.MaxLength = 11;
            this.txtRuc.Size = new System.Drawing.Size(318, 20);
            this.txtRuc.TabIndex = 0;
            this.txtRuc.EditValueChanged += new System.EventHandler(this.txtRuc_EditValueChanged);
            this.txtRuc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRuc_KeyDown);
            this.txtRuc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRuc_KeyPress);
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(11, 84);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(147, 13);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "¿ A que se dedica la empresa ?";
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.labelControl25);
            this.groupControl5.Controls.Add(this.cboTMoneda);
            this.groupControl5.Controls.Add(this.labelControl24);
            this.groupControl5.Controls.Add(this.seMontoFacturado);
            this.groupControl5.Controls.Add(this.deFechaConversion);
            this.groupControl5.Controls.Add(this.deFechaRegistro);
            this.groupControl5.Controls.Add(this.labelControl16);
            this.groupControl5.Controls.Add(this.labelControl19);
            this.groupControl5.Controls.Add(this.labelControl18);
            this.groupControl5.Controls.Add(this.labelControl15);
            this.groupControl5.Controls.Add(this.lueetapa);
            this.groupControl5.Controls.Add(this.labelControl17);
            this.groupControl5.Controls.Add(this.labelControl12);
            this.groupControl5.Controls.Add(this.semonto);
            this.groupControl5.Controls.Add(this.labelControl14);
            this.groupControl5.Controls.Add(this.labelControl1);
            this.groupControl5.Controls.Add(this.txtNombre);
            this.groupControl5.Controls.Add(this.lueUsuario);
            this.groupControl5.Controls.Add(this.labelControl3);
            this.groupControl5.Controls.Add(this.labelControl2);
            this.groupControl5.Controls.Add(this.labelControl13);
            this.groupControl5.Controls.Add(this.txtRequerimiento);
            this.groupControl5.Controls.Add(this.lueLinea);
            this.groupControl5.Location = new System.Drawing.Point(21, 70);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(472, 411);
            this.groupControl5.TabIndex = 26;
            this.groupControl5.Text = "Datos";
            this.groupControl5.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl5_Paint);
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(11, 117);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(61, 13);
            this.labelControl25.TabIndex = 36;
            this.labelControl25.Text = "Tipo Moneda";
            // 
            // cboTMoneda
            // 
            this.cboTMoneda.EditValue = "SOLES";
            this.cboTMoneda.Enabled = false;
            this.cboTMoneda.Location = new System.Drawing.Point(154, 108);
            this.cboTMoneda.Name = "cboTMoneda";
            this.cboTMoneda.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTMoneda.Properties.Items.AddRange(new object[] {
            "SOLES",
            "DOLARES"});
            this.cboTMoneda.Size = new System.Drawing.Size(300, 20);
            this.cboTMoneda.TabIndex = 2;
            this.cboTMoneda.SelectedIndexChanged += new System.EventHandler(this.cboTMoneda_SelectedIndexChanged);
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(11, 70);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(82, 13);
            this.labelControl24.TabIndex = 35;
            this.labelControl24.Text = "Monto Facturado";
            // 
            // seMontoFacturado
            // 
            this.seMontoFacturado.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seMontoFacturado.Enabled = false;
            this.seMontoFacturado.Location = new System.Drawing.Point(154, 65);
            this.seMontoFacturado.Name = "seMontoFacturado";
            this.seMontoFacturado.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.seMontoFacturado.Properties.Appearance.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seMontoFacturado.Properties.Appearance.Options.UseBackColor = true;
            this.seMontoFacturado.Properties.Appearance.Options.UseFont = true;
            this.seMontoFacturado.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.seMontoFacturado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seMontoFacturado.Properties.DisplayFormat.FormatString = "S/ #.##";
            this.seMontoFacturado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.seMontoFacturado.Size = new System.Drawing.Size(300, 22);
            this.seMontoFacturado.TabIndex = 1;
            // 
            // deFechaConversion
            // 
            this.deFechaConversion.EditValue = null;
            this.deFechaConversion.Enabled = false;
            this.deFechaConversion.Location = new System.Drawing.Point(154, 186);
            this.deFechaConversion.Name = "deFechaConversion";
            this.deFechaConversion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaConversion.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaConversion.Properties.DisplayFormat.FormatString = "D";
            this.deFechaConversion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFechaConversion.Properties.EditFormat.FormatString = "D";
            this.deFechaConversion.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFechaConversion.Properties.Mask.EditMask = "D";
            this.deFechaConversion.Size = new System.Drawing.Size(300, 20);
            this.deFechaConversion.TabIndex = 5;
            // 
            // deFechaRegistro
            // 
            this.deFechaRegistro.EditValue = null;
            this.deFechaRegistro.Enabled = false;
            this.deFechaRegistro.Location = new System.Drawing.Point(154, 160);
            this.deFechaRegistro.Name = "deFechaRegistro";
            this.deFechaRegistro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaRegistro.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaRegistro.Properties.DisplayFormat.FormatString = "D";
            this.deFechaRegistro.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFechaRegistro.Properties.EditFormat.FormatString = "D";
            this.deFechaRegistro.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deFechaRegistro.Properties.Mask.EditMask = "D";
            this.deFechaRegistro.Size = new System.Drawing.Size(300, 20);
            this.deFechaRegistro.TabIndex = 4;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(144, 343);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(6, 13);
            this.labelControl16.TabIndex = 23;
            this.labelControl16.Text = "*";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(11, 189);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(86, 13);
            this.labelControl19.TabIndex = 30;
            this.labelControl19.Text = "Fecha Conversion";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(11, 163);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(72, 13);
            this.labelControl18.TabIndex = 29;
            this.labelControl18.Text = "Fecha Registro";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(144, 239);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 13);
            this.labelControl15.TabIndex = 22;
            this.labelControl15.Text = "*";
            // 
            // lueetapa
            // 
            this.lueetapa.Enabled = false;
            this.lueetapa.Location = new System.Drawing.Point(154, 134);
            this.lueetapa.Name = "lueetapa";
            this.lueetapa.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueetapa.Size = new System.Drawing.Size(300, 20);
            this.lueetapa.TabIndex = 3;
            this.lueetapa.EditValueChanged += new System.EventHandler(this.lueetapa_EditValueChanged);
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(11, 137);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(113, 13);
            this.labelControl17.TabIndex = 27;
            this.labelControl17.Text = "Etapa de la negociacion";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(144, 215);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 13);
            this.labelControl12.TabIndex = 21;
            this.labelControl12.Text = "*";
            // 
            // semonto
            // 
            this.semonto.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.semonto.Enabled = false;
            this.semonto.Location = new System.Drawing.Point(154, 23);
            this.semonto.Name = "semonto";
            this.semonto.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.semonto.Properties.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold);
            this.semonto.Properties.Appearance.Options.UseBackColor = true;
            this.semonto.Properties.Appearance.Options.UseFont = true;
            this.semonto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.semonto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.semonto.Properties.DisplayFormat.FormatString = "S/ #.##";
            this.semonto.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.semonto.Size = new System.Drawing.Size(300, 36);
            this.semonto.TabIndex = 0;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(11, 369);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(36, 13);
            this.labelControl14.TabIndex = 18;
            this.labelControl14.Text = "Usuario";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(80, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Monto y Moneda";
            // 
            // txtNombre
            // 
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(154, 212);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(300, 20);
            this.txtNombre.TabIndex = 6;
            // 
            // lueUsuario
            // 
            this.lueUsuario.Enabled = false;
            this.lueUsuario.Location = new System.Drawing.Point(154, 366);
            this.lueUsuario.Name = "lueUsuario";
            this.lueUsuario.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueUsuario.Size = new System.Drawing.Size(300, 20);
            this.lueUsuario.TabIndex = 9;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 239);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Requerimiento";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 219);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(126, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Nombre de la Oportunidad";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(11, 318);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(25, 13);
            this.labelControl13.TabIndex = 16;
            this.labelControl13.Text = "Linea";
            // 
            // lueLinea
            // 
            this.lueLinea.Enabled = false;
            this.lueLinea.Location = new System.Drawing.Point(154, 340);
            this.lueLinea.Name = "lueLinea";
            this.lueLinea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLinea.Size = new System.Drawing.Size(300, 20);
            this.lueLinea.TabIndex = 8;
            this.lueLinea.EditValueChanged += new System.EventHandler(this.lueLinea_EditValueChanged);
            // 
            // gcAutocompletarEmpresa
            // 
            this.gcAutocompletarEmpresa.Location = new System.Drawing.Point(132, 51);
            this.gcAutocompletarEmpresa.MainView = this.gvAutocompletarEmpresa;
            this.gcAutocompletarEmpresa.Name = "gcAutocompletarEmpresa";
            this.gcAutocompletarEmpresa.Size = new System.Drawing.Size(318, 96);
            this.gcAutocompletarEmpresa.TabIndex = 27;
            this.gcAutocompletarEmpresa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAutocompletarEmpresa});
            this.gcAutocompletarEmpresa.Visible = false;
            this.gcAutocompletarEmpresa.DoubleClick += new System.EventHandler(this.gcAutocompletarEmpresa_DoubleClick);
            this.gcAutocompletarEmpresa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gcAutocompletarEmpresa_KeyPress);
            // 
            // gvAutocompletarEmpresa
            // 
            this.gvAutocompletarEmpresa.GridControl = this.gcAutocompletarEmpresa;
            this.gvAutocompletarEmpresa.Name = "gvAutocompletarEmpresa";
            this.gvAutocompletarEmpresa.OptionsBehavior.Editable = false;
            this.gvAutocompletarEmpresa.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.gvAutocompletarEmpresa.OptionsView.ShowColumnHeaders = false;
            this.gvAutocompletarEmpresa.OptionsView.ShowGroupPanel = false;
            // 
            // errorIcono
            // 
            this.errorIcono.ContainerControl = this;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(765, 496);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(100, 40);
            this.btnGuardar.TabIndex = 0;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.btnActividades);
            this.groupControl1.Controls.Add(this.btnHistorial);
            this.groupControl1.Controls.Add(this.labelControl23);
            this.groupControl1.Controls.Add(this.labelControl22);
            this.groupControl1.Controls.Add(this.labelControl21);
            this.groupControl1.Controls.Add(this.labelControl20);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.rcEtapas);
            this.groupControl1.Controls.Add(this.groupControl5);
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnNuevo);
            this.groupControl1.Controls.Add(this.btnCancelar);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.btnEditar);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(991, 546);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "NEGOCIACION";
            this.groupControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.groupControl1_MouseDown);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gcAutocompletarContacto);
            this.groupControl2.Controls.Add(this.btnBorrarContactos);
            this.groupControl2.Controls.Add(this.gcContactos);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.txtFijo);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtMovil);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.txtCorreo);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.txtContacto);
            this.groupControl2.Controls.Add(this.btnAgregarContactos);
            this.groupControl2.Location = new System.Drawing.Point(499, 71);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(472, 267);
            this.groupControl2.TabIndex = 36;
            this.groupControl2.Text = "Contacto";
            // 
            // gcAutocompletarContacto
            // 
            this.gcAutocompletarContacto.Location = new System.Drawing.Point(132, 51);
            this.gcAutocompletarContacto.MainView = this.gvAutocompletarContacto;
            this.gcAutocompletarContacto.Name = "gcAutocompletarContacto";
            this.gcAutocompletarContacto.Size = new System.Drawing.Size(318, 96);
            this.gcAutocompletarContacto.TabIndex = 1;
            this.gcAutocompletarContacto.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAutocompletarContacto});
            this.gcAutocompletarContacto.Visible = false;
            this.gcAutocompletarContacto.DoubleClick += new System.EventHandler(this.gcAutocompletar_DoubleClick);
            this.gcAutocompletarContacto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gcAutocompletarContacto_KeyPress);
            // 
            // gvAutocompletarContacto
            // 
            this.gvAutocompletarContacto.GridControl = this.gcAutocompletarContacto;
            this.gvAutocompletarContacto.Name = "gvAutocompletarContacto";
            this.gvAutocompletarContacto.OptionsBehavior.Editable = false;
            this.gvAutocompletarContacto.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.gvAutocompletarContacto.OptionsView.ShowColumnHeaders = false;
            this.gvAutocompletarContacto.OptionsView.ShowGroupPanel = false;
            // 
            // btnBorrarContactos
            // 
            this.btnBorrarContactos.Enabled = false;
            this.btnBorrarContactos.ImageOptions.Image = global::VISTA.Properties.Resources.clearall_16x16;
            this.btnBorrarContactos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBorrarContactos.Location = new System.Drawing.Point(432, 110);
            this.btnBorrarContactos.Name = "btnBorrarContactos";
            this.btnBorrarContactos.Size = new System.Drawing.Size(18, 18);
            this.btnBorrarContactos.TabIndex = 26;
            this.btnBorrarContactos.Click += new System.EventHandler(this.btnBorrarContactos_Click);
            // 
            // gcContactos
            // 
            this.gcContactos.Enabled = false;
            this.gcContactos.Location = new System.Drawing.Point(5, 129);
            this.gcContactos.MainView = this.gvContactos;
            this.gcContactos.Name = "gcContactos";
            this.gcContactos.Size = new System.Drawing.Size(462, 133);
            this.gcContactos.TabIndex = 4;
            this.gcContactos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContactos});
            // 
            // gvContactos
            // 
            this.gvContactos.GridControl = this.gcContactos;
            this.gvContactos.Name = "gvContactos";
            this.gvContactos.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(280, 87);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(32, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "N° Fijo";
            // 
            // txtFijo
            // 
            this.txtFijo.Enabled = false;
            this.txtFijo.Location = new System.Drawing.Point(330, 84);
            this.txtFijo.Name = "txtFijo";
            this.txtFijo.Size = new System.Drawing.Size(120, 20);
            this.txtFijo.TabIndex = 3;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(11, 87);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 21;
            this.labelControl6.Text = "N° Movil";
            // 
            // txtMovil
            // 
            this.txtMovil.Enabled = false;
            this.txtMovil.Location = new System.Drawing.Point(132, 84);
            this.txtMovil.Name = "txtMovil";
            this.txtMovil.Size = new System.Drawing.Size(120, 20);
            this.txtMovil.TabIndex = 2;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(11, 61);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(88, 13);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Correo Electronico";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Enabled = false;
            this.txtCorreo.Location = new System.Drawing.Point(132, 58);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(318, 20);
            this.txtCorreo.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(11, 35);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Nombres";
            // 
            // txtContacto
            // 
            this.txtContacto.EditValue = "";
            this.txtContacto.Enabled = false;
            this.txtContacto.Location = new System.Drawing.Point(132, 32);
            this.txtContacto.Name = "txtContacto";
            this.txtContacto.Size = new System.Drawing.Size(318, 20);
            this.txtContacto.TabIndex = 0;
            this.txtContacto.EditValueChanged += new System.EventHandler(this.txtContacto_EditValueChanged);
            this.txtContacto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContacto_KeyDown);
            // 
            // btnAgregarContactos
            // 
            this.btnAgregarContactos.Enabled = false;
            this.btnAgregarContactos.ImageOptions.Image = global::VISTA.Properties.Resources.add_16x16;
            this.btnAgregarContactos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAgregarContactos.Location = new System.Drawing.Point(408, 110);
            this.btnAgregarContactos.Name = "btnAgregarContactos";
            this.btnAgregarContactos.Size = new System.Drawing.Size(18, 18);
            this.btnAgregarContactos.TabIndex = 27;
            this.btnAgregarContactos.Click += new System.EventHandler(this.btnAgregarContactos_Click);
            // 
            // btnActividades
            // 
            this.btnActividades.ImageOptions.Image = global::VISTA.Properties.Resources.chartsshowlegend_32x321;
            this.btnActividades.Location = new System.Drawing.Point(851, 25);
            this.btnActividades.Name = "btnActividades";
            this.btnActividades.Size = new System.Drawing.Size(120, 40);
            this.btnActividades.TabIndex = 35;
            this.btnActividades.Text = "ACTIVIDADES";
            this.btnActividades.Click += new System.EventHandler(this.btnActividades_Click);
            // 
            // btnHistorial
            // 
            this.btnHistorial.ImageOptions.Image = global::VISTA.Properties.Resources.historyitem_32x32;
            this.btnHistorial.Location = new System.Drawing.Point(510, 497);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(100, 40);
            this.btnHistorial.TabIndex = 2;
            this.btnHistorial.Text = "HISTORIAL";
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(446, 52);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(29, 13);
            this.labelControl23.TabIndex = 33;
            this.labelControl23.Text = "Cierre";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(350, 52);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(15, 13);
            this.labelControl22.TabIndex = 32;
            this.labelControl22.Text = "OC";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(217, 52);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(81, 13);
            this.labelControl21.TabIndex = 31;
            this.labelControl21.Text = "Pre Confirmacion";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(130, 52);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(57, 13);
            this.labelControl20.TabIndex = 30;
            this.labelControl20.Text = "Negociacion";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(33, 52);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(49, 13);
            this.labelControl11.TabIndex = 29;
            this.labelControl11.Text = "Cotizacion";
            // 
            // rcEtapas
            // 
            this.rcEtapas.EditValue = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.rcEtapas.Location = new System.Drawing.Point(21, 23);
            this.rcEtapas.Name = "rcEtapas";
            this.rcEtapas.Properties.CheckedGlyph = ((System.Drawing.Image)(resources.GetObject("rcEtapas.Properties.CheckedGlyph")));
            this.rcEtapas.Properties.FirstItemValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rcEtapas.Properties.Glyph = ((System.Drawing.Image)(resources.GetObject("rcEtapas.Properties.Glyph")));
            this.rcEtapas.Properties.HoverGlyph = ((System.Drawing.Image)(resources.GetObject("rcEtapas.Properties.HoverGlyph")));
            this.rcEtapas.Properties.ItemIndent = 30;
            this.rcEtapas.Rating = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.rcEtapas.Size = new System.Drawing.Size(474, 25);
            this.rcEtapas.TabIndex = 28;
            this.rcEtapas.Text = "ratingControl1";
            this.rcEtapas.EditValueChanged += new System.EventHandler(this.rcEtapas_EditValueChanged);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Enabled = false;
            this.btnNuevo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.ImageOptions.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(659, 497);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(100, 40);
            this.btnNuevo.TabIndex = 3;
            this.btnNuevo.Text = "NUEVO";
            this.btnNuevo.Visible = false;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.ImageOptions.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(871, 497);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 40);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "CERRAR";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gcAutocompletarEmpresa);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.txtrubro);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.txtRazonSocial);
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.txtRuc);
            this.groupControl3.Location = new System.Drawing.Point(499, 344);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(472, 137);
            this.groupControl3.TabIndex = 20;
            this.groupControl3.Text = "Datos de Empresa";
            // 
            // btnEditar
            // 
            this.btnEditar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.ImageOptions.Image")));
            this.btnEditar.Location = new System.Drawing.Point(765, 497);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(100, 40);
            this.btnEditar.TabIndex = 24;
            this.btnEditar.Text = "EDITAR";
            // 
            // Negociacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 546);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Negociacion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Negociacion";
            this.Load += new System.EventHandler(this.Negociacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTMoneda.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seMontoFacturado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaConversion.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaConversion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaRegistro.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaRegistro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueetapa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.semonto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombre.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIcono)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcEtapas.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.MemoEdit txtRequerimiento;
        public DevExpress.XtraEditors.TextEdit txtrubro;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        public DevExpress.XtraEditors.TextEdit txtRazonSocial;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.TextEdit txtRuc;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        public DevExpress.XtraEditors.LookUpEdit lueUsuario;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        public DevExpress.XtraEditors.LookUpEdit lueLinea;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.TextEdit txtNombre;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraGrid.GridControl gcAutocompletarEmpresa;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAutocompletarEmpresa;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorIcono;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        public DevExpress.XtraEditors.DateEdit deFechaRegistro;
        public DevExpress.XtraEditors.DateEdit deFechaConversion;
        public DevExpress.XtraEditors.LabelControl labelControl8;
        public DevExpress.XtraEditors.SpinEdit semonto;
        public DevExpress.XtraEditors.LookUpEdit lueetapa;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        public DevExpress.XtraEditors.RatingControl rcEtapas;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        public DevExpress.XtraEditors.SpinEdit seMontoFacturado;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.ComboBoxEdit cboTMoneda;
        private DevExpress.XtraEditors.SimpleButton btnHistorial;
        private DevExpress.XtraEditors.SimpleButton btnActividades;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        public DevExpress.XtraGrid.GridControl gcAutocompletarContacto;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAutocompletarContacto;
        private DevExpress.XtraEditors.SimpleButton btnBorrarContactos;
        public DevExpress.XtraGrid.GridControl gcContactos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContactos;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.TextEdit txtFijo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        public DevExpress.XtraEditors.TextEdit txtMovil;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.TextEdit txtCorreo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.TextEdit txtContacto;
        private DevExpress.XtraEditors.SimpleButton btnAgregarContactos;
    }
}