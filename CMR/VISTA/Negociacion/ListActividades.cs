﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;
using DevExpress.XtraGrid.Views.Grid;

namespace VISTA.Negociacion
{
    public partial class ListActividades : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public int IdNegociacion = 0;
        public int Opcion = 0;
        public int IdUsuario = 0;
        public int EstadoNegociacion = 0;

        public ListActividades()
        {
            InitializeComponent();
        }
        public void setActualizar()
        {
            gcActividades.DataSource = MActividad.Mostrar("", 0, this.IdNegociacion, 3);
            gvActividades.Columns[0].Visible = false;
            gvActividades.Columns[1].Visible = false;
            gvActividades.Columns[4].Visible = false;
            gvActividades.Columns[6].Visible = false;
            gvActividades.Columns[8].Visible = false;
            gvActividades.Columns[9].Visible = false;
            gvActividades.Columns[10].Visible = false;
            gvActividades.Columns[7].DisplayFormat.FormatString = "hh:mm:ss";
        }

        private void ListActividades_Load(object sender, EventArgs e)
        {
            gcActividades.DataSource = MActividad.Mostrar("", 0, this.IdNegociacion, 3);
            gvActividades.Columns[0].Visible = false;
            gvActividades.Columns[1].Visible = false;
            gvActividades.Columns[4].Visible = false;
            gvActividades.Columns[6].Visible = false;
            gvActividades.Columns[8].Visible = false;
            gvActividades.Columns[9].Visible = false;
            gvActividades.Columns[10].Visible = false;
            gvActividades.Columns[7].DisplayFormat.FormatString = "hh:mm:ss";

            if (this.EstadoNegociacion == 0)
            {
                btnAgregar.Enabled = true;
                btnActualizar.Enabled = true;
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void gcActividades_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (this.IdUsuario == Convert.ToInt32(gvActividades.GetFocusedRowCellValue("idusuario").ToString()))
                {
                    AgregarActividad agregarActividad = new AgregarActividad();
                    agregarActividad.IdUsuario = Convert.ToInt32(gvActividades.GetFocusedRowCellValue("idusuario").ToString());
                    agregarActividad.deFecha.Text = gvActividades.GetFocusedRowCellValue("fecha").ToString().Substring(0, 10);
                    agregarActividad.teHora.Time = Convert.ToDateTime(gvActividades.GetFocusedRowCellValue("hora").ToString());//.Substring(11, 8)
                    agregarActividad.txtAsunto.Text = gvActividades.GetFocusedRowCellValue("asunto").ToString();
                    agregarActividad.txtDescripcion.Text = gvActividades.GetFocusedRowCellValue("descripcion").ToString();
                    agregarActividad.IdNegociacion = Convert.ToInt32(gvActividades.GetFocusedRowCellValue("idnegociacion").ToString());
                    agregarActividad.IdActividad = Convert.ToInt32(gvActividades.GetFocusedRowCellValue("id").ToString());
                    agregarActividad.setLookUpEdit(gvActividades.GetFocusedRowCellValue("TActividad").ToString());
                    agregarActividad.Estado = Convert.ToInt32(gvActividades.GetFocusedRowCellValue("estado").ToString());
                    agregarActividad.VarEditar = true;
                    agregarActividad.VarNuevo = false;
                    agregarActividad.EstadoNegociacion = EstadoNegociacion;
                    AddOwnedForm(agregarActividad);
                    agregarActividad.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Actividad no asignada", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {

            }
            
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarActividad actividad = new AgregarActividad();
            actividad.IdNegociacion = IdNegociacion;
            actividad.IdUsuario = IdUsuario; 
            actividad.IdActividadAnterior = EstadoNegociacion;
            AddOwnedForm(actividad);
            actividad.Show();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            gcActividades.DataSource = MActividad.Mostrar("", 0, this.IdNegociacion, 3);
            gvActividades.Columns[0].Visible = false;
            gvActividades.Columns[1].Visible = false;
            gvActividades.Columns[4].Visible = false;
            gvActividades.Columns[6].Visible = false;
            gvActividades.Columns[8].Visible = false;
            gvActividades.Columns[9].Visible = false;
            gvActividades.Columns[10].Visible = false;
            gvActividades.Columns[7].DisplayFormat.FormatString = "hh:mm:ss";
        }

        private void gvActividades_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            GridView currentView = sender as GridView;
                e.Appearance.ForeColor = Color.Blue;
                int estadoAct = Convert.ToInt32(currentView.GetRowCellValue(e.RowHandle, "estado"));
                if (estadoAct == 1)
                {
                        e.Appearance.ForeColor = Color.Gray;
                    
                }

                
                DateTime date = Convert.ToDateTime(currentView.GetRowCellValue(e.RowHandle, "fecha"));
                TimeSpan dias = date.Subtract(DateTime.Today);
                if (estadoAct == 0)
                {
                    if (dias.Days <= 2)
                    {
                        e.Appearance.ForeColor = Color.FromArgb(225, 228, 31);
                    }

                    if (0 >= dias.Days)
                    {
                        e.Appearance.ForeColor = Color.Red;
                    }

                    if (dias.Days > 2)
                    {
                        e.Appearance.ForeColor = Color.Blue;
                    }
                }
                if (estadoAct == 1)
                {
                    e.Appearance.ForeColor = Color.Gray;
                }

            
        }
    }
}