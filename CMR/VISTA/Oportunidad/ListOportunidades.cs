﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using MODELO;
using VISTA.Properties;
using DevExpress.XtraGrid.Views.Grid;

namespace VISTA.Procesos
{
    public partial class ListOportunidades : DevExpress.XtraEditors.XtraForm
    {
        DataTable dtPendientes;
        DataTable dtCerrados;
        DataTable dtNoUtil;
        DataTable dtFiltroPendientes;
        DataTable dtFiltroCerrados;
        DataTable dtFiltroNoUtil;

        DataTable dtDatosDePaginacion;

        public int MaxAsignado = 0;
        private int Asignado = 0;
        public int IdUsuario = 0;
        public int SuperLinea = 0;
        public int IdLinea = 0;

        private int PaginaMaximaPendientes = 1;
        private int PaginaMaximaConvertidos = 1;
        private int PaginaMaximaNoUtil = 1;
        bool Pendiente = false;
        bool Convertidos = false;
        bool NoUtil = false;

        public ListOportunidades()
        {
            InitializeComponent();
        }

        public void setActualizar()
        {

            if (this.SuperLinea == 0)
            {
                if (Pendiente)
                {
                    MostrarOportunidades("", IdUsuario, 1, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = true;
                    marcaConvertidos.Visible = false;
                    MarcaNoUtil.Visible = false;
                    //////////
                }

                if (Convertidos)
                {
                    MostrarOportunidades("", IdUsuario, 1, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaConvertidos.Visible = true;
                    MarcaNoUtil.Visible = false;
                    //////////
                }

                if (NoUtil)
                {
                    MostrarOportunidades("", IdUsuario, 1, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaConvertidos.Visible = false;
                    MarcaNoUtil.Visible = true;
                    //////////
                }

                /////////////// Boton Pendiente ////////////
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                /////////////// Boton Cerrados ////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            else if (this.SuperLinea == 1 || SuperLinea == 3)
            {
                if (Pendiente)
                {
                    MostrarOportunidades("", IdUsuario, 2, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = true;
                    marcaConvertidos.Visible = false;
                    MarcaNoUtil.Visible = false;
                    //////////
                }

                if (Convertidos)
                {
                    MostrarOportunidades("", IdUsuario, 2, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaConvertidos.Visible = true;
                    MarcaNoUtil.Visible = false;
                    //////////
                }

                if (NoUtil)
                {
                    MostrarOportunidades("", IdUsuario, 2, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                    //////////MARCAS//
                    marcaPendiente.Visible = false;
                    marcaConvertidos.Visible = false;
                    MarcaNoUtil.Visible = true;
                    //////////
                }
                
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                /////////////// Boton Cerrados //////////////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ///////////////////////
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "CONVERTIDOS " + RConvertidos["CONVERTIDOS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASCONVERTIDOS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "NO UTIL " + RNoUtil["NOUTIL"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASNOUTIL"].ToString());

        }

        public void SetDatosUsuario(int id, int superlinea, int idlinea, int maxasignado)
        {
            this.IdUsuario = id;
            this.SuperLinea = superlinea;
            this.IdLinea = idlinea;
            this.MaxAsignado = maxasignado;
        }

        private void OcultarColumnas()
        {
            this.gvOportunidades.Columns[0].Visible = false;
            this.gvOportunidades.Columns[1].Visible = false;
            this.gvOportunidades.Columns[7].Visible = false;
            this.gvOportunidades.Columns[8].Visible = false;
            this.gvOportunidades.Columns[9].Visible = false;
            this.gvOportunidades.Columns[10].Visible = false;
            this.gvOportunidades.Columns[11].Visible = false;
            this.gvOportunidades.Columns[12].Visible = false;
            this.gvOportunidades.Columns[14].Visible = false;
            this.gvOportunidades.Columns[15].Visible = false;
            this.gvOportunidades.Columns[16].Visible = false;
            this.gvOportunidades.Columns[17].Visible = false;
            gvOportunidades.Columns[2].OptionsColumn.AllowEdit = false;
            gvOportunidades.Columns[3].OptionsColumn.AllowEdit = false;
            gvOportunidades.Columns[4].OptionsColumn.AllowEdit = false;
            gvOportunidades.Columns[5].OptionsColumn.AllowEdit = false;
            gvOportunidades.Columns[6].OptionsColumn.AllowEdit = false;
            gvOportunidades.Columns[13].OptionsColumn.AllowEdit = false;
        }

        public void MostrarOportunidades(string Texto, int idusuario, int opcion, int estado, int idlinea, int tamanopagina, int paginaactual)
        {
            this.gcOportunidades.DataSource = MOportunidad.Mostrar(Texto, 
                                                                   idusuario, 
                                                                   opcion, 
                                                                   idlinea,
                                                                   tamanopagina,
                                                                   paginaactual,
                                                                   estado);
            this.OcultarColumnas();
        }

        int ValidarDecimal(string dato)
        {
            decimal d = Convert.ToDecimal(dato);
            int entero = Convert.ToInt32(d);

            if (d > entero)
            {
                entero = entero + 1;
            }

            return entero;
        }

        private void ListOportunidades_Load(object sender, EventArgs e)
        {
            this.Pendiente = true;
            this.Convertidos = false;
            this.NoUtil = false;

            if (this.SuperLinea == 0)
            {
                MostrarOportunidades("", IdUsuario, 1, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Pendiente ////////////
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                /////////////// Boton Cerrados ////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ////////////
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            else if (this.SuperLinea == 1 || this.SuperLinea == 3)
            {
                MostrarOportunidades("", IdUsuario, 2, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Pendiente /////////////////////
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                /////////////// Boton Cerrados //////////////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                /////////////// Boton No Util ///////////////////////
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "CONVERTIDOS " + RConvertidos["CONVERTIDOS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASCONVERTIDOS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "NO UTIL " + RNoUtil["NOUTIL"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASNOUTIL"].ToString());



            ///////////////// Creacion de Columnas para las acciones /////////////
            GridColumn ColumnConvertir = gvOportunidades.Columns.AddVisible("Convertir", string.Empty);
            ColumnConvertir.AppearanceCell.BackColor = Color.LightGray;
            ColumnConvertir.ColumnEdit = btnConvertir;
            //ColumnConvertir.OptionsColumn.AllowEdit = false;
            ColumnConvertir.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            /////////////////////////  BUTTON 2 //////////////////////7777

            GridColumn ColumnVer = gvOportunidades.Columns.AddVisible("Solicitar", string.Empty);
            //ColumnVer.AppearanceCell.BackColor = Color.Red;
            ColumnVer.ColumnEdit = btnSolicitar;
            ColumnVer.Caption = "Solicitar";
            //btnConvertir.Appearance.BackColor = Color.Red;
            ColumnVer.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;

            gvOportunidades.Columns[18].OptionsColumn.AllowEdit = true;

            
            
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Procesos.RegOportunidad oportunidad = new Procesos.RegOportunidad();
            oportunidad.IdLinea = IdLinea;
            AddOwnedForm(oportunidad);
            oportunidad.Show();

            //gvOportunidades_RowCellStyle(Nothing, Nothing);
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            string Rpta = "";

            if (this.IdUsuario == Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString()))
            {
                var a = gvOportunidades.GetFocusedRowCellValue("contactos").ToString();
                var b = gvOportunidades.GetFocusedRowCellValue("razon_social").ToString();
                var c = gvOportunidades.GetFocusedRowCellValue("ruc").ToString();
                //gvOportunidades.GetFocusedRowCellValue("contactos").ToString() == "0" ||
                if ((gvOportunidades.GetFocusedRowCellValue("razon_social").ToString() == "" ||
                gvOportunidades.GetFocusedRowCellValue("ruc").ToString() == ""))
                {
                    MessageBox.Show("No puedes convertir la Oportunidad ya que algunos datos importantes estan incompletos", "Sistema DCGTEC", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                }
                else
                {
                    //MessageBox.Show(Convert.ToString(gvOportunidades.GetFocusedRowCellValue("id")));
                    Class.MsgBoxUtil msgBoxUtil = new Class.MsgBoxUtil();
                    msgBoxUtil.HackMessageBox("Util", "No Util", "Cancelar");

                    DialogResult resul = MessageBox.Show("¿ Deseas convertir la Oportunidad en Prospecto UTIL o NO UTIL ?", "Sistema DCGTEC", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (resul == DialogResult.Yes)
                    {
                        Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()),
                                    0,
                                    "",
                                    "",
                                    0,
                                    DateTime.Today,
                                    0,
                                    0,
                                    1,
                                    3);

                        
                        if (Convert.ToInt32(Rpta) > 0)
                        {
                            Rpta = MNegociaciones.MantNegociacion(0,
                                                       Convert.ToDateTime(gvOportunidades.GetFocusedRowCellValue("fecha").ToString()),
                                                       DateTime.Today,
                                                       gvOportunidades.GetFocusedRowCellValue("nombre").ToString(),
                                                       Convert.ToDouble(0),
                                                       0,
                                                       0,
                                                       0,
                                                       IdUsuario,
                                                       IdLinea,
                                                       0,
                                                       Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idempresa").ToString()),
                                                       gvOportunidades.GetFocusedRowCellValue("requerimiento").ToString(),
                                                       Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()),
                                                       1);

                            //// GUARDAR CONTACTOS /////////////////////
                            MContactos.MantEmpresaContacto(Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idempresa").ToString()), Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()), Convert.ToInt32(Rpta), 4);

                            //Principal principal = new  Principal();
                            //principal.AbrirOportunidades();
                            

                        }
                        else
                        {
                            MessageBox.Show("Ocurrio un error al intentar convertir la oportunidad, porfavor vuelva a intentarlo", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                        if (Convert.ToInt32(Rpta) > 0)
                        {
                            if (MessageBox.Show("La Oportunidad a sido convertida con exito", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                            {
                                //Principal principal = new Principal();
                                //Negociacion.LisNegociaciones Formulario;
                                //Formulario = principal.PanelContenedor.Controls.OfType<Negociacion.LisNegociaciones>().FirstOrDefault();//Busca en la Conecion el Form

                                //if (Formulario == null)
                                //{
                                //    Formulario = new Negociacion.LisNegociaciones();
                                //    Formulario.TopLevel = false;
                                //    principal.PanelContenedor.Controls.Add(Formulario);
                                //    principal.PanelContenedor.Tag = Formulario;
                                //    Formulario.SetDatosUsuario(this.IdUsuario, this.SuperLinea, this.IdLinea);
                                //    //Formulario.MdiParent = this;
                                //    Formulario.Show();
                                //    Formulario.BringToFront();
                                //}
                                //else
                                //{
                                //    Formulario.BringToFront();
                                //    Formulario.setActualizar();
                                //}
                            }
                        }
                        else
                        {
                            MessageBox.Show("Ocurrio un error al intentar convertir la oportunidad, porfavor vuelva a intentarlo", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    else if (resul == DialogResult.No)
                    {
                        Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()),
                                    0,
                                    "",
                                    "",
                                    0,
                                    DateTime.Today,
                                    0,
                                    0,
                                    2,
                                    3);
                    }
                    this.setActualizar();
                }
                
            }
            else
            {
                MessageBox.Show("Oportunidad no asignada", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            
        }

        private void btnPendiente_Click(object sender, EventArgs e)
        {
            this.Pendiente = true;
            this.Convertidos = false;
            this.NoUtil = false;

            //////////MARCAS//
            marcaPendiente.Visible = true;
            marcaConvertidos.Visible = false;
            MarcaNoUtil.Visible = false;
            //////////

            if (SuperLinea == 0)
            {
                MostrarOportunidades("", IdUsuario, 1, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Pendiente ////////////
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
            }
            else if (SuperLinea == 1 || SuperLinea == 3)
            {
                MostrarOportunidades("", IdUsuario, 2, 0, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Pendiente /////////////////////
                dtPendientes = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
                dtFiltroPendientes = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 0);
            }

            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "CONVERTIDOS " + RConvertidos["CONVERTIDOS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASCONVERTIDOS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "NO UTIL " + RNoUtil["NOUTIL"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASNOUTIL"].ToString());

            txtpaginacion.Text = "1";
        }

        private void btnCerrado_Click(object sender, EventArgs e)
        {
            this.Pendiente = false;
            this.Convertidos = true;
            this.NoUtil = false;

            //////////MARCAS//
            marcaPendiente.Visible = false;
            marcaConvertidos.Visible = true;
            MarcaNoUtil.Visible = false;
            //////////

            if (SuperLinea == 0)
            {
                MostrarOportunidades("", IdUsuario, 1, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Cerrados ////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

            }
            else if (SuperLinea == 1 || SuperLinea == 3)
            {
                MostrarOportunidades("", IdUsuario, 2, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                /////////////// Boton Cerrados //////////////////////
                dtCerrados = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);
                dtFiltroCerrados = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 1);

            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "CONVERTIDOS " + RConvertidos["CONVERTIDOS"];
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "NO UTIL " + RNoUtil["NOUTIL"];

            txtpaginacion.Text = "1";
        }

        private void btnNoUtil_Click(object sender, EventArgs e)
        {
            this.Pendiente = false;
            this.Convertidos = false;
            this.NoUtil = true;

            //////////MARCAS//
            marcaPendiente.Visible = false;
            marcaConvertidos.Visible = false;
            MarcaNoUtil.Visible = true;
            //////////

            if (SuperLinea == 0)
            {
                MostrarOportunidades("", IdUsuario, 1, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 1, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            else if (SuperLinea == 1 || SuperLinea == 3)
            {
                MostrarOportunidades("", IdUsuario, 2, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text));
                dtNoUtil = MOportunidad.Mostrar("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
                dtFiltroNoUtil = MOportunidad.MostrarDatosDePaginacion("", IdUsuario, 2, IdLinea, Convert.ToInt32(cboCantRegistros.Text), Convert.ToInt32(txtpaginacion.Text), 2);
            }
            /////////////// Boton Pendiente ////////////
            DataRow RPendientes = dtFiltroPendientes.Rows[0];
            btnPendiente.Text = "PENDIENTES " + RPendientes["PENDIENTES"];
            PaginaMaximaPendientes = ValidarDecimal(RPendientes["PAGINASPENDIENTES"].ToString());
            /////////////// Boton Cerrados ////////////
            DataRow RConvertidos = dtFiltroCerrados.Rows[0];
            btnCerrado.Text = "CONVERTIDOS " + RConvertidos["CONVERTIDOS"];
            PaginaMaximaConvertidos = ValidarDecimal(RConvertidos["PAGINASCONVERTIDOS"].ToString());
            /////////////// Boton No Util ////////////
            DataRow RNoUtil = dtFiltroNoUtil.Rows[0];
            btnNoUtil.Text = "NO UTIL " + RNoUtil["NOUTIL"];
            PaginaMaximaNoUtil = ValidarDecimal(RNoUtil["PAGINASNOUTIL"].ToString());

            txtpaginacion.Text = "1";
        }

        private void gcOportunidades_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                var estado = gvOportunidades.GetFocusedRowCellValue("estado").ToString();
                if (gvOportunidades.GetFocusedRowCellValue("estado").ToString() != "3")
                {
                    if (this.IdUsuario == Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString()))
                    {
                        RegOportunidad regOportunidad = new RegOportunidad();
                        regOportunidad.IdOportunidad = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString());
                        regOportunidad.txtNomOportunidad.Text = gvOportunidades.GetFocusedRowCellValue("nombre").ToString();
                        regOportunidad.txtRequerimiento.Text = gvOportunidades.GetFocusedRowCellValue("requerimiento").ToString();
                        //regOportunidad.txtContacto.Text = gvOportunidades.GetFocusedRowCellValue("contacto").ToString();
                        //regOportunidad.txtCorreo.Text = gvOportunidades.GetFocusedRowCellValue("correo").ToString();
                        //regOportunidad.txtMovil.Text = gvOportunidades.GetFocusedRowCellValue("movil").ToString();
                        //regOportunidad.txtFijo.Text = gvOportunidades.GetFocusedRowCellValue("fijo").ToString();
                        //regOportunidad.txtMovil.Text = gvOportunidades.GetFocusedRowCellValue("movil").ToString();
                        regOportunidad.txtRuc.Text = gvOportunidades.GetFocusedRowCellValue("ruc").ToString();
                        regOportunidad.txtRazonSocial.Text = gvOportunidades.GetFocusedRowCellValue("razon_social").ToString();
                        regOportunidad.txtrubro.Text = gvOportunidades.GetFocusedRowCellValue("rubro_empresa").ToString();
                        regOportunidad.setLookUpEdit(gvOportunidades.GetFocusedRowCellValue("tcomunicacion").ToString(),
                                                     gvOportunidades.GetFocusedRowCellValue("linea").ToString(),
                                                     gvOportunidades.GetFocusedRowCellValue("usuario").ToString());
                        regOportunidad.IdUsuario = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString());
                        //if (gvOportunidades.GetFocusedRowCellValue("idcontacto").ToString() != "")
                        //{
                        //    regOportunidad.IdContacto = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idcontacto").ToString());
                        //}
                        if (gvOportunidades.GetFocusedRowCellValue("idempresa").ToString() != "")
                        {
                            regOportunidad.IdEmpresa = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idempresa").ToString());

                        }

                        regOportunidad.OpcionListContactos = 1;
                        regOportunidad.IdEmpresaContacto = gvOportunidades.GetFocusedRowCellValue("idempresa").ToString();
                        regOportunidad.VarEditar = true;
                        regOportunidad.VarNuevo = false;
                        regOportunidad.gcAutocompletarContacto.Visible = false;
                        regOportunidad.gcAutocompletarEmpresa.Visible = false;
                        regOportunidad.nuevoform = false;
                        AddOwnedForm(regOportunidad);
                        regOportunidad.ShowDialog();
                    }
                    else if (this.SuperLinea == 1)
                    {
                        RegOportunidad regOportunidad = new RegOportunidad();
                        regOportunidad.IdOportunidad = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString());
                        regOportunidad.txtNomOportunidad.Text = gvOportunidades.GetFocusedRowCellValue("nombre").ToString();
                        regOportunidad.txtRequerimiento.Text = gvOportunidades.GetFocusedRowCellValue("requerimiento").ToString();
                        //regOportunidad.txtContacto.Text = gvOportunidades.GetFocusedRowCellValue("contacto").ToString();
                        //regOportunidad.txtCorreo.Text = gvOportunidades.GetFocusedRowCellValue("correo").ToString();
                        //regOportunidad.txtMovil.Text = gvOportunidades.GetFocusedRowCellValue("movil").ToString();
                        //regOportunidad.txtFijo.Text = gvOportunidades.GetFocusedRowCellValue("fijo").ToString();
                        //regOportunidad.txtMovil.Text = gvOportunidades.GetFocusedRowCellValue("movil").ToString();
                        regOportunidad.txtRuc.Text = gvOportunidades.GetFocusedRowCellValue("ruc").ToString();
                        regOportunidad.txtRazonSocial.Text = gvOportunidades.GetFocusedRowCellValue("razon_social").ToString();
                        regOportunidad.txtrubro.Text = gvOportunidades.GetFocusedRowCellValue("rubro_empresa").ToString();
                        regOportunidad.setLookUpEdit(gvOportunidades.GetFocusedRowCellValue("tcomunicacion").ToString(),
                                                     gvOportunidades.GetFocusedRowCellValue("linea").ToString(),
                                                     gvOportunidades.GetFocusedRowCellValue("usuario").ToString());
                        regOportunidad.IdUsuario = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString());
                        //if (gvOportunidades.GetFocusedRowCellValue("idcontacto").ToString() != "")
                        //{
                        //    regOportunidad.IdContacto = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idcontacto").ToString());
                        //}
                        if (gvOportunidades.GetFocusedRowCellValue("idempresa").ToString() != "")
                        {
                            regOportunidad.IdEmpresa = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idempresa").ToString());

                        }

                        regOportunidad.OpcionListContactos = 1;
                        regOportunidad.IdEmpresaContacto = gvOportunidades.GetFocusedRowCellValue("idempresa").ToString();
                        regOportunidad.VarEditar = true;
                        regOportunidad.VarNuevo = false;
                        regOportunidad.gcAutocompletarContacto.Visible = false;
                        regOportunidad.gcAutocompletarEmpresa.Visible = false;
                        regOportunidad.nuevoform = false;
                        AddOwnedForm(regOportunidad);
                        regOportunidad.ShowDialog();
                    }
                }            
            }
            catch (Exception )
            {
                
            }
           
        }

        private void gvOportunidades_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
        //    GridView currentView = sender as GridView;
        //    if (e.Column.FieldName == "Solicitar")
        //    {
        //        bool value = Convert.ToBoolean(currentView.GetRowCellValue(e.RowHandle, "solicitado"));
        //        if (value)
        //        {
        //            e.Appearance.BackColor = Color.Red;
        //            e.Column.OptionsColumn.AllowEdit = false;
        //        }
        //    }
        
        }

        private void btnSolicitar_Click(object sender, EventArgs e)
        { 
            
        }

        private void gvOportunidades_ShowingEditor(object sender, CancelEventArgs e)
        {
            //MessageBox.Show("DEMO");
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            setActualizar();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnver_solicitudes_Click(object sender, EventArgs e)
        {

        }

        private void gvOportunidades_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            string esta = Convert.ToString(gvOportunidades.GetRowCellValue(e.RowHandle, "estado"));
            if (esta == "0")
            {
                if (this.SuperLinea == 1)
                {
                    if (e.Column.Caption == "Solicitar")
                    {

                        MOportunidad oportunidad = new MOportunidad();
                        gcSolicitudes.DataSource = oportunidad.MostrarSolicitudes(
                                Convert.ToInt32(gvOportunidades.GetRowCellValue(e.RowHandle, "id")),
                                Convert.ToInt32(gvOportunidades.GetRowCellValue(e.RowHandle, "idusuario")));


                        if (gvSolicitudes.RowCount == 0)
                        {
                            RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                            ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                            ritem.ReadOnly = true;
                            ritem.Buttons[0].Visible = false;
                            e.RepositoryItem = ritem;
                            btnSolicitar.Buttons[0].ImageOptions.Image = VISTA.Properties.Resources.show_16x16;
                        }
                    }

                }
                else if (this.SuperLinea == 0)
                {
                    if (e.Column.Caption == "Solicitar")
                    {
                        string asignado = Convert.ToString(gvOportunidades.GetRowCellValue(e.RowHandle, "usuario"));
                        bool val = Convert.ToBoolean(gvOportunidades.GetRowCellValue(e.RowHandle, "solicitado"));
                        if (val || asignado != "")
                        {
                            RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                            ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                            ritem.ReadOnly = true;
                            ritem.Buttons[0].Visible = false;
                            //ritem.Buttons[1].Visible = true;
                            //ritem.NullText = "SOLICITADO";
                            e.RepositoryItem = ritem;
                        }
                    }
                }

                if (e.Column.FieldName == "Convertir")
                {
                    int value = Convert.ToInt32(gvOportunidades.GetRowCellValue(e.RowHandle, "idusuario"));
                    if (value != IdUsuario)
                    {
                        RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                        ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                        ritem.ReadOnly = true;
                        ritem.Buttons[0].Visible = false;
                        e.RepositoryItem = ritem;
                    }

                }
            }
            else
            {
                if (e.Column.Caption == "Solicitar")
                {
                        RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                        ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                        ritem.ReadOnly = true;
                        ritem.Buttons[0].Visible = false;
                        e.RepositoryItem = ritem;
                    
                }

                if (e.Column.FieldName == "Convertir")
                {
                        RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                        ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                        ritem.ReadOnly = true;
                        ritem.Buttons[0].Visible = false;
                        e.RepositoryItem = ritem;
                    

                }
            }


            this.Asignado = ContarAsignados();
        }

        private int ContarAsignados()
        {
            int i = 0;
            int d = 0;

            try
            {
                
                foreach (DataRow r in dtPendientes.Rows)
                {
                    d = Convert.ToInt32(r["idusuario"]);
                    if (d > 0)
                    {
                        i++;
                    }
                }
            }
            catch (Exception)
            {

            }
            

            return i;
        }

        private void gcOportunidades_Click(object sender, EventArgs e)
        {
            try
            {
                //MOportunidad oportunidad = new MOportunidad();
                //gcSolicitudes.DataSource = oportunidad.MostrarSolicitudes(
                //    Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()),
                //    Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString()));
            }
            catch (Exception)
            {
            }
            
        }

        private void btnSolicitar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Caption == "ACTION")
            {
                string Rpta = "";

                if (this.SuperLinea == 1)
                {
                    LisSolicitudes solicitudes = new LisSolicitudes();
                    solicitudes.IdOportunidad = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString());
                    solicitudes.IdUsuario = Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("idusuario").ToString());
                    AddOwnedForm(solicitudes);
                    solicitudes.ShowDialog();
                }
                else
                {
                    if (this.Asignado >= this.MaxAsignado)
                    {
                        MessageBox.Show("No puedes solicitar mas Oportunidades: " + "\n" + "Has llegado al limite de asignaciones", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (Convert.ToBoolean(gvOportunidades.GetFocusedRowCellValue("solicitado").ToString()) == false)
                        {
                            MOportunidad oportunidad = new MOportunidad();
                            Rpta = oportunidad.SolicitarOportunidad(Convert.ToInt32(gvOportunidades.GetFocusedRowCellValue("id").ToString()),
                                                            this.IdUsuario,
                                                            1);

                            if (Rpta == "OK")
                            {
                                //MessageBox.Show("Solicitud Correcta", "Sistema DCGTEC");
                                MessageBox.Show("Solicitud Correcta", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                setActualizar();
                            }
                            else
                            {
                                //MessageBox.Show("Error en la solicitud: " + Rpta, "Sistema DCGTEC");
                                MessageBox.Show("Error en la solicitud: " + Rpta, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Oportunidad ya solicitada", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    
                }
            }
            
        }

        private void txtpaginacion_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.setActualizar();
            }
        }

        private void cboCantRegistros_SelectedIndexChanged(object sender, EventArgs e)
        {
            setActualizar();
        }

        private void btnPaginaAnterior_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtpaginacion.Text) > 1)
            {
                txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) - 1);
            }
            setActualizar();
        }

        private void btnPaginaSiguiente_Click(object sender, EventArgs e)
        {
            if (this.Pendiente)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) < PaginaMaximaPendientes)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }

            if (this.Convertidos)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) < PaginaMaximaConvertidos)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }

            if (this.NoUtil)
            {
                if (Convert.ToInt32(txtpaginacion.Text) >= 0)
                {
                    if (Convert.ToInt32(txtpaginacion.Text) < PaginaMaximaNoUtil)
                    {
                        txtpaginacion.Text = Convert.ToString(Convert.ToInt32(txtpaginacion.Text) + 1);
                    }
                }
            }

            
            setActualizar();
        }

        private void btnPrimeraPagina_Click(object sender, EventArgs e)
        {
            txtpaginacion.Text = "1";
            setActualizar();
        }

        private void btnUltimaPagina_Click(object sender, EventArgs e)
        {
            if (this.Pendiente)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaPendientes);
            }

            if (this.Convertidos)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaConvertidos);
            }

            if (this.NoUtil)
            {
                txtpaginacion.Text = Convert.ToString(PaginaMaximaNoUtil);
            }
        }

        private void txtpaginacion_EditValueChanged(object sender, EventArgs e)
        {
            setActualizar();
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {

        }
    }
}