﻿namespace VISTA.Procesos
{
    partial class LisSolicitudes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LisSolicitudes));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gcSolicitudes = new DevExpress.XtraGrid.GridControl();
            this.gvSolicitudes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAsignar = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itemAsignar = new System.Windows.Forms.ToolStripMenuItem();
            this.itemBorrar = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAsignar)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gcSolicitudes
            // 
            this.gcSolicitudes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSolicitudes.Location = new System.Drawing.Point(0, 0);
            this.gcSolicitudes.MainView = this.gvSolicitudes;
            this.gcSolicitudes.Name = "gcSolicitudes";
            this.gcSolicitudes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnAsignar});
            this.gcSolicitudes.Size = new System.Drawing.Size(400, 264);
            this.gcSolicitudes.TabIndex = 0;
            this.gcSolicitudes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSolicitudes});
            this.gcSolicitudes.DoubleClick += new System.EventHandler(this.gcSolicitudes_DoubleClick);
            this.gcSolicitudes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gcSolicitudes_MouseClick);
            // 
            // gvSolicitudes
            // 
            this.gvSolicitudes.GridControl = this.gcSolicitudes;
            this.gvSolicitudes.Name = "gvSolicitudes";
            this.gvSolicitudes.OptionsBehavior.Editable = false;
            this.gvSolicitudes.OptionsBehavior.ReadOnly = true;
            this.gvSolicitudes.OptionsView.ShowGroupPanel = false;
            this.gvSolicitudes.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvSolicitudes_CustomRowCellEdit);
            this.gvSolicitudes.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvSolicitudes_PopupMenuShowing);
            // 
            // btnAsignar
            // 
            this.btnAsignar.AutoHeight = false;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            this.btnAsignar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnAsignar.Name = "btnAsignar";
            this.btnAsignar.ReadOnly = true;
            this.btnAsignar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.simpleButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 36);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton2.Location = new System.Drawing.Point(331, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(30, 30);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(152, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(88, 19);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Solicitudes";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(367, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(30, 30);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gcSolicitudes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 264);
            this.panel2.TabIndex = 2;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemAsignar,
            this.itemBorrar});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(124, 48);
            // 
            // itemAsignar
            // 
            this.itemAsignar.Name = "itemAsignar";
            this.itemAsignar.Size = new System.Drawing.Size(123, 22);
            this.itemAsignar.Text = "ASIGNAR";
            this.itemAsignar.Click += new System.EventHandler(this.uSUARIOToolStripMenuItem_Click);
            // 
            // itemBorrar
            // 
            this.itemBorrar.Name = "itemBorrar";
            this.itemBorrar.Size = new System.Drawing.Size(123, 22);
            this.itemBorrar.Text = "BORRAR";
            this.itemBorrar.Click += new System.EventHandler(this.itemBorrar_Click);
            // 
            // LisSolicitudes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 300);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LisSolicitudes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LisSolicitudes";
            this.Load += new System.EventHandler(this.LisSolicitudes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAsignar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcSolicitudes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSolicitudes;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem itemAsignar;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit btnAsignar;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.ToolStripMenuItem itemBorrar;
    }
}