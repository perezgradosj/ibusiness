﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;

namespace VISTA.Procesos
{
    public partial class RegOportunidad : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        //Variable que nos indica si vamos a insertar un nuevo producto
        public bool VarNuevo = true;
        //Variable que nos indica si vamos a modificar un producto
        public bool VarEditar = false;
        ////// Variable para el Id de la Oportunidad
        public int IdOportunidad;
        /////// Variable para almacenar el Id del Contacto
        public int IdContacto = 0;
        public int IdEmpresa = 0;
        public int IdUsuario = 0;
        public int IdLinea = 0;
        public bool nuevoform = true;
        public DataTable dtContactos;
        private int EmpresaNueva = 0;
        ////////////////////////// llenar Look Up Edit
        string TComunicacion, Linea, Usuario;

        //////////////////////////
        public int OpcionListContactos = 0;
        public string IdEmpresaContacto = "";

        private DataTable dtUsuarios;
        private DataTable dtLineas;

        private static RegOportunidad _instancia;

        //Creamos una instancia para poder utilizar los
        //Objetos del formulario
        public static RegOportunidad GetInstancia()
        {
            if (_instancia == null)
            {
                _instancia = new RegOportunidad();
            }
            return _instancia;
        }

        public void setLookUpEdit(string tcomunicacion, string linea, string usuario)
        {
            this.TComunicacion = tcomunicacion;
            this.Linea = linea;
            this.Usuario = usuario;
            Botones();
        }

        public void setLineas(string _Id, string _Nombre)
        {
            bool registrar = true;
            if (lueTipoComunicacion.Text == _Nombre)
            {
                this.MensajeError("Linea Seleccionada como principal");
            }
            else
            {
                foreach (DataRow row in dtLineas.Rows)
                {
                    if (Convert.ToString(row["id"]) == _Id)
                    {
                        registrar = false;
                        this.MensajeError("Linea ya Registrada");
                    }
                }
                if (registrar)
                {
                    //Agregar ese detalle al datalistadoDetalle
                    DataRow row = this.dtLineas.NewRow();
                    row["id"] = _Id;
                    row["nombre"] = _Nombre;
                    this.dtLineas.Rows.Add(row);
                }
            }
        }

        public void setUsuarios(string _Id, string _Nombre)
        {
            bool registrar = true;
                foreach (DataRow row in dtUsuarios.Rows)
                {
                    if (Convert.ToString(row["id"]) == _Id)
                    {
                        registrar = false;
                        this.MensajeError("Usuario ya Registrado");
                    }
                }
                if (registrar)
                {
                    //Agregar ese detalle al datalistadoDetalle
                    DataRow row = this.dtUsuarios.NewRow();
                    row["id"] = _Id;
                    row["nombre"] = _Nombre;
                    this.dtUsuarios.Rows.Add(row);
                }
        }

        //Creamos un método para enviar los valores recibidos
        //a la caja de texto txtIdcategoria
        public void setContacto(int id, string nombres, string correo, string movil, string fijo)
        {
            this.IdContacto = id;
            this.txtContacto.Text = nombres;
            this.txtCorreo.Text = correo;
            this.txtMovil.Text = movil;
            this.txtFijo.Text = fijo;
        }

        private void Habilitar(bool Valor)
        {
            this.lueTipoComunicacion.Enabled = Valor;
            this.txtNomOportunidad.Enabled = Valor;
            this.txtRequerimiento.Enabled = Valor;
            //this.txtContacto.Enabled = Valor;
            //this.txtCorreo.Enabled = Valor;
            //this.txtFijo.Enabled = Valor;
            //this.txtMovil.Enabled = Valor;
            this.txtRuc.Enabled = Valor;
            this.txtRazonSocial.Enabled = Valor;
            //this.txtrubro.Enabled = Valor;
        }

        private void Botones()
        {
            if (this.VarNuevo || this.VarEditar)
            {
                this.Habilitar(true);
                this.btnNuevo.Enabled = false;
                this.btnGuardar.Enabled = true;
                this.btnEditar.Visible = false;
                this.btnCancelar.Enabled = true;
            }
            else
            {
                this.Habilitar(false);
                this.btnNuevo.Enabled = true;
                this.btnGuardar.Enabled = false;
                this.btnEditar.Visible = true;
                this.btnCancelar.Enabled = false;
            }
        }

        private void Limpiar()
        {
            //this.id = 0;
            this.lueTipoComunicacion.Text = string.Empty;
            this.txtNomOportunidad.Text = string.Empty;
            this.txtRequerimiento.Text = string.Empty;
            this.txtContacto.Text = string.Empty;
            this.txtCorreo.Text = string.Empty;
            this.txtMovil.Text = string.Empty;
            this.txtFijo.Text = string.Empty;
            this.txtRuc.Text = string.Empty;
            this.txtRazonSocial.Text = string.Empty;
            this.txtrubro.Text = string.Empty;
            this.IdContacto = 0;
            this.errorIcono.ClearErrors();
        }

        public void CrearTablaContactos()
        {
            this.dtContactos = new DataTable("Contactos");
            this.dtContactos.Columns.Add("id", System.Type.GetType("System.Int32"));
            this.dtContactos.Columns.Add("nombres", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("correo", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("movil", System.Type.GetType("System.String"));
            this.dtContactos.Columns.Add("fijo", System.Type.GetType("System.String"));
            //Relacionar nuestro DataGRidView con nuestro DataTable
            this.gcContactos.DataSource = this.dtContactos;

            if (OpcionListContactos == 1)
            {
                //this.gcContactos.DataSource = MContactos.Mostrar(IdEmpresaContacto,
                //                                            3);

                this.dtContactos = MContactos.Mostrar(IdEmpresaContacto,
                                                      0,
                                                      IdOportunidad,
                                                      3);

                this.gcContactos.DataSource = this.dtContactos;
            }

            this.gvContactos.Columns[0].Visible = false;
        }

        public RegOportunidad()
        {
            InitializeComponent();
        }

        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        //Mostrar Mensaje de Error
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void CargarTipoComunicaciones()
        {
            MTipoComunicacion mTipoComunicacion = new MTipoComunicacion();
            DataTable dt = mTipoComunicacion.Mostrar();

            lueTipoComunicacion.Properties.ValueMember = "id";
            lueTipoComunicacion.Properties.DisplayMember = "nombre";

            lueTipoComunicacion.Properties.DataSource = dt;
            
        }

        public void CargarLineas()
        {
            MLineas mLineas = new MLineas();
            DataTable dt = mLineas.Mostrar();

            lueLinea.Properties.ValueMember = "id";
            lueLinea.Properties.DisplayMember = "nombre";

            lueLinea.Properties.DataSource = dt;

        }

        private void CargarUsuarios()
        {
            int linea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
            MUsuario mUsuario = new MUsuario();
            DataTable dt = mUsuario.Listar(linea);

            lueUsuario.Properties.ValueMember = "id";
            lueUsuario.Properties.DisplayMember = "nombre";

            lueUsuario.Properties.DataSource = dt;

        }

        private void LimpiarDatosContacto()
        {
            txtContacto.Text = string.Empty;
            txtCorreo.Text = string.Empty;
            txtMovil.Text = string.Empty;
            txtFijo.Text = string.Empty;
        }

        private void Guardar()
        {
            
            try
            {
                string rpta = "";
                //this.dtContactos.Rows.Count == 0
                if (this.lueTipoComunicacion.Text == "[Vacío]" ||
                    this.txtNomOportunidad.Text == string.Empty ||
                    this.txtRequerimiento.Text == string.Empty ||
                    this.lueLinea.Text == "[Vacío]" 
                    )
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(lueTipoComunicacion, "Ingrese un Valor");
                    errorIcono.SetError(txtNomOportunidad, "Ingrese un Valor");
                    errorIcono.SetError(txtRequerimiento, "Ingrese un Valor");
                    errorIcono.SetError(lueLinea, "Ingrese un Valor");
                    /*errorIcono.SetError(txtRuc, "Ingrese un Valor");
                    errorIcono.SetError(txtRazonSocial, "Ingrese un Valor");*/
                }
                else
                {
                    if ((txtRazonSocial.Text != "" && txtRuc.Text != "") | (txtRuc.Text == "" && txtRazonSocial.Text == ""))
                    {
                        if (this.VarNuevo)
                        {
                            int idTipoComunicacion = Convert.ToInt32(lueTipoComunicacion.GetColumnValue("id"));
                            int idLinea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
                            int _idUsuario = 0;
                            if (lueUsuario.Text != "")
                            {
                                _idUsuario = Convert.ToInt32(lueUsuario.GetColumnValue("id"));
                            }
                            
                            if (gvAutocompletarEmpresa.RowCount == 0)
                            {
                                string valorid = "0";
                                if (txtRuc.Text != "")
                                {
                                    valorid = MEmpresa.MantEmpresa(0,
                                                 txtRuc.Text,
                                                 txtRazonSocial.Text,
                                                 txtrubro.Text,
                                                 1);
                                }

                                if (valorid == "Error")
                                {
                                    this.IdEmpresa = 0;
                                }
                                else
                                {
                                    this.IdEmpresa = Convert.ToInt32(valorid);
                                    this.EmpresaNueva = Convert.ToInt32(valorid);
                                }
                            }
                            
                            rpta = MOportunidad.MantOportunidad(IdOportunidad,
                                                    idTipoComunicacion,
                                                    this.txtNomOportunidad.Text.Trim().ToUpper(),
                                                    this.txtRequerimiento.Text,
                                                    this.IdEmpresa,
                                                    DateTime.Today,
                                                    idLinea,
                                                    _idUsuario,
                                                    0,
                                                    1);

                            this.IdOportunidad = Convert.ToInt32(rpta);

                            //// GUARDAR CONTACTOS /////////////////////

                            MContactos.MantContacto(dtContactos, this.IdEmpresa, 0 , this.IdOportunidad, 1);

                            if (_idUsuario != 0)
                            {
                                MAsignaciones.MantAsginacionesHistorial(0,
                                                                    this.IdOportunidad,
                                                                    _idUsuario,
                                                                    DateTime.Now,
                                                                    DateTime.Now,
                                                                    1);
                                this.nuevoform = false;
                            }
                            if (Convert.ToInt32(rpta) > 0)
                            {
                                this.IdUsuario = _idUsuario;
                            }

                        }
                        else
                        {
                            //this.nuevoform = false;
                            int idTipoComunicacion = Convert.ToInt32(lueTipoComunicacion.GetColumnValue("id"));
                            int idLinea = Convert.ToInt32(lueLinea.GetColumnValue("id"));
                            int _idUsuario = 0;
                            if (lueUsuario.Text != "")
                            {
                                _idUsuario = Convert.ToInt32(lueUsuario.GetColumnValue("id"));
                            }

                            if (txtRuc.Text == "")
                            {
                                this.IdEmpresa = 0;
                            }

                            if (gvAutocompletarEmpresa.RowCount == 0)
                            {
                                if (this.IdEmpresa == this.EmpresaNueva)
                                {
                                    string valorid = "0";
                                    if (txtRuc.Text != "")
                                    {
                                        valorid = MEmpresa.MantEmpresa(0,
                                                     txtRuc.Text,
                                                     txtRazonSocial.Text,
                                                     txtrubro.Text,
                                                     1);
                                    }

                                    if (valorid == "Error")
                                    {
                                        this.IdEmpresa = 0;
                                    }
                                    else
                                    {
                                        this.IdEmpresa = Convert.ToInt32(valorid);
                                    }
                                }
                                
                            }

                            rpta = MOportunidad.MantOportunidad(IdOportunidad,
                                                    idTipoComunicacion,
                                                    this.txtNomOportunidad.Text.Trim().ToUpper(),
                                                    this.txtRequerimiento.Text,
                                                    this.IdEmpresa,
                                                    DateTime.Today,
                                                    idLinea,
                                                    _idUsuario,
                                                    0,
                                                    2);

                            //// GUARDAR CONTACTOS /////////////////////
                            //MContactos.MantEmpresaContacto(this.IdEmpresa, this.IdOportunidad, 0, 3);
                            MContactos.MantContacto(dtContactos, this.IdEmpresa, 0 , IdOportunidad, 1);

                            if (this.IdUsuario != _idUsuario)
                            {
                                if (this.nuevoform)
                                {
                                    string rpta1 = "", rpta2 = "";
                                    rpta1 = MAsignaciones.MantAsginacionesHistorial(0,
                                                                        this.IdOportunidad,
                                                                        this.IdUsuario,
                                                                        DateTime.Now,
                                                                        DateTime.Now,
                                                                        2);

                                    rpta2 = MAsignaciones.MantAsginacionesHistorial(0,
                                                                        this.IdOportunidad,
                                                                        _idUsuario,
                                                                        DateTime.Now,
                                                                        DateTime.Now,
                                                                        1);
                                    this.nuevoform = false;
                                }

                            }

                            if (Convert.ToInt32(rpta) > 0)
                            {
                                this.IdUsuario = _idUsuario;
                            }
                            
                        }

                        if (rpta.Equals("OK"))
                        {
                            if (this.VarNuevo)
                            {
                                this.MensajeOk("Se Insertó de forma correcta el registro");
                                this.errorIcono.ClearErrors();
                            }
                            else
                            {
                                this.MensajeOk("Se Actualizó de forma correcta el registro");
                                this.errorIcono.Dispose();
                            }
                        }

                        if (Convert.ToInt32(rpta) > 0)
                        {
                            if (this.VarNuevo)
                            {
                                this.MensajeOk("Se Insertó de forma correcta el registro");
                                this.VarNuevo = false;
                                this.VarEditar = true;
                                this.errorIcono.ClearErrors();
                            }
                            else
                            {
                                this.MensajeOk("Se Actualizó de forma correcta el registro");

                                this.errorIcono.ClearErrors();
                            }
                        }

                        else
                        {
                            this.MensajeError(rpta);
                        }

                        //this.VarNuevo = false;
                        //this.VarEditar = false;
                        //this.Botones();
                        //this.Limpiar();
                        //this.MostrarUsuarios();

                        ListOportunidades listOportunidades = Owner as ListOportunidades;
                        //listOportunidades.MostrarOportunidades("", IdUsuario, 1, 0, IdLinea);
                        listOportunidades.setActualizar();
                    }
                    if (txtRuc.Text != "")
                    {
                        if (txtRazonSocial.Text == "")
                        {
                            MensajeError("Ingrese Razon Social de la Empresa");
                        }
                    }
                    else if (txtRazonSocial.Text != "")
                    {
                        if (txtRuc.Text == "")
                        {
                            MensajeError("Ingrese Ruc de la Empresa");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private void frmOportunida_Load(object sender, EventArgs e)
        {
            //this.Limpiar();
            CargarTipoComunicaciones();
            CargarLineas();

            /////////////Carga Editar ////////
            if (this.VarEditar)
            {
                lueTipoComunicacion.EditValue = lueTipoComunicacion.Properties.GetKeyValueByDisplayText(TComunicacion);
                lueLinea.EditValue = lueLinea.Properties.GetKeyValueByDisplayText(Linea);
                lueUsuario.EditValue = lueUsuario.Properties.GetKeyValueByDisplayText(Usuario);
            }

            //CARGAR CONTACTOS Autocompletar
            gcAutocompletarContacto.DataSource = MContactos.Mostrar(txtContacto.Text, 0,0, 2);
            //CARGAR CONTACTOS Autocompletar
            gcAutocompletarContacto.DataSource = MEmpresa.Mostrar(txtRuc.Text, 3);

            CrearTablaContactos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            this.VarNuevo = true;
            this.VarEditar = false;
            //this.Botones();
            this.Limpiar();
            this.lueTipoComunicacion.Focus();
            this.IdOportunidad = 0;
            this.dtContactos.Clear();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //if (this.lueTipoComunicacion.Text != "[Vacío]" ||
            //        this.txtNomOportunidad.Text != string.Empty ||
            //        this.txtRequerimiento.Text != string.Empty ||
            //        this.lueLinea.Text != "[Vacío]")
            //{
            //    if (MessageBox.Show("¿Hay datos Importantes sin guardar, desea salir sin guardar?", "Sistemas DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes )
            //    {
            //        this.Close();
            //    }

            //}
            //else
            //{
                this.Close();

            VarNuevo = true;
            VarEditar = false;
            //

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {

        }

        private void lueLinea_MouseUp(object sender, MouseEventArgs e)
        {
            
        }

        private void txtRuc_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números 
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso 
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan 
                e.Handled = true;
            }
        }

        private void txtRuc_EditValueChanged(object sender, EventArgs e)
        {
            if (txtRuc.Text != "")
            {
                gcAutocompletarEmpresa.DataSource = MEmpresa.Mostrar(txtRuc.Text, 3);
                gcAutocompletarEmpresa.Visible = true;
                gvAutocompletarEmpresa.Columns[0].Visible = false;
                gvAutocompletarEmpresa.Columns[2].Visible = false;
                gvAutocompletarEmpresa.Columns[3].Visible = false;

                if (gvAutocompletarEmpresa.RowCount < 1)
                {
                    gcAutocompletarEmpresa.Visible = false;
                }
            }
            else
            {
                gcAutocompletarEmpresa.Visible = false;
                this.IdEmpresa = 0;
            }

            if (txtRuc.Text.Length == 0)
            {
                txtrubro.Text = string.Empty;
                txtRazonSocial.Text = string.Empty;
            }
            this.EmpresaNueva = 0;
        }

        private void txtContacto_EditValueChanged(object sender, EventArgs e)
        {
            if (txtContacto.Text != "")
            {
                gcAutocompletarContacto.DataSource = MContactos.Mostrar(txtContacto.Text,0,0, 2);
                gcAutocompletarContacto.Visible = true;
                gvAutocompletarContacto.Columns[0].Visible = false;
                gvAutocompletarContacto.Columns[2].Visible = false;
                gvAutocompletarContacto.Columns[3].Visible = false;
                gvAutocompletarContacto.Columns[4].Visible = false;

                if (gvAutocompletarContacto.RowCount < 1)
                {
                    gcAutocompletarContacto.Visible = false;
                }
            }
            else
            {
                gcAutocompletarContacto.Visible = false;
                this.IdContacto = 0;
            }

            if (txtContacto.Text.Length == 0)
            {
                txtCorreo.Text = "";
                txtMovil.Text = "";
                txtFijo.Text = "";
            }
        }

        private void txtContacto_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    gvAutocompletarContacto.Focus();
                    break;
            }
        }

        private void gcAutocompletar_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtContacto.Focus();
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.IdContacto = Convert.ToInt32(gvAutocompletarContacto.GetFocusedRowCellValue("id").ToString());
                this.txtContacto.Text = gvAutocompletarContacto.GetFocusedRowCellValue("nombres").ToString();
                this.txtCorreo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("correo").ToString();
                this.txtFijo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("fijo").ToString();
                this.txtMovil.Text = gvAutocompletarContacto.GetFocusedRowCellValue("movil").ToString();
                gcAutocompletarContacto.Visible = false;
            }
        }

        private void gcAutocompletar_DoubleClick(object sender, EventArgs e)
        {
            this.IdContacto = Convert.ToInt32(gvAutocompletarContacto.GetFocusedRowCellValue("id").ToString());
            this.txtContacto.Text = gvAutocompletarContacto.GetFocusedRowCellValue("nombres").ToString();
            this.txtCorreo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("correo").ToString();
            this.txtFijo.Text = gvAutocompletarContacto.GetFocusedRowCellValue("fijo").ToString();
            this.txtMovil.Text = gvAutocompletarContacto.GetFocusedRowCellValue("movil").ToString();
            gcAutocompletarContacto.Visible = false;
        }

        private void txtRuc_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    gcAutocompletarEmpresa.Focus();
                    break;
            }
        }

        private void gcAutocompletarEmpresa_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtRuc.Focus();
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                this.IdEmpresa = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
                this.txtRuc.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("ruc").ToString();
                this.txtRazonSocial.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("razon_social").ToString();
                this.txtrubro.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("rubro").ToString();
                this.EmpresaNueva = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
                gcAutocompletarEmpresa.Visible = false;
            }
        }

        private void gcAutocompletarEmpresa_DoubleClick(object sender, EventArgs e)
        {
            txtRuc.Focus();
            this.IdEmpresa = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            this.txtRuc.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("ruc").ToString();
            this.txtRazonSocial.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("razon_social").ToString();
            this.txtrubro.Text = gvAutocompletarEmpresa.GetFocusedRowCellValue("rubro").ToString();
            this.EmpresaNueva = Convert.ToInt32(gvAutocompletarEmpresa.GetFocusedRowCellValue("id").ToString());
            gcAutocompletarEmpresa.Visible = false;
        }

        private void txtNomOportunidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);
        }

        private void txtRuc_TextChanged(object sender, EventArgs e)
        {
            if (txtRuc.Text.Length == 11)
            {
                txtrubro.Enabled = true;
            }
            else
            {
                txtrubro.Enabled = false;
            }
        }

        private void groupControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lueUsuario_EditValueChanged(object sender, EventArgs e)
        {
            this.nuevoform = true;
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            Formulario.AsignacionHistorial historial = new Formulario.AsignacionHistorial();
            historial.Negociacion = txtNomOportunidad.Text;
            AddOwnedForm(historial);
            historial.Show();
        }

        private void btnAgregarContactos_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtContacto.Text == string.Empty || (this.txtCorreo.Text == string.Empty
                    || this.txtFijo.Text == string.Empty || this.txtMovil.Text == string.Empty))
                {
                    MensajeError("Falta ingresar algunos datos, serán remarcados");
                    errorIcono.SetError(txtContacto, "Ingrese un Valor");
                    errorIcono.SetError(txtCorreo, "Ingrese un Valor");
                    errorIcono.SetError(txtMovil, "Ingrese un Valor");
                    errorIcono.SetError(txtFijo, "Ingrese un Valor");
                }
                else
                {
                    bool registrar = true;
                    foreach (DataRow row in dtContactos.Rows)
                    {
                        if (Convert.ToString(row["Nombres"]) == this.txtContacto.Text)
                        {
                            registrar = false;
                            this.MensajeError("Cliente ya ingresado");
                        }
                    }
                    if (registrar)
                    {
                        //Agregar ese detalle al datalistadoDetalle
                        DataRow row = this.dtContactos.NewRow();
                        row["Id"] = this.IdContacto;
                        row["Nombres"] = this.txtContacto.Text;
                        row["Correo"] = this.txtCorreo.Text;
                        row["Movil"] = this.txtMovil.Text;
                        row["Fijo"] = this.txtFijo.Text;
                        this.dtContactos.Rows.Add(row);
                        this.LimpiarDatosContacto();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }


        }

        private void btnBorrarContactos_Click(object sender, EventArgs e)
        {
            try
            {
                int indicefila = this.gvContactos.GetFocusedDataSourceRowIndex();
                DataRow row = this.dtContactos.Rows[indicefila];
                //Preguntamos la existencia del contacto en la tabla EmpresaContacto
                var array = row.ItemArray[0];
                int idcontacto = int.Parse(array.ToString());
                int result = MContactos.verifyContactoOportunidad(idcontacto, IdOportunidad);
                if(result != 0)
                    MContactos.deleteEmpresaContacto(result);

                //Removemos la fila
                this.dtContactos.Rows.Remove(row);

                //gvContactos.DeleteSelectedRows();
            }
            catch (Exception ex)
            {

            }
        }

        private void txtFijo_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != '-'))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }*/
        }

        private void lueLinea_Validating(object sender, CancelEventArgs e)
        {
            //Se ejecuta despues de cambiar el valor
            //CargarUsuarios();
        }

        private void lueLinea_EditValueChanged(object sender, EventArgs e)
        {
            // SE Ejecuta cada ves que se cambia de valor 
            CargarUsuarios();
        }
    }
}