﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;

namespace VISTA.Procesos
{
    public partial class LisSolicitudes : DevExpress.XtraEditors.XtraForm
    {
        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public int IdOportunidad;
        public int IdUsuario;

        private void SetActualizar()
        {
            MOportunidad oportunidad = new MOportunidad();
            gcSolicitudes.DataSource = oportunidad.MostrarSolicitudes(IdOportunidad, IdUsuario);

            

            gvSolicitudes.Columns[0].Visible = false;
            gvSolicitudes.Columns[1].Visible = false;
            gvSolicitudes.Columns[2].Visible = false;
            gvSolicitudes.Columns[6].Visible = false;
        }

        public LisSolicitudes()
        {
            InitializeComponent();
        }

        private void LisSolicitudes_Load(object sender, EventArgs e)
        {
            SetActualizar();
            if (gvSolicitudes.RowCount == 0)
            {
                MessageBox.Show("No hay solicitudes para esta oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                GridColumn ColumnConvertir = gvSolicitudes.Columns.AddVisible("Estado", string.Empty);
                ColumnConvertir.AppearanceCell.BackColor = Color.LightGray;
                ColumnConvertir.Caption = "Estado";
                ColumnConvertir.ColumnEdit = btnAsignar;
                //ColumnConvertir.OptionsColumn.AllowEdit = false;
                ColumnConvertir.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            }
        }

        private void gcSolicitudes_DoubleClick(object sender, EventArgs e)
        {
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAsignar_Click(object sender, EventArgs e)
        {
            string Rpta = "";
            try
            {
                DialogResult dialog = MessageBox.Show("¿Desea Asignar la Oportunidad a este usuario?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                    0,
                                                    "",
                                                    "",
                                                    0,
                                                    DateTime.Today,
                                                    0,
                                                    Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
                                                    0,
                                                    4);

                    MAsignaciones.MantAsginacionesHistorial(0,
                                                            Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                            Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
                                                            DateTime.Today,
                                                            Convert.ToDateTime("01/01/0001"),
                                                            1);

                    if (Rpta != "0")
                    {
                        //if (e.Button.Caption == "NO ASIGNADO")
                        //{
                        //    e.Button.Caption = "ASIGNADO";
                        //}
                        //else
                        //{
                        //    e.Button.Caption = "NO ASIGNADO";
                        //}

                        ListOportunidades listOportunidades = Owner as ListOportunidades;
                        listOportunidades.setActualizar();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la asignacion de la Oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gvSolicitudes_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.Caption == "Estado")
            {

                bool asignado = Convert.ToBoolean(gvSolicitudes.GetRowCellValue(e.RowHandle, "asignado"));
                    
                if (!asignado)
                {
                    RepositoryItemButtonEdit ritem = new RepositoryItemButtonEdit();
                    ritem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
                    ritem.ReadOnly = true;
                    ritem.Buttons[0].Visible = false;
                    e.RepositoryItem = ritem;
                }
            }
        }

        private void btnAsignar_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //string Rpta = "";
            //try
            //{
            //    if (e.Button.Caption =="NO ASIGNADO")
            //    {

            //    }
            //    DialogResult dialog = MessageBox.Show("¿Desea Asignar la Oportunidad a este usuario?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //    if (dialog == DialogResult.Yes)
            //    {
            //        Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
            //                                        0,
            //                                        "",
            //                                        "",
            //                                        0,
            //                                        0,
            //                                        DateTime.Today,
            //                                        0,
            //                                        Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
            //                                        0,
            //                                        4);
            //        if (Rpta != "0")
            //        {
            //            //if (e.Button.Caption == "NO ASIGNADO")
            //            //{
            //            //    e.Button.Caption = "ASIGNADO";
            //            //}
            //            //else
            //            //{
            //            //    e.Button.Caption = "NO ASIGNADO";
            //            //}

            //            ListOportunidades listOportunidades = Owner as ListOportunidades;
            //            listOportunidades.setActualizar();
            //        }
                   
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error en la asignacion de la Oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void gcSolicitudes_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    if (e.Button == MouseButtons.Right)
            //    {
            //        ContextMenuStrip menu = new ContextMenuStrip();
            //        int posiscion = gvSolicitudes.HitTest(e.X, e.Y).RowIndex;
            //        // if (posiscion >= 0) {
            //        menu.Items.Add("agregar").Name = "AGREGAR";
            //        menu.Items.Add("eliminar").Name = "Eliminar";
            //        menu.Items.Add("detalles").Name = "DETALLES";
            //        //}
            //        menu.Show(lista_dias, new Point(e.X, e.Y));
            //    }
            //}

        }

        private void gvSolicitudes_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
        //    if (e.HitInfo.Column.Caption == "Estado")
        //    {
        //        MessageBox.Show("d");
        //    }
            
                GridView view = (GridView)sender;
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);
                if (hitInfo.InRow)
                {
                    view.FocusedRowHandle = hitInfo.RowHandle;
                    contextMenuStrip1.Show(view.GridControl, e.Point);
                }
            
        }

        private void uSUARIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Rpta = "";
            try
            {
                bool asignado = Convert.ToBoolean(gvSolicitudes.GetFocusedRowCellValue("asignado").ToString());
                if (asignado)
                {
                    MessageBox.Show("Oportunidad ya asignada", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DialogResult dialog = MessageBox.Show("¿Desea Asignar la Oportunidad a este usuario?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialog == DialogResult.Yes)
                    {
                        Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                        0,
                                                        "",
                                                        "",
                                                        0,
                                                        DateTime.Today,
                                                        0,
                                                        Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
                                                        0,
                                                        4);
                        MAsignaciones.MantAsginacionesHistorial(0,
                                                                Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                                Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
                                                                DateTime.Now,
                                                                DateTime.Now,
                                                                1);

                        if (Rpta != "0")
                        {
                            //if (e.Button.Caption == "NO ASIGNADO")
                            //{
                            //    e.Button.Caption = "ASIGNADO";
                            //}
                            //else
                            //{
                            //    e.Button.Caption = "NO ASIGNADO";
                            //}

                            ListOportunidades listOportunidades = Owner as ListOportunidades;
                            listOportunidades.setActualizar();
                            this.SetActualizar();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la asignacion de la Oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            string Rpta = "";
            try
            {
                DialogResult dialog = MessageBox.Show("¿Seguro que deseas limpiar las asignaciones?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                    0,
                                                    "",
                                                    "",
                                                    0,
                                                    DateTime.Today,
                                                    0,
                                                    0,
                                                    0,
                                                    4);
                    if (Rpta != "0")
                    {
                        //if (e.Button.Caption == "NO ASIGNADO")
                        //{
                        //    e.Button.Caption = "ASIGNADO";
                        //}
                        //else
                        //{
                        //    e.Button.Caption = "NO ASIGNADO";
                        //}

                        ListOportunidades listOportunidades = Owner as ListOportunidades;
                        listOportunidades.setActualizar();
                        this.SetActualizar();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al limpiar las asignaciones de la Oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void itemBorrar_Click(object sender, EventArgs e)
        {
            string Rpta = "";
            try
            {
                DialogResult dialog = MessageBox.Show("¿Seguro que deseas limpiar las asignaciones?", "Sistema DCGTEC", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    Rpta = MOportunidad.MantOportunidad(Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idoportunidad").ToString()),
                                                    0,
                                                    "",
                                                    "",
                                                    0,
                                                    DateTime.Today,
                                                    0,
                                                    Convert.ToInt32(gvSolicitudes.GetFocusedRowCellValue("idusuario").ToString()),
                                                    0,
                                                    5);
                    if (Rpta != "0")
                    {
                        //if (e.Button.Caption == "NO ASIGNADO")
                        //{
                        //    e.Button.Caption = "ASIGNADO";
                        //}
                        //else
                        //{
                        //    e.Button.Caption = "NO ASIGNADO";
                        //}

                        ListOportunidades listOportunidades = Owner as ListOportunidades;
                        listOportunidades.setActualizar();
                        this.SetActualizar();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al limpiar las asignaciones de la Oportunidad", "Sistema DCGTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}