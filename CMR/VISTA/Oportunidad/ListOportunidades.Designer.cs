﻿using MODELO;

namespace VISTA.Procesos
{
    partial class ListOportunidades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListOportunidades));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.KeyShortcut keyShortcut1 = new DevExpress.Utils.KeyShortcut();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.KeyShortcut keyShortcut2 = new DevExpress.Utils.KeyShortcut();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.KeyShortcut keyShortcut3 = new DevExpress.Utils.KeyShortcut();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MarcaNoUtil = new System.Windows.Forms.Panel();
            this.marcaConvertidos = new System.Windows.Forms.Panel();
            this.marcaPendiente = new System.Windows.Forms.Panel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcSolicitudes = new DevExpress.XtraGrid.GridControl();
            this.gvSolicitudes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.btnNoUtil = new DevExpress.XtraEditors.SimpleButton();
            this.btnCerrado = new DevExpress.XtraEditors.SimpleButton();
            this.btnPendiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtpaginacion = new DevExpress.XtraEditors.TextEdit();
            this.btnUltimaPagina = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaginaSiguiente = new DevExpress.XtraEditors.SimpleButton();
            this.btnPrimeraPagina = new DevExpress.XtraEditors.SimpleButton();
            this.btnPaginaAnterior = new DevExpress.XtraEditors.SimpleButton();
            this.cboCantRegistros = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gcOportunidades = new DevExpress.XtraGrid.GridControl();
            this.gvOportunidades = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnConvertir = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btnSolicitar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCantRegistros.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOportunidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOportunidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConvertir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSolicitar)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MarcaNoUtil);
            this.panel1.Controls.Add(this.marcaConvertidos);
            this.panel1.Controls.Add(this.marcaPendiente);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.gcSolicitudes);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.btnActualizar);
            this.panel1.Controls.Add(this.btnNoUtil);
            this.panel1.Controls.Add(this.btnCerrado);
            this.panel1.Controls.Add(this.btnPendiente);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 82);
            this.panel1.TabIndex = 0;
            // 
            // MarcaNoUtil
            // 
            this.MarcaNoUtil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MarcaNoUtil.BackColor = System.Drawing.Color.Green;
            this.MarcaNoUtil.Location = new System.Drawing.Point(815, 12);
            this.MarcaNoUtil.Name = "MarcaNoUtil";
            this.MarcaNoUtil.Size = new System.Drawing.Size(160, 10);
            this.MarcaNoUtil.TabIndex = 16;
            this.MarcaNoUtil.Visible = false;
            // 
            // marcaConvertidos
            // 
            this.marcaConvertidos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.marcaConvertidos.BackColor = System.Drawing.Color.Green;
            this.marcaConvertidos.Location = new System.Drawing.Point(649, 12);
            this.marcaConvertidos.Name = "marcaConvertidos";
            this.marcaConvertidos.Size = new System.Drawing.Size(160, 10);
            this.marcaConvertidos.TabIndex = 16;
            this.marcaConvertidos.Visible = false;
            // 
            // marcaPendiente
            // 
            this.marcaPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.marcaPendiente.BackColor = System.Drawing.Color.Green;
            this.marcaPendiente.Location = new System.Drawing.Point(483, 12);
            this.marcaPendiente.Name = "marcaPendiente";
            this.marcaPendiente.Size = new System.Drawing.Size(160, 10);
            this.marcaPendiente.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(196, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(222, 31);
            this.labelControl1.TabIndex = 14;
            this.labelControl1.Text = "OPORTUNIDADES";
            // 
            // gcSolicitudes
            // 
            this.gcSolicitudes.Location = new System.Drawing.Point(538, 73);
            this.gcSolicitudes.MainView = this.gvSolicitudes;
            this.gcSolicitudes.Name = "gcSolicitudes";
            this.gcSolicitudes.Size = new System.Drawing.Size(289, 55);
            this.gcSolicitudes.TabIndex = 12;
            this.gcSolicitudes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSolicitudes});
            this.gcSolicitudes.Visible = false;
            // 
            // gvSolicitudes
            // 
            this.gvSolicitudes.GridControl = this.gcSolicitudes;
            this.gvSolicitudes.Name = "gvSolicitudes";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.AutoSize = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(12, 20);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(40, 38);
            this.simpleButton2.TabIndex = 11;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizar.Appearance.Options.UseFont = true;
            this.btnActualizar.AutoSize = true;
            this.btnActualizar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizar.ImageOptions.Image")));
            this.btnActualizar.Location = new System.Drawing.Point(104, 20);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(40, 38);
            this.btnActualizar.TabIndex = 10;
            this.btnActualizar.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnNoUtil
            // 
            this.btnNoUtil.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNoUtil.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoUtil.Appearance.Options.UseFont = true;
            this.btnNoUtil.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNoUtil.ImageOptions.Image")));
            this.btnNoUtil.Location = new System.Drawing.Point(815, 12);
            this.btnNoUtil.Name = "btnNoUtil";
            this.btnNoUtil.Size = new System.Drawing.Size(160, 55);
            this.btnNoUtil.TabIndex = 9;
            this.btnNoUtil.Text = "NO UTIL   8";
            this.btnNoUtil.Click += new System.EventHandler(this.btnNoUtil_Click);
            // 
            // btnCerrado
            // 
            this.btnCerrado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrado.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrado.Appearance.Options.UseFont = true;
            this.btnCerrado.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrado.ImageOptions.Image")));
            this.btnCerrado.Location = new System.Drawing.Point(649, 12);
            this.btnCerrado.Name = "btnCerrado";
            this.btnCerrado.Size = new System.Drawing.Size(160, 55);
            this.btnCerrado.TabIndex = 8;
            this.btnCerrado.Text = "CONVERTIDOS   15";
            this.btnCerrado.Click += new System.EventHandler(this.btnCerrado_Click);
            // 
            // btnPendiente
            // 
            this.btnPendiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPendiente.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPendiente.Appearance.Options.UseFont = true;
            this.btnPendiente.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPendiente.ImageOptions.Image")));
            this.btnPendiente.Location = new System.Drawing.Point(483, 12);
            this.btnPendiente.Name = "btnPendiente";
            this.btnPendiente.Size = new System.Drawing.Size(160, 55);
            this.btnPendiente.TabIndex = 7;
            this.btnPendiente.Text = "PENDIENTE   10";
            this.btnPendiente.Click += new System.EventHandler(this.btnPendiente_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Appearance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Appearance.Options.UseFont = true;
            this.btnBuscar.AutoSize = true;
            this.btnBuscar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.ImageOptions.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(58, 20);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(40, 38);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.simpleButton1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtpaginacion);
            this.panel2.Controls.Add(this.btnUltimaPagina);
            this.panel2.Controls.Add(this.btnPaginaSiguiente);
            this.panel2.Controls.Add(this.btnPrimeraPagina);
            this.panel2.Controls.Add(this.btnPaginaAnterior);
            this.panel2.Controls.Add(this.cboCantRegistros);
            this.panel2.Controls.Add(this.gcOportunidades);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(984, 379);
            this.panel2.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.ImageOptions.Image = global::VISTA.Properties.Resources.doublelast_16x16;
            this.simpleButton1.Location = new System.Drawing.Point(956, 351);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(25, 25);
            this.simpleButton1.TabIndex = 7;
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(635, 357);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "150 Registros || 20 Paginas";
            // 
            // txtpaginacion
            // 
            this.txtpaginacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpaginacion.EditValue = "1";
            this.txtpaginacion.Location = new System.Drawing.Point(863, 354);
            this.txtpaginacion.Name = "txtpaginacion";
            // 
            // 
            // 
            this.txtpaginacion.Properties.Appearance.Options.UseTextOptions = true;
            this.txtpaginacion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtpaginacion.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txtpaginacion.Size = new System.Drawing.Size(25, 20);
            this.txtpaginacion.TabIndex = 4;
            this.txtpaginacion.EditValueChanged += new System.EventHandler(this.txtpaginacion_EditValueChanged);
            this.txtpaginacion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpaginacion_KeyPress);
            // 
            // btnUltimaPagina
            // 
            this.btnUltimaPagina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUltimaPagina.ImageOptions.Image = global::VISTA.Properties.Resources.doublelast_16x16;
            this.btnUltimaPagina.Location = new System.Drawing.Point(925, 351);
            this.btnUltimaPagina.Name = "btnUltimaPagina";
            this.btnUltimaPagina.Size = new System.Drawing.Size(25, 25);
            this.btnUltimaPagina.TabIndex = 3;
            this.btnUltimaPagina.Click += new System.EventHandler(this.btnUltimaPagina_Click);
            // 
            // btnPaginaSiguiente
            // 
            this.btnPaginaSiguiente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaginaSiguiente.ImageOptions.Image = global::VISTA.Properties.Resources.next_16x16;
            this.btnPaginaSiguiente.Location = new System.Drawing.Point(894, 351);
            this.btnPaginaSiguiente.Name = "btnPaginaSiguiente";
            this.btnPaginaSiguiente.Size = new System.Drawing.Size(25, 25);
            this.btnPaginaSiguiente.TabIndex = 2;
            this.btnPaginaSiguiente.Click += new System.EventHandler(this.btnPaginaSiguiente_Click);
            // 
            // btnPrimeraPagina
            // 
            this.btnPrimeraPagina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrimeraPagina.ImageOptions.Image = global::VISTA.Properties.Resources.doublefirst_16x16;
            this.btnPrimeraPagina.Location = new System.Drawing.Point(801, 351);
            this.btnPrimeraPagina.Name = "btnPrimeraPagina";
            this.btnPrimeraPagina.Size = new System.Drawing.Size(25, 25);
            this.btnPrimeraPagina.TabIndex = 0;
            this.btnPrimeraPagina.Click += new System.EventHandler(this.btnPrimeraPagina_Click);
            // 
            // btnPaginaAnterior
            // 
            this.btnPaginaAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPaginaAnterior.ImageOptions.Image = global::VISTA.Properties.Resources.prev_16x16;
            this.btnPaginaAnterior.Location = new System.Drawing.Point(832, 351);
            this.btnPaginaAnterior.Name = "btnPaginaAnterior";
            this.btnPaginaAnterior.Size = new System.Drawing.Size(25, 25);
            this.btnPaginaAnterior.TabIndex = 1;
            this.btnPaginaAnterior.Click += new System.EventHandler(this.btnPaginaAnterior_Click);
            // 
            // cboCantRegistros
            // 
            this.cboCantRegistros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cboCantRegistros.EditValue = "20";
            this.cboCantRegistros.Location = new System.Drawing.Point(3, 354);
            this.cboCantRegistros.Name = "cboCantRegistros";
            // 
            // 
            // 
            this.cboCantRegistros.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboCantRegistros.Properties.Items.AddRange(new object[] {
            "20",
            "50",
            "100"});
            this.cboCantRegistros.Size = new System.Drawing.Size(50, 20);
            this.cboCantRegistros.TabIndex = 6;
            this.cboCantRegistros.SelectedIndexChanged += new System.EventHandler(this.cboCantRegistros_SelectedIndexChanged);
            // 
            // gcOportunidades
            // 
            this.gcOportunidades.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOportunidades.Location = new System.Drawing.Point(0, 0);
            this.gcOportunidades.MainView = this.gvOportunidades;
            this.gcOportunidades.Name = "gcOportunidades";
            this.gcOportunidades.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnConvertir,
            this.btnSolicitar});
            this.gcOportunidades.Size = new System.Drawing.Size(984, 379);
            this.gcOportunidades.TabIndex = 0;
            this.gcOportunidades.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOportunidades});
            this.gcOportunidades.Click += new System.EventHandler(this.gcOportunidades_Click);
            this.gcOportunidades.DoubleClick += new System.EventHandler(this.gcOportunidades_DoubleClick);
            // 
            // gvOportunidades
            // 
            this.gvOportunidades.GridControl = this.gcOportunidades;
            this.gvOportunidades.Name = "gvOportunidades";
            this.gvOportunidades.OptionsBehavior.ReadOnly = true;
            this.gvOportunidades.OptionsView.ShowFooter = true;
            this.gvOportunidades.OptionsView.ShowGroupPanel = false;
            this.gvOportunidades.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvOportunidades_RowCellStyle);
            this.gvOportunidades.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvOportunidades_CustomRowCellEdit);
            this.gvOportunidades.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gvOportunidades_ShowingEditor);
            // 
            // btnConvertir
            // 
            this.btnConvertir.AutoHeight = false;
            editorButtonImageOptions1.Image = global::VISTA.Properties.Resources.convert_16x16;
            serializableAppearanceObject1.ForeColor = System.Drawing.Color.White;
            serializableAppearanceObject1.Options.UseForeColor = true;
            this.btnConvertir.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions1, keyShortcut1, serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // btnSolicitar
            // 
            this.btnSolicitar.AutoHeight = false;
            editorButtonImageOptions2.Image = global::VISTA.Properties.Resources.handtool_16x16;
            this.btnSolicitar.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "ACTION", -1, true, true, false, editorButtonImageOptions2, keyShortcut2, serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "SOLICITADO", -1, true, false, false, editorButtonImageOptions3, keyShortcut3, serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnSolicitar.Name = "btnSolicitar";
            this.btnSolicitar.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btnSolicitar.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnSolicitar_ButtonClick);
            this.btnSolicitar.Click += new System.EventHandler(this.btnSolicitar_Click);
            // 
            // ListOportunidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListOportunidades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListOportunidades";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ListOportunidades_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSolicitudes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSolicitudes)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtpaginacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCantRegistros.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcOportunidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOportunidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConvertir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSolicitar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private DevExpress.XtraGrid.GridControl gcOportunidades;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOportunidades;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnConvertir;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnSolicitar;
        private DevExpress.XtraEditors.SimpleButton btnNoUtil;
        private DevExpress.XtraEditors.SimpleButton btnCerrado;
        private DevExpress.XtraEditors.SimpleButton btnPendiente;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGrid.GridControl gcSolicitudes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSolicitudes;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnPrimeraPagina;
        private DevExpress.XtraEditors.SimpleButton btnUltimaPagina;
        private DevExpress.XtraEditors.SimpleButton btnPaginaSiguiente;
        private DevExpress.XtraEditors.SimpleButton btnPaginaAnterior;
        private DevExpress.XtraEditors.TextEdit txtpaginacion;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.ComboBoxEdit cboCantRegistros;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Panel MarcaNoUtil;
        private System.Windows.Forms.Panel marcaConvertidos;
        private System.Windows.Forms.Panel marcaPendiente;
    }
}