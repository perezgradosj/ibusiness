﻿namespace VISTA.Procesos
{
    partial class RegOportunidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegOportunidad));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnHistorial = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lueUsuario = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lueLinea = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lueTipoComunicacion = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomOportunidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtRequerimiento = new DevExpress.XtraEditors.MemoEdit();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gcAutocompletarEmpresa = new DevExpress.XtraGrid.GridControl();
            this.gvAutocompletarEmpresa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtrubro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtRazonSocial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtRuc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcAutocompletarContacto = new DevExpress.XtraGrid.GridControl();
            this.gvAutocompletarContacto = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnBorrarContactos = new DevExpress.XtraEditors.SimpleButton();
            this.gcContactos = new DevExpress.XtraGrid.GridControl();
            this.gvContactos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtFijo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtMovil = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtCorreo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtContacto = new DevExpress.XtraEditors.TextEdit();
            this.btnAgregarContactos = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            this.errorIcono = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoComunicacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomOportunidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarContacto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIcono)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnHistorial);
            this.groupControl1.Controls.Add(this.groupControl5);
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnNuevo);
            this.groupControl1.Controls.Add(this.btnCancelar);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.btnEditar);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(961, 494);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "OPORTUNIDADES";
            this.groupControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.groupControl1_MouseDown);
            // 
            // btnHistorial
            // 
            this.btnHistorial.ImageOptions.Image = global::VISTA.Properties.Resources.historyitem_32x32;
            this.btnHistorial.Location = new System.Drawing.Point(483, 442);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(100, 40);
            this.btnHistorial.TabIndex = 3;
            this.btnHistorial.Text = "HISTORIAL";
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.labelControl16);
            this.groupControl5.Controls.Add(this.labelControl15);
            this.groupControl5.Controls.Add(this.labelControl12);
            this.groupControl5.Controls.Add(this.labelControl11);
            this.groupControl5.Controls.Add(this.labelControl14);
            this.groupControl5.Controls.Add(this.lueUsuario);
            this.groupControl5.Controls.Add(this.labelControl13);
            this.groupControl5.Controls.Add(this.lueLinea);
            this.groupControl5.Controls.Add(this.labelControl1);
            this.groupControl5.Controls.Add(this.lueTipoComunicacion);
            this.groupControl5.Controls.Add(this.labelControl2);
            this.groupControl5.Controls.Add(this.txtNomOportunidad);
            this.groupControl5.Controls.Add(this.labelControl3);
            this.groupControl5.Controls.Add(this.txtRequerimiento);
            this.groupControl5.Location = new System.Drawing.Point(5, 23);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(472, 459);
            this.groupControl5.TabIndex = 26;
            this.groupControl5.Text = "Datos";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(140, 94);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(6, 13);
            this.labelControl16.TabIndex = 23;
            this.labelControl16.Text = "*";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(140, 144);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(6, 13);
            this.labelControl15.TabIndex = 22;
            this.labelControl15.Text = "*";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl12.Appearance.Options.UseForeColor = true;
            this.labelControl12.Location = new System.Drawing.Point(140, 68);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(6, 13);
            this.labelControl12.TabIndex = 21;
            this.labelControl12.Text = "*";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl11.Appearance.Options.UseForeColor = true;
            this.labelControl11.Location = new System.Drawing.Point(140, 32);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(6, 13);
            this.labelControl11.TabIndex = 20;
            this.labelControl11.Text = "*";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(11, 120);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(36, 13);
            this.labelControl14.TabIndex = 18;
            this.labelControl14.Text = "Usuario";
            // 
            // lueUsuario
            // 
            this.lueUsuario.Location = new System.Drawing.Point(150, 117);
            this.lueUsuario.Name = "lueUsuario";
            this.lueUsuario.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueUsuario.Size = new System.Drawing.Size(300, 20);
            this.lueUsuario.TabIndex = 3;
            this.lueUsuario.EditValueChanged += new System.EventHandler(this.lueUsuario_EditValueChanged);
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(11, 94);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(25, 13);
            this.labelControl13.TabIndex = 16;
            this.labelControl13.Text = "Linea";
            // 
            // lueLinea
            // 
            this.lueLinea.Location = new System.Drawing.Point(150, 91);
            this.lueLinea.Name = "lueLinea";
            this.lueLinea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLinea.Size = new System.Drawing.Size(300, 20);
            this.lueLinea.TabIndex = 2;
            this.lueLinea.EditValueChanged += new System.EventHandler(this.lueLinea_EditValueChanged);
            this.lueLinea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lueLinea_MouseUp);
            this.lueLinea.Validating += new System.ComponentModel.CancelEventHandler(this.lueLinea_Validating);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(11, 32);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo de Comunicacion";
            // 
            // lueTipoComunicacion
            // 
            this.lueTipoComunicacion.Location = new System.Drawing.Point(150, 29);
            this.lueTipoComunicacion.Name = "lueTipoComunicacion";
            this.lueTipoComunicacion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoComunicacion.Size = new System.Drawing.Size(300, 20);
            this.lueTipoComunicacion.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(11, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(126, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Nombre de la Oportunidad";
            // 
            // txtNomOportunidad
            // 
            this.txtNomOportunidad.Location = new System.Drawing.Point(150, 65);
            this.txtNomOportunidad.Name = "txtNomOportunidad";
            this.txtNomOportunidad.Size = new System.Drawing.Size(300, 20);
            this.txtNomOportunidad.TabIndex = 1;
            this.txtNomOportunidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomOportunidad_KeyPress);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(11, 144);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Requerimiento";
            // 
            // txtRequerimiento
            // 
            this.txtRequerimiento.Location = new System.Drawing.Point(150, 143);
            this.txtRequerimiento.Name = "txtRequerimiento";
            this.txtRequerimiento.Size = new System.Drawing.Size(300, 311);
            this.txtRequerimiento.TabIndex = 4;
            // 
            // btnGuardar
            // 
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(749, 441);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(100, 40);
            this.btnGuardar.TabIndex = 0;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.ImageOptions.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(643, 442);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(100, 40);
            this.btnNuevo.TabIndex = 2;
            this.btnNuevo.Text = "NUEVO";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.ImageOptions.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(855, 442);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 40);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "CERRAR";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gcAutocompletarEmpresa);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.txtrubro);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.txtRazonSocial);
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.txtRuc);
            this.groupControl3.Location = new System.Drawing.Point(483, 296);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(472, 137);
            this.groupControl3.TabIndex = 20;
            this.groupControl3.Text = "Datos de Empresa";
            // 
            // gcAutocompletarEmpresa
            // 
            this.gcAutocompletarEmpresa.Location = new System.Drawing.Point(132, 51);
            this.gcAutocompletarEmpresa.MainView = this.gvAutocompletarEmpresa;
            this.gcAutocompletarEmpresa.Name = "gcAutocompletarEmpresa";
            this.gcAutocompletarEmpresa.Size = new System.Drawing.Size(318, 96);
            this.gcAutocompletarEmpresa.TabIndex = 27;
            this.gcAutocompletarEmpresa.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAutocompletarEmpresa});
            this.gcAutocompletarEmpresa.Visible = false;
            this.gcAutocompletarEmpresa.DoubleClick += new System.EventHandler(this.gcAutocompletarEmpresa_DoubleClick);
            this.gcAutocompletarEmpresa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gcAutocompletarEmpresa_KeyPress);
            // 
            // gvAutocompletarEmpresa
            // 
            this.gvAutocompletarEmpresa.GridControl = this.gcAutocompletarEmpresa;
            this.gvAutocompletarEmpresa.Name = "gvAutocompletarEmpresa";
            this.gvAutocompletarEmpresa.OptionsBehavior.Editable = false;
            this.gvAutocompletarEmpresa.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.gvAutocompletarEmpresa.OptionsView.ShowColumnHeaders = false;
            this.gvAutocompletarEmpresa.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(11, 84);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(147, 13);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "¿ A que se dedica la empresa ?";
            // 
            // txtrubro
            // 
            this.txtrubro.Enabled = false;
            this.txtrubro.Location = new System.Drawing.Point(132, 103);
            this.txtrubro.Name = "txtrubro";
            this.txtrubro.Size = new System.Drawing.Size(318, 20);
            this.txtrubro.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(11, 61);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 13);
            this.labelControl9.TabIndex = 25;
            this.labelControl9.Text = "Razon Social";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Location = new System.Drawing.Point(132, 58);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(318, 20);
            this.txtRazonSocial.TabIndex = 1;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(11, 35);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(33, 13);
            this.labelControl10.TabIndex = 23;
            this.labelControl10.Text = "R.U.C.";
            // 
            // txtRuc
            // 
            this.txtRuc.Location = new System.Drawing.Point(132, 32);
            this.txtRuc.Name = "txtRuc";
            this.txtRuc.Properties.MaxLength = 11;
            this.txtRuc.Size = new System.Drawing.Size(318, 20);
            this.txtRuc.TabIndex = 0;
            this.txtRuc.EditValueChanged += new System.EventHandler(this.txtRuc_EditValueChanged);
            this.txtRuc.TextChanged += new System.EventHandler(this.txtRuc_TextChanged);
            this.txtRuc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRuc_KeyDown);
            this.txtRuc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRuc_KeyPress);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gcAutocompletarContacto);
            this.groupControl2.Controls.Add(this.btnBorrarContactos);
            this.groupControl2.Controls.Add(this.gcContactos);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.txtFijo);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtMovil);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.txtCorreo);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.txtContacto);
            this.groupControl2.Controls.Add(this.btnAgregarContactos);
            this.groupControl2.Location = new System.Drawing.Point(483, 23);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(472, 267);
            this.groupControl2.TabIndex = 19;
            this.groupControl2.Text = "Contacto";
            // 
            // gcAutocompletarContacto
            // 
            this.gcAutocompletarContacto.Location = new System.Drawing.Point(132, 49);
            this.gcAutocompletarContacto.MainView = this.gvAutocompletarContacto;
            this.gcAutocompletarContacto.Name = "gcAutocompletarContacto";
            this.gcAutocompletarContacto.Size = new System.Drawing.Size(318, 98);
            this.gcAutocompletarContacto.TabIndex = 1;
            this.gcAutocompletarContacto.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAutocompletarContacto});
            this.gcAutocompletarContacto.Visible = false;
            this.gcAutocompletarContacto.DoubleClick += new System.EventHandler(this.gcAutocompletar_DoubleClick);
            this.gcAutocompletarContacto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gcAutocompletar_KeyPress);
            // 
            // gvAutocompletarContacto
            // 
            this.gvAutocompletarContacto.GridControl = this.gcAutocompletarContacto;
            this.gvAutocompletarContacto.Name = "gvAutocompletarContacto";
            this.gvAutocompletarContacto.OptionsBehavior.Editable = false;
            this.gvAutocompletarContacto.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.gvAutocompletarContacto.OptionsView.ShowColumnHeaders = false;
            this.gvAutocompletarContacto.OptionsView.ShowGroupPanel = false;
            // 
            // btnBorrarContactos
            // 
            this.btnBorrarContactos.ImageOptions.Image = global::VISTA.Properties.Resources.clearall_16x16;
            this.btnBorrarContactos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnBorrarContactos.Location = new System.Drawing.Point(432, 110);
            this.btnBorrarContactos.Name = "btnBorrarContactos";
            this.btnBorrarContactos.Size = new System.Drawing.Size(18, 18);
            this.btnBorrarContactos.TabIndex = 5;
            this.btnBorrarContactos.Click += new System.EventHandler(this.btnBorrarContactos_Click);
            // 
            // gcContactos
            // 
            this.gcContactos.Location = new System.Drawing.Point(5, 129);
            this.gcContactos.MainView = this.gvContactos;
            this.gcContactos.Name = "gcContactos";
            this.gcContactos.Size = new System.Drawing.Size(462, 133);
            this.gcContactos.TabIndex = 25;
            this.gcContactos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContactos});
            // 
            // gvContactos
            // 
            this.gvContactos.GridControl = this.gcContactos;
            this.gvContactos.Name = "gvContactos";
            this.gvContactos.OptionsView.ShowGroupPanel = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(280, 87);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(32, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "N° Fijo";
            // 
            // txtFijo
            // 
            this.txtFijo.Location = new System.Drawing.Point(330, 84);
            this.txtFijo.Name = "txtFijo";
            this.txtFijo.Size = new System.Drawing.Size(120, 20);
            this.txtFijo.TabIndex = 3;
            this.txtFijo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFijo_KeyPress);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(11, 87);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 21;
            this.labelControl6.Text = "N° Movil";
            // 
            // txtMovil
            // 
            this.txtMovil.Location = new System.Drawing.Point(132, 84);
            this.txtMovil.Name = "txtMovil";
            this.txtMovil.Size = new System.Drawing.Size(120, 20);
            this.txtMovil.TabIndex = 2;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(11, 61);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(88, 13);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Correo Electronico";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(132, 58);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(318, 20);
            this.txtCorreo.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(11, 35);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Nombres";
            // 
            // txtContacto
            // 
            this.txtContacto.EditValue = "";
            this.txtContacto.Location = new System.Drawing.Point(132, 32);
            this.txtContacto.Name = "txtContacto";
            this.txtContacto.Size = new System.Drawing.Size(318, 20);
            this.txtContacto.TabIndex = 0;
            this.txtContacto.EditValueChanged += new System.EventHandler(this.txtContacto_EditValueChanged);
            this.txtContacto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContacto_KeyDown);
            // 
            // btnAgregarContactos
            // 
            this.btnAgregarContactos.ImageOptions.Image = global::VISTA.Properties.Resources.add_16x16;
            this.btnAgregarContactos.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAgregarContactos.Location = new System.Drawing.Point(408, 110);
            this.btnAgregarContactos.Name = "btnAgregarContactos";
            this.btnAgregarContactos.Size = new System.Drawing.Size(18, 18);
            this.btnAgregarContactos.TabIndex = 4;
            this.btnAgregarContactos.Click += new System.EventHandler(this.btnAgregarContactos_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.ImageOptions.Image")));
            this.btnEditar.Location = new System.Drawing.Point(749, 442);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(100, 40);
            this.btnEditar.TabIndex = 24;
            this.btnEditar.Text = "EDITAR";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // errorIcono
            // 
            this.errorIcono.ContainerControl = this;
            // 
            // RegOportunidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 494);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RegOportunidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmOportunida";
            this.Load += new System.EventHandler(this.frmOportunida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLinea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoComunicacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomOportunidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAutocompletarContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAutocompletarContacto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorIcono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider errorIcono;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        public DevExpress.XtraEditors.MemoEdit txtRequerimiento;
        public DevExpress.XtraEditors.TextEdit txtNomOportunidad;
        public DevExpress.XtraEditors.TextEdit txtrubro;
        public DevExpress.XtraEditors.TextEdit txtRazonSocial;
        public DevExpress.XtraEditors.TextEdit txtRuc;
        public DevExpress.XtraEditors.TextEdit txtFijo;
        public DevExpress.XtraEditors.TextEdit txtMovil;
        public DevExpress.XtraEditors.TextEdit txtCorreo;
        public DevExpress.XtraEditors.TextEdit txtContacto;
        public DevExpress.XtraEditors.LookUpEdit lueTipoComunicacion;
        public DevExpress.XtraEditors.LookUpEdit lueUsuario;
        public DevExpress.XtraEditors.LookUpEdit lueLinea;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAutocompletarContacto;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAutocompletarEmpresa;
        public DevExpress.XtraGrid.GridControl gcAutocompletarContacto;
        public DevExpress.XtraGrid.GridControl gcAutocompletarEmpresa;
        private DevExpress.XtraEditors.SimpleButton btnHistorial;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContactos;
        private DevExpress.XtraEditors.SimpleButton btnAgregarContactos;
        private DevExpress.XtraEditors.SimpleButton btnBorrarContactos;
        public DevExpress.XtraGrid.GridControl gcContactos;
    }
}