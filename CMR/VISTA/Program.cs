﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using CONTROLADOR;

namespace VISTA
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //DevExpress.Skins.SkinManager.EnableFormSkins();
            //DevExpress.UserSkins.BonusSkins.Register();
            //UserLookAndFeel.Default.SetSkinStyle("Office 2013");

            //if (CONTROLADOR.Properties.Settings.Default.cadenaConexion == "")
            //{
            //    XtraMessageBox.Show("Especifique el Origen de Datos");
            //    Application.Run(new Conexion.Conexion());
            //}
            //else
            //{
            Properties.Settings settings = new Properties.Settings();
            string conexion = settings.SisDcgConnectionString;
            CONTROLADOR.Conexion.cn = conexion;
            Application.Run(new USUARIO.Login());
            //}
            
        }
    }
}
