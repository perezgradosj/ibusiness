﻿namespace VISTA
{
    partial class XtraForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraForm2));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btneliminar = new DevExpress.XtraEditors.SimpleButton();
            this.btneditar = new DevExpress.XtraEditors.SimpleButton();
            this.btncancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnnuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnguardar = new DevExpress.XtraEditors.SimpleButton();
            this.tlnegociaciones = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tloportunidades = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.dgcUsuarios = new DevExpress.XtraGrid.GridControl();
            this.dgvUsuarios = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.gcOportunidades = new DevExpress.XtraGrid.GridControl();
            this.gvOportunidades = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAgregarOpor = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminarOpor = new DevExpress.XtraEditors.SimpleButton();
            this.gcNegociaciones = new DevExpress.XtraGrid.GridControl();
            this.gvNegociaciones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAgregarNego = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminarNego = new DevExpress.XtraEditors.SimpleButton();
            this.btn_verpassword = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtpassword = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtUsuario = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.chkAdmin = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtOport_Max = new DevExpress.XtraEditors.TextEdit();
            this.luelinea = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtApe_Materno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.cboSexo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtApe_Paterno = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtNombres = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlnegociaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tloportunidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgcUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOportunidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOportunidades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNegociaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNegociaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOport_Max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luelinea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApe_Materno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApe_Paterno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombres.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnActualizar);
            this.groupControl1.Controls.Add(this.groupControl4);
            this.groupControl1.Controls.Add(this.dgcUsuarios);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(950, 585);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "USUARIO";
            // 
            // btnActualizar
            // 
            this.btnActualizar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnActualizar.ImageOptions.Image")));
            this.btnActualizar.Location = new System.Drawing.Point(215, 23);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(23, 23);
            this.btnActualizar.TabIndex = 17;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.btneliminar);
            this.groupControl4.Controls.Add(this.btneditar);
            this.groupControl4.Controls.Add(this.btncancelar);
            this.groupControl4.Controls.Add(this.btnnuevo);
            this.groupControl4.Controls.Add(this.btnguardar);
            this.groupControl4.Controls.Add(this.tlnegociaciones);
            this.groupControl4.Controls.Add(this.tloportunidades);
            this.groupControl4.Location = new System.Drawing.Point(780, 23);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(199, 477);
            this.groupControl4.TabIndex = 4;
            this.groupControl4.Text = "Acciones";
            // 
            // btneliminar
            // 
            this.btneliminar.Location = new System.Drawing.Point(46, 252);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.Size = new System.Drawing.Size(107, 34);
            this.btneliminar.TabIndex = 11;
            this.btneliminar.Text = "Eliminar";
            // 
            // btneditar
            // 
            this.btneditar.Location = new System.Drawing.Point(46, 188);
            this.btneditar.Name = "btneditar";
            this.btneditar.Size = new System.Drawing.Size(107, 34);
            this.btneditar.TabIndex = 10;
            this.btneditar.Text = "Editar";
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(46, 316);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(107, 34);
            this.btncancelar.TabIndex = 9;
            this.btncancelar.Text = "Cancelar";
            // 
            // btnnuevo
            // 
            this.btnnuevo.Location = new System.Drawing.Point(46, 126);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.Size = new System.Drawing.Size(107, 34);
            this.btnnuevo.TabIndex = 8;
            this.btnnuevo.Text = "Nuevo";
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(46, 188);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(107, 34);
            this.btnguardar.TabIndex = 12;
            this.btnguardar.Text = "Guardar";
            // 
            // tlnegociaciones
            // 
            this.tlnegociaciones.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn1});
            this.tlnegociaciones.Location = new System.Drawing.Point(35, 370);
            this.tlnegociaciones.Name = "tlnegociaciones";
            this.tlnegociaciones.OptionsView.ShowCheckBoxes = true;
            this.tlnegociaciones.OptionsView.ShowFirstLines = false;
            this.tlnegociaciones.OptionsView.ShowHorzLines = false;
            this.tlnegociaciones.OptionsView.ShowIndicator = false;
            this.tlnegociaciones.OptionsView.ShowVertLines = false;
            this.tlnegociaciones.Size = new System.Drawing.Size(10, 59);
            this.tlnegociaciones.TabIndex = 2;
            this.tlnegociaciones.Visible = false;
            // 
            // treeListColumn1
            // 
            this.treeListColumn1.Caption = "NEGOCIACIONES";
            this.treeListColumn1.FieldName = "NEGOCIACIONES";
            this.treeListColumn1.Name = "treeListColumn1";
            this.treeListColumn1.OptionsColumn.AllowEdit = false;
            this.treeListColumn1.Visible = true;
            this.treeListColumn1.VisibleIndex = 0;
            // 
            // tloportunidades
            // 
            this.tloportunidades.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn2});
            this.tloportunidades.Location = new System.Drawing.Point(16, 370);
            this.tloportunidades.Name = "tloportunidades";
            this.tloportunidades.OptionsView.ShowCheckBoxes = true;
            this.tloportunidades.OptionsView.ShowFirstLines = false;
            this.tloportunidades.OptionsView.ShowHorzLines = false;
            this.tloportunidades.OptionsView.ShowIndicator = false;
            this.tloportunidades.OptionsView.ShowVertLines = false;
            this.tloportunidades.Size = new System.Drawing.Size(13, 59);
            this.tloportunidades.TabIndex = 3;
            this.tloportunidades.Visible = false;
            // 
            // treeListColumn2
            // 
            this.treeListColumn2.Caption = "OPORTUNIDADES";
            this.treeListColumn2.FieldName = "OPORTUNIDADES";
            this.treeListColumn2.Name = "treeListColumn2";
            this.treeListColumn2.Visible = true;
            this.treeListColumn2.VisibleIndex = 0;
            // 
            // dgcUsuarios
            // 
            this.dgcUsuarios.Location = new System.Drawing.Point(12, 23);
            this.dgcUsuarios.MainView = this.dgvUsuarios;
            this.dgcUsuarios.Name = "dgcUsuarios";
            this.dgcUsuarios.Size = new System.Drawing.Size(226, 477);
            this.dgcUsuarios.TabIndex = 3;
            this.dgcUsuarios.TabStop = false;
            this.dgcUsuarios.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvUsuarios});
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.GridControl = this.dgcUsuarios;
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.OptionsBehavior.Editable = false;
            this.dgvUsuarios.OptionsView.ShowAutoFilterRow = true;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.gcOportunidades);
            this.groupControl3.Controls.Add(this.btnAgregarOpor);
            this.groupControl3.Controls.Add(this.btnEliminarOpor);
            this.groupControl3.Controls.Add(this.gcNegociaciones);
            this.groupControl3.Controls.Add(this.btnAgregarNego);
            this.groupControl3.Controls.Add(this.btnEliminarNego);
            this.groupControl3.Controls.Add(this.btn_verpassword);
            this.groupControl3.Controls.Add(this.labelControl4);
            this.groupControl3.Controls.Add(this.txtpassword);
            this.groupControl3.Controls.Add(this.labelControl5);
            this.groupControl3.Controls.Add(this.txtUsuario);
            this.groupControl3.Location = new System.Drawing.Point(244, 199);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(529, 301);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Accesos";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(185, 113);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(71, 13);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Oportunidades";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(435, 113);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(68, 13);
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Text = "Negociaciones";
            // 
            // gcOportunidades
            // 
            this.gcOportunidades.Location = new System.Drawing.Point(26, 137);
            this.gcOportunidades.MainView = this.gvOportunidades;
            this.gcOportunidades.Name = "gcOportunidades";
            this.gcOportunidades.Size = new System.Drawing.Size(230, 159);
            this.gcOportunidades.TabIndex = 14;
            this.gcOportunidades.TabStop = false;
            this.gcOportunidades.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOportunidades,
            this.gridView2});
            // 
            // gvOportunidades
            // 
            this.gvOportunidades.GridControl = this.gcOportunidades;
            this.gvOportunidades.Name = "gvOportunidades";
            this.gvOportunidades.OptionsBehavior.Editable = false;
            this.gvOportunidades.OptionsView.ShowGroupPanel = false;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcOportunidades;
            this.gridView2.Name = "gridView2";
            // 
            // btnAgregarOpor
            // 
            this.btnAgregarOpor.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarOpor.ImageOptions.Image")));
            this.btnAgregarOpor.Location = new System.Drawing.Point(26, 108);
            this.btnAgregarOpor.Name = "btnAgregarOpor";
            this.btnAgregarOpor.Size = new System.Drawing.Size(23, 23);
            this.btnAgregarOpor.TabIndex = 13;
            // 
            // btnEliminarOpor
            // 
            this.btnEliminarOpor.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarOpor.ImageOptions.Image")));
            this.btnEliminarOpor.Location = new System.Drawing.Point(55, 108);
            this.btnEliminarOpor.Name = "btnEliminarOpor";
            this.btnEliminarOpor.Size = new System.Drawing.Size(23, 23);
            this.btnEliminarOpor.TabIndex = 12;
            // 
            // gcNegociaciones
            // 
            this.gcNegociaciones.Location = new System.Drawing.Point(273, 137);
            this.gcNegociaciones.MainView = this.gvNegociaciones;
            this.gcNegociaciones.Name = "gcNegociaciones";
            this.gcNegociaciones.Size = new System.Drawing.Size(230, 159);
            this.gcNegociaciones.TabIndex = 11;
            this.gcNegociaciones.TabStop = false;
            this.gcNegociaciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNegociaciones,
            this.gridView1});
            // 
            // gvNegociaciones
            // 
            this.gvNegociaciones.GridControl = this.gcNegociaciones;
            this.gvNegociaciones.Name = "gvNegociaciones";
            this.gvNegociaciones.OptionsBehavior.Editable = false;
            this.gvNegociaciones.OptionsView.ShowGroupPanel = false;
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcNegociaciones;
            this.gridView1.Name = "gridView1";
            // 
            // btnAgregarNego
            // 
            this.btnAgregarNego.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarNego.ImageOptions.Image")));
            this.btnAgregarNego.Location = new System.Drawing.Point(273, 108);
            this.btnAgregarNego.Name = "btnAgregarNego";
            this.btnAgregarNego.Size = new System.Drawing.Size(23, 23);
            this.btnAgregarNego.TabIndex = 10;
            // 
            // btnEliminarNego
            // 
            this.btnEliminarNego.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarNego.ImageOptions.Image")));
            this.btnEliminarNego.Location = new System.Drawing.Point(302, 108);
            this.btnEliminarNego.Name = "btnEliminarNego";
            this.btnEliminarNego.Size = new System.Drawing.Size(23, 23);
            this.btnEliminarNego.TabIndex = 9;
            // 
            // btn_verpassword
            // 
            this.btn_verpassword.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_verpassword.ImageOptions.Image")));
            this.btn_verpassword.Location = new System.Drawing.Point(413, 57);
            this.btn_verpassword.Name = "btn_verpassword";
            this.btn_verpassword.Size = new System.Drawing.Size(23, 23);
            this.btn_verpassword.TabIndex = 8;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(26, 62);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(56, 13);
            this.labelControl4.TabIndex = 7;
            this.labelControl4.Text = "Contraseña";
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(107, 59);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Properties.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(300, 20);
            this.txtpassword.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(26, 36);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 13);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "Usuario";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(107, 33);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(300, 20);
            this.txtUsuario.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.chkAdmin);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.txtOport_Max);
            this.groupControl2.Controls.Add(this.luelinea);
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.txtApe_Materno);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.cboSexo);
            this.groupControl2.Controls.Add(this.labelControl3);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.txtApe_Paterno);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.txtNombres);
            this.groupControl2.Location = new System.Drawing.Point(245, 23);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(529, 170);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Datos Personales";
            // 
            // chkAdmin
            // 
            this.chkAdmin.Location = new System.Drawing.Point(106, 144);
            this.chkAdmin.Name = "chkAdmin";
            this.chkAdmin.Properties.Caption = "Supervisor de Linea";
            this.chkAdmin.Size = new System.Drawing.Size(153, 19);
            this.chkAdmin.TabIndex = 12;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(267, 121);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(55, 13);
            this.labelControl10.TabIndex = 11;
            this.labelControl10.Text = "Max Oport.";
            // 
            // txtOport_Max
            // 
            this.txtOport_Max.EditValue = "4";
            this.txtOport_Max.Location = new System.Drawing.Point(351, 118);
            this.txtOport_Max.Name = "txtOport_Max";
            this.txtOport_Max.Size = new System.Drawing.Size(150, 20);
            this.txtOport_Max.TabIndex = 10;
            // 
            // luelinea
            // 
            this.luelinea.Location = new System.Drawing.Point(109, 118);
            this.luelinea.Name = "luelinea";
            this.luelinea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luelinea.Size = new System.Drawing.Size(150, 20);
            this.luelinea.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(265, 69);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(80, 13);
            this.labelControl7.TabIndex = 9;
            this.labelControl7.Text = "Apellido Materno";
            // 
            // txtApe_Materno
            // 
            this.txtApe_Materno.Location = new System.Drawing.Point(351, 66);
            this.txtApe_Materno.Name = "txtApe_Materno";
            this.txtApe_Materno.Size = new System.Drawing.Size(150, 20);
            this.txtApe_Materno.TabIndex = 2;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(25, 121);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(25, 13);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Linea";
            // 
            // cboSexo
            // 
            this.cboSexo.EditValue = "Masculino";
            this.cboSexo.Location = new System.Drawing.Point(109, 92);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboSexo.Properties.Items.AddRange(new object[] {
            "",
            "Masculino",
            "Femenino"});
            this.cboSexo.Size = new System.Drawing.Size(150, 20);
            this.cboSexo.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(25, 95);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Sexo";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 69);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Apellido Paterno";
            // 
            // txtApe_Paterno
            // 
            this.txtApe_Paterno.Location = new System.Drawing.Point(109, 66);
            this.txtApe_Paterno.Name = "txtApe_Paterno";
            this.txtApe_Paterno.Size = new System.Drawing.Size(150, 20);
            this.txtApe_Paterno.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 43);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(42, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Nombres";
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(109, 40);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(392, 20);
            this.txtNombres.TabIndex = 0;
            // 
            // XtraForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 585);
            this.Controls.Add(this.groupControl1);
            this.Name = "XtraForm2";
            this.Text = "XtraForm2";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlnegociaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tloportunidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgcUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOportunidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOportunidades)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNegociaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNegociaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsuario.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOport_Max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luelinea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApe_Materno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtApe_Paterno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNombres.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton btneliminar;
        private DevExpress.XtraEditors.SimpleButton btneditar;
        private DevExpress.XtraEditors.SimpleButton btncancelar;
        private DevExpress.XtraEditors.SimpleButton btnnuevo;
        private DevExpress.XtraEditors.SimpleButton btnguardar;
        private DevExpress.XtraTreeList.TreeList tlnegociaciones;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.TreeList tloportunidades;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraGrid.GridControl dgcUsuarios;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvUsuarios;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraGrid.GridControl gcOportunidades;
        private DevExpress.XtraGrid.Views.Grid.GridView gvOportunidades;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton btnAgregarOpor;
        private DevExpress.XtraEditors.SimpleButton btnEliminarOpor;
        private DevExpress.XtraGrid.GridControl gcNegociaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNegociaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnAgregarNego;
        private DevExpress.XtraEditors.SimpleButton btnEliminarNego;
        private DevExpress.XtraEditors.SimpleButton btn_verpassword;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtpassword;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtUsuario;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit chkAdmin;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtOport_Max;
        private DevExpress.XtraEditors.LookUpEdit luelinea;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtApe_Materno;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.ComboBoxEdit cboSexo;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtApe_Paterno;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtNombres;
    }
}