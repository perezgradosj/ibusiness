﻿namespace VISTA.Vistas
{
    partial class VistaLineas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcLineas = new DevExpress.XtraGrid.GridControl();
            this.gvLineas = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcLineas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLineas)).BeginInit();
            this.SuspendLayout();
            // 
            // gcLineas
            // 
            this.gcLineas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLineas.Location = new System.Drawing.Point(0, 0);
            this.gcLineas.MainView = this.gvLineas;
            this.gcLineas.Name = "gcLineas";
            this.gcLineas.Size = new System.Drawing.Size(234, 311);
            this.gcLineas.TabIndex = 0;
            this.gcLineas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLineas});
            this.gcLineas.Click += new System.EventHandler(this.gridControl1_Click);
            this.gcLineas.DoubleClick += new System.EventHandler(this.gcNegociaciones_DoubleClick);
            // 
            // gvLineas
            // 
            this.gvLineas.GridControl = this.gcLineas;
            this.gvLineas.Name = "gvLineas";
            this.gvLineas.OptionsBehavior.Editable = false;
            this.gvLineas.OptionsView.ShowAutoFilterRow = true;
            this.gvLineas.OptionsView.ShowGroupPanel = false;
            // 
            // VistaLineas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 311);
            this.Controls.Add(this.gcLineas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VistaLineas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VistaNegociaciones";
            this.Load += new System.EventHandler(this.VistaNegociaciones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcLineas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLineas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcLineas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLineas;
    }
}