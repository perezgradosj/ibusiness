﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;

namespace VISTA.Vistas
{
    public partial class VistaContactos : DevExpress.XtraEditors.XtraForm
    {
        //private static VistaContactos _instancia;

        ////Creamos una instancia para poder utilizar los
        ////Objetos del formulario
        //public static VistaContactos GetInstancia()
        //{
        //    if (_instancia == null)
        //    {
        //        _instancia = new VistaContactos();
        //    }
        //    return _instancia;
        //}

        public VistaContactos()
        {
            InitializeComponent();
        }

        private void MostrarUsuarios(string texto, int opcion)
        {
            this.gcContactos.DataSource = MContactos.BuscarNombre(texto, opcion);
            this.lblRegistros.Text = "Total de Registros: " + this.gvContactos.RowCount.ToString();
        }

        private void VistaContactos_Load(object sender, EventArgs e)
        {
            MostrarUsuarios("", 2);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (cbofiltro.SelectedIndex == 0)
            {
                MostrarUsuarios(txttexto.Text, 2);
            }
            else if (cbofiltro.SelectedIndex == 1)
            {
                MostrarUsuarios(txttexto.Text, 3);
            }
            
        }

        private void txttexto_TextChanged(object sender, EventArgs e)
        {
            if (cbofiltro.SelectedIndex == 0)
            {
                MostrarUsuarios(txttexto.Text, 2);
            }
            else if (cbofiltro.SelectedIndex == 1)
            {
                MostrarUsuarios(txttexto.Text, 3);
            }
        }

        private void gcContactos_DoubleClick(object sender, EventArgs e)
        {
            //Procesos.frmOportunidad form = Procesos.frmOportunidad.GetInstancia();
            //int id;
            //string nombres, correo, movil, fijo;
            //id = Convert.ToInt32(this.gvContactos.GetFocusedRowCellValue("id").ToString());
            //nombres = this.gvContactos.GetFocusedRowCellValue("nombres").ToString() + " " +
            //          this.gvContactos.GetFocusedRowCellValue("apellidos").ToString();
            //correo = this.gvContactos.GetFocusedRowCellValue("correo").ToString();
            //movil = this.gvContactos.GetFocusedRowCellValue("movil").ToString();
            //fijo = this.gvContactos.GetFocusedRowCellValue("fijo").ToString();
            //form.setContacto(id,nombres, correo, movil, fijo);
            //this.Hide();

            Procesos.RegOportunidad form = Owner as Procesos.RegOportunidad;
            int id;
            string nombres, correo, movil, fijo;
            id = Convert.ToInt32(this.gvContactos.GetFocusedRowCellValue("id").ToString());
            nombres = this.gvContactos.GetFocusedRowCellValue("nombres").ToString() + " " +
                      this.gvContactos.GetFocusedRowCellValue("apellidos").ToString();
            correo = this.gvContactos.GetFocusedRowCellValue("correo").ToString();
            movil = this.gvContactos.GetFocusedRowCellValue("movil").ToString();
            fijo = this.gvContactos.GetFocusedRowCellValue("fijo").ToString();
            form.setContacto(id, nombres, correo, movil, fijo);
            this.Close();


        }
    }
}