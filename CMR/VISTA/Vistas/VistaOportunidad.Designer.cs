﻿namespace VISTA.Vistas
{
    partial class VistaOportunidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaOportunidad));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gcUsuarios = new DevExpress.XtraGrid.GridControl();
            this.gvUsuarios = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAgregarUsuario = new DevExpress.XtraEditors.SimpleButton();
            this.brnEliminarUsuario = new DevExpress.XtraEditors.SimpleButton();
            this.gcLineas = new DevExpress.XtraGrid.GridControl();
            this.gvLineas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAgregarLineas = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminarLineas = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtrubro = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtRazonSocial = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtRuc = new DevExpress.XtraEditors.TextEdit();
            this.clbLineas = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.clbUsuario = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtFijo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtMovil = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtCorreo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnBuscarContacto = new DevExpress.XtraEditors.SimpleButton();
            this.txtContacto = new DevExpress.XtraEditors.TextEdit();
            this.txtRequerimiento = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtNomOportunidad = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueTipoComunicacion = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLineas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLineas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbLineas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbUsuario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomOportunidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoComunicacion.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnNuevo);
            this.groupControl1.Controls.Add(this.btnCancelar);
            this.groupControl1.Controls.Add(this.groupControl4);
            this.groupControl1.Controls.Add(this.groupControl3);
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.txtRequerimiento);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtNomOportunidad);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.lueTipoComunicacion);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.btnEditar);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(916, 586);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "OPORTUNIDADES";
            // 
            // btnGuardar
            // 
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(590, 517);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(100, 40);
            this.btnGuardar.TabIndex = 23;
            this.btnGuardar.Text = "GUARDAR";
            // 
            // btnNuevo
            // 
            this.btnNuevo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.ImageOptions.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(484, 517);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(100, 40);
            this.btnNuevo.TabIndex = 23;
            this.btnNuevo.Text = "NUEVO";
            // 
            // btnCancelar
            // 
            this.btnCancelar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.ImageOptions.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(696, 517);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 40);
            this.btnCancelar.TabIndex = 22;
            this.btnCancelar.Text = "CANCELAR";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gcUsuarios);
            this.groupControl4.Controls.Add(this.btnAgregarUsuario);
            this.groupControl4.Controls.Add(this.brnEliminarUsuario);
            this.groupControl4.Controls.Add(this.gcLineas);
            this.groupControl4.Controls.Add(this.btnAgregarLineas);
            this.groupControl4.Controls.Add(this.btnEliminarLineas);
            this.groupControl4.Controls.Add(this.labelControl12);
            this.groupControl4.Controls.Add(this.labelControl11);
            this.groupControl4.Location = new System.Drawing.Point(473, 23);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(323, 488);
            this.groupControl4.TabIndex = 21;
            this.groupControl4.Text = "Asignacion";
            // 
            // gcUsuarios
            // 
            this.gcUsuarios.Location = new System.Drawing.Point(21, 296);
            this.gcUsuarios.MainView = this.gvUsuarios;
            this.gcUsuarios.Name = "gcUsuarios";
            this.gcUsuarios.Size = new System.Drawing.Size(262, 167);
            this.gcUsuarios.TabIndex = 17;
            this.gcUsuarios.TabStop = false;
            this.gcUsuarios.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUsuarios});
            // 
            // gvUsuarios
            // 
            this.gvUsuarios.GridControl = this.gcUsuarios;
            this.gvUsuarios.Name = "gvUsuarios";
            this.gvUsuarios.OptionsBehavior.Editable = false;
            this.gvUsuarios.OptionsView.ShowGroupPanel = false;
            // 
            // btnAgregarUsuario
            // 
            this.btnAgregarUsuario.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarUsuario.ImageOptions.Image")));
            this.btnAgregarUsuario.Location = new System.Drawing.Point(231, 267);
            this.btnAgregarUsuario.Name = "btnAgregarUsuario";
            this.btnAgregarUsuario.Size = new System.Drawing.Size(23, 23);
            this.btnAgregarUsuario.TabIndex = 16;
            // 
            // brnEliminarUsuario
            // 
            this.brnEliminarUsuario.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("brnEliminarUsuario.ImageOptions.Image")));
            this.brnEliminarUsuario.Location = new System.Drawing.Point(260, 267);
            this.brnEliminarUsuario.Name = "brnEliminarUsuario";
            this.brnEliminarUsuario.Size = new System.Drawing.Size(23, 23);
            this.brnEliminarUsuario.TabIndex = 15;
            // 
            // gcLineas
            // 
            this.gcLineas.Location = new System.Drawing.Point(21, 56);
            this.gcLineas.MainView = this.gvLineas;
            this.gcLineas.Name = "gcLineas";
            this.gcLineas.Size = new System.Drawing.Size(262, 167);
            this.gcLineas.TabIndex = 14;
            this.gcLineas.TabStop = false;
            this.gcLineas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLineas});
            // 
            // gvLineas
            // 
            this.gvLineas.GridControl = this.gcLineas;
            this.gvLineas.Name = "gvLineas";
            this.gvLineas.OptionsBehavior.Editable = false;
            this.gvLineas.OptionsView.ShowGroupPanel = false;
            // 
            // btnAgregarLineas
            // 
            this.btnAgregarLineas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarLineas.ImageOptions.Image")));
            this.btnAgregarLineas.Location = new System.Drawing.Point(231, 27);
            this.btnAgregarLineas.Name = "btnAgregarLineas";
            this.btnAgregarLineas.Size = new System.Drawing.Size(23, 23);
            this.btnAgregarLineas.TabIndex = 13;
            // 
            // btnEliminarLineas
            // 
            this.btnEliminarLineas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarLineas.ImageOptions.Image")));
            this.btnEliminarLineas.Location = new System.Drawing.Point(260, 27);
            this.btnEliminarLineas.Name = "btnEliminarLineas";
            this.btnEliminarLineas.Size = new System.Drawing.Size(23, 23);
            this.btnEliminarLineas.TabIndex = 12;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(21, 266);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(46, 13);
            this.labelControl12.TabIndex = 3;
            this.labelControl12.Text = "USUARIO";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(21, 32);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 13);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "LINEAS";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl8);
            this.groupControl3.Controls.Add(this.txtrubro);
            this.groupControl3.Controls.Add(this.labelControl9);
            this.groupControl3.Controls.Add(this.txtRazonSocial);
            this.groupControl3.Controls.Add(this.labelControl10);
            this.groupControl3.Controls.Add(this.txtRuc);
            this.groupControl3.Controls.Add(this.clbLineas);
            this.groupControl3.Controls.Add(this.clbUsuario);
            this.groupControl3.Location = new System.Drawing.Point(21, 367);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(432, 144);
            this.groupControl3.TabIndex = 20;
            this.groupControl3.Text = "Datos de Empresa";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(18, 87);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(147, 13);
            this.labelControl8.TabIndex = 27;
            this.labelControl8.Text = "¿ A que se dedica la empresa ?";
            // 
            // txtrubro
            // 
            this.txtrubro.Location = new System.Drawing.Point(132, 106);
            this.txtrubro.Name = "txtrubro";
            this.txtrubro.Size = new System.Drawing.Size(221, 20);
            this.txtrubro.TabIndex = 28;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(18, 61);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 13);
            this.labelControl9.TabIndex = 25;
            this.labelControl9.Text = "Razon Social";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Location = new System.Drawing.Point(132, 58);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(221, 20);
            this.txtRazonSocial.TabIndex = 26;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(18, 35);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(33, 13);
            this.labelControl10.TabIndex = 23;
            this.labelControl10.Text = "R.U.C.";
            // 
            // txtRuc
            // 
            this.txtRuc.Location = new System.Drawing.Point(132, 32);
            this.txtRuc.Name = "txtRuc";
            this.txtRuc.Size = new System.Drawing.Size(221, 20);
            this.txtRuc.TabIndex = 24;
            // 
            // clbLineas
            // 
            this.clbLineas.Location = new System.Drawing.Point(406, 23);
            this.clbLineas.Name = "clbLineas";
            this.clbLineas.Size = new System.Drawing.Size(21, 66);
            this.clbLineas.TabIndex = 0;
            this.clbLineas.Visible = false;
            // 
            // clbUsuario
            // 
            this.clbUsuario.Location = new System.Drawing.Point(379, 23);
            this.clbUsuario.Name = "clbUsuario";
            this.clbUsuario.Size = new System.Drawing.Size(21, 66);
            this.clbUsuario.TabIndex = 1;
            this.clbUsuario.Visible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl7);
            this.groupControl2.Controls.Add(this.txtFijo);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.txtMovil);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.txtCorreo);
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.btnBuscarContacto);
            this.groupControl2.Controls.Add(this.txtContacto);
            this.groupControl2.Location = new System.Drawing.Point(21, 212);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(432, 149);
            this.groupControl2.TabIndex = 19;
            this.groupControl2.Text = "Contacto";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(21, 113);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(32, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "N° Fijo";
            // 
            // txtFijo
            // 
            this.txtFijo.Enabled = false;
            this.txtFijo.Location = new System.Drawing.Point(132, 110);
            this.txtFijo.Name = "txtFijo";
            this.txtFijo.Size = new System.Drawing.Size(173, 20);
            this.txtFijo.TabIndex = 24;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(21, 87);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 21;
            this.labelControl6.Text = "N° Movil";
            // 
            // txtMovil
            // 
            this.txtMovil.Enabled = false;
            this.txtMovil.Location = new System.Drawing.Point(132, 84);
            this.txtMovil.Name = "txtMovil";
            this.txtMovil.Size = new System.Drawing.Size(173, 20);
            this.txtMovil.TabIndex = 22;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(21, 61);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(88, 13);
            this.labelControl5.TabIndex = 19;
            this.labelControl5.Text = "Correo Electronico";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Enabled = false;
            this.txtCorreo.Location = new System.Drawing.Point(132, 58);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(221, 20);
            this.txtCorreo.TabIndex = 20;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(21, 35);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 16;
            this.labelControl4.Text = "Nombres";
            // 
            // btnBuscarContacto
            // 
            this.btnBuscarContacto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarContacto.ImageOptions.Image")));
            this.btnBuscarContacto.Location = new System.Drawing.Point(371, 34);
            this.btnBuscarContacto.Name = "btnBuscarContacto";
            this.btnBuscarContacto.Size = new System.Drawing.Size(40, 40);
            this.btnBuscarContacto.TabIndex = 18;
            // 
            // txtContacto
            // 
            this.txtContacto.Enabled = false;
            this.txtContacto.Location = new System.Drawing.Point(132, 32);
            this.txtContacto.Name = "txtContacto";
            this.txtContacto.Size = new System.Drawing.Size(221, 20);
            this.txtContacto.TabIndex = 17;
            // 
            // txtRequerimiento
            // 
            this.txtRequerimiento.Location = new System.Drawing.Point(153, 110);
            this.txtRequerimiento.Name = "txtRequerimiento";
            this.txtRequerimiento.Size = new System.Drawing.Size(300, 96);
            this.txtRequerimiento.TabIndex = 15;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(21, 111);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(69, 13);
            this.labelControl3.TabIndex = 14;
            this.labelControl3.Text = "Requerimiento";
            // 
            // txtNomOportunidad
            // 
            this.txtNomOportunidad.Location = new System.Drawing.Point(153, 71);
            this.txtNomOportunidad.Name = "txtNomOportunidad";
            this.txtNomOportunidad.Size = new System.Drawing.Size(300, 20);
            this.txtNomOportunidad.TabIndex = 13;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(21, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(126, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "Nombre de la Oportunidad";
            // 
            // lueTipoComunicacion
            // 
            this.lueTipoComunicacion.Location = new System.Drawing.Point(153, 35);
            this.lueTipoComunicacion.Name = "lueTipoComunicacion";
            this.lueTipoComunicacion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTipoComunicacion.Size = new System.Drawing.Size(300, 20);
            this.lueTipoComunicacion.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tipo de Comunicacion";
            // 
            // btnEditar
            // 
            this.btnEditar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.ImageOptions.Image")));
            this.btnEditar.Location = new System.Drawing.Point(590, 517);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(100, 40);
            this.btnEditar.TabIndex = 24;
            this.btnEditar.Text = "EDITAR";
            // 
            // VistaOportunidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 586);
            this.Controls.Add(this.groupControl1);
            this.Name = "VistaOportunidad";
            this.Text = "VistaOportunidad";
            this.Load += new System.EventHandler(this.VistaOportunidad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLineas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLineas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtrubro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRazonSocial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbLineas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clbUsuario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFijo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMovil.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCorreo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContacto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRequerimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomOportunidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueTipoComunicacion.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gcUsuarios;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUsuarios;
        private DevExpress.XtraEditors.SimpleButton btnAgregarUsuario;
        private DevExpress.XtraEditors.SimpleButton brnEliminarUsuario;
        private DevExpress.XtraGrid.GridControl gcLineas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLineas;
        private DevExpress.XtraEditors.SimpleButton btnAgregarLineas;
        private DevExpress.XtraEditors.SimpleButton btnEliminarLineas;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtrubro;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtRazonSocial;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtRuc;
        private DevExpress.XtraEditors.CheckedListBoxControl clbLineas;
        private DevExpress.XtraEditors.CheckedListBoxControl clbUsuario;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtFijo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtMovil;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtCorreo;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnBuscarContacto;
        private DevExpress.XtraEditors.TextEdit txtContacto;
        private DevExpress.XtraEditors.MemoEdit txtRequerimiento;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtNomOportunidad;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueTipoComunicacion;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
    }
}