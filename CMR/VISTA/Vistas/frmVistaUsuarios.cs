﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;

namespace VISTA.Vistas
{
    public partial class frmVistaUsuarios : DevExpress.XtraEditors.XtraForm
    {
        public string data = string.Empty;

        public frmVistaUsuarios()
        {
            InitializeComponent();
        }

        private void MostrarUsuarios()
        {
            this.gcUsuarios.DataSource = MUsuario.Mostrar();
            OcultarColumnas();
        }
        
        private void OcultarColumnas()
        {
            gvUsuarios.Columns[0].Visible = false;
            gvUsuarios.Columns[2].Visible = false;
            gvUsuarios.Columns[4].Visible = false;
            gvUsuarios.Columns[5].Visible = false;
            gvUsuarios.Columns[6].Visible = false;
            gvUsuarios.Columns[7].Visible = false;
        }

        private void frmVistaUsuarios_Load(object sender, EventArgs e)
        {
            MostrarUsuarios();
        }

        private void gcUsuarios_DoubleClick(object sender, EventArgs e)
        {
            if (data == "FormOportunidades")
            {
                Procesos.RegOportunidad form = Owner as Procesos.RegOportunidad;
                string id, nombre;
                id = this.gvUsuarios.GetFocusedRowCellValue("id").ToString();
                nombre = this.gvUsuarios.GetFocusedRowCellValue("nombre").ToString();
                form.setUsuarios(id, nombre);
                data = string.Empty;
                this.Close();
            }
        }
    }
}