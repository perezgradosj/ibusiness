﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;

namespace VISTA.Vistas
{
    public partial class VistaLineas : DevExpress.XtraEditors.XtraForm
    {
        public string data = string.Empty;

        public VistaLineas()
        {
            InitializeComponent();
        }

        private void MostrarNegociaciaciones()
        {
            this.gcLineas.DataSource = MNegociaciones.MostrarLineas();
            OcultarColumnas();
        }

        private void MostrarOportunidades()
        {
            this.gcLineas.DataSource = MOportunidad.MostrarLineas();
            OcultarColumnas();
        }

        private void OcultarColumnas()
        {
            gvLineas.Columns[0].Visible = false;
            gvLineas.Columns[2].Visible = false;
        }

        private void VistaNegociaciones_Load(object sender, EventArgs e)
        {
            if (data == "Negociaciones")
            {
                MostrarNegociaciaciones();
            }
            else if (data == "Oportunidades")
            {
                MostrarOportunidades();
            }else if (data == "RegistroOportunidad")
            {
                MostrarOportunidades();
            }

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {
            
        }

        private void gcNegociaciones_DoubleClick(object sender, EventArgs e)
        {
            if (data == "Negociaciones")
            {
                USUARIO.RegUsuario form = Owner as USUARIO.RegUsuario;
                string id, nombre;
                id = this.gvLineas.GetFocusedRowCellValue("id").ToString();
                nombre = this.gvLineas.GetFocusedRowCellValue("nombre").ToString();
                form.setNegociaciones(id, nombre);
                data = string.Empty;
                this.Close();
            }
            else if (data == "Oportunidades")
            {
                USUARIO.RegUsuario form = Owner as USUARIO.RegUsuario;
                string id, nombre;
                id = this.gvLineas.GetFocusedRowCellValue("id").ToString();
                nombre = this.gvLineas.GetFocusedRowCellValue("nombre").ToString();
                form.setOportunidades(id, nombre);
                data = string.Empty;
                this.Close();
            }
            else if (data == "RegistroOportunidad")
            {
                Procesos.RegOportunidad form = Owner as Procesos.RegOportunidad;
                string id, nombre;
                id = this.gvLineas.GetFocusedRowCellValue("id").ToString();
                nombre = this.gvLineas.GetFocusedRowCellValue("nombre").ToString();
                form.setLineas(id, nombre);
                data = string.Empty;
                this.Close();
            }

        }
    }
}