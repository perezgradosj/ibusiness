﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace VISTA.Vistas
{
    public partial class VistaOportunidad : DevExpress.XtraEditors.XtraForm
    {
        ////Variable que nos indica si vamos a insertar un nuevo producto
        //private bool VarNuevo = false;
        ////Variable que nos indica si vamos a modificar un producto
        //private bool VarEditar = false;
        ///////// Variable para almacenar el Id del Contacto
        //int IdContacto;

        //private DataTable dtUsuarios;
        //private DataTable dtLineas;

        //public void setLineas(string _Id, string _Nombre)
        //{
        //    bool registrar = true;
        //    if (lueTipoComunicacion.Text == _Nombre)
        //    {
        //        this.MensajeError("Linea Seleccionada como principal");
        //    }
        //    else
        //    {
        //        foreach (DataRow row in dtLineas.Rows)
        //        {
        //            if (Convert.ToString(row["id"]) == _Id)
        //            {
        //                registrar = false;
        //                this.MensajeError("Linea ya Registrada");
        //            }
        //        }
        //        if (registrar)
        //        {
        //            //Agregar ese detalle al datalistadoDetalle
        //            DataRow row = this.dtLineas.NewRow();
        //            row["id"] = _Id;
        //            row["nombre"] = _Nombre;
        //            this.dtLineas.Rows.Add(row);
        //        }
        //    }
        //}

        //public void setUsuarios(string _Id, string _Nombre)
        //{
        //    bool registrar = true;
        //    foreach (DataRow row in dtUsuarios.Rows)
        //    {
        //        if (Convert.ToString(row["id"]) == _Id)
        //        {
        //            registrar = false;
        //            this.MensajeError("Usuario ya Registrado");
        //        }
        //    }
        //    if (registrar)
        //    {
        //        //Agregar ese detalle al datalistadoDetalle
        //        DataRow row = this.dtUsuarios.NewRow();
        //        row["id"] = _Id;
        //        row["nombre"] = _Nombre;
        //        this.dtUsuarios.Rows.Add(row);
        //    }
        //}

        //private void crearTablaLineas()
        //{
        //    this.dtLineas = new DataTable("DtLineas");
        //    this.dtLineas.Columns.Add("id", System.Type.GetType("System.Int32"));
        //    this.dtLineas.Columns.Add("nombre", System.Type.GetType("System.String"));
        //    //Relacionar nuestro DataGRidView con nuestro DataTable
        //    this.gcLineas.DataSource = this.dtLineas;

        //    gvLineas.Columns[0].Visible = false;
        //}

        //private void crearTablaUsuarios()
        //{
        //    this.dtUsuarios = new DataTable("DtUsuarios");
        //    this.dtUsuarios.Columns.Add("id", System.Type.GetType("System.Int32"));
        //    this.dtUsuarios.Columns.Add("nombre", System.Type.GetType("System.String"));
        //    //Relacionar nuestro DataGRidView con nuestro DataTable
        //    this.gcUsuarios.DataSource = this.dtUsuarios;
        //    gvUsuarios.Columns[0].Visible = false;
        //}

        ////Creamos un método para enviar los valores recibidos
        ////a la caja de texto txtIdcategoria
        //public void setContacto(int id, string nombres, string correo, string movil, string fijo)
        //{
        //    this.IdContacto = id;
        //    this.txtContacto.Text = nombres;
        //    this.txtCorreo.Text = correo;
        //    this.txtMovil.Text = movil;
        //    this.txtFijo.Text = fijo;
        //}

        //private void Habilitar(bool Valor)
        //{
        //    this.lueTipoComunicacion.Enabled = Valor;
        //    this.txtNomOportunidad.Enabled = Valor;
        //    this.txtRequerimiento.Enabled = Valor;
        //    //this.txtContacto.Enabled = Valor;
        //    //this.txtCorreo.Enabled = Valor;
        //    //this.txtFijo.Enabled = Valor;
        //    //this.txtMovil.Enabled = Valor;
        //    this.btnBuscarContacto.Enabled = Valor;
        //    this.txtRuc.Enabled = Valor;
        //    this.txtRazonSocial.Enabled = Valor;
        //    this.txtrubro.Enabled = Valor;
        //    this.btnAgregarLineas.Enabled = Valor;
        //    this.btnEliminarLineas.Enabled = Valor;
        //    this.btnAgregarUsuario.Enabled = Valor;
        //    this.brnEliminarUsuario.Enabled = Valor;
        //}

        //private void Botones()
        //{
        //    if (this.VarNuevo || this.VarEditar)
        //    {
        //        this.Habilitar(true);
        //        this.btnNuevo.Enabled = false;
        //        this.btnGuardar.Enabled = true;
        //        this.btnEditar.Visible = false;
        //        this.btnCancelar.Enabled = true;
        //    }
        //    else
        //    {
        //        this.Habilitar(false);
        //        this.btnNuevo.Enabled = true;
        //        this.btnGuardar.Enabled = false;
        //        this.btnEditar.Visible = true;
        //        this.btnCancelar.Enabled = false;
        //    }
        //}

        //private void Limpiar()
        //{
        //    //this.id = 0;
        //    this.lueTipoComunicacion.Text = string.Empty;
        //    this.txtNomOportunidad.Text = string.Empty;
        //    this.txtRequerimiento.Text = string.Empty;
        //    this.txtContacto.Text = string.Empty;
        //    this.txtCorreo.Text = string.Empty;
        //    this.txtMovil.Text = string.Empty;
        //    this.txtFijo.Text = string.Empty;
        //    this.txtRuc.Text = string.Empty;
        //    this.txtRazonSocial.Text = string.Empty;
        //    this.txtrubro.Text = string.Empty;
        //    this.IdContacto = 0;
        //    this.crearTablaLineas();
        //    this.crearTablaUsuarios();
        //    this.errorIcono.ClearErrors();
        //}

        public VistaOportunidad()
        {
            InitializeComponent();
        }

        private void VistaOportunidad_Load(object sender, EventArgs e)
        {

        }
    }
}