﻿namespace VISTA.Vistas
{
    partial class VistaContactos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VistaContactos));
            this.txttexto = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbofiltro = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblRegistros = new DevExpress.XtraEditors.LabelControl();
            this.gcContactos = new DevExpress.XtraGrid.GridControl();
            this.gvContactos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txttexto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbofiltro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).BeginInit();
            this.SuspendLayout();
            // 
            // txttexto
            // 
            this.txttexto.Location = new System.Drawing.Point(12, 32);
            this.txttexto.Name = "txttexto";
            this.txttexto.Size = new System.Drawing.Size(461, 20);
            this.txttexto.TabIndex = 0;
            this.txttexto.TextChanged += new System.EventHandler(this.txttexto_TextChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.cbofiltro);
            this.groupControl1.Controls.Add(this.lblRegistros);
            this.groupControl1.Controls.Add(this.gcContactos);
            this.groupControl1.Controls.Add(this.btnBuscar);
            this.groupControl1.Controls.Add(this.txttexto);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(672, 318);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "CONTACTOS";
            // 
            // cbofiltro
            // 
            this.cbofiltro.EditValue = "Por Nombre";
            this.cbofiltro.Location = new System.Drawing.Point(479, 33);
            this.cbofiltro.Name = "cbofiltro";
            this.cbofiltro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbofiltro.Properties.Items.AddRange(new object[] {
            "Por Nombre",
            "Por Apellido"});
            this.cbofiltro.Size = new System.Drawing.Size(100, 20);
            this.cbofiltro.TabIndex = 4;
            // 
            // lblRegistros
            // 
            this.lblRegistros.Location = new System.Drawing.Point(12, 293);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(63, 13);
            this.lblRegistros.TabIndex = 3;
            this.lblRegistros.Text = "labelControl1";
            // 
            // gcContactos
            // 
            this.gcContactos.Location = new System.Drawing.Point(12, 58);
            this.gcContactos.MainView = this.gvContactos;
            this.gcContactos.Name = "gcContactos";
            this.gcContactos.Size = new System.Drawing.Size(648, 222);
            this.gcContactos.TabIndex = 2;
            this.gcContactos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContactos});
            this.gcContactos.DoubleClick += new System.EventHandler(this.gcContactos_DoubleClick);
            // 
            // gvContactos
            // 
            this.gvContactos.GridControl = this.gcContactos;
            this.gvContactos.Name = "gvContactos";
            this.gvContactos.OptionsBehavior.Editable = false;
            this.gvContactos.OptionsView.ShowGroupPanel = false;
            // 
            // btnBuscar
            // 
            this.btnBuscar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.ImageOptions.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(585, 30);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "BUSCAR";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // VistaContactos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 318);
            this.Controls.Add(this.groupControl1);
            this.Name = "VistaContactos";
            this.Text = "VistaContactos";
            this.Load += new System.EventHandler(this.VistaContactos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txttexto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbofiltro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContactos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContactos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txttexto;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl lblRegistros;
        private DevExpress.XtraGrid.GridControl gcContactos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContactos;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private DevExpress.XtraEditors.ComboBoxEdit cbofiltro;
    }
}