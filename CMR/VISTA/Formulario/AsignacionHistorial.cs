﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MODELO;
//Importamos el using para poder mover el formulario
using System.Runtime.InteropServices;

namespace VISTA.Formulario
{
    public partial class AsignacionHistorial : DevExpress.XtraEditors.XtraForm
    {

        //         Elementos para mover un formulario
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        /////////////////////////////////////////////////

        public string Negociacion = "";
        public string Usuario = "";

        public AsignacionHistorial()
        {
            InitializeComponent();
        }

        private void OcultarColumnas()
        {
            gvHistorial.Columns[0].Visible = false;
            gvHistorial.Columns[1].Visible = false;
            gvHistorial.Columns[3].Visible = false;
            
        }

        private void AsignacionHistorial_Load(object sender, EventArgs e)
        {
            MAsignaciones asignaciones = new MAsignaciones();
            gcHistorial.DataSource =  asignaciones.MostrarHistorial(Negociacion, "", 3);
            OcultarColumnas();
        }

        private void txttexto_EditValueChanged(object sender, EventArgs e)
        {
            MAsignaciones asignaciones = new MAsignaciones();
            gcHistorial.DataSource = asignaciones.MostrarHistorial(Negociacion, txttexto.Text, 3);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panelControl1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}