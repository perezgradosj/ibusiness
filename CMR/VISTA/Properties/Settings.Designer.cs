﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace VISTA.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.7.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string cadenaConexion {
            get {
                return ((string)(this["cadenaConexion"]));
            }
            set {
                this["cadenaConexion"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string nomServidor {
            get {
                return ((string)(this["nomServidor"]));
            }
            set {
                this["nomServidor"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string usuSrv {
            get {
                return ((string)(this["usuSrv"]));
            }
            set {
                this["usuSrv"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string passSrv {
            get {
                return ((string)(this["passSrv"]));
            }
            set {
                this["passSrv"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool checkContra {
            get {
                return ((bool)(this["checkContra"]));
            }
            set {
                this["checkContra"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string nombreBD {
            get {
                return ((string)(this["nombreBD"]));
            }
            set {
                this["nombreBD"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool checkUsuario {
            get {
                return ((bool)(this["checkUsuario"]));
            }
            set {
                this["checkUsuario"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool checkGuardar {
            get {
                return ((bool)(this["checkGuardar"]));
            }
            set {
                this["checkGuardar"] = value;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.1.72,14330;Initial Catalog=SisDcg;User ID=Desarrollo;Password" +
            "=DCGtec2019@")]
        public string SisDcgConnectionString {
            get {
                string response = ConfigurationManager.ConnectionStrings["VISTA.Properties.Settings.SisDcgConnectionString"].ConnectionString;
                try
                {
                    using (SqlConnection conn = new SqlConnection(response))
                    {
                        conn.Open();
                        return response;
                    }
                }
                catch
                {
                    response = ConfigurationManager.ConnectionStrings["ConexionExterna"].ConnectionString;
                    return response;
                }
                //return ((string)(this["SisDcgConnectionString"]));
                //return response;
            }
        }
    }
}
