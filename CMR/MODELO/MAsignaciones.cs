﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CONTROLADOR;
using ENTIDAD;

namespace MODELO
{
    public class MAsignaciones
    {
        public DataTable MostrarHistorial(string negociacion, string usuario, int opcion)
        {
            CAsignacion Asign = new CAsignacion();
            return Asign.Mostrar(usuario,negociacion, opcion);
        }
        public static string MantAsginacionesHistorial(int id, int idnegociacion, int idusuario, DateTime fecha_inicio,
                                                        DateTime fecha_fin, int opcion)
        {
            EAsignaciones Asig = new EAsignaciones();
            CAsignacion Asign = new CAsignacion();
            Asig.Id = id;
            Asig.IdNegociacion = idnegociacion;
            Asig.IdUsuario = idusuario;
            Asig.Fecha_Inicio = fecha_inicio;
            Asig.Fecha_Fin = fecha_fin;
            Asig.Opcion = opcion;
            return Asign.MantAsignacionesHistorial(Asig);
        }
    }
}
