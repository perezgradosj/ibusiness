﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using CONTROLADOR;
using System.Data;

namespace MODELO
{
    public class MOportunidad
    {
        public static DataTable Mostrar(string textobuscar, int idusuario, int opcion, int idlinea, int tamano_pagina,
                                        int pagina_actual, int estado)
        {
            EOportunidad Opor = new EOportunidad();
            COportunidad Oport = new COportunidad();

            Opor.TexoBuscar = textobuscar;
            Opor.IdUsuario = idusuario;
            Opor.Opcion = opcion;
            Opor.IdLinea = idlinea;
            Opor.TamanoPagina = tamano_pagina;
            Opor.PaginaActual = pagina_actual;
            Opor.Estado = estado;
           
            return Oport.Mostrar(Opor);
        }

        public static DataTable MostrarDatosDePaginacion(string textobuscar, int idusuario, int opcion, int idlinea, int tamano_pagina,
                                        int pagina_actual, int estado)
        {
            EOportunidad Opor = new EOportunidad();
            COportunidad Oport = new COportunidad();

            Opor.TexoBuscar = textobuscar;
            Opor.IdUsuario = idusuario;
            Opor.Opcion = opcion;
            Opor.IdLinea = idlinea;
            Opor.TamanoPagina = tamano_pagina;
            Opor.PaginaActual = pagina_actual;
            Opor.Estado = estado;

            return Oport.MostrarDatosDePaginacion(Opor);
        }

        public static DataTable MostrarLineas()
        {
            return new COportunidad().MostrarLineas();
        }

        public DataTable MostrarSolicitudes(int idoportunidad, int idusuario)
        {
            return new COportunidad().MostrarSolicitudes(idoportunidad, idusuario);
        }

        public DataTable MostrarNodes()
        {
            return new COportunidad().MostrarLineas();
        }

        public static string MantOportunidad(int id, int idtipo_comunicacion, string nombre, string requerimiento,
        int idempresa, DateTime fecha, 
        int idlinea, int idusuario, int estado, int opcion)
        {
            string Rpta = "";
            EOportunidad Opor = new EOportunidad();
            COportunidad Oport = new COportunidad();
            Opor.Id = id;
            Opor.IdTipoComunicacion = idtipo_comunicacion;
            Opor.Nombre = nombre;
            Opor.Requerimiento = requerimiento;
            Opor.IdEmpresa = idempresa;
            Opor.Fecha = fecha;
            Opor.IdLinea = idlinea;
            Opor.IdUsuario = idusuario;
            Opor.Estado = estado;
            Opor.Opcion = opcion;

            return Rpta = Oport.MantOportunidad(Opor);
        }

        public string SolicitarOportunidad(int idoportunidad, int idusuario, int opcion)
        {
            string Rpta = "";
            COportunidad Oport = new COportunidad();

            return Rpta = Oport.RegistrarSolicitudOportunidad(idoportunidad, idusuario, opcion);
        }

        public string RegistrarSolicitudOportunidad(int idoportunidad, int idusuario, int opcion)
        {
            string Rpta = "";
            COportunidad Oport = new COportunidad();

            return Rpta = Oport.RegistrarSolicitudOportunidad(idoportunidad, idusuario, opcion);
        }
    }
}
