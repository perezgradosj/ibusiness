﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using CONTROLADOR;
using System.Data;

namespace MODELO
{
    public class MTipoComunicacion
    {
        public DataTable Mostrar()
        {
            return new CTipoComunicacion().Mostrar();
        }

        public static string Mantenimiento(int id, string nombre, int opcion)
        {
            CTipoComunicacion TCom = new CTipoComunicacion();
            ETipoComunicacion ECom = new ETipoComunicacion();
            ECom.Id = id;
            ECom.Nombre = nombre;
            ECom.Opcion = opcion;
            return TCom.Mantenimiento(ECom);
        }
    }
}
