﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using CONTROLADOR;
using System.Data;

namespace MODELO
{
    public class MNegociaciones
    {
        public static DataTable Mostrar(string textobuscar, int estado, int idusuario, int idlinea, int tamano_pagina,
                                        int pagina_actual, int opcion)
        {
            ENegociacion Neg = new ENegociacion();
            CNegociaciones Nego = new CNegociaciones();

            Neg.TextoBuscar = textobuscar;
            Neg.IdUsuario = idusuario;
            Neg.Opcion = opcion;
            Neg.Estado = estado;
            Neg.IdLinea = idlinea;
            Neg.TamanoPagina = tamano_pagina;
            Neg.PaginaActual = pagina_actual;
            return Nego.Mostrar(Neg);
        }

        public static DataTable MostrarDatosDePaginacion(string textobuscar, int estado, int idusuario, int idlinea, int tamano_pagina,
                                        int pagina_actual, int opcion)
        {
            ENegociacion Neg = new ENegociacion();
            CNegociaciones Nego = new CNegociaciones();

            Neg.TextoBuscar = textobuscar;
            Neg.IdUsuario = idusuario;
            Neg.Opcion = opcion;
            Neg.Estado = estado;
            Neg.IdLinea = idlinea;
            Neg.TamanoPagina = tamano_pagina;
            Neg.PaginaActual = pagina_actual;

            return Nego.MostrarDatosDePaginacion(Neg);
        }

        public static string MantNegociacion(int id, DateTime fecha_registro, DateTime fecha_conversion, string nombre,
        double monto, int etapa, int tipo_moneda, int estado, int idusuario, int idlinea, 
        double monto_facturado, int idempresa, string requerimiento, int idoportunidad, int opcion)
        {
            string Rpta = "";
            ENegociacion Neg = new ENegociacion();
            CNegociaciones Nego = new CNegociaciones();
            Neg.Id = id;
            Neg.Fecha_Registro = fecha_registro;
            Neg.Fecha_Conversion = fecha_conversion;
            Neg.Nombre = nombre;
            Neg.Monto = monto;
            Neg.Etapa = etapa;
            Neg.Tipo_Moneda = tipo_moneda;
            Neg.Estado = estado;
            Neg.IdUsuario = idusuario;
            Neg.IdLinea = idlinea;
            Neg.Monto_Facturado = monto_facturado;
            Neg.IdEmpresa = idempresa;
            Neg.Requerimiento = requerimiento;
            Neg.IdOportunidad = idoportunidad;
            Neg.Opcion = opcion;

            return Rpta = Nego.MantNegociacion(Neg);
        }

        public static string MantNegociacion(int id, int estado, int etapa, int opcion)
        {
            string Rpta = "";
            ENegociacion Neg = new ENegociacion();
            CNegociaciones Nego = new CNegociaciones();
            Neg.Id = id;
            Neg.Estado = estado;
            Neg.Etapa = etapa;
            Neg.Opcion = opcion;

            return Rpta = Nego.MantNegociacion(Neg);
        }

        public static DataTable MostrarLineas()
        {
            return new CNegociaciones().MostrarLineas();
        }

        public DataTable MostrarNodes()
        {
            return new CNegociaciones().MostrarLineas();
        }

    }
}
