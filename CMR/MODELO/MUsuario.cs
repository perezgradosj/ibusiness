﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDAD;
using CONTROLADOR;
using System.Data;
using DevExpress.XtraTreeList.Nodes;

namespace MODELO
{
    public class MUsuario
    {
        //public static string Insertar(string nombre, string ape_paterno, string ape_materno,
        //    char sexo, int idlinea, string usuario, string password, int opcion, List<TreeListNode> Negociaciones, List<TreeListNode> Oportunidades)
        //{
        //    EUsuario Usu = new EUsuario();
        //    CUsuario Usua = new CUsuario();
        //    Usu.Nombre = nombre;
        //    Usu.Ape_Paterno = ape_paterno;
        //    Usu.Ape_Materno = ape_materno;
        //    Usu.Sexo = sexo;
        //    Usu.IdLinea = idlinea;
        //    Usu.Usuario = usuario;
        //    Usu.Password = password;
        //    Usu.Opcion = opcion;

        //    List<CUsuarioNegociaciones> ListNegociaciones = new List<CUsuarioNegociaciones>();
        //    foreach (TreeListNode valores in Negociaciones)
        //    {
        //        CUsuarioNegociaciones UsuarioNego = new CUsuarioNegociaciones();
        //        UsuarioNego.IdLinea = Convert.ToInt32(valores.Nodes.ParentNode.Tag);
        //        ListNegociaciones.Add(UsuarioNego);
        //    }

        //    List<CUsuarioOportunidades> ListOportunidades = new List<CUsuarioOportunidades>();
        //    foreach (TreeListNode valores in Oportunidades)
        //    {
        //        CUsuarioOportunidades UsuarioOpor = new CUsuarioOportunidades();
        //        UsuarioOpor.IdLinea = Convert.ToInt32(valores.Nodes.ParentNode.Tag);
        //        ListOportunidades.Add(UsuarioOpor);
        //    }

        //    return Usua.Insertar(Usu, ListNegociaciones, ListOportunidades);
        //}

        public static string Insertar(string nombre, string ape_paterno, string ape_materno,
        char sexo, int idlinea, string usuario, string password, int opcion, 
        DataTable dtNegociaciones, DataTable dtOportunidades, int oport_max, int superlinea)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Nombre = nombre;
            Usu.Ape_Paterno = ape_paterno;
            Usu.Ape_Materno = ape_materno;
            Usu.Sexo = sexo;
            Usu.IdLinea = idlinea;
            Usu.Usuario = usuario;
            Usu.Password = password;
            Usu.Opcion = opcion;
            Usu.Oport_Max = oport_max;
            Usu.SuperLinea = superlinea;

            List<CUsuarioNegociaciones> Negociaciones = new List<CUsuarioNegociaciones>();
            foreach (DataRow row in dtNegociaciones.Rows)
            {
                CUsuarioNegociaciones Nego = new CUsuarioNegociaciones();
                Nego.IdLinea = Convert.ToInt32(row["id"].ToString());
                Negociaciones.Add(Nego);
            }

            List<CUsuarioOportunidades> Oportunidades = new List<CUsuarioOportunidades>();
            foreach (DataRow row in dtOportunidades.Rows)
            {
                CUsuarioOportunidades Opor = new CUsuarioOportunidades();
                Opor.IdLinea = Convert.ToInt32(row["id"].ToString());
                Oportunidades.Add(Opor);
            }

            return Usua.Insertar(Usu, Negociaciones, Oportunidades);
        }

        public static string Editar(int id, string nombre, string ape_paterno, string ape_materno,
            char sexo, int idlinea, string usuario, string password, int oport_max, int opcion,
            int superlinea)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Id = id;
            Usu.Nombre = nombre;
            Usu.Ape_Paterno = ape_paterno;
            Usu.Ape_Materno = ape_materno;
            Usu.Sexo = sexo;
            Usu.IdLinea = idlinea;
            Usu.Usuario = usuario;
            Usu.Password = password;
            Usu.Oport_Max = oport_max;
            Usu.Opcion = opcion;
            Usu.SuperLinea = superlinea;
            return Usua.Editar(Usu);
        }

        public static string Eliminar(int id)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Id = id;
            return Usua.Eliminar(Usu);
        }

        public static DataTable Mostrar()
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.IdLinea = 0;
            return Usua.Mostrar(Usu);
        }

        public DataTable Listar(int idlinea)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.IdLinea = idlinea;
            return Usua.Mostrar(Usu);
        }

        public static DataTable MostrarNegociaciones(string texto)
        {
            CUsuario Usua = new CUsuario();
            return Usua.MostrarNegociaciones(texto);
        }

        public static DataTable MostrarOportunidades(string texto)
        {
            CUsuario Usua = new CUsuario();
            return Usua.MostrarOportunidades(texto);
        }

        public static string EliminarNegociaciones(int id, DataTable dtNegociaciones)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Id = id;

            List<CUsuarioNegociaciones> Negociaciones = new List<CUsuarioNegociaciones>();
            foreach (DataRow row in dtNegociaciones.Rows)
            {
                CUsuarioNegociaciones Nego = new CUsuarioNegociaciones();
                Nego.IdLinea = Convert.ToInt32(row["id"].ToString());
                Negociaciones.Add(Nego);
            }

            return Usua.EliminarNegociaciones(Usu, Negociaciones);
        }

        public static string EliminarOportunidades(int id, DataTable dtOportunidades)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Id = id;

            List<CUsuarioOportunidades> Oportunidades = new List<CUsuarioOportunidades>();
            foreach (DataRow row in dtOportunidades.Rows)
            {
                CUsuarioOportunidades Opor = new CUsuarioOportunidades();
                Opor.IdLinea = Convert.ToInt32(row["id"].ToString());
                Oportunidades.Add(Opor);
            }

            return Usua.EliminarOportunidades(Usu, Oportunidades);
        }

        public static DataTable Login(string usuario, string password, int Opcion)
        {
            EUsuario Usu = new EUsuario();
            CUsuario Usua = new CUsuario();
            Usu.Usuario = usuario;
            Usu.Password = password;
            Usu.Opcion = Opcion;
            return Usua.Login(Usu);
        }
    }
}
