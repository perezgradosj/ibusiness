﻿using CONTROLADOR;
using ENTIDAD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELO
{
    public class MActividad
    {
        public static DataTable Mostrar(string texto, int idusuario, int idnegociacion, int opcion)
        {
            EActividad Act = new EActividad();
            CActividad Acti = new CActividad();
            Act.TextoBuscar = texto;
            Act.IdUsuario = idusuario;
            Act.IdNegociacion = idnegociacion;
            Act.Opcion = opcion;
            return Acti.Mostrar(Act);
        }

        public static string MantActividad(int id, int idtipo_actividad, string asunto, string descripcion, DateTime fecha,
        int idcontacto, DateTime hora ,int idusuario, int estado, int idnegociacion, int opcion)
        {
            string Rpta = "";
            EActividad Act = new EActividad();
            CActividad Acti = new CActividad();
            Act.Id = id;
            Act.IdTipoActividad = idtipo_actividad;
            Act.Asunto = asunto;
            Act.Descripcion = descripcion;
            Act.Fecha = fecha;
            Act.IdContacto = idcontacto;
            Act.Hora = hora;
            Act.IdUsuario = idusuario;
            Act.Estado = estado;
            Act.IdNegociacion = idnegociacion;
            Act.Opcion = opcion;

            return Rpta = Acti.MantActividad(Act);
        }
    }
}
