﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CONTROLADOR;
using ENTIDAD;
using System.Data;

namespace MODELO
{
    public class MContactos
    {
        public static DataTable Mostrar(string texto, int idnegociacion, int idoportunidad, int opcion)
        {
            EContactos Cont = new EContactos();
            CContactos Contac = new CContactos();
            Cont.TextoBuscar = texto;
            Cont.Opcion = opcion;
            Cont.IdNegociacion = idnegociacion;
            Cont.IdOportunidad = idoportunidad;
            return Contac.Mostrar(Cont);
        }



        public static DataTable BuscarNombre(string texto, int opcion)
        {
            EContactos Cont = new EContactos();
            CContactos Contac = new CContactos();
            Cont.TextoBuscar = texto;
            Cont.Opcion = opcion;
            return Contac.BuscarNombre(Cont);
        }

        public static string MantContacto(int id, string nombres, string correo, string movil,
        string fijo, int opcion)
        {
            string Rpta = "";
            EContactos Con = new EContactos();
            CContactos Cont = new CContactos();
            Con.Id = id;
            Con.Nombres = nombres;
            Con.Correo = correo;
            Con.Movil = movil;
            Con.Fijo = fijo;
            Con.Opcion = opcion;

            return Rpta = Cont.MantContacto(Con);
        }

        public static string MantContacto(DataTable dtContactos, int IdEmpresa, int idnegociacion,
                                            int idoportunidad, int opcion)
        {
            string Rpta = "";
            foreach (DataRow r in dtContactos.Rows)
            {
                EContactos eContactos = new EContactos();
                CContactos cContactos = new CContactos();
                if (Convert.ToInt32(r["Id"].ToString()) == 0)
                {
                    int idcontacto = 0;
                    eContactos.Nombres = r["Nombres"].ToString();
                    eContactos.Correo = r["Correo"].ToString();
                    eContactos.Movil = r["Movil"].ToString();
                    eContactos.Fijo = r["Fijo"].ToString();
                    eContactos.Opcion = 1;
                    idcontacto = Convert.ToInt32(cContactos.MantContacto(eContactos));

                    if (idcontacto != 0)
                    {
                        Rpta = MantEmpresaContacto(0, IdEmpresa, idcontacto, idnegociacion, idoportunidad, 1);
                    }
                    
                }
                else if (opcion <= 1)
                {
                    Rpta = MantEmpresaContacto(0, IdEmpresa, Convert.ToInt32(r["Id"].ToString()), idnegociacion,idoportunidad, opcion);
                }
            }

            return Rpta;
        }

        public static string MantEmpresaContacto(int id, int idempresa, int idcontacto, 
                                                int idnegociacion, int idoportunidad, int opcion)
        {
            string Rpta = "";
            CContactos Cont = new CContactos();

            return Rpta = Cont.MantenimientoEmpresaContacto(id, idempresa, idnegociacion, idoportunidad, idcontacto, opcion);
        }

        public static string MantEmpresaContacto(int IdEmpresa, int IdOportunidad, int IdNegociacion, int opcion)
        {
            string Rpta = "";
            CContactos Cont = new CContactos();

            return Rpta = Cont.MantenimientoEmpresaContacto(0, IdEmpresa, IdNegociacion, IdOportunidad,0, opcion);
        }

        public static int verifyContactoOportunidad(int idcontacto, int idOportunidad)
        {
            int Rpta = 0;
            CContactos Cont = new CContactos();
            return Rpta = Cont.verifyContactoOportunidad(idcontacto, idOportunidad);
        }

        public static void deleteEmpresaContacto(int result)
        {
            CContactos Cont = new CContactos();
            Cont.deleteEmpresaContacto(result);
        }
    }
}
