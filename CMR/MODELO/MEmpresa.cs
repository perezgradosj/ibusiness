﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CONTROLADOR;
using ENTIDAD;

namespace MODELO
{
    public class MEmpresa
    {
        public static DataTable Mostrar(string texto, int opcion)
        {
            EEmpresa Emp = new EEmpresa();
            CEmpresa Empre = new CEmpresa();
            Emp.TextoBuscar = texto;
            Emp.Opcion = opcion;
            return Empre.Mostrar(Emp);
        }

        public static string MantEmpresa(int id, string ruc, string razon_social, string rubro,
                                         int opcion)
        {
            string Rpta = "";
            EEmpresa Emp = new EEmpresa();
            CEmpresa Empre = new CEmpresa();
            Emp.Id = id;
            Emp.Ruc = ruc;
            Emp.Razon_Social = razon_social;
            Emp.Rubro = rubro;
            Emp.Opcion = opcion;
            //List<EContactos> listContacto = new List<EContactos>(); ;
            //foreach (DataRow row in dtContactos.Rows)
            //{
            //    EContactos eContactos = new EContactos();
            //    CContactos cContactos = new CContactos();
            //    if (Convert.ToInt32(row["Id"].ToString()) == 0)
            //    {
            //        eContactos.Nombres = row["Nombres"].ToString();
            //        eContactos.Correo = row["Correo"].ToString();
            //        eContactos.Movil = row["Movil"].ToString();
            //        eContactos.Fijo = row["Fijo"].ToString();
            //        listContacto.Add(eContactos);
            //    }                
                
            //}

            return Rpta = Empre.MantEmpresa(Emp);
        }
    }
}
